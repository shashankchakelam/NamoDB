#!/bin/bash
set -ex

# Drop Namo DB if exists and recreate
mysql -uroot -pkaka123 -e "DROP DATABASE IF EXISTS namo; CREATE DATABASE namo;"

# Create virtualenv. Interesting point to note: you don't need to check if you're
# already running in a virtualenv as it's a safe command to simply create and activate
# a new one (even of the same name)
virtualenv .venv
source .venv/bin/activate

# Build, install the namo module
pip install -r requirements.txt
pip install -r test-requirements.txt
pip install -e git+https://github.com/JioCloudCompute/jcsclient.git@new_jcs_client#egg=jcsclient
python setup.py install

# Populate seed data into namo db
python namo/tests/unit/populate_seed_data.py

# Deactivate virtualenv
deactivate
