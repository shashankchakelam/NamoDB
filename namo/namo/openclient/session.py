# from jcsclient import 
from jcsclient import config
import os
from mistralclient.api import client as mistral_client
from oslo_config import cfg


mistral_opts = [
    cfg.StrOpt('mistral_url',
                default='http://localhost:8989/v2',
                help='mistral service url'),
]

workflow_opts = [
    cfg.StrOpt('workflow_url',
                default='http://localhost:3001',
                help='workflow service url'),
    cfg.StrOpt('default_private_network_name',
                default='private',
                help='Private network name to start VM with'),
]


jiocloud_opts = [
    cfg.StrOpt('access_key',
                default=' ',
                help='access key'),
    cfg.StrOpt('secret_key',
                default=' ',
                help='secret key'),
    cfg.StrOpt('compute_url',
                default='https://compute.ind-west-1.staging.jiocloudservices.com/',
                help='compute url'),
    cfg.StrOpt('vpc_url',
                default='https://vpc.ind-west-1.staging.jiocloudservices.com',
                help='vpc_url'),
    cfg.StrOpt('dss_url',
                default='https://dss.ind-west-1.staging.jiocloudservices.com',
                help='dss_url'),
    cfg.StrOpt('iam_url',
                default='https://iam.ind-west-1.staging.jiocloudservices.com',
                help='iam_url'),
    cfg.StrOpt('ImageId',
                default='ami-8843cebc',
                help='Glance Image Id'),
    cfg.StrOpt('InstanceTypeId',
                default='c1.large',
                help='Flavor Type'),
    cfg.StrOpt('SubnetId',
                default='subnet-f2dd57f7',
                help='Subnet Id'),
    cfg.StrOpt('SecurityGroupId',
                default='sg-6a594d2b',
                help='Default Security Group Id'),
]

CONF = cfg.CONF
CONF.register_opts(mistral_opts, group='mistral')
CONF.register_opts(workflow_opts, group='workflow')
CONF.register_opts(jiocloud_opts, group='jiocloud')


def setup_jcsclient():
    # Just to initialize the configuration.
    args =["--insecure"]
    # We are setting the environment varibale because new jcsclient takes these variables 
    # from environment parameters.
    os.environ["ACCESS_KEY"] = CONF.jiocloud.access_key
    os.environ["SECRET_KEY"] = CONF.jiocloud.secret_key
    os.environ["COMPUTE_URL"]= CONF.jiocloud.compute_url
    os.environ["VPC_URL"] = CONF.jiocloud.vpc_url
    os.environ["DSS_URL"] = CONF.jiocloud.dss_url
    os.environ["IAM_URL"] = CONF.jiocloud.iam_url
    config.setup_config_handler(args)


def mistral_session():
    return mistral_client.client(mistral_url=CONF.mistral.mistral_url)
