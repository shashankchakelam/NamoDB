#! /usr/bin/python
#sudo apt-get install libmysqlclient-dev python-mysqldb
# TODO: sudo python setup.py install && sudo pip install namo
from namo.tests.unit import conf_fixture
from oslo_db import options
from namo.db.mysqlalchemy import models
conf_fixture.set_defaults()
models.register_models()

print 'Tables successfully created'

# mysql> insert into raga (version, url, state, updated_at, created_at, deleted) values ('22', 'blahurl', 'PREFERRED', NOW(), NOW(), 0);
