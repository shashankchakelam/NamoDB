#!/usr/share/python/namo-api/bin/python

import os
import sys
import eventlet

eventlet.patcher.monkey_patch(all=False, socket=True, time=True,
                              select=True, thread=True, os=True)
possible_topdir = os.path.normpath(os.path.join(os.path.abspath(sys.argv[0]),
                                   os.pardir,
                                   os.pardir))
if os.path.exists(os.path.join(possible_topdir, 'namo', '__init__.py')):
    sys.path.insert(0, possible_topdir)

from namo.common import utils
from oslo_config import cfg
from oslo_log import log as logging

from namo.common import config
from namo.common import exception
from namo.common import wsgi
from namo.openclient import session


CONF = cfg.CONF
CONF.import_group("profiler", "namo.common.wsgi")
logging.register_options(CONF)

KNOWN_EXCEPTIONS = (RuntimeError,
                    exception.WorkerCreationFailure,
                    "BadWorkflowApiConfiguration")

def fail(e):
    global KNOWN_EXCEPTIONS
    return_code = KNOWN_EXCEPTIONS.index(type(e)) + 1
    sys.stderr.write("ERROR: %s\n" % utils.exception_to_str(e))
    sys.exit(return_code)

def register_workflow(name, filepath, workbook_dict, MISTRAL_CONN):
    if workbook_dict.get(name) is None:
        f = open(filepath, 'r')
        file = f.read()
        f.close()
        MISTRAL_CONN.workbooks.create(file)

def register_allworkflow():
    MISTRAL_CONN = session.mistral_session()
    workbook_dict = {}
    workbooks = MISTRAL_CONN.workbooks.list()
    for workbook in workbooks:
        workbook_dict[workbook.name] = True

    # NOTE(rushiagr): Now that the yaml file paths are hardcoded, we need to
    # make sure we keep these files in /etc/namo-workflowapi directory, even
    # when we want to run namo in a dev environment. Installation from debian
    # pkg will automatically place yaml files in /etc/namo-workflowapi
    # directory
    register_workflow('master', '/etc/namo-workflowapi/master.yaml', workbook_dict, MISTRAL_CONN)
    register_workflow('autobackup', '/etc/namo-workflowapi/automated-backup.yaml', workbook_dict, MISTRAL_CONN)
    register_workflow('cleanautobkp', '/etc/namo-workflowapi/clean-auto-bkp.yaml', workbook_dict, MISTRAL_CONN)
    register_workflow('createdb', '/etc/namo-workflowapi/create_instance.yaml', workbook_dict, MISTRAL_CONN)
    register_workflow('deletedb', '/etc/namo-workflowapi/delete_instance.yaml', workbook_dict, MISTRAL_CONN)
    register_workflow('createbackup', '/etc/namo-workflowapi/create_backup.yaml', workbook_dict, MISTRAL_CONN)
    register_workflow('deletebackup', '/etc/namo-workflowapi/delete_backup.yaml', workbook_dict, MISTRAL_CONN)
    register_workflow('restore', '/etc/namo-workflowapi/restore_instance.yaml', workbook_dict, MISTRAL_CONN)
    register_workflow('modifydb', '/etc/namo-workflowapi/modify_instance.yaml', workbook_dict, MISTRAL_CONN)
    body = {}
    body['workflowapi_url'] = CONF.workflow.workflow_url
    MISTRAL_CONN.executions.create('master.scheduletasks', body)
    body['scheduled_count'] = 0
    MISTRAL_CONN.executions.create('autobackup.scheduletasks', body)
    MISTRAL_CONN.executions.create('cleanautobkp.scheduletasks', body)

def main():
    try:
        config.parse_args()
        wsgi.set_eventlet_hub()
        logging.setup(CONF, 'namo')
        server = wsgi.Server(initialize_namo_store=False)
        register_allworkflow()
        server.start(config.load_paste_app('namo-workflowapi'), default_port=3001)
        server.wait()
    except KNOWN_EXCEPTIONS as e:
        fail(e)


if __name__ == '__main__':
    main()
