#!/usr/share/python/namo-api/bin/python

from oslo_config import cfg
from namo.common import config
from oslo_db import options
from namo.db.mysqlalchemy import models

CONF = cfg.CONF
db_opts = [
    cfg.StrOpt('connection',
                default='mysql://root:tempdbpass@10.140.214.37/namo?charset=utf8',
                help='Connections to db')]

def main():
    config.parse_args()
    CONF.register_opts(db_opts, group='database')
    models.register_models()

if __name__ == '__main__':
    main()
