# Copyright 2010 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""Namo exception subclasses"""

import six
import six.moves.urllib.parse as urlparse

from namo import i18n

_ = i18n._

_FATAL_EXCEPTION_FORMAT_ERRORS = False


class RedirectException(Exception):
    def __init__(self, url):
        self.url = urlparse.urlparse(url)


class NamoException(Exception):
    """
    Base Namo Exception

    To correctly use this class, inherit from it and define
    a 'message' property. That message will get printf'd
    with the keyword arguments provided to the constructor.
    """
    message = _("An unknown exception occurred")

    def __init__(self, message=None, *args, **kwargs):
        if not message:
            message = self.message
        try:
            if kwargs:
                message = message % kwargs
        except Exception as e:
            print e.message
            if _FATAL_EXCEPTION_FORMAT_ERRORS:
                raise
            else:
                # at least get the core message out if something happened
                pass
        self.msg = message
        super(NamoException, self).__init__(message)

    def __unicode__(self):
        # NOTE(flwang): By default, self.msg is an instance of Message, which
        # can't be converted by str(). Based on the definition of
        # __unicode__, it should return unicode always.
        return six.text_type(self.msg)


### Using The functions

class NotAuthorized(NamoException):
    message = _("Not authorized.")
    code = 403

class NotFound(NamoException):
    message = _("An object with the specified identifier was not found.")

class AdminRequired(NotAuthorized):
    message = _("User does not have admin privileges")

class WorkflowNotFoundByExecutionId(NotFound):
    message = _("Workflow Execution Id %(id)s could not be found.")

class InstanceNotFoundById(NotFound):
    message = _("Instance %(id)s could not be found.")

class DP_TaskNotFoundById(NotFound):
    message = _("Task %(id)s could not be found.")

class BucketNotFoundById(NotFound):
    message = _("Bucket %(id)s could not be found.")

class BucketNotFoundByOwnerId(NotFound):
    message = _("Bucket %(id)s could not be found.")

class InstanceNotFoundByOwnerIdAndName(NotFound):
    message = _("Instance with owner Id %(owner_id)s and name %(name)s could not be found.")

class InstanceNotInDesiredState(NotFound):
    # TODO(rushiagr): pass lc and cs values and print in the logs
    message = _("Instance does not have desired lifecycle-changestate values.")

class DBTypeNotFoundByName(NotFound):
    message = _("DBType %(name)s could not be found.")

class KeyPairNotFound(NotFound):
    message = _("Keypair with id %(id)s could not be found.")

class HostNotFoundById(NotFound):
    message = _("Host with host id %(id)s could not be found.")

class HostNotFoundByInstanceId(NotFound):
    message = _("Host with instance id %(instance_id)s could not be found.")

class VolumeSnapshotNotFoundByBackupId(NotFound):
    message = _("VolumeSnapshot with backup_id %(backup_id)s could not be found.")

class VolumeNotFoundById(NotFound):
    message = _("Volume with id %(id)s could not be found.")

class VolumeNotFoundByHostId(NotFound):
    message = _("Volume with host_id %(host_id)s could not be found.")

class BackupNotFoundByOwnerIdAndName(NotFound):
    message = _("Backup with owner Id %(owner_id)s and name %(name)s could not be found.")

class FlavorNotFound(NotFound):
    message = _("Flavor with flavor_name %(flavor_name)s could not be found.")

class ImageNotFoundByArtefactId(NotFound):
    message = _("Image with artefact_id %(artefact_id)s could not be found.")

class ImageNotFoundByDbEngineVersionAndOS(NotFound):
    message = _("Image with database_engine_version %(database_engine_version)s " \
                + ", database_engine %(database_engine)s, "\
                + " os_version %(os_version)s could not be found.")

class MasterWorkflowNotFound(NotFound):
    message = _("Master workflow entry not found")

class ArtefactNotFound(NotFound):
    message = _("Artefact Info Not Found")

class PreferedGlobalEngineNotFound(NotFound):
    message = _("Prefered Global Engine with engine_name %(name)s not found")

class InfiniteWorkflowNotFound(NotFound):
    message = _("Infinite workflow entry for %(workflow_type)s not found")
### Done

class Forbidden(NamoException):
    message = _("You are not authorized to complete this action.")

class WorkerCreationFailure(NamoException):
    message = _("Server worker creation failed: %(reason)s.")

# Generic class for all database related exceptions
class DBException(NamoException):
    message = _("Database operation failed.")

class DbConditionalUpdateFailed(DBException):
    message = _("Conditional update failed for table %(table)s id %(id)s: "
    "Expected value for column %(column)s:  %(expected)s, actual found: "
    "%(actual)s.")

class DbEntryNotFound(DBException):
    message = _("Database Entry: %(table)s:%(id)s not found")

class UserLimitNotFoundByOwnerId(DBException):
    message = _("UserLimit with owner Id %(account_id)s could not be found.")

class MaxRetryCount(NamoException):
    message = _("Max re try count %(max_retry_count) reached")
