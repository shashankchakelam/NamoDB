import logging

from asteval import Interpreter

'''
File to contain all common methods and values related to parameter manipulation and usage
'''

# Based on http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_WorkingWithParamGroups.html#USER_ParamValuesRef
parameter_variables = ['AllocatedStorage', 'DBInstanceClassMemory', 'EndPointPort']
parameter_functions = ['GREATEST', 'LEAST']
parameter_arithmetic_operators = ['*', '/', '+']

# TODO(shank): Two deviations from AWS:
# 1. Lack of a "log" function
# 2. Instead of a "sum" function, introduced a "+" arithmetic operator
# The primary reason for both of these is the same: There is no directly substitutable method in python
# In the case of log, python defaults to using base as 'e' while AWS wants '2'. In python this will need to be
# converted to log(value, 2). This parsing is tricky. Similarly, the sum method in python takes input as a list.
# Hence, we'll need to convert SUM(value1, value2, value3) into sum([value1, value2, value3]). Both of these changes
# require a similar type of parsing - finding the corresponding closing brace and replacing with the appropriate value.
# This seems possible with the PyParsing library. We can implement these 2 later without breaking backward compatibility

def calculate_param_value(value, param_variables):
    '''
    We support formulas and functions as part defining a parameter value. This function executes the formula and
    functions, if there are any, and returns back the calculated value

    Parameters
    ----------
    value: Parameter value to parse and calculate
    param_variables: Dictionary containing values of supported parameter variables

    Returns
    -------
    Calculated value of the parameter
    '''

    # Interesting note on evaluating strings as mathematical expressions in Python.
    # 1. Why not use eval() or pyparsing?
    # https://opensourcehacker.com/2014/10/29/safe-evaluation-of-math-expressions-in-pure-python/
    # Not using the approach mentioned in the above blog either because it doesn't solve all issues mentioned either
    # 2. asteval seems to be the safest and simplest way of going about this. Note the caveats:
    # http://newville.github.io/asteval/motivation.html#how-safe-is-asteval
    # Hence, we still need to ensure, in our case, that all characters given as input are ones we wish to support

    # Supporting only expressions of the format specified in:
    # http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_WorkingWithParamGroups.html#USER_ParamValuesRef

    # Check if input is an expression to be evaluated or not
    if '{' in value and '}' in value:
        logging.debug('Parameter value contains a formula to be evaluated: {0}'.format(value))
        _validate_characters_in_parameter_expression(value)

        # Replacing the formula {<content>} with int(<content>) so that we get a truncated integer value
        # Not using math.trunc() as that returns back a decimal value
        value = value.replace('{', 'int(')
        value = value.replace('}', ')')

        # Replacing the exposed functions with corresponding python methods
        value = value.replace('GREATEST', 'max')
        value = value.replace('LEAST', 'min')

        # Replace variables in the formula with their corresponding values
        for var_name, var_value in param_variables.iteritems():
            if var_name in value:
                value = value.replace(var_name, str(var_value))

        # Evaluate the expression and return the value
        aeval = Interpreter()
        result = aeval(value)
        return result
    else:
        logging.debug('Parameter value does NOT contain a formula')
        return value

def _validate_characters_in_parameter_expression(expression):
    '''
    Validates that the specified DB parameter expression contains only supported characters and strings.
    Note that this does NOT validate the correctness of the expression, just the characters contained.

    Parameters
    ----------
    expression: Parameter expression to be validated

    Returns
    -------
    Nothing
    '''
    expression_clone = ''.join(expression)
    expression_clone = expression_clone.replace(' ', '')

    # Verify that the only non-numeric characters specified are supported ones
    supported_characters = ['*', '/', '+', '(', ')', '{', '}', ',']
    for character in supported_characters:
        expression_clone = expression_clone.replace(character, '')

    for variable in parameter_variables:
        expression_clone = expression_clone.replace(variable, '')

    for function in parameter_functions:
        expression_clone = expression_clone.replace(function, '')

    for operator in parameter_arithmetic_operators:
        expression_clone = expression_clone.replace(operator, '')

    if not expression_clone.isdigit():
        raise ValueError('Invalid expression for parameter: {0}', expression)
