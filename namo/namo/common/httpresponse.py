from webob import Response
import httplib
import json


def ok(req):
    return Response(request=req, status=httplib.OK, content_type='application/json')


# TODO(rushiagr): no need to pass 'req' object here. Response body doesn't need
# anything from request object.
def data(req, result):
    response = Response(request=req,
                        status=httplib.OK,
                        content_type='application/json')
    response.body = json.dumps(result,
                               sort_keys=True,
                               indent=4,
                               separators=(',', ': '))
    return response

def data_array(req, name, items):
    response = Response(request=req,
                        status=httplib.OK,
                        content_type='application/json')
    item_array = []
    for item in items:
        item_array.append(item.to_dict())
    response.body = json.dumps({name: item_array},
                               sort_keys=True,
                               indent=4,
                               separators=(',', ': '))
    return response


def not_found(req):
    return Response(request=req,
                    status=httplib.NOT_FOUND,
                    content_type='application/json')


def internal_server_error(req):
    return Response(request=req,
                    status=httplib.INTERNAL_SERVER_ERROR,
                    content_type='application/json')

def not_acceptable(req):
    return Response(request=req,
                    status=httplib.NOT_ACCEPTABLE,
                    content_type='application/json')
