
"""
Utilities and helper functions common to all RDS projects
"""

import errno
import os
import traceback

from requests import exceptions as req_exc
import sqlalchemy
import webob

NAMO_TEST_SOCKET_FD_STR = 'NAMO_TEST_SOCKET_FD'

# TODO(rushiagr): write tests for functions in this file

def safe_mkdirs(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

def safe_remove(path):
    try:
        os.remove(path)
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise

def exc_to_webob_exc(f):
    '''
    Decorator which catches any exception throws by a method.

    And sends HTTP 500 error instead.
    '''
    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception:
            # TODO(rushiagr): instead of printing, log exception
            traceback.print_exc()
            return webob.exc.HTTPInternalServerError()
    return inner

def db_exc_to_webob_exc(f):
    '''
    Decorator which catches db exception thrown by a method.

    And sends 500 error instead.
    '''
    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except sqlalchemy.exc.SQLAlchemyError:
            # TODO(rushiagr): instead of printing, log exception
            traceback.print_exc()
            return webob.exc.HTTPInternalServerError()
    return inner

def requests_exc_to_webob_exc(f):
    '''
    Decorator to handle exceptions raised by requests method.

    If another type of exception is thrown, we'll log it and return internal
    server error anyway.
    '''
    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except req_exc.RequestException:
            # TODO(rushiagr): soon, handle all requests exception on a case by case
            # basis: ConnectionError, ConnectTimeout, ReadTimeout, HTTPError. See
            # http://www.mobify.com/blog/http-requests-are-hard/
            traceback.print_exc()
            return webob.exc.HTTPInternalServerError()
        except:
            # Generic exception raising
            traceback.print_exc()
            return webob.exc.HTTPInternalServerError()
    return inner


def get_test_suite_socket():
    global NAMO_TEST_SOCKET_FD_STR
    if NAMO_TEST_SOCKET_FD_STR in os.environ:
        fd = int(os.environ[NAMO_TEST_SOCKET_FD_STR])
        sock = socket.fromfd(fd, socket.AF_INET, socket.SOCK_STREAM)
        if six.PY2:
            sock = socket.SocketType(_sock=sock)
        sock.listen(CONF.backlog)
        del os.environ[NAMO_TEST_SOCKET_FD_STR]
        os.close(fd)
        return sock
    return None
