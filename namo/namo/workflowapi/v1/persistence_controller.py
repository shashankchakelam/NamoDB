# Copyright 2012 OpenStack Foundation.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import logging
import webob

from namo.common import exception
from namo.common import httpresponse
from namo.common import parameters
from namo.common import utils
from namo.common import wsgi
from namo.db.mysqlalchemy import api as db
from oslo_config import cfg

CONF = cfg.CONF
default_opts = [
    cfg.IntOpt('max_retry_count_backups',
                default=10,
                help='Batch size for getting pending instances.'),
]

CONF.register_opts(default_opts, group='DEFAULT')

'''
The persistence (metadata backend) controller.

Basically handles all calls which goes to our metadata backend database.
Basically will just call db api methods.
'''


class PersistenceController(object):

    # TODO(rushiagr): use logging here

    @utils.db_exc_to_webob_exc
    def mark_final_backup_completed(self, req, body):
        db.final_backup_completed(req.context, body['backup_id'])
        return webob.exc.HTTPOk()

    @utils.db_exc_to_webob_exc
    def backup_mark_completed(self, req, body):
        db.backup_mark_completed(req.context, body['backup_id'])
        db.workflow_mark_completed(body['execution_id'])
        return webob.exc.HTTPOk()

    @utils.db_exc_to_webob_exc
    def backup_mark_deletion_completed(self, req, body):
        db.backup_mark_deletion_completed(req.context, body['backup_id'])
        db.workflow_mark_completed(body['execution_id'])
        return webob.exc.HTTPOk()

    @utils.db_exc_to_webob_exc
    def restore_mark_completed(self, req, body):
        instance_id = body['instance_id']
        backup_id = body['backup_id']
        db.restore_mark_completed(req.context, instance_id, backup_id)
        db.workflow_mark_completed(body['execution_id'])
        return webob.exc.HTTPOk()

    @utils.db_exc_to_webob_exc
    def backup_increment_tries(self, req, body):
        data = {}
        try:
            db.backup_increment_tries(req.context,
                                      body['backup_id'],
                                      CONF.DEFAULT.max_retry_count_backups)
            data['retry'] = 1
        except exception.MaxRetryCount:
            data['retry'] = 0
        return httpresponse.data(req, data)

    def create_volume_snapshot_in_db(self, req, body):
        logging.debug(body)
        db.volume_snapshot_create(req.context,
                                   body['volume_id'],
                                   body['backup_id'],
                                   body['cinder_snapshot_id'])
        return webob.exc.HTTPOk()

    def delete_volume_snapshot_in_db(self,
                                     req,
                                     backup_id):
        db.volume_snapshot_delete(req.context,
                                  backup_id)

        return webob.exc.HTTPOk()

    def get_cinder_snapshot_id_by_backup_id(self, req, backup_id):
        vs = db.volume_snapshot_get_by_backup_id(req.context, backup_id)
        result = {}
        result['cinder_snapshot_id'] = vs.cinder_snapshot_id
        return httpresponse.data(req, result)

    def populate_endpoint_bucketid_in_db(self, req, body):
        instance_id = body['instance_id']
        endpoint = body['endpoint']
        bucket_id = body['bucket_id']
        values = {}
        values['endpoint'] = endpoint
        values['bucket_id'] = bucket_id
        db.instance_update_by_id(req.context, instance_id, values)
        return webob.exc.HTTPOk()

    @utils.db_exc_to_webob_exc
    def save_bucket_info_in_db(self, req, body):
        name = body['bucket_name']
        state = body['state']
        owner_id = body['customer_id']
        attached_policy_id = body['policy_id']

        bucket = db.bucket_create_entry(req.context,name,owner_id,attached_policy_id,state)
        result = {}
        result['bucket_id'] = bucket.id
        return httpresponse.data(req, result)

    @utils.db_exc_to_webob_exc
    def get_bucket_details(self, req, owner_id):
        logging.debug('Getting bucket infomation for owner_id: {0}'.format(owner_id))
        data = {}
        data['bucket_exists'] = 0
        data['policy_exists'] = 0
        try:
            bucket_ref = db.get_bucket_details_for_owner_id(req.context, owner_id=owner_id)
            data['bucket_exists'] = 1
            if bucket_ref.attached_policy_id is not None:
                data['policy_exists'] = 1
            data['name'] = bucket_ref.name
            data['id'] = bucket_ref.id
        except exception.BucketNotFoundByOwnerId:
            logging.debug('Bucket does not exist for owner_id: {0}'.format(owner_id))
            # As bucketid and bucket_name is required on workflow side
            data['name'] = "FAKE_RDS"
            data['id'] = "FAKE_ID"

        return httpresponse.data(req, data)

    @utils.db_exc_to_webob_exc
    def delete_bucket_in_db(self, req, body):
        bucket_id = body['bucket_id']
        db.delete_bucket(req.context,bucket_id)
        return webob.exc.HTTPOk
        
    @utils.db_exc_to_webob_exc
    def get_default_parameters(self, req, instance_id):
        logging.debug('Getting default parameters for instance_id: {0}'.format(instance_id))
        default_parameters = db.get_default_parameters_for_instance(instance_id)

        logging.debug('Getting values of parameter variables for instance_id: {0}'.format(instance_id))
        parameter_variables = db.get_parameter_variable_values_for_instance(instance_id)

        logging.debug('Calculating the values of the default parameters for instance_id: {0}'.format(instance_id))
        for parameter in default_parameters:
            parameter['value'] = parameters.calculate_param_value(parameter['value'], parameter_variables)

        data = {}
        data['parameters'] = default_parameters
        return httpresponse.data(req, data)

    @utils.db_exc_to_webob_exc
    def get_changed_parameters_for_scale_compute(self, req, instance_id, target_nova_flavor_id):
        logging.debug('Getting default parameters for instance_id: {0}'.format(instance_id))
        default_parameters = db.get_default_parameters_for_instance(instance_id)

        logging.debug('Getting values of parameter variables for instance_id: {0}'.format(instance_id))
        parameter_variables = db.get_parameter_variable_values_for_scale_instance(req.context,instance_id,target_nova_flavor_id)

        logging.debug('Calculating the values of the default parameters for instance_id: {0}'.format(instance_id))
        for parameter in default_parameters:
            parameter['value'] = parameters.calculate_param_value(parameter['value'], parameter_variables)

        data = {}
        data['parameters'] = default_parameters
        return httpresponse.data(req, data)

def create_resource():
    controller = PersistenceController()
    return wsgi.Resource(controller)
