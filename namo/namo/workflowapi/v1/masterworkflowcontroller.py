import uuid
import utils as helper
import exceptions as jcs_exc
from jcsclient.compute import Controller as Compute
from jcsclient.vpc import Controller as Vpc
from namo import i18n
from namo.common import exception
from namo.common import wsgi
from namo.common import httpresponse
from namo.common import utils
from namo.db.mysqlalchemy import api,models
from namo.openclient import session
from oslo_config import cfg
from oslo_utils import timeutils
import json
import logging

CONF = cfg.CONF
_ = i18n._
_LW = i18n._LW

workflow_opts = [
    cfg.IntOpt('get_instance_batch_size',
               default=10,
               help='Batch size for getting pending instances.'),
    cfg.IntOpt('get_backup_batch_size',
               default=10,
               help='Batch size for getting pending backups.'),
]

CONF.register_opts(workflow_opts, group='workflow')


# TODO(rushiagr): remove words 'nova', 'cinder' etc from this file. We should
# rename, for example, 'delete_keypair_nova' to 'delete_keypair_jcs' maybe.

# TODO(rushiagr): Create another controller for normal workflows which doesn't
# come under masterworkflow. So there will be only two methods under
# MasterworkflowController: check_idempotency and find_and_schedule_pending_tasks.


class MasterworkflowController(object):



    def __init__(self):
        # TODO(rushiagr): create a parent class where we do this
        # initialization, so that we don't need to do this all the time we
        # create a new controller, avoiding situation when we accidentally
        # forget to do this.
        session.setup_jcsclient()
        self.non_scheduled_operation_id = self.get_non_scheduled_operation_id()

    def get_non_scheduled_operation_id(self):

        operations_info = api.get_dp_operations(session=None, operation_type='NOT_SCHEDULED')
        if not operations_info:
            logging.info('For %s, There is no operation in Progress. Exiting!')
            return None
        return operations_info[0].id



    def check_idempotency(self, req, body):
        # TODO(rushiagr): write a better name, or alternatively a descriptive
        #   docstring, so that it's clearer what this method is for and what it
        #   does
        execution_id = body['execution_id']
        api.masterworkflow_check_and_update(req.context, execution_id)
        result = {}
        data = api.masterworkflow_get(req.context)
        if str(data.execution_id) == execution_id:
            result['id'] = 1
        else:
            result['id'] = 0
        return httpresponse.data(req, result)

    def set_execution_id_null_for_mfw(self, req, body):
        execution_id = body['execution_id']
        api.set_execution_id_null_for_mfw(req.context, execution_id)
        data = api.masterworkflow_get(req.context)
        if data.execution_id is None:
            logging.debug('Execution id set to NULL. Input execution id:' + str(execution_id))
            return httpresponse.ok(req)
        logging.debug('Setting Execution Id Failed. Input execution id:' + str(execution_id))
        return httpresponse.internal_server_error(req)

    def start_new_mfw(self, req):
        data = {}
        data['workflowapi_url'] = CONF.workflow.workflow_url
        session.mistral_session().executions.create('master.scheduletasks', data)
        return httpresponse.ok(req)

    def start_new_backup_workflow(self, req, body):
        old_execution_id = body['execution_id']
        logging.debug('updating backup as failure for execution id' + str(old_execution_id) \
                      + 'in workflow table')

        values = {}
        values['state'] = 'FAILURE'
        api.update_workflow_record(old_execution_id, values)

        mistral_input = {}
        mistral_input['instance_id'] = body['instance_id']
        mistral_input['cinder_uuid'] = body['cinder_uuid']
        mistral_input['backup_id'] = body['backup_id']
        mistral_input['owner_id'] = body['owner_id']
        mistral_input['instance_ip'] = body['instance_ip']
        mistral_input['volume_id'] = body['volume_id']
        mistral_input['workflowapi_url'] = CONF.workflow.workflow_url
        mistral_result = session.mistral_session().executions.create(
            'createbackup.createbackup', mistral_input)
        api.create_workflow_record(mistral_result.id, 'CREATE_BACKUP',
                                   instance_id=body['instance_id'],
                                   backup_id=body['backup_id'])

        return httpresponse.ok(req)

    @utils.exc_to_webob_exc
    def find_and_schedule_pending_tasks(self, req):
        # TODO(rushiagr): if mistral is in error state, but we change status
        # from creating/pending to creating/applying, we need to revert back
        # the state to creating/pending
        self._get_pending_instances_and_schedule(req)
        self._get_pending_backups_and_schedule(req)
        logging.debug('find_and_schedule_pending_tasks was successful but throwing bad req error')
        return httpresponse.not_acceptable(req)

    def _get_pending_backups_and_schedule(self, req):
        pending_backups = api.backup_get_all_pending(req.context,
                                                     CONF.workflow.get_backup_batch_size)

        backups_pending_creation = [bkp for bkp in pending_backups if bkp['lifecycle'] == 'CREATING']

        # Schedule all backups which are pending creation
        for backup in backups_pending_creation:
            inst = api.instance_get_by_id('', backup['instance_id'])

            if inst.lifecycle == 'PENDING' \
                    and inst.changestate == 'DELETING':
                logging.debug('Ignore this snapshot as it will get picked up by Delete workflow' \
                              + 'Instance Id:' + backup['instance_id'] \
                              + 'Backup Id:' + backup['backup_id'])
                continue

            bkp, inst = api.mark_backup_creation_started(req.context, backup)
            if not bkp or not inst:
                continue

            mistral_input = api.construct_mistral_create_backup_input(req.context, bkp, inst)
            mistral_input['workflowapi_url'] = CONF.workflow.workflow_url
            mistral_result = session.mistral_session().executions.create(
                'createbackup.createbackup', mistral_input)
            api.create_workflow_record(mistral_result.id, 'CREATE_BACKUP', instance_id=inst.id, backup_id=bkp.id)

        # Schedule all backups which are pending deletion

        backups_pending_deletion = [bkp for bkp in pending_backups if
                                    bkp['lifecycle'] == 'DELETING']

        for backup in backups_pending_deletion:
            bkp = api.mark_backup_deletion_started(None, backup)

            if not bkp:
                continue

            mistral_input = api.construct_mistral_delete_backup_input(None,
                                                                      backup['backup_id'])
            mistral_input['workflowapi_url'] = CONF.workflow.workflow_url
            mistral_result = session.mistral_session().executions.create(
                'deletebackup.deletebackup', mistral_input)
            api.create_workflow_record(mistral_result.id, 'DELETE_BACKUP', backup_id=bkp.id)

    def _take_final_backup(self, instance_id):
        """
        This function will determine if we need to take final backup before delete
        If we need to take final backup before delete we will
        :param instance_id:
        :return: return if we need to take a final backup or not
        """
        try:
            logging.debug('Checing if we need to take final backup' \
                          + 'For Instance id:' + str(instance_id))
            backups = api.backup_get_by_filters(None, None,
                                                instance_id=instance_id,
                                                lifecycle='CREATING',
                                                changestate='PENDING',
                                                deleted=False)
            if len(backups) > 1:
                raise Exception('More that one CREATING PENDING entry' \
                                + 'exists in backupid for instance id' + str(instance_id))

            return "True", backups[0].id

        except exception.DbEntryNotFound:
            return "False", None
        pass

    def _get_pending_instances_and_schedule(self, req):
        # TODO(rushiagr): rename 'item' to 'instance' and 'items' to
        # 'instances'
        items = api.instance_get_n_pending(req.context,
                                           CONF.workflow.get_instance_batch_size)
        for item in items:
            if item.lifecycle == 'CREATING':
                self.start_create_instance_wf_and_update_db(req, item)
            elif item.lifecycle == 'DELETING':
                try:
                    self.start_delete_instance_wf_and_update_db(req, item)
                except Exception:
                    continue
            elif item.lifecycle == 'RESTORING':
                self.start_restore_instance_wf_and_update_db(req, item)
            elif item.lifecycle == 'BACKUP':
                # This will be handled by the backup scheduler so just ignoring
                # for now
                continue
            elif item.lifecycle == 'MODIFYING':
                self.start_modify_instance_wf_and_update_db(req, item)
            else:
                raise Exception

    def start_restore_instance_wf_and_update_db(self, req, instance):
        body = self.get_instance_common_parameters(req, instance)

        body['backup_id'] = instance.restored_from_backup_id
        body['local_flavor_id'] = instance.nova_flavor.id
        body['log_retention_period'] = instance.log_retention_period
        api.mark_restore_started(req.context, instance)
        mistral_result = session.mistral_session().executions.create('restore.restore', body)
        api.create_workflow_record(mistral_result.id, 'RESTORE_INSTANCE', instance_id=instance.id,
                                   backup_id=instance.restored_from_backup_id)

    def start_create_instance_wf_and_update_db(self, req, instance):
        body = self.get_instance_common_parameters(req, instance)

        body['local_flavor_id'] = instance.nova_flavor.id
        body['port'] = instance.port
        body['master_user_password'] = instance.master_user_password
        body['master_user_name'] = instance.master_username
        body['log_retention_period'] = instance.log_retention_period

        mistral_result = session.mistral_session().executions.create('createdb.createdb', body)
        api.create_workflow_record(mistral_result.id, 'CREATE_INSTANCE', instance_id=instance.id)

        body['changestate'] = 'APPLYING'
        body['name'] = instance.name
        # We have change name in the instances table to flavor_id
        # Change the table name from nova_flavor to flavor id
        body['nova_flavor_id'] = instance.nova_flavor.id

        api.instance_update(req, body['instance_id'], body)

    def start_modify_instance_wf_and_update_db(self, req, instance):

        '''
                   This function will basically look for pending data plane tasks associated to an instance and assign it to a
                   handler to schedule relevant workflows based on the tasks.

        '''

        body = self.get_instance_common_parameters(req, instance)
        session = api.get_session()
        body.pop('artefact_id')
        body['log_retention_period'] = instance.log_retention_period
        if self.non_scheduled_operation_id is not None:
            task = self._find_task_to_schedule(session, self.non_scheduled_operation_id, instance)
            self._schedule_workflow_from_task(req, task, instance, body)



    def _find_task_to_schedule(self, session, operation_id, instance):
        '''
            This function will find the task for which workflow needs to be started.
            It will also put in the execution_id in the task_runner_id of the column, so that no other workflow works on these tasks.
            For now before migrating the code to Task workflow, using a self defined task runner id.

        '''
        instance_ids = []
        instance_ids.append(instance.id)
        execution_id = str (uuid.uuid1())
        q = api.get_pending_dp_task_ids_query(operation_id=operation_id,instance_ids=instance_ids,session=session)
        api.update_task_runner(execution_id, session=session, query=q)
        return api.model_query('', models.DP_Tasks, session=session, task_runner_id=execution_id).all()


    def _schedule_workflow_from_task(self, req, task, instance, body):

        '''
            This function will loop through all the tasks, find out the relevant task data and
            schedule workflows out of them.

        '''
        task_data = task[0].task_data
        task_data_json_values = json.loads(task_data)
        for task_data_json_value in task_data_json_values:
            if(task_data_json_value['parameter'] == 'nova_flavor_id'):
                body['target_nova_flavor_id'] = task_data_json_value['targetValue']
                nova_flavor = api.nova_flavor_get(req.context,task_data_json_value['targetValue'])
                body['target_local_flavor_id'] = nova_flavor.id
                mistral_result = session.mistral_session().executions.create('modifydb.scale_compute', body)
                api.create_workflow_record(mistral_result.id, 'MODIFY_INSTANCE', instance_id=instance.id)


        api.update_task_status(task[0].id,'IN-PROGRESS')
        body['changestate'] = 'APPLYING'
        body['name'] = instance.name

        # We have change name in the instances table to flavor_id
        # Change the table name from nova_flavor to flavor id
        body['nova_flavor_id'] = instance.nova_flavor.id
        api.instance_update(req, body['instance_id'], body)


    def start_delete_instance_wf_and_update_db(self, req, instance):
        body = self.get_instance_common_parameters(req, instance)

        bucket = instance.bucket

        if bucket is None:
            # using fake bucket name RDS as bucket name cannot be in caps, we cannot send empty as
            # there is problem in that.
            body['bucket_name'] = "RDS"
            body['bucket_exist'] = 0
        else:
            body['bucket_name'] = bucket.name
            body['bucket_exist'] = 1

        try:
            body['take_final_backup'], body['backup_id'] = self._take_final_backup(instance.id)
        except Exception:
            logging.exception('_take_final_backup')
            raise
        body.pop('artefact_id')
        body['local_flavor_id'] = instance.nova_flavor.id
        mistral_result = session.mistral_session().executions.create('deletedb.deletedb', body)
        api.create_workflow_record(mistral_result.id, 'DELETE_INSTANCE', instance_id=instance.id)

        body['changestate'] = 'APPLYING'
        body['name'] = instance.name
        # We have change name in the instances table to flavor_id
        # Change the table name from nova_flavor to flavor id
        body['nova_flavor_id'] = instance.nova_flavor.id

        if body['take_final_backup'] == 'True':
            logging.debug('Taking final snapshot for backup_id:' + str(body['backup_id']))
            api.mark_backup_deletion_started_with_final_snapshot(None,
                                                                 body['backup_id'])
        else:
            api.instance_update(req, body['instance_id'], body)

    def get_instance_common_parameters(self, req, instance):
        body = {}
        str_owner_id = str(instance.owner_id)
        body['owner_id'] = str_owner_id.rjust(12, '0')
        body['instance_id'] = instance.id
        # TODO(rushiagr): we're putting instance_name and name both
        # keys' value as item.name. I'm sure we can remove one of them.
        # In future :)
        body['instance_name'] = instance.name
        artefact_id = instance.artefact_id
        artefact = api.get_artefact_joinedload(req.context,
                                               artefact_id)
        # print "buckets:" +  str(instance.buckets)
        body['database_engine'] = artefact.db_engine.name
        body['artefact_id'] = artefact_id
        # TODO(rushiagr): call image_get_for_artefact() and pass img
        # info here only, instead of making a call later on
        image = api.image_get_for_artefact(req.context, artefact_id)
        # TODO(rushiagr): keeping az same here for now coz I don't
        # understand from where this value is supposed to be fetched
        # from. Also note that this value is put into hosts table, not
        # instances
        body['az'] = "nova"
        # TODO(rushiagr): just use storage_size, and not size,
        # everywhere
        body['size'] = instance.storage_size
        # TODO / NOTE(rushiagr): this is a mess right now. api.instance_update
        # expects 'nova_flavor_id' to be the ID of row in nova_flavors
        # table. But mistral's create() expects it to be set to the id
        # in Nova for the flavor. So modifying the value accordingly
        body['nova_flavor_id'] = instance.nova_flavor.nova_flavor_id
        body['nova_image_id'] = image.glance_uuid
        body['local_image_id'] = image.id
        body['workflowapi_url'] = CONF.workflow.workflow_url
        return body



    # TODO change the functionname to a more meaningful one
    def instance_update_in_db(self, req, body):
        mark_completed = False
        if body['lifecycle'] == 'ACTIVE' and body['changestate'] == 'NONE':
            body['instance_creation_time'] = timeutils.utcnow()
            mark_completed = True

        if (body['lifecycle'] == 'DELETING' and body['changestate'] == 'NONE' and body.get('deleted') == 'TRUE'):
            body['deleted'] = api.get_instance_delete_max_count(instance_id=body['instance_id']) + 1
            mark_completed = True

        session = api.get_session()
        with session.begin(subtransactions=True):
            api.instance_update_by_id(req, body['instance_id'], body, session=session)
            if mark_completed:
                api.workflow_mark_completed(body['execution_id'], session=session)
                api.update_task_status_from_progress_to_success_by_instance_id(body['instance_id'], 'SUCCESS')
            return httpresponse.ok(req)

    def keypair_create_in_nova(self, req):
        compute = Compute()
        params = ["create-key-pair", "--key-name", uuid.uuid4().hex]
        resp = compute.create_key_pair(params)
        resp_dict = helper.get_response(resp)
        result = {
            'nova_keypair_id': resp_dict['CreateKeyPairResponse']['keyName'],
            'private_key': resp_dict['CreateKeyPairResponse']['keyMaterial']
        }
        return httpresponse.data(req, result)

    def delete_keypair_nova(self, req, body):
        keyid = body['nova_keypair_id']
        compute = Compute()
        params = ["delete-key-pair", "--key-name", keyid]
        resp = compute.delete_key_pair(params)
        resp_dict = helper.get_response(resp)
        if resp_dict['DeleteKeyPairResponse']['return'] != 'true':
            # TODO(rushiagr): create a CloudError exception?
            raise Exception
        return httpresponse.ok(req)

    def create_vm_in_nova(self, req, body):
        nova_image_id = body['nova_image_id']
        az = body['az']
        owner_id = body['owner_id']
        nova_flavor_id = body['nova_flavor_id']
        nova_keypair_id = body['nova_keypair_id']

        # TODO(rushiagr): volume size is hardcoded below, which is the root
        # disk size. We might need to make this configurable/change in future

        # TODO(rushiagr): we can use delete_on_termination = true in future to
        # delete the volume when the instance deletes. This will save us one
        # step in that we won't then have to delete the cinder volume. But I
        # guess it's not worth the effort since we're moving to amazon style
        # APIs pretty soon.

        compute = Compute()
        params = ["run-instances", "--image-id", nova_image_id, "--instance-type-id", nova_flavor_id, "--subnet-id", CONF.jiocloud.SubnetId , "--security-group-ids", CONF.jiocloud.SecurityGroupId , "--key-name", nova_keypair_id , "--instance-count", "1"]

        resp = compute.run_instances(params)
        resp_dict = helper.get_response(resp)

        result = {}
        result['nova_vm_id'] = resp_dict['RunInstancesResponse']['instancesSet']['item']['instanceId']
        return httpresponse.data(req, result)

    def delete_vm(self, req, body):
        compute = Compute()
        params = ["terminate-instances", "--instance-ids", body['nova_vm_id']]
        resp = compute.terminate_instances(params)
        resp_dict = helper.get_response(resp)
        if resp_dict['TerminateInstancesResponse']['instancesSet']['item']['currentState'] != 'shutting-down':
            # TODO(rushiagr): create a CloudError exception?
            raise Exception
        return httpresponse.ok(req)

    def check_vm_active_in_nova(self, req, nova_vm_id):
        compute = Compute()
        params = ["describe-instances", "--instance-ids", nova_vm_id]
        resp = compute.describe_instances(params)
        resp_dict = helper.get_response(resp)
        state = resp_dict['DescribeInstancesResponse']['instancesSet']['item']['instanceState']
        if state == 'running':
            return httpresponse.ok(req)
        else:
            return httpresponse.not_found(req)

    def check_vm_deleted(self, req, nova_vm_id):
        try:
            compute = Compute()
            params = ["describe-instances", "--instance-ids", nova_vm_id]
            resp = compute.describe_instances(params)
            resp_dict = helper.get_response(resp)
            return httpresponse.internal_server_error(req)
        except Exception:
            logging.exception('check_vm_deleted')
            return httpresponse.ok(req)

    def create_floating_ip(self, req):
        vpc = Vpc()
        params = ["allocate-address", "--domain", "vpc"]
        resp = vpc.allocate_address(params)
        resp_dict = helper.get_response(resp)
        result = {}
        result['nova_vm_ip'] = resp_dict['AllocateAddressResponse']['publicIp']
        result['nova_vm_ip_id'] = resp_dict['AllocateAddressResponse']['allocationId']
        return httpresponse.data(req, result)

    def attach_floating_ip_to_instance(self, req, body):
        logging.debug("Associating floating IP: {0} with VM: {1}".format(body['nova_vm_ip_id'], body['nova_vm_id']))
        vpc = Vpc()
        params = ["associate-address", "--allocation-id",body['nova_vm_ip_id'] , "--instance-id" , body['nova_vm_id'] ]
        resp = vpc.associate_address(params)
        resp_dict = helper.get_response(resp)
        logging.debug("Reponse of attaching {0} with {1} is: {2}".format(body['nova_vm_ip_id'], body['nova_vm_id'],
                                                                         str(resp_dict)))

        if resp_dict['AssociateAddressResponse']['return'] == 'true':
            return httpresponse.ok(req)
        else:
            logging.debug('AssociateAddressResponse Failed')
            return httpresponse.internal_server_error(req)

    def check_floating_ip_attached(self, req, nova_vm_id, nova_vm_ip):
        logging.debug("Checking Floating IP Attached: VM ID: {0}, VM IP: {1}".format(nova_vm_id, nova_vm_ip))
        compute = Compute()
        params = ["describe-instances", "--instance-ids", nova_vm_id]
        resp = compute.describe_instances(params)
        resp_dict = helper.get_response(resp)
        ip = resp_dict['DescribeInstancesResponse']['instancesSet']['item']['ipAddress']
        if ip == nova_vm_ip:
            return httpresponse.ok(req)
        else:
            logging.debug('IP mis match')
            return httpresponse.internal_server_error(req)


    def delete_floating_ip(self, req, body):
        vpc = Vpc()
        params = ["release-address", "--allocation-id" , body['nova_vm_ip_id'] ]
        resp = vpc.release_address(params)
        resp_dict = helper.get_response(resp)
        if resp_dict['ReleaseAddressResponse']['return'] == 'true':
            return httpresponse.ok(req)
        else:
            logging.debug('Error while releasing address for floating ip')
            return httpresponse.internal_server_error(req)

    # TODO : Work from here. tomorrow
    def volume_create_in_cinder(self, req, body):
        # TODO(rushiagr): remove name 'cinder' from method
        compute = Compute()
        params = ["create-volume", "--size", body['size']]
        resp = compute.create_volume(params)
        resp_dict = helper.get_response(resp)
        result = {
            'nova_volume_id': resp_dict['CreateVolumeResponse']['volumeId'],
            # TODO(rushiagr): start using volume type returned from response
            # once it actually starts returning the same
            'nova_volume_type': 'standard'
        }
        return httpresponse.data(req, result)

    def check_volume_available(self, req, nova_volume_id):
        compute = Compute()
        params = ["describe-volumes", "--volume-ids", nova_volume_id]
        resp = compute.describe_volumes(params)
        resp_dict = helper.get_response(resp)
        resp_volumes = resp_dict['DescribeVolumesResponse']['volumeSet']
        if resp_volumes['item']['status'] == 'available':
            return httpresponse.ok(req)

        return httpresponse.not_found(req)

    def check_volume_deleted(self, req, nova_volume_id):
        try:
            compute = Compute()
            params = ["describe-volumes", "--volume-ids", nova_volume_id]
            resp = compute.describe_volumes(params)
            resp_dict = helper.get_response(resp)
        except jcs_exc.HTTP404:
            return httpresponse.ok(req)
        return httpresponse.internal_server_error(req)

    def check_volume_attached(self, req, nova_volume_id, nova_vm_id):
        # TODO(rushiagr): we're not using nova_vm_id at all
        try:
            compute = Compute()
            params = ["describe-volumes", "--volume-ids", nova_volume_id]
            resp = compute.describe_volumes(params)
            resp_dict = helper.get_response(resp)
        except jcs_exc.HTTP400:
            return httpresponse.not_found(req)
        resp_volumes = resp_dict['DescribeVolumesResponse']['volumeSet']
        if resp_volumes['item']['status'] == 'in-use':
            return httpresponse.ok(req)

        return httpresponse.not_found(req)

    def attach_volume(self, req, body):
        device = '/dev/vdb'
        compute = Compute()
        params = ["attach-volume", "--volume-id", body['nova_volume_id'], "--instance-id", body['nova_vm_id'], "--device", device ]
        resp = compute.attach_volume(params)
        resp_dict = helper.get_response(resp)

        return httpresponse.ok(req)

    def detach_volume(self, req, body):
        compute = Compute()
        params = ["detach-volume", "--volume-id", body['nova_volume_id'], "--instance-id", body['nova_vm_id'] ]
        compute.detach_volume(params)
        return httpresponse.ok(req)

    def delete_volume(self, req, body):
        try:
            compute = Compute()
            params = ["delete-volume", "--volume-id", body['nova_volume_id'] ]
            resp = compute.delete_volume(params)
            resp_dict = helper.get_response(resp)
            if resp_dict['DeleteVolumeResponse']['return'] == 'true':
                return httpresponse.ok(req)
        except jcs_exc.HTTP400:
            pass

        return httpresponse.internal_server_error(req)

    def keypair_save_in_db(self, req, body):
        keypair = api.keypair_create(req,
                                     body['nova_keypair_id'],
                                     body['private_key'])
        result = {}
        result['local_keypair_id'] = keypair.id
        return httpresponse.data(req, result)

    def keypair_delete_from_db(self, req, body):
        api.keypair_destroy(req, body['local_keypair_id'])
        return httpresponse.ok(req)

    def keypair_get_from_db(self, req, local_keypair_id):
        keypair = api.keypair_get(req, local_keypair_id)
        result = {}
        result['nova_keypair_id'] = keypair.nova_key_id
        result['private_key'] = keypair.private_key
        result['deleted'] = keypair.deleted
        return httpresponse.data(req, result)

    def host_get_from_db(self, req, instance_id):
        session = api.get_session()
        host = api.host_get_by_instance_id(req.context, instance_id,session)
        result = {}
        result['local_keypair_id'] = host.keypair_id
        result['nova_vm_id'] = host.nova_uuid
        result['local_host_id'] = host.id
        result['nova_vm_ip_id'] = host.vm_ip_id
        result['nova_keypair_id'] = host.keypair.nova_key_id
        return httpresponse.data(req, result)

    def host_save_in_db(self, req, body):
        host = api.host_create(req, body['instance_id'],
                               body['local_keypair_id'],
                               body['az'], body['nova_vm_id'],
                               body['local_flavor_id'],
                               body['nova_vm_ip'],
                               body['nova_vm_ip_id'],
                               body['artefact_id'])
        result = {}
        result['local_host_id'] = host.id
        return httpresponse.data(req, result)

    def host_update_in_db(self, req, body):
        host = api.host_update(req, body['instance_id'],
                               body['az'], body['nova_vm_id'],
                               body['local_flavor_id'])
        result = {}
        result['local_host_id'] = host.id
        return httpresponse.data(req, result)

    def host_delete_in_db(self, req, body):
        api.host_destroy(req, body['local_host_id'])
        return httpresponse.ok(req)

    def volume_save_in_db(self, req, body):
        volume = api. \
            volume_create(req, body['nova_volume_id'],
                          body['local_host_id'],
                          body['nova_volume_type'],
                          body['nova_attach_status'])
        result = {}
        result['local_volume_id'] = volume.id
        return httpresponse.data(req, result)

    def volume_get_from_db(self, req, host_id):
        volume = api. \
            volume_get_by_host_id(req, host_id)
        result = {}
        result['local_volume_id'] = volume.id
        result['nova_volume_id'] = volume.cinder_uuid
        result['local_host_id'] = volume.host_id
        result['nova_volume_type'] = volume.volume_type
        result['attach_status'] = volume.attach_status
        return httpresponse.data(req, result)

    def volume_update_in_db(self, req, body):
        volume = api. \
            volume_change_status(req, body['local_volume_id'],
                                 body['attach_status'])
        return httpresponse.ok(req)

    def volume_delete_in_db(self, req, body):
        api. \
            volume_destroy(req, body['local_volume_id'])
        return httpresponse.ok(req)


def create_resource():
    controller = MasterworkflowController()
    return wsgi.Resource(controller)
