import json
import xmltodict
import exceptions

def get_response(resp):
    """
    Get the modified response in form of json.
    """
    if resp.status_code >= 400:
        #print resp.content
        print 'Exception %s thrown! Status code:' % resp.status_code
        try:
            print 'Error content: ', json.dumps(json.loads(resp.content),
                                                indent=4, sort_keys=True)
        except:
            resp_dict = dict()
            resp_ordereddict = xmltodict.parse(resp.content)
            print json.dumps(resp_ordereddict, indent=4, sort_keys=True)
        print 'Refer to, jcs --help'
        if resp.status_code == 400:
            raise exceptions.HTTP400()
        elif resp.status_code == 404:
            raise exceptions.HTTP404()
        raise Exception
    response = resp.content
    print "\'--------------------------------------------------\'"
    try:
        resp_dict = dict()
        
        if response is not '':
            if isinstance(response,dict):
                resp_dict = response
            else:
                resp_dict = json.loads(response)
            print json.dumps(resp_dict, indent=4, sort_keys=True)
    except:
        #print response
        resp_dict = dict()
        resp_ordereddict = xmltodict.parse(response)
        resp_json_string = json.dumps(resp_ordereddict, indent=4,
                                        sort_keys=True)
        # handle the case of keypair data
        resp_dict = json.loads(resp_json_string)
        resp_json_string = resp_json_string.replace("\\n", "\n")
        print (resp_json_string)
    print "\n\nRequest successfully executed !"
    return resp_dict
    