import requests
import logging
import httplib
import json
import datetime
import utils as helper
import exceptions as jcs_exc
from jcsclient.compute import Controller as Compute
from jcsclient.dss_api.dss_main import DSS
from jcsclient.iam import Controller as IAM 
from jcsclient.vpc import Controller as Vpc

from namo.common import httpresponse
from namo.common import wsgi
from namo.openclient import session

class CloudController(object):

    def __init__(self):
        # TODO(rushiagr): create a parent class where we do this
        # initialization, so that we don't need to do this all the time we
        # create a new controller, avoiding situation when we accidentally
        # forget to do this.
        session.setup_jcsclient()
        self.db_bucket_log_prefix = "DbLogs/"

    def _check_if_none(self, id):
        if id is None:
           logging.debug('Getting None Value if id')
           return httpresponse.internal_server_error(req)
        logging.debug('Getting'+ str(id) +'Value for id')

    def create_bucket(self, req, body):
        try:
            self._check_if_none(body['customer_id'])
            customer_id = body['customer_id']
            bucket_prefix = "rds-" + (str(customer_id)).lower()
            count = 0
            dss = DSS()
            while True:
                bucket_name = bucket_prefix + "-" + str(count)
                params = ["head-bucket", "--bucket", bucket_name]
                try:
                    response = dss.main(params)
                    if response.status_code == httplib.NOT_FOUND:
                        break
                    count += 1
                except requests.exceptions.ConnectionError:
                    logging.exception('Connection Error while checking bucket existence')
                    return httpresponse.internal_server_error(req)
                except Exception:
                    logging.exception('Exception in create_bucket')
                    return httpresponse.internal_server_error(req)
            # Create bucket in case bucket is not there in dss.
            bucket_name = bucket_prefix + "-" + str(count)
            params = ["create-bucket", "--bucket", bucket_name]
            response = dss.main(params)
            if response.status_code == httplib.OK:
                result = {
                    'bucket_name': bucket_name
                }
                return httpresponse.data(req, result)
            else:
                return httpresponse.internal_server_error(req)
        except requests.exceptions.ConnectionError:
            logging.exception('Connection Error in create_bucket')
            return httpresponse.internal_server_error(req)
        except Exception:
            logging.exception('Exception in create_bucket')
            return httpresponse.internal_server_error(req)

    def check_bucket_created(self, req, bucket_name):
        try:
            self._check_if_none(bucket_name)
            dss = DSS()
            params = ["head-bucket", "--bucket", bucket_name]
            response = dss.main(params)
            if response.status_code == httplib.NOT_FOUND:
                return httpresponse.internal_server_error(req)
            return httpresponse.ok(req)
        except Exception as e:
            logging.exception(e.message)
            return httpresponse.internal_server_error(req)

    def check_bucket_exist(self, req, bucket_name):
        try:
            self._check_if_none(bucket_name)
            dss = DSS()
            params = ["head-bucket", "--bucket", bucket_name]
            response = dss.main(params)
            bucket_exists = 0
            if response.status_code == httplib.NOT_FOUND:
                bucket_exists = 0
            else:
                bucket_exists = 1
            result = {
                'bucket_exists': bucket_exists
            }
            return httpresponse.data(req, result)
        except Exception as e:
            logging.exception(e.message)
            return httpresponse.internal_server_error(req)

    def delete_bucket_prefix(self, req, body):
        try:
            self._check_if_none(body['bucket_name'])
            self._check_if_none(body['prefix_name'])
            bucket_name = body['bucket_name']
            prefix_name = self.db_bucket_log_prefix + body['prefix_name']

            is_truncated = self.__delete_bucket_n_objs(bucket_name, prefix_name, 20)
            while is_truncated == "true":
                is_truncated = self.__delete_bucket_n_objs(bucket_name, prefix_name, 20)

            return httpresponse.ok(req)
        except requests.exceptions.ConnectionError:
            logging.exception('Connection Error in delete_snapshot_in_cinder')
        except Exception:
            logging.exception('Exception in delete_bucket')
        return httpresponse.internal_server_error(req)

    def __delete_bucket_n_objs(self, bucket_name, prefix_name, number):
        dss = DSS()
        is_truncated = "false"
        params = ["list-objects", "--bucket", bucket_name, "--prefix", prefix_name, "--max-items", str(number)]
        res = dss.main(params)
        response = helper.get_response(res)
        is_truncated = response["ListBucketResult"]["IsTruncated"]
        if "Contents" in response["ListBucketResult"]:
            items = response["ListBucketResult"]["Contents"]
            # TODO: Varun(Check with DSS) it's bad that we need to check this-
            # but in case only one result dss does not return list
            if isinstance(items, list):
                for item in items:
                    key = item["Key"]
                    self.__delete_dss_object(key,bucket_name)
            else:
                key = items["Key"]
                self.__delete_dss_object(key, bucket_name)
        return is_truncated

    def __delete_dss_object(self, key, bucket_name):
        dss = DSS()
        params = ["delete-object", "--bucket", bucket_name, "--key", key]
        dss.main(params)

    def set_bucket_permission(self, req, body):
        try:
            self._check_if_none(body['customer_account_id'])
            self._check_if_none(body['bucket_name'])
            customer_account_id = body['customer_account_id']
            bucket_name = body['bucket_name']
            # TODO(Varun) Call to iam api to set permission on the bucket.
            account_principle = "jrn:jcs:iam:" + str(customer_account_id) + ":*"
            policy_name = "RDB-RDS-" + datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
            policy_doc_dict = {}
            policy_doc_dict['name'] = policy_name
            policy_doc_dict['statement'] = [{"action": ["jrn:jcs:dss:GetObject", "jrn:jcs:dss:ListBucket"], "principle": [account_principle], "effect": "allow"}]

            policy_string = json.dumps(policy_doc_dict)

            iam = IAM()
            params = ["create-resource-based-policy", "--policy-document", policy_string]
            response = iam.create_resource_based_policy(params)
            res = helper.get_response(response)
            policy_id = res['policy']['id']
            iam_resource = {}

            # TODO(Varun) Remove the hardcoded account id once iam done with their code changes.
            resource_bucket = "jrn:jcs:dss::Bucket:" + bucket_name
            iam_resource['resource'] = [resource_bucket]
            iam_resource_string = json.dumps(iam_resource)
            resource_params = ["attach-policy-to-resource","--resource", iam_resource_string, "--policy-id", policy_id]
            response = iam.attach_policy_to_resource(resource_params)
            res = helper.get_response(response)
            result ={}
            result['policy_id'] = policy_id
            return httpresponse.data(req, result)
        except Exception as identifier:
            return httpresponse.internal_server_error(req)

    def delete_snapshot_in_cinder(self, req, cinder_snapshot_id):
        try:
            
            # Only on connection error we will retry for any other error we
            # will never retry. Garbage collector service will take care of
            # deleting orphan cinder volumes
            self._check_if_none(cinder_snapshot_id)
            compute = Compute()
            params =["delete-snapshot", "--snapshot-id", cinder_snapshot_id]
            
            compute.delete_snapshot(params)
        except requests.exceptions.ConnectionError as e:
            logging.exception('Connection Error in delete_snapshot_in_cinder')
            return httpresponse.internal_server_error(req)
        except Exception:
            logging.exception('Exception in delete_snapshot_in_cinder')
        return httpresponse.ok(req)

    def take_snapshot_in_sbs(self, req, sbs_volume_id):
        try:
            self._check_if_none(sbs_volume_id)
            body = {}
            compute = Compute()
            params =["create-snapshot", "--volume-id", sbs_volume_id]
            resp = compute.create_snapshot(params)
            resp_dict = helper.get_response(resp)
            logging.debug('SBS response: ' + str(resp_dict))
            if resp_dict['CreateSnapshotResponse']['status'] == 'pending':
                body['sbs_snapshot_id'] = resp_dict['CreateSnapshotResponse']['snapshotId']
                return httpresponse.data(req, body)
            else:
                logging.debug('Got different status: '\
                              + resp_dict['CreateSnapshotResponse']['status'])
        except requests.exceptions.ConnectionError:
            logging.exception('')
        except Exception:
            logging.exception('')

        return httpresponse.internal_server_error(req)

    def check_snapshot_deleted(self, req, cinder_snapshot_id):
        try:
            self._check_if_none(cinder_snapshot_id)
            compute = Compute()
            params =["describe-snapshots", "--snapshot-ids", cinder_snapshot_id]
            resp = compute.describe_snapshots(params)
            response = helper.get_response(resp)
            logging.debug(str(response))
        except jcs_exc.HTTP404:
            return httpresponse.ok(req)
        except Exception as e:
            logging.debug(e.message)

        return httpresponse.internal_server_error(req)

    def get_snapshot_create_status(self, req, cinder_snapshot_id):
        try:
            self._check_if_none(cinder_snapshot_id)
            compute = Compute()
            params =["describe-snapshots", "--snapshot-ids", cinder_snapshot_id]
            resp = compute.describe_snapshots(params)
            resp_dict = helper.get_response(resp)
            result = {}
            result['status'] = resp_dict['DescribeSnapshotsResponse']['snapshotSet']['item']['status']
            result['sbs_snapshot_id'] = cinder_snapshot_id
            status = result['status']
            ## TODO Check with SBS team regarding error states
            if status in ['completed', 'error']:
                logging.debug('Printing Status: ' + str(result))
                return httpresponse.data(req, result)
            else:
                logging.debug('Getting some other   status: ' + result['status'])
                return httpresponse.internal_server_error(req)
        except jcs_exc.HTTP400:
            return httpresponse.not_found(req)
        except Exception as e:
            logging.debug(e.message)
            return httpresponse.internal_server_error(req)

    def volume_restore_in_cinder(self, req, body):
        """
        restore a volume from snapshot id
        """
        self._check_if_none(body['cinder_snapshot_id'])
        self._check_if_none(body['size'])
        compute = Compute()
        params =["create-volume", "--snapshot-id", body['cinder_snapshot_id'], "--size", str(body['size'])]
        resp = compute.create_volume(params)
        resp_dict = helper.get_response(resp)
        result = {}
        result['nova_volume_id'] = resp_dict['CreateVolumeResponse']['volumeId']
        result['nova_volume_type'] = 'standard'
        return httpresponse.data(req, result)

    def volume_create_in_cinder(self, req, body):
        """
        Create a fresh volume in cinder with given size
        """
        # TODO(rushiagr): remove name 'cinder' from method
        compute = Compute()
        params =["create-volume", "--size", str(body['size'])]
        resp = compute.create_volume(params)
        resp_dict = helper.get_response(resp)
        result = {
            'nova_volume_id': resp_dict['CreateVolumeResponse']['volumeId'],
            # TODO(rushiagr): start using volume type returned from response
            # once it actually starts returning the same
            'nova_volume_type': 'standard'
        }
        return httpresponse.data(req, result)

    def get_boot_volume_id_for_instance(self, req, nova_vm_id):
        compute = Compute()
        params =["describe-instances", "--instance-ids", nova_vm_id]
        resp = compute.describe_instances(params)
        resp_dict = helper.get_response(resp)

        bdms = resp_dict['DescribeInstancesResponse']['instancesSet']['item']['blockDeviceMapping']['item']
        logging.debug(bdms)
        for bdm in bdms:
            if str(bdm['deviceName']) == '/dev/vda':
                logging.debug('ok got the boot volime')
                return httpresponse.data(req, {'boot_volume_id': str(bdm['volumeId'])})
        return httpresponse.not_found(req)

    def get_association_id(self, req, nova_vm_ip_id):

        """
        Get the association ip of a vm with the help of allocation id
        Parameters
        ----------
        req - http request .
        nova_vm_ip_id - allocation id of vm

        Returns
        -------
        association id of vm.
        """
        association_id = 'not-exist'
        try:
            self._check_if_none(nova_vm_ip_id)
            vpc = Vpc()
            params =["describe-addresses", "--allocation-ids", nova_vm_ip_id]
            resp = vpc.describe_addresses(params)
            resp_dict = helper.get_response(resp)
            if "associationId" in resp_dict['DescribeAddressesResponse']['addressesSet']['item']:
                association_id_exists = 1
                association_id = resp_dict['DescribeAddressesResponse']['addressesSet']['item']['associationId']
            else:
                association_id_exists = 0
            public_ip = resp_dict['DescribeAddressesResponse']['addressesSet']['item']['publicIp']
            result = {
                'association_id': association_id,
                'association_id_exists': association_id_exists,
                'nova_vm_ip' : public_ip
            }
            return httpresponse.data(req, result)
        except jcs_exc.HTTP400 as e:
            # As allocation if doesn't exist then returning vpc_association_exist=0
            logging.exception(e.message)
            association_id_exists = 0
            result = {
                'association_id': association_id,
                'association_id_exists': association_id_exists
            }
            return httpresponse.data(req, result)
        except Exception as e:
            logging.exception(e.message)
            return httpresponse.internal_server_error(req)

    def disassociate_vm_address(self, req, body):
        """
        Dis-associate an association id before deleting floating ip of a vm.
        Parameters
        ----------
        req - http request
        body - from body we get nova_association_id
        """
        try:
            self._check_if_none(body['association_id'])
            vpc = Vpc()
            params =["disassociate-address", "--association-id", body['association_id']]
            resp = vpc.disassociate_address(params)
            resp_dict = helper.get_response(resp)
            return httpresponse.ok(req)
        except jcs_exc.HTTP400 as e:
            logging.exception(e.message)
            return httpresponse.not_found(req)
        except requests.exceptions.ConnectionError as e:
            logging.exception('Connection Error in disassociate_address')
            return httpresponse.internal_server_error(req)
        except Exception:
            logging.exception('Exception in disassociate_address')
            return httpresponse.internal_server_error(req)


def create_resource():
    controller = CloudController()
    return wsgi.Resource(controller)