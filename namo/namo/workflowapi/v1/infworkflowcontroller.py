import uuid
from namo import i18n
from namo.common import exception
from namo.common import wsgi
from namo.common import httpresponse
from namo.common import utils
from namo.db.mysqlalchemy import api
from namo.db.mysqlalchemy import models
from namo.openclient import session
from oslo_config import cfg

import logging

CONF = cfg.CONF
_ = i18n._
_LW = i18n._LW

class InfiniteWorkflowController(object):

    def __init__(self):
        session.setup_jcsclient()

    @utils.exc_to_webob_exc
    def check_idempotency(self, req, body):
        execution_id = body['execution_id']
        logging.debug('For %s, Checking idempotency for execution id:%s'%(self.workflow_type,str(execution_id)))

        api.infinite_workflow_check_and_update(execution_id, self.workflow_type)
        result = {}
        data = api.infinite_workflow_get(self.workflow_type)
        if str(data.execution_id) == execution_id:
            result['acquired_lock'] = 1
            logging.info('For %s, lock given to execution_id : %s'%(self.workflow_type, str(execution_id)))
        else:
            result['acquired_lock'] = 0

        logging.debug('For %s, result of check_idempotency for execution id\
        :%s = %s'%(self.workflow_type, str(execution_id), str(result['acquired_lock'])))
        return httpresponse.data(req, result)

    @utils.exc_to_webob_exc
    def set_execution_id_null(self, req, body):
        execution_id = body['execution_id']
        logging.debug('For %s, set_execution_id_null_for_auto_backups for execution id:%s'%(self.workflow_type,str(execution_id)))

        api.set_execution_id_null_for_infinite_workflow(execution_id, self.workflow_type)
        data = api.infinite_workflow_get(self.workflow_type)
        if data.execution_id is None:
            logging.info('Execution id set to NULL for %s. Input execution id: %s'%(self.workflow_type, str(execution_id)))
            return httpresponse.ok(req)
        logging.exception('Setting Execution Id Failed for %s. Input execution id: %s'%(self.workflow_type, str(execution_id)))
        return httpresponse.internal_server_error(req)

    @utils.exc_to_webob_exc
    def start_new_infinite_workflow(self, req):
        logging.info('For %s, start_new_auto_backup_workflow'%(self.workflow_type))
        data = {}
        data['workflowapi_url'] = CONF.workflow.workflow_url
        data['scheduled_count'] = 0
        session.mistral_session().executions.create(self.workflow_execution, data)
        logging.info('For %s, started new infinite workflow execution with name %s'%(self.workflow_type, self.workflow_execution))
        return httpresponse.ok(req)

    # TODO(shm): try not to make a http call for this, should be just used for increasing counter. Try std.echo from mistral yaml ?
    @utils.exc_to_webob_exc
    def redo_find_and_schedule(self, req, body):
        logging.info('For %s, redo_find_and_schedule:'%(self.workflow_type))
        scheduled_count = 0
        if body.has_key('scheduled_count'):
            scheduled_count = int(body['scheduled_count'])

        scheduled_count = scheduled_count + 1
        result = {}
        result['scheduled_count']=scheduled_count
        logging.info('For %s, incremented scheduled_count to %s'%(self.workflow_type, scheduled_count))
        return httpresponse.data(req, result)

