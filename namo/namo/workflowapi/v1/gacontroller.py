import json
import requests
import logging
from asteval import Interpreter
from namo.common import wsgi
from namo.common import httpresponse
from namo.common import utils
from namo.db import api
from namo.openclient import session
from oslo_config import cfg
from requests import exceptions as req_exc
from webob import exc

from oslo_config import cfg

raga_opts = [
    cfg.StrOpt('port',
                default='8080',
                help='guest agent port'),
    cfg.StrOpt('freeze_block_time',
                default=10,
                help='freeze block time in secs'),
]
CONF = cfg.CONF
CONF.register_opts(raga_opts, group='raga')
'''
The guest agent controller.

It is responsible for interactions with guest agent.

Note that all the methods will return HTTP 200 OK in case of success, and
will throw HTTP 400 internal server error for any and every thing else
(which might not be ideal, but it works! :) ).
'''

CONF = cfg.CONF

workflow_opts = [
    cfg.IntOpt('raga_primary_port',
                default=8080,
                help='Port on which raga primary will run.'),
    cfg.IntOpt('raga_secondary_port',
                default=8081,
                help='Port on which raga secondary will run.'),
]

CONF.register_opts(workflow_opts, group='workflow')

# TODO(rushiagr): move all methods to use decorators for internalservererror
# TODO(rushiagr): make config options for raga ports
# TODO(rushiagr): start using helper methods everywhere in this file
# TODO(rushiagr): use traceback to print exception messages. Better: log before
# throwing an exception

class GuestAgentController(object):

    # Helper methods for ease-of-use

    @classmethod
    def _get_port(cls, raga_type):
        if raga_type.lower() == 'primary':
            return str(CONF.workflow.raga_primary_port)
        elif raga_type.lower() == 'secondary':
            return str(CONF.workflow.raga_secondary_port)
        else:
            raise Exception

    @classmethod
    def _get_full_url(cls, ip, url, raga_type=None):
        """
        Generate full URL from provided params.

        E.g. if ip is '192.168.1.1', type is 'primary', and url is
        '/v1/health', the full URL will be 'http://192.168.1.1:8080/v1/health'
        """
        if raga_type is None:
            raga_type = 'primary'
        else:
            raga_type = raga_type.lower()
        return 'http://%s:%s%s' % (ip, cls._get_port(raga_type), url)

    # TODO(rushiagr): make type optional here too?
    @classmethod
    def _do_get_request(cls, ip, type, url):
        return requests.get(cls._get_full_url(ip, url, type))

    @classmethod
    def _do_post_request(cls, ip, type, url, body=None):
        if body:
            logging.debug('Request Body is: ' + str(body) )
        logging.debug(cls._get_full_url(ip, url, type))
        resp = requests.post(cls._get_full_url(ip, url, type), data=body)
        logging.debug('Status code of POST Response is: ' + str(resp.status_code))
        logging.debug('Response message: ' + resp.content)
        if resp.status_code >= 400:
            raise Exception('Post request failed. Status code: '+str(resp.status_code)+' ,response content: '+resp.content)
        return resp

    def check_ga_active(self, req, ip, type):
        # TODO(rushiagr): we should have multiple different types of
        # health-checking methods, one for checking raga health (primary and
        # secondary both), one for checking if mysql is up or not, and maybe
        # another one to check system info from outside. Or maybe, we can club
        # all of this into one single api call, which will return all these
        # info in its return json
        # TODO(rushiagr): rename this method to check_mysql_service_running
        try:
            port = self._get_port(type)
            r = self._do_get_request(ip, type, '/v1/service')
            if r.json()['status'] == 'start/running':
                return httpresponse.ok(req)
            else:
                return httpresponse.internal_server_error(req)
        except:
            return httpresponse.internal_server_error(req)

    def check_instance_service_active(self, req, ip, type):
        try:
            port = self._get_port(type)
            r = self._do_get_request(ip, type, '/v1/check_instance_service_health')
            if r.json()['status'] =='UP':
                return httpresponse.ok(req)
            else:
                return httpresponse.internal_server_error(req)
        except:
            return httpresponse.internal_server_error(req)

    def ga_service_action(self, req, body):
        # TODO(rushiagr): Think of a better name for this method. The current
        # name is slightly unclear.
        try:
            data = { }
            data['action'] = body['action']
            content = self._do_post_request(body['ip'],
                                            body['type'],
                                            '/v1/service',
                                            json.dumps(data))
            return httpresponse.ok(req)
        except:
            return httpresponse.internal_server_error(req)

    def get_format_status(self, req, ip, type):
        try:
            r = self._do_get_request(ip, type, '/v1/format')
            if r.json()['status'] == 'completed':
                return httpresponse.ok(req)
            else:
                return httpresponse.internal_server_error(req)
        except:
            return httpresponse.internal_server_error(req)

    def format_volume(self, req, body):
        try:
            r = self._do_post_request(body['ip'],
                                      body['type'],
                                      '/v1/format')
        except Exception as e:
            logging.debug(e.message)
            return httpresponse.internal_server_error(req)
        return httpresponse.ok(req)

    @utils.requests_exc_to_webob_exc
    def check_raga_health(self, req, ip, type):
        resp = self._do_get_request(ip, type, '/v1/versioninfo')
        return exc.HTTPOk()

    def upgrade_raga(self, req, ip, type, body=None):
        '''
        'type' can be 'primary' or 'secondary'.

        Note that if you call /workflow/raga/upgrade/IP/primary, a call
        will be made to secondary raga because to upgrade primary raga, we
        are calling secondary raga.

        You can pass 'desired_version' in the body. By default, the
        desired_version will be fetched from database.
        '''
        if body is not None and type(body) == dict and body.get('desired_version') is not None:
            desired_version = body.get('desired_version')
            raga = api.get_raga_by_filters(req, version=desired_version)
        else:
            # TODO(rushiagr): get desired version from database
            raga = api.get_latest_raga(req)
            desired_version = raga.version
            # TODO(rushiagr): remove this line
            desired_version = '020d45c5'


        # NOTE: we have to make a call to secondary raga to upgrade primary
        # raga, and vice versa

        other_type = 'primary' if type.lower() == 'secondary' else 'secondary'
        # TODO(rushiagr): write db api here to maintain state in database
        # while upgrading

        # TODO(rushiagr): write api here to get latest raga package url.
        # Hardcoding for now.
        pkg_url = raga.url
        deb_pkg = 'rp.deb' if type == 'primary' else 'rs.deb'
        pkg_url = 'https://s3-ap-southeast-1.amazonaws.com/jio-dbaas/%s' % deb_pkg
        data = {'pkg_url': pkg_url}
        try:
            # TODO(rushiagr): handle if this call fails due to some error, or
            # more importantly in case of already an upgrade happening
            # TODO(rushiagr): we can optimize a little bit here. Note that
            # we are making two database calls here in this same method. we
            # can instead make on. Not something which should be done before
            # march 15 release
            is_success = api.set_intent_for_upgrading_raga(req.context, ip,
                    desired_raga_version=desired_version)
            if not is_success:
                # TODO(rushiagr): should raise a better error here
                return httpresponse.internal_server_error(req)
            resp = self._do_post_request(ip,
                                         other_type,
                                         '/v1/upgrade',
                                         json.dumps(data))
        # TODO(rushiagr): write a more generic requests exception here
        except:
            return httpresponse.internal_server_error(req)
        return httpresponse.ok(req)

    def set_port(self, req, body):
        try:
            r = self._do_post_request(body['ip'],
                                      body['type'],
                                      '/v1/set_port',
                                      json.dumps(body))
        except Exception as e:
            return httpresponse.internal_server_error(req)
        return httpresponse.ok(req)

    def create_master_user(self, req, body):
        try:
            r = self._do_post_request(body['ip'],
                                      body['type'],
                                      '/v1/master_user',
                                      json.dumps(body))
        except Exception as e:
            return httpresponse.internal_server_error(req)
        return httpresponse.ok(req)

    def start_log_tasks(self, req, body):
        """
        Start log tasks on the customer RDS instance.
        """
        try:
            r = self._do_post_request(body['ip'],
                                      body['type'],
                                      '/v1/start_db_logs_tasks',
                                      json.dumps(body))
        except Exception as e:
            return httpresponse.internal_server_error(req)
        return httpresponse.ok(req)

# TODO(rushiagr): Should I start all methods corresponding to POST request with
# a 'do_', and all methods corresponding to GET request with a 'get_'?
    def get_upgrade_raga_status(self, req, ip, type):
        '''
        We should first get state from db, to find out what was the initial
        and the desired version of raga, and then call raga to find out its
        own version.
        Note that to get status of primary raga, you just contact primary raga
        and not reverse. Vice versa for secondary.
        '''
        # TODO(rushiagr): make a call to db and get this state info
        v1, v2 = api.get_upgrade_info_from_db(req, ip)

        upgrade_start_version = v1
        upgrade_end_version = v2
        upgrade_start_version = '101f4551'
        upgrade_end_version = '020d45c5'

        port = self._get_port(type)

        try:
            resp = self._do_get_request(ip, type, '/v1/versioninfo')
        # TODO(rushiagr): write a more generic requests exception here
        except:
            return httpresponse.internal_server_error(req)

        # TODO(rushiagr): should I call it 'version' instead of 'git_hash'?
        actual_raga_version = resp.json()['git_hash']

        if actual_raga_version == upgrade_end_version:
            # TODO(rushiagr): write a proper response here. Also, write the 'else' condition
            # TODO(rushiagr): Too bad we're raising  HTTP 400 when we want negative response.
            #   Instead, we should be doing like how I've done in the two commented lines
            #   below, and parse the POST output in the YAML file
            #return httpresponse.data(req, {'status': 'completed'})
            api.mark_upgrade_complete(req.context, ip)
            return httpresponse.ok(req)
        elif actual_raga_version == upgrade_start_version:
            #return httpresponse.data(req, {'status': 'ongoing'})
            # TODO(rushiagr): need to do a better thing here instead of 404 error
            return httpresponse.internal_server_error(req)

    def call_raga_to_start_backup(self, req, body):
        params = {}
        params['backup_id'] = body['backup_id']
        params['volume_id'] = body['cinder_uuid']
        params['access_key'] = CONF.jiocloud.access_key
        params['secret_key'] = CONF.jiocloud.secret_key
        params['compute_url'] = CONF.jiocloud.compute_url
        params['vpc_url'] = CONF.jiocloud.vpc_url
        params['block_time'] = CONF.raga.freeze_block_time

        return self._do_post_request(body['ip'],
                                     body['type'],
                                     '/v1/backup',
                                     json.dumps(params))

    def check_raga_for_backup_status(self, req,
                                     ip,
                                     type,
                                     backup_id):
        response = self._do_get_request(ip,
                                        type,
                                        '/v1/backup/' + backup_id)
        result ={}
        if response.content == '""':
            result['status'] = 'EMPTY'
            result['cinder_snapshot_id'] = None
        else:
            res = json.loads(response.content)
            result['status'] = res['status']
            result['cinder_snapshot_id'] = res['backup_id']
        if result['status'] in ['BACKUP_FAILED', 'BACKUP_SUCCESS']:
            if result['cinder_snapshot_id'] is None:
                result['status'] = 'BACKUP_FAILED_WITH_SNAPSHOT_ID_NULL'
            logging.debug('Print Status: ' + result['status'])
            return httpresponse.data(req, result)

        logging.debug('Getting some other   status: ' + result['status'])
        return httpresponse.internal_server_error(req)

    @utils.requests_exc_to_webob_exc
    def raga_restore_database_on_instance(self, req, body):
        try:
            r = self._do_post_request(body['ip'],
                                      body['type'],
                                      '/v1/crash_recovery')
        except Exception as e:
            logging.debug(e.message)
            return httpresponse.internal_server_error(req)
        return httpresponse.ok(req)

    def wait_for_raga_crash_recovery_done(self, req, ip, type):
        try:
            r = self._do_get_request(ip, type, '/v1/crash_recovery')
            logging.debug(r)
            if r.json()['status'] == 'completed':
                return httpresponse.ok(req)
            else:
                return httpresponse.internal_server_error(req)
        except:
            return httpresponse.internal_server_error(req)

    def set_parameters(self, req, body):
        logging.debug('Parameters being set: {0}'.format(body['parameters']))
        try:
            self._do_post_request(body['ip'],
                                  body['type'],
                                  '/v1/parameters',
                                  json.dumps({'parameters': body['parameters']}))
        except Exception as e:
            logging.debug(e.message)
            return httpresponse.internal_server_error(req)
        return httpresponse.ok(req)

def create_resource():
    controller = GuestAgentController()
    return wsgi.Resource(controller)
