from namo.common import wsgi
from namo.workflowapi.v1 import autobackupcontroller
from namo.workflowapi.v1 import cleanautobkpcontroller 
from namo.workflowapi.v1 import cloud_controller
from namo.workflowapi.v1 import masterworkflowcontroller
from namo.workflowapi.v1 import gacontroller
from namo.workflowapi.v1 import persistence_controller

import routes

class API(wsgi.Router):
    def __init__(self, mapper=None):
        if mapper is None:
            mapper = routes.Mapper()
        super(API, self).__init__(mapper)

        masterworkflow_resource = masterworkflowcontroller.create_resource()
        auto_backup_resource = autobackupcontroller.create_resource()
        clean_auto_bkp_resource = cleanautobkpcontroller.create_resource()
        ga_resource = gacontroller.create_resource()
        persistence_resource = persistence_controller.create_resource()
        cloud_resource = cloud_controller.create_resource()

        # APIs for masterworkflow starts with /masterworkflow/* and for others,
        # it starts with /workflow/*
        mapper.connect('/masterworkflow/checkidempotency',
                       controller=masterworkflow_resource,
                       action='check_idempotency',
                       conditions={'method': ['PUT']})
        mapper.connect('/masterworkflow/find_and_schedule_pending_tasks',
                       controller=masterworkflow_resource,
                       action='find_and_schedule_pending_tasks',
                       conditions={'method': ['GET']})


        mapper.connect('/workflow/db/instances',
                       controller=masterworkflow_resource,
                       action='instance_update_in_db',
                       conditions={'method': ['PUT']})
        mapper.connect('/workflow/nova/instances',
                       controller=masterworkflow_resource,
                       action='create_vm_in_nova',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/nova/deletevm',
                       controller=masterworkflow_resource,
                       action='delete_vm',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/db/keypairs/{local_keypair_id}',
                       controller=masterworkflow_resource,
                       action='keypair_get_from_db',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/nova/keypairs',
                       controller=masterworkflow_resource,
                       action='keypair_create_in_nova',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/nova/keypairs',
                       controller=masterworkflow_resource,
                       action='delete_keypair_nova',
                       conditions={'method': ['DELETE']})
        mapper.connect('/workflow/db/keypairs',
                       controller=masterworkflow_resource,
                       action='keypair_save_in_db',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/db/keypairs',
                       controller=masterworkflow_resource,
                       action='keypair_delete_from_db',
                       conditions={'method': ['DELETE']})
        mapper.connect('/workflow/db/hosts/{instance_id}',
                       controller=masterworkflow_resource,
                       action='host_get_from_db',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/db/hosts',
                       controller=masterworkflow_resource,
                       action='host_save_in_db',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/db/hosts',
                       controller=masterworkflow_resource,
                       action='host_update_in_db',
                       conditions={'method': ['PUT']})
        mapper.connect('/workflow/db/hosts',
                       controller=masterworkflow_resource,
                       action='host_delete_in_db',
                       conditions={'method': ['DELETE']})
        mapper.connect('/workflow/db/volumes/{host_id}',
                       controller=masterworkflow_resource,
                       action='volume_get_from_db',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/db/volumes',
                       controller=masterworkflow_resource,
                       action='volume_save_in_db',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/db/volumes',
                       controller=masterworkflow_resource,
                       action='volume_update_in_db',
                       conditions={'method': ['PUT']})
        mapper.connect('/workflow/db/volumes',
                       controller=masterworkflow_resource,
                       action='volume_delete_in_db',
                       conditions={'method': ['DELETE']})
        mapper.connect('/workflow/nova/attachvolume',
                       controller=masterworkflow_resource,
                       action='attach_volume',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/nova/detachvolume',
                       controller=masterworkflow_resource,
                       action='detach_volume',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/cinder/deletevolume',
                       controller=masterworkflow_resource,
                       action='delete_volume',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/nova/checkvmactive/{nova_vm_id}',
                       controller=masterworkflow_resource,
                       action='check_vm_active_in_nova',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/cinder/checkvolumeavailable/{nova_volume_id}',
                       controller=masterworkflow_resource,
                       action='check_volume_available',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/nova/checkvolumeattached/' +
                       '{nova_volume_id}/{nova_vm_id}',
                       controller=masterworkflow_resource,
                       action='check_volume_attached',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/nova/checkvmdeleted/{nova_vm_id}',
                       controller=masterworkflow_resource,
                       action='check_vm_deleted',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/cinder/checkvolumedelete/{nova_volume_id}',
                       controller=masterworkflow_resource,
                       action='check_volume_deleted',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/checkvolumdeattached/' +
                       '{nova_volume_id}/{nova_vm_id}',
                       controller=masterworkflow_resource,
                       action='check_volume_deattached',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/neutron/floatingips',
                       controller=masterworkflow_resource,
                       action='create_floating_ip',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/nova/floatingips',
                       controller=masterworkflow_resource,
                       action='delete_floating_ip',
                       conditions={'method': ['DELETE']})
        mapper.connect('/workflow/nova/floatingipattach',
                       controller=masterworkflow_resource,
                       action='attach_floating_ip_to_instance',
                       conditions={'method': ['POST']})
        mapper.connect('/workflow/nova/floatingips/{nova_vm_id}/{nova_vm_ip}',
                       controller=masterworkflow_resource,
                       action='check_floating_ip_attached',
                       conditions={'method': ['GET']})
        # TODO(rushiagr): Rethink if it's a good idea to pass IP as part of
        # URL string
        mapper.connect('/workflow/raga/checkactive/{ip}/{type}',
                       controller=ga_resource,
                       action='check_ga_active',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/dbstatus/',
                       controller=ga_resource,
                       action='ga_service_action',
                       conditions={'method': ['POST']})
        # TODO(rushiagr): fix volumeformat api inconsistency: one has IP one
        # doesn't.
        mapper.connect('/workflow/raga/volumeformat/{ip}/{type}',
                       controller=ga_resource,
                       action='get_format_status',
                       conditions={'method': ['GET']})
        mapper.connect('/workflow/raga/volumeformat',
                       controller=ga_resource,
                       action='format_volume',
                       conditions={'method': ['POST']})
        mapper.connect(
                '/workflow/raga/upgrade/{ip}/{type}',
                controller=ga_resource,
                action='upgrade_raga',
                conditions={'method': ['POST']})
        mapper.connect(
                '/workflow/raga/upgrade/{ip}/{type}',
                controller=ga_resource,
                action='get_upgrade_raga_status',
                conditions={'method': ['GET']})
        mapper.connect('/workflow/raga/set_port',
                       controller=ga_resource,
                       action='set_port',
                       conditions={'method':['PUT']})
        mapper.connect('/workflow/raga/create_master_user',
                       controller=ga_resource,
                       action='create_master_user',
                       conditions={'method': ['POST']})

        # TODO(rushiagr): move all above mappings to below way

        mappings = {
            'raga': [
                # TODO(rushiagr): better idea to have '{ip}' before 'health',
                # 'backups' etc in the URL? We'll anyway require '{ip}' in all
                # raga calls. Same about '{type}'?
                # NOTE: 'type' is either 'primary' or 'secondary'
                ['GET', '/health/{ip}/{type}', 'check_raga_health'],
                ['GET', '/check_instance_service_active/{ip}/{type}', 'check_instance_service_active'],
                ['POST', '/backups',
                    'call_raga_to_start_backup'],
                ['GET', '/backups/{ip}/{type}/{backup_id}',
                    'check_raga_for_backup_status'],
                ['POST', '/crash_recovery', 'raga_restore_database_on_instance'],
                ['GET', '/crash_recovery/{ip}/{type}', 'wait_for_raga_crash_recovery_done'],
                ['POST', '/parameters', 'set_parameters'],
                ['POST', '/start_log_tasks', 'start_log_tasks']
                ],
            'db': [
                ['PUT', '/backups/final', 'mark_final_backup_completed'],
                ['PUT', '/backups', 'backup_mark_completed'],
                ['DELETE', '/backups', 'backup_mark_deletion_completed'],
                ['PUT', '/backups/tries', 'backup_increment_tries'],
                ['PUT', '/restore', 'restore_mark_completed'],
                ['POST', '/volume_snapshots', 'create_volume_snapshot_in_db'],
                ['DELETE', '/volume_snapshots/{backup_id}', 'delete_volume_snapshot_in_db'],
                ['GET', '/volume_snapshots/{backup_id}', 'get_cinder_snapshot_id_by_backup_id'],
                ['PUT', '/instances/endpoint/bucketid', 'populate_endpoint_bucketid_in_db'],
                ['GET', '/default_parameters/{instance_id}', 'get_default_parameters'],
                ['GET', '/default_parameters_for_scale_compute/{instance_id}/{target_nova_flavor_id}', 'get_changed_parameters_for_scale_compute'],
                ['GET', '/get_bucket_details/{owner_id}', 'get_bucket_details'],
                ['POST', '/buckets', 'save_bucket_info_in_db'],
                ['DELETE', '/buckets', 'delete_bucket_in_db']
                ],
            'cloud': [
                ['DELETE', '/snapshots/{cinder_snapshot_id}', 'delete_snapshot_in_cinder'],
                ['POST', '/snapshots/{sbs_volume_id}', 'take_snapshot_in_sbs'],
                ['GET', '/snapshots/created/{cinder_snapshot_id}', 'get_snapshot_create_status'],
                ['GET', '/snapshots/deleted/{cinder_snapshot_id}', 'check_snapshot_deleted'],
                ['POST', '/volumes', 'volume_create_in_cinder'],
                ['POST', '/volumes/restore', 'volume_restore_in_cinder'],
                ['GET', '/servers/boot_volume_id/{nova_vm_id}', 'get_boot_volume_id_for_instance'],
                ['GET', '/vpc/get_association_id/{nova_vm_ip_id}', 'get_association_id'],
                ['POST', '/vpc/disassociate_vm_address', 'disassociate_vm_address'],
                ['POST', '/create_bucket', 'create_bucket'],
                ['GET', '/check_bucket/{bucket_name}', 'check_bucket_created'],
                ['GET', '/check_bucket_exist/{bucket_name}', 'check_bucket_exist'],
                ['POST', '/set_bucket_permission', 'set_bucket_permission'],
                ['DELETE', '/delete_bucket_prefix', 'delete_bucket_prefix']
                ],
            'mwf': [
                ['PUT', '/set_execution_id_null_for_mfw', 'set_execution_id_null_for_mfw'],
                ['POST', '/start_new_mfw', 'start_new_mfw'],
                ['POST', '/backups/restart', 'start_new_backup_workflow']
                ],
            'autobkp': [
                ['PUT', '/check_idempotency', 'check_idempotency'],
                ['PUT', '/set_execution_id_null', 'set_execution_id_null'],
                ['POST', '/find_and_schedule', 'find_and_schedule_auto_backups'],
                ['POST', '/start_new_workflow', 'start_new_infinite_workflow'],
                ['POST', '/redo_find_and_schedule', 'redo_find_and_schedule'],
                ],
            'cleanautobkp': [
                ['PUT', '/check_idempotency', 'check_idempotency'],
                ['PUT', '/set_execution_id_null', 'set_execution_id_null'],
                ['POST', '/find_and_schedule', 'find_and_schedule_clean_auto_bkps'],
                ['POST', '/start_new_workflow', 'start_new_infinite_workflow'],
                ['POST', '/redo_find_and_schedule', 'redo_find_and_schedule'],
                ],
            }

        controllers = {
                'raga': ga_resource,
                'db': persistence_resource,
                'cinder': masterworkflow_resource,
                'cloud': cloud_resource,
                'mwf': masterworkflow_resource,
                'autobkp': auto_backup_resource,
                'cleanautobkp': clean_auto_bkp_resource,
                }

        for key in mappings:
            for entry in mappings[key]:
                mapper.connect(
                    '/workflow/' + key + entry[1],
                    controller=controllers[key],
                    action=entry[2],
                    conditions={'method': [entry[0]]}
                    )

        reject_method_resource = wsgi.Resource(wsgi.RejectMethodController())
