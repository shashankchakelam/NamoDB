import uuid
from infworkflowcontroller import InfiniteWorkflowController
from namo import i18n
from namo.common import exception
from namo.common import wsgi
from namo.common import httpresponse
from namo.common import utils
from namo.db.mysqlalchemy import api
from namo.db.mysqlalchemy import models
from namo.openclient import session
from oslo_config import cfg
from oslo_utils import timeutils
import re

import logging

CONF = cfg.CONF
_ = i18n._
_LW = i18n._LW

class AutoBackupWorkflowController(InfiniteWorkflowController):

    def __init__(self):
        self.workflow_type = "AUTO_BACKUP"
        self.workflow_execution = "autobackup.scheduletasks"

    @utils.exc_to_webob_exc
    def find_and_schedule_auto_backups(self, req):
        logging.info('For %s, find_and_schedule_auto_backups'%(self.workflow_type))
        logging.info('For %s, Trying to create backup entry for instances in their backup window'%(self.workflow_type))

        to_blacklist_instance_ids = api.find_instance_ids_with_recent_auto_backups(time_hours=1)
        black_list_ids = []
        for i in to_blacklist_instance_ids:
            black_list_ids.append(i.instance_id)

        logging.info('For %s, The snapshot has been recently taken for following instances : %s. So Not considering for backup now'%(self.workflow_type, str(black_list_ids)))

        I = models.Instance
        session = api.get_session()
        with session.begin(subtransactions=True):

            update_q = api.eligible_instances_in_backup_window_query(black_list_ids, session)
            # Doing this to lock the table rows 
            instances = update_q.with_for_update().all()
            logging.info('For %s, Trying to create backup entry for total %s instances'%(self.workflow_type, len(instances)))
            self._create_backup_entries(instances, session)

        logging.info('For %s, Trying to create backup entry for previously missed instances (not in their window)'%(self.workflow_type))

        session = api.get_session()
        with session.begin(subtransactions=True):

            missing_q = api.missing_auto_backups_query()
            miss_instances = missing_q.with_for_update().all()
            self._create_backup_entries(miss_instances, session)

        logging.info('For %s, Successfully created backup entries !' %(self.workflow_type))
        #logging.debug('find_and_schedule_pending_tasks was successful but throwing bad req error')
        #return httpresponse.not_acceptable(req)
        return httpresponse.ok(req)

    def _get_snapshot_name(self, instance):
        '''
            Based on the snapshot name validation in apiwebservice. 
            (See : apiwebservice.validators.apis.validate_snapshot_identifier)
        '''
        snapshot_name = "automated-" + str(instance.owner_id) + "-" + str(instance.name) + "-" + str(timeutils.utcnow()) 
        snapshot_name = snapshot_name.replace(" ", "")
        snapshot_name = snapshot_name.replace(":", "-")
        snapshot_name = snapshot_name.replace(".", "-")
        snapshot_name = re.sub("(-)+", '-', snapshot_name) 
        snapshot_name = snapshot_name.lower() 
        return str(snapshot_name[:254])
        

    def _create_backup_entries(self, instances, session=None): 
        if instances == None:
            return

        session = session or api.get_session()
        for instance in instances:
            snapshot_name = self._get_snapshot_name(instance)

            logging.info('For %s, Creating snapshot entry for instance_id : %s with snapshot name: %s'%(self.workflow_type, str(instance.id), snapshot_name))

            try:
                with session.begin(subtransactions=True):
            
                    api.backup_create_from_instance('', 'AUTOMATED', instance, snapshot_name, session)
                    api.lccs_conditional_update(
                        None,
                        models.Instance,
                        instance['id'],
                        'BACKUP',
                        'PENDING',
                        'ACTIVE',
                        'NONE',
                        session=session)
            except exception.DbConditionalUpdateFailed as e1:
                # TODO delete / rollback the entry created in backup
                logging.exception('For automated snapshot of instance id %s , updating\
                instance db to BACKUP/PENDING failed. Eating the exception ' %(instance.id))
            except Exception as e2:
                # TODO delete / rollback the entry created in backup
                logging.exception('For automated snapshot of instance id %s , updating\
                instance db to BACKUP/PENDING failed with unknown exception. Eating the exception ' %(instance.id))

def create_resource():
    controller = AutoBackupWorkflowController()
    return wsgi.Resource(controller)
