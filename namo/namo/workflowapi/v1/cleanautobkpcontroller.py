import uuid
from datetime import timedelta
from infworkflowcontroller import InfiniteWorkflowController
from namo import i18n
from namo.common import exception
from namo.common import wsgi
from namo.common import httpresponse
from namo.common import utils
from namo.db.mysqlalchemy import api
from namo.db.mysqlalchemy import models
from namo.openclient import session
from oslo_config import cfg
from oslo_utils import timeutils
import re

import logging

CONF = cfg.CONF
_ = i18n._
_LW = i18n._LW

class CleanAutoBkpController(InfiniteWorkflowController):

    def __init__(self):
        self.workflow_type = "CLEAN_AUTO_BACKUP"
        self.workflow_execution = "cleanautobackup.scheduletasks"

    @utils.exc_to_webob_exc
    def find_and_schedule_clean_auto_bkps(self, req):
        logging.info('For %s, find_and_schedule_clean_auto_backups'%(self.workflow_type))
        session = api.get_session()
        with session.begin(subtransactions=True):
            query_result = api.get_available_backups_per_instance(session=session, bkp_type='AUTOMATED')
            to_delete_bkp_ids = self._find_to_del_bkp_ids(query_result)
            self._create_delete_bkp_entries(session, to_delete_bkp_ids)
        
        logging.info('For %s, Updated the backup entry for Automated backups to be deleted'%(self.workflow_type))
        return httpresponse.ok(req)

    def _find_to_del_bkp_ids(self, query_result):
        '''
            Assumption: query_result has format - list <instance_id, backup_retention_days, concat(backup_created_at,'##',backup_id)>
            @returns : list of to be deleted backup ids
        '''
        to_del_bkp_ids = []
        if query_result is None:
            return to_del_bkp_ids 
        for item in query_result:
            bkp_retention_days = item[1]
            created_id_list = item[2].split(',')
            created_id_map = {}
            for created_id in created_id_list:
                i = created_id.split('##')
                created_id_map[i[0]] = i[1]

            sorted_list = sorted(created_id_map) # Sorts in Acending order of created_at
            len_sorted_list = len(sorted_list)
            if len_sorted_list <= 1:
                continue
            threshold_time = str(timeutils.utcnow() - timedelta(days=(bkp_retention_days)))
            # Never consider the latest backup for deletion. This is primarily to cater to use case where autobacup could not be taken because of system faults
            to_check_delete_list = sorted_list[:(len_sorted_list-1)]
            
            # only mark those for deletion whose creation time is older than the retention_days
            for t in to_check_delete_list:
                if t > threshold_time:
                    continue
                to_del_bkp_ids.append(created_id_map[t])
        logging.info('For %s, Following backup ids will be deleted : %s'%(self.workflow_type, str(to_del_bkp_ids)))
        return to_del_bkp_ids 
        
    def _create_delete_bkp_entries(self, session, to_delete_bkp_ids):
        if to_delete_bkp_ids is None:
            return
        for bkp_id in to_delete_bkp_ids:
            try:
                 api.lccs_conditional_update(
                        None,
                        models.Backup,
                        bkp_id,
                        'DELETING',
                        'PENDING',
                        'ACTIVE',
                        'NONE',
                        session)
            except exception.DbConditionalUpdateFailed:
                logging.exception("For %s, Unable to create delete backup entry for backup id : %s"%(self.workflow_type, str(bkp_id)))
                pass
 

def create_resource():
    controller = CleanAutoBkpController()
    return wsgi.Resource(controller)
