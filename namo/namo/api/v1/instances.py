__author__ = 'adarshkoyya'

import copy
import namo.schema
from namo.common import wsgi

import httplib
import json
from webob import Response
from webob.exc import HTTPBadRequest

from namo import i18n
from namo.api.v1 import controller
from namo.db import api

_ = i18n._
_LW = i18n._LW

class InstaceController(controller.BaseController):
    def _getResponse(self, name, item, req):
        response = Response(request=req,
                                  status=httplib.OK,
                                  content_type='application/json')
        response.body = json.dumps({name: item.to_dict()}, sort_keys=True, indent=4)#, separators=(',', ': '))
        return response

    def _getResponseArray(self, name, items, req):
        response = Response(request=req,
                                  status=httplib.OK,
                                  content_type='application/json')
        itemArray = []
        for item in items:
            itemArray.append(item.to_dict())
        response.body = json.dumps({name: itemArray}, sort_keys=True, indent=4, separators=(',', ': '))
        return response

    def create(self, req, owner_id, body):
        # NOTE(rushiagr): assumption is body will contain 'engine' as one key,
        # whose supported value is 'mysql' as of now. Optional:
        # 'engine_version' key with value '5.6' are the only values which are
        # supported right now
        context = req.context
        instance = body
        try:
            # TODO(rushiagr): rename 'api' to 'db' as it's db api
            artefact_id = api.get_artefact_info(context, body['engine'],
                    body.get('engine_version'))
            instance['artefact_id'] = artefact_id
            item = api.instance_create(context, owner_id, instance)
            return self._getResponse('instance', item, req)
        # TODO(rushiagr): raise specific errors and nice human-readable error
        # messages. E.g. if database is not present, we should say so in the
        # error message
        except Exception as e:
            raise HTTPBadRequest(explanation=e, content_type="text/plain")

    def get(self, req, owner_id, name):
        context = req.context
        try:
            item = api.instance_get(context, owner_id, name)
            return self._getResponse('instance', item, req)
        except Exception as e:
            raise HTTPBadRequest(explanation=e, content_type="text/plain")

    def getall(self, req, owner_id):
        context = req.context
        try:
            items = api.instance_get_all(context, owner_id)
            return self._getResponseArray('instances', items, req)
        except Exception as e:
            raise HTTPBadRequest(explanation=e, content_type="text/plain")

    def delete(self, req, owner_id, name):
        """
        Deletes the instance and all its chunks from the Namo

        :param req: The WSGI/Webob Request object
        :param id: The opaque instance identifier

        :raises HttpBadRequest if image registry is invalid
        :raises HttpNotFound if image or any chunk is not available
        :raises HttpUnauthorized if image or any chunk is not
                deleteable by the requesting user
        """
        context = req.context
        try:
            api.instance_destroy(context, owner_id, name)
            return Response(body='', status=200)
        except Exception as e:
            raise HTTPBadRequest(explanation=e.msg, content_type="text/plain")

"""
    "lifecycle": {
        "description": _("The type of instance represented by this content"),
        "enum": [
            "ACTIVE",
            "CREATING",
            "DELETING"
        ],
        "type": "string"
    },
    "changestate": {
        "descriptiaon": _("The current status of this instance visible only to developers"),
        "enum": [
            "NONE",
            "PENDING",
            "APPLYING"
        ],
        "type": "string"
    },
"""

_INSTANCE_SCHEMA = {
    "id": {
        "description": _("An identifier for the instance"),
        "type": "integer"
    },
    "state": {
        "descriptiaon": _("The current status of this instance"),
        "enum": [
            "CREATING",
            "ACTIVE",
            "DELETING"
        ],
        "type": "string"
    },
    "name": {
        "description": _("The name of the data base instance"),
        "type": "string",
    },
    "owner_id": {
        "description": _("The owner id details"),
        "type": "string",
    },
    "storagesize": {
        "description": _("Storage Size Details"),
        "type": "integer",
    },
    'self': {'type': 'string'},
    'schema': {'type': 'string'}
}

def get_instance_schema():
    properties = copy.deepcopy(_INSTANCE_SCHEMA)
    schema = namo.schema.Schema('instance', properties)
    return schema


def _get_partial_instance_schema():
    properties = copy.deepcopy(_INSTANCE_SCHEMA)
    hide_properties = ['lifecycle', 'changestate']
    for key in hide_properties:
        del properties[key]
    schema = namo.schema.Schema('instance', properties)
    return schema


def get_collection_schema():
    instance_schema = get_instance_schema()
    return namo.schema.CollectionSchema('instances', instance_schema)

def create_resource():
    """Instance resource factory method"""
    #instance_schema = get_instance_schema()
    #partial_instance_schema = _get_partial_instance_schema()
    #deserializer = RequestDeserializer(instance_schema)
    #serializer = ResponseSerializer(instance_schema, partial_instance_schema)
    controller = InstaceController()
    return wsgi.Resource(controller)#, deserializer, serializer)
