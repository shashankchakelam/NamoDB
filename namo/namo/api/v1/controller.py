# Copyright 2011 OpenStack Foundation
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_log import log as logging
import webob.exc

from namo.common import exception
from namo import i18n

LOG = logging.getLogger(__name__)
_ = i18n._

#We Can Delete this file or write some basic common function for Controllers
class BaseController(object):
    def get_dummy(self):
        print "This method is doing nothing will never be called"

    """
    def get_instance_meta_or_404(self, request, instance_id):
        context = request.context
        try:
            return webob.exc.HTTPOk("Adarsh",
                                    request=request)
        except exception.NotFound:
            msg = "Instance with identifier %s not found" % instance_id
            LOG.debug(msg)
            raise webob.exc.HTTPNotFound(
                msg, request=request, content_type='text/plain')
        except exception.Forbidden:
            msg = "Forbidden image access"
            LOG.debug(msg)
            raise webob.exc.HTTPForbidden(msg,
                                          request=request,
                                          content_type='text/plain')
    """
