from namo.api.v1 import instances
from namo.common import wsgi


class Controller(object):

    def __init__(self):
        self.instance_schema = instances.get_instance_schema()
        self.instance_collection_schema = instances.get_collection_schema()

    def instance(self, req):
        return self.instance_schema.minimal()

    def instances(self, req):
        return self.instance_collection_schema.minimal()



def create_resource():
    controller = Controller()
    return wsgi.Resource(controller)