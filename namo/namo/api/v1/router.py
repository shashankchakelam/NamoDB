__author__ = 'adarshkoyya'
# Copyright 2011 OpenStack Foundation
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


from namo.common import wsgi
from namo.api.v1 import instances
from namo.api.v1 import schemas
import routes

class API(wsgi.Router):

    def __init__(self, mapper=None):
        if(mapper == None): # create mapper
            mapper = routes.Mapper()
        super(API,self).__init__(mapper)

        schemas_resource = schemas.create_resource()
        instance_resource = instances.create_resource()
        reject_method_resource = wsgi.Resource(wsgi.RejectMethodController())

        # TODO(rushiagr): the following methods doesn't look like are being
        # used, so I commented them out. Remove the commented lines in future
        # mapper.connect('/schemas/instance',
        #                controller=schemas_resource,
        #                action='instance',
        #                conditions={'method': ['GET']})
        # mapper.connect('/schemas/instance',
        #                controller=reject_method_resource,
        #                action='reject',
        #                allowed_methods='GET',
        #                conditions={'method': ['POST', 'PUT', 'DELETE',
        #                                       'PATCH', 'HEAD']})
        #
        # mapper.connect('/schemas/instances',
        #                controller=schemas_resource,
        #                action='instances',
        #                conditions={'method': ['GET']})
        # mapper.connect('/schemas/instances',
        #                controller=reject_method_resource,
        #                action='reject',
        #                allowed_methods='GET',
        #                conditions={'method': ['POST', 'PUT', 'DELETE',
        #                                       'PATCH', 'HEAD']})


        mapper.connect('/{owner_id}/instances',
                       controller=instance_resource,
                       action='create',
                       conditions={'method': ['POST']})

        mapper.connect('/{owner_id}/instances',
                       controller=instance_resource,
                       action='getall',
                       conditions={'method': ['GET']})

        mapper.connect('/{owner_id}/instances',
                       controller=reject_method_resource,
                       action='reject',
                       allowed_methods='GET, POST',
                       conditions={'method': ['PUT', 'DELETE', 'PATCH',
                                              'HEAD']})

        mapper.connect('/{owner_id}/instances/{name}',
                       controller=instance_resource,
                       action='get',
                       conditions={'method': ['GET']})

        mapper.connect('/{owner_id}/instances/{name}',
                       controller=instance_resource,
                       action='delete',
                       conditions={'method': ['DELETE']})

        mapper.connect('/{owner_id}/instances/{name}',
                       controller=reject_method_resource,
                       action='reject',
                       allowed_methods='GET, DELETE',
                       conditions={'method': ['POST', 'PUT', 'PATCH',
                                              'HEAD']})
