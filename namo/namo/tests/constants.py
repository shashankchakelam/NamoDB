"""
File that contains all constants used by namo tests
"""
# Backup Window values
test_backup_window_start = '12:15'
test_backup_window_end = '12:50'

# Maintenance Window values
test_maintenance_window_start_day = 'SUN'
test_maintenance_window_start_time = '23:31'
test_maintenance_window_end_day = 'MON'
test_maintenance_window_end_time = '00:06'

# MasterUser Name and Password
test_master_username = 'shank1234567890q'
test_master_user_password = 'abcde12345'
test_port = 3306
test_log_retention_period = 5
