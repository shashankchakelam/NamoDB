import unittest
from namo.tests.unit.workflowapi.v1 import test_masterworkflow
from namo.tests.unit import conf_fixture
from namo.db.mysqlalchemy import models

conf_fixture.set_defaults()
models.register_models()

workflow = test_masterworkflow.TestWorkflowapi()

class TestApiMethods(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print 'Started running workflow tests setUpClass() method'
        workflow.create_base_objects()
        print 'Finished running workflow tests setUpClass() method'

    @classmethod
    def tearDownClass(cls):
        print 'Started running workflow tests tearDownClass() method'
        workflow.delete_all_base_objects()
        print 'Finished running workflow tests tearDownClass() method'

    def test_save_keypair_local(self):
        id1, id2 = workflow.save_keypair_local()
        self.assertEqual(id1, id2)


    def test_get_keypair_local(self):
        id1, id2 = workflow.save_keypair_local()
        self.assertEqual(id1, id2)

    def test_delete_keypair_local(self):
        self.assertEqual('OK', workflow.delete_keypair_local())


    def test_create_host_local(self):
        id1, id2 = workflow.create_host_local()
        self.assertEqual(id1, id2)

    def test_get_host_local(self):
        id1, id2 = workflow.create_host_local()
        self.assertEqual(id1, id2)

    def test_delete_host_local(self):
        self.assertEqual('OK', workflow.delete_host_local())

if __name__ == '__main__':
    unittest.main()
