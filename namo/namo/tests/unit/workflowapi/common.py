import uuid
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models
from namo.tests import constants
from random import randint

def set_up_class_helper():
    data = {}
    nova_keypair_id = uuid.uuid4()
    instance_name = str(uuid.uuid4())
    vm_ip = str(randint(0,255)) + '.' \
            + str(randint(0,255)) + '.' \
            + str(randint(0,255)) + '.' \
             + str(randint(0,255))
    data['keypair'] = db.keypair_create('', nova_keypair_id, 'privatekey')
    data['flavor'] = db.nova_flavor_get('', 'c1.4xlarge')
    data['instance'] = db\
        .instance_create_entry_from_user_supplied_values(
            '',
            randint(0,999999999),
            'mysql',
            '5.6',
            instance_name,
            'c1.4xlarge',
            20,
            constants.test_master_username,
            constants.test_master_user_password,
            constants.test_backup_window_start,
            constants.test_backup_window_end,
            constants.test_maintenance_window_start_day,
            constants.test_maintenance_window_start_time,
            constants.test_maintenance_window_end_day,
            constants.test_maintenance_window_end_time,
            constants.test_port,
            constants.test_log_retention_period)
    data['instance_artefact'] = [artefact for artefact in db._get_all('',
        models.Artefact) if artefact.id == data['instance'].artefact_id][0]

    # TODO(rushiagr): better way to pass 'host' to test methods, than to
    # attach it to class object
    data['host'] = db.host_create('', data['instance'].id,
                                  data['keypair'].id, '', '',
                                  data['flavor'].id,
                                  vm_ip,  # Ip should be unique
                                  '',
                                  data['instance'].artefact_id)
    return data

def tear_down_class_helper(data):
    db._hard_delete_by_id('', models.Host, data['host'].id)
    db._hard_delete_by_id('', models.Instance, data['instance'].id)
    db._hard_delete_by_id('', models.Keypair, data['keypair'].id)
    pass


def create_test_instance(**kwargs):
    """
    kwargs can be key-values you want to set on the instance db object.

    E.g. lifecycle='RESTORING' will make instance's lifecycle 'RESTORING'
    """

    instance = db.instance_create_entry_from_user_supplied_values(
            '',
            randint(0,999999999),
            'mysql',
            '5.6',
            uuid.uuid4().hex,
            'c1.4xlarge',
            20,
            constants.test_master_username,
            constants.test_master_user_password,
            constants.test_backup_window_start,
            constants.test_backup_window_end,
            constants.test_maintenance_window_start_day,
            constants.test_maintenance_window_start_time,
            constants.test_maintenance_window_end_day,
            constants.test_maintenance_window_end_time,
            constants.test_port,
            constants.test_log_retention_period)

    if not kwargs:
        return instance
    return db._update_by_db_object(instance, **kwargs)

def create_test_backup(instance, **kwargs):
    """
    kwargs can be key-values you want to set on the backup instance.

    E.g. lifecycle='RESTORING' will make backup's lifecycle 'RESTORING'
    """

    backup = db.backup_create_from_instance('', 'AUTOMATED', instance,
            uuid.uuid4().hex)
    if not kwargs:
        return backup
    return db._update_by_db_object(backup, **kwargs)

def create_test_bucket(body):
    name = body['bucket_name']
    state = body['state']
    owner_id = body['customer_id']
    attached_policy_id = body['policy_id']

    bucket = db.bucket_create_entry('', name, owner_id, attached_policy_id, state)
    return bucket


def create_test_host(instance, **kwargs):
    """
    kwargs can be key-values you want to set on the instance db object.

    E.g. lifecycle='RESTORING' will make instance's lifecycle 'RESTORING'
    """
    keypair = db._get_all('', models.Keypair)[0]
    flavor = db._get_all('', models.Flavor)[0]
    host = db.host_create('', instance.id, keypair.id, 'az', 'nova-uuid',
            flavor.id, ('ip-1.'+uuid.uuid4().hex)[:19], 'ip-uuid', instance.artefact_id)
    if not kwargs:
        return host
    return db._update_by_db_object(host, **kwargs)


def create_test_volume(host, **kwargs):
    """
    kwargs can be key-values you want to set on the instance db object.

    E.g. lifecycle='RESTORING' will make instance's lifecycle 'RESTORING'
    """
    volume = db.volume_create('', 'cinder-'+uuid.uuid4().hex[:10], host.id,
    'voltype', 'NOT ATTACHED')
    if not kwargs:
        return volume
    return db._update_by_db_object(volume, **kwargs)

def print_backup_info(backup):
    print '+++++++++++++++++++++++'
    print "Backup name: " + backup.name
    print "Backup id: " + str(backup.id)
    print "Backup instance id: " + str(backup.instance_id)
    print "Backup lifecycle: " + backup.lifecycle
    print "Backup changestate: " + backup.changestate
    print '+++++++++++++++++++++++'

def print_instance_info(instance):
    print '+++++++++++++++++++++++'
    print "Instance name: " + instance.name
    print "Instance id: " + str(instance.id)
    print "Instance lifecycle: " + instance.lifecycle
    print "Instance changestate: " + instance.changestate
    print '+++++++++++++++++++++++'
