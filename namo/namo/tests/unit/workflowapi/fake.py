import json
import uuid

class Request:
    def __init__(self):
        self.context = ''


class Response:
    def __init__(self, status):
        self.val = status

    def json(self):
        status = {}
        status['status'] = self.val
        return status

class Mistral:
    def __init__(self):
        self.id = str(uuid.uuid4())

class ResOk:
    def __init__(self):
        self.status_code = 200
        self.content = 'Testing'

class Res:
    def __init__(self, content):
        self.content = content

class Keypair:
    def __init__(self):
        self.name = 'Test'
        self.private_key = 'Test_private_key'

class VM:
    def __init__(self):
        self.id = str(uuid.uuid4())
        self.networks = {}
        self.networks['private'] = ['192.0.0.1']

class Volume:
    def __init__(self):
        self.id = str(uuid.uuid4())
        self.volume_id = self.id
        self.volume_type = 'Test'
        self._info = {}
        server = {}
        server['server_id'] = 1
        self._info['attachments'] = [server]

class Image:
    def __init__(self):
        self.id = 0

class SG:
    def __init__(self):
        self.human_id = 'Fake'

class Server:
    def create(self, name, image, flavor, meta=None, files=None,
               reservation_id=None, min_count=None,
               max_count=None, security_groups=None, userdata=None,
               key_name=None, availability_zone=None,
               block_device_mapping=None, block_device_mapping_v2=None,
               nics=None, scheduler_hints=None,
               config_drive=None, disk_config=None, **kwargs):
            return VM()

class Client:
    def __init__(self):
            self.servers = Server()


def create_dict(dictionary):
    return Res(json.dumps(dictionary))


def format_post(url, data):
    return ""

def request_post_error(url, data):
    raise Exception('Internal Server Error')

def format_post_error(url):
    raise Exception('Internal Server Error')

def image(name):
    return Image()

def security_group():
    return  SG()

def nova_client():
    return Client()

def reset_globals():
    return ""

def server():
    return Server()

def delete(fakeself, id):
    return ""

def findall_success(fakeself, id, status):
    return [VM()]

def findall_error(fakeself, id, status):
    return []

def findall_deleted_error(fakeself, id):
    return [VM()]

def findall_deleted_sucess(fakeself, id):
    return []

def findall_attach_sucess(fakeself, id, status):
    return [Volume()]

def findall_attach_error(fakeself, id, status):
    return []

def attach_volume(fakself, vmid, volumeid, device):
    return ""

def detach_volume(fakself, vmid, volumeid):
    return ""

def create(meta=None, files=None,
                reservation_id=None, min_count=None,
                max_count=None, security_groups=None, userdata=None,
                key_name=None, availability_zone=None,
                block_device_mapping=None, block_device_mapping_v2=None,
                nics=None, scheduler_hints=None,
                config_drive=None, disk_config=None, **kwargs):
    return VM()

def volume_create(fakeself, size, name,
                       description, availability_zone):
    return Volume()

def create_keypair(fakeself, keyid):
    return Keypair()

def delete_keypair(fakself, keyid):
    return ""


def get_auth_token_and_tenant_id_from_keystone(params):
    params['authtoken'] = 'auth_token'
    params['tenant_id'] = 'adarsh'