import json
from random import randint
import unittest
import uuid
import mistralclient
import mock
from mock import patch
from namo.common import exception
from namo.db.mysqlalchemy import api
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models
from namo.openclient import session
from namo.tests import constants
from namo.tests.unit.workflowapi import fake
from namo.tests.unit.workflowapi import common
from namo.workflowapi.v1 import autobackupcontroller 

from oslo_config import cfg
CONF = cfg.CONF

class FakeExecution():
    def __init__(self, id):
        self.id = id

class AutoBkpWorkflowAPITestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.data = common.set_up_class_helper()

    @classmethod
    def tearDownClass(cls):
        common.tear_down_class_helper(cls.data)

    def setUp(self):
        self.bkp = autobackupcontroller.AutoBackupWorkflowController()
        self.request = mock.Mock()
        CONF.workflow.workflow_url = 'workflow://url'
        CONF.workflow.get_backup_batch_size = 20
        CONF.workflow.get_instance_batch_size = 20

    def test_check_idempotency(self):
        autobkp = api.infinite_workflow_get('AUTO_BACKUP')
        data = {}
        if autobkp.execution_id:
            print autobkp.execution_id
            data['execution_id'] = autobkp.execution_id
            self.bkp.set_execution_id_null(self.request, data)
        data['execution_id'] = str(uuid.uuid4())
        resp = self.bkp.check_idempotency(self.request, data)
        result = json.loads(resp.body)
        self.assertEqual(result['acquired_lock'], 1)
        autobkp = api.infinite_workflow_get('AUTO_BACKUP')
        self.assertEqual(data['execution_id'], autobkp.execution_id)

        # Second time it should return 0
        data['execution_id'] = str(uuid.uuid4())
        resp = self.bkp.check_idempotency(self.request, data)
        result = json.loads(resp.body)
        self.assertEqual(result['acquired_lock'], 0)

    def test_set_execution_id_null(self):
        autobkp = api.infinite_workflow_get('AUTO_BACKUP')
        data = {}
        if autobkp.execution_id is None:
            data['execution_id'] = str(uuid.uuid4())
            resp = self.bkp.check_idempotency(self.request, data)
            result = json.loads(resp.body)
            self.assertEqual(result['acquired_lock'], 1)
        autobkp = api.infinite_workflow_get('AUTO_BACKUP')
        data['execution_id'] = autobkp.execution_id
        self.bkp.set_execution_id_null(self.request, data)
        autobkp = api.infinite_workflow_get('AUTO_BACKUP')
        self.assertEqual(autobkp.execution_id, None)

    @mock.patch('mistralclient.api.v2.executions.ExecutionManager.create')
    def test_start_new_bkp(self, mock_create):
        resp = self.bkp.start_new_infinite_workflow(self.request)
        self.assertEqual(resp.status_code, 200)

