import json
import mock
import unittest
import uuid
import httplib
from namo.tests.unit.workflowapi import fake
from namo.workflowapi.v1 import cloud_controller
from namo.workflowapi.v1 import exceptions as jcs_exc

class OutputResponse():
    
    def __init__(self, content, status_code):
        self.content = content
        self.status_code = status_code

class TestPersistenceController(unittest.TestCase):

    def setUp(self):
        self.controller = cloud_controller.create_resource().controller
        self.mock_req = mock.Mock()
        self.mock_context = self.mock_req.context

    @mock.patch('jcsclient.compute.Controller.delete_snapshot')
    def test_delete_snapshot_in_cinder_seccess(self, mock_delete_snapshot):
        resp = self.controller.delete_snapshot_in_cinder(self.mock_req,
                                                         'some_id')
        self.assertEqual(resp.status_code, 200)

    @mock.patch('jcsclient.compute.Controller.delete_snapshot', side_effect=Exception('Any Error'))
    def test_delete_snapshot_in_cinder_failure(self, mock_delete_snapshot):
        resp = self.controller.delete_snapshot_in_cinder(self.mock_req,
                                                         'some_id')
        self.assertEqual(resp.status_code, 200)

    @mock.patch('jcsclient.compute.Controller.create_snapshot', side_effect=Exception('Any Error'))
    def test_take_snapshot_in_sbs_error_1(self, mock_create_snapshot):
        resp = self.controller.take_snapshot_in_sbs(self.mock_req,
                                                    'some_id')
        self.assertEqual(resp.status_code, 500)

    @mock.patch('jcsclient.compute.Controller.create_snapshot')
    def test_take_snapshot_in_sbs_error_2(self, mock_create_snapshot):
        mock_create_snapshot.return_value = ""
        resp = self.controller.take_snapshot_in_sbs(self.mock_req,
                                                    'some_id')
        self.assertEqual(resp.status_code, 500)

    @mock.patch('jcsclient.compute.Controller.create_snapshot')
    def test_take_snapshot_in_sbs_success(self, mock_create_snapshot):
        resp_dict = {}
        resp_dict['CreateSnapshotResponse'] = {}
        resp_dict['CreateSnapshotResponse']['status'] = 'pending'
        resp_dict['CreateSnapshotResponse']['snapshotId'] = 'snapshot_id'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_create_snapshot.return_value = res
        resp = self.controller.take_snapshot_in_sbs(self.mock_req,
                                                    'some_id')
        self.assertEqual(resp.status_code, 200)
        result = json.loads(resp.body)
        self.assertEqual(result['sbs_snapshot_id'], 'snapshot_id')

    # @mock.patch(jcsclient.compute.Controller.describe_snapshots', side_effect=jcs_exc.HTTP404)
    # def test_check_snapshot_deleted_success(self, mock_delete_snapshot):
    #     snapshot_dict = {}
    #     snapshot_set_dict = {'snapshotSet': None}
    #     snapshot_dict['DescribeSnapshotsResponse'] = snapshot_set_dict
    #     mock_delete_snapshot.return_value = snapshot_dict
    #     resp = self.controller.check_snapshot_deleted(self.mock_req,
    #                                                   'some_id')
    #     assert resp.status_code == 200

    #@mock.patch('cinderclient.v2.volume_backups.VolumeBackupManager.get',
    #            return_value=httpresponse.internal_server_error(''))
    @mock.patch('jcsclient.compute.Controller.describe_snapshots')
    def test_check_snapshot_deleted_failed(self, mock_describe_snapshots):
        resp = self.controller.check_snapshot_deleted(self.mock_req,
                                                      'some_id')
        self.assertEqual(resp.status_code, 500)

    @mock.patch('jcsclient.compute.Controller.describe_snapshots')
    def test_get_snapshot_create_status_pending(self, mock_describe_snapshots):
        resp_dict = {}
        resp_dict['DescribeSnapshotsResponse'] = {}
        resp_dict['DescribeSnapshotsResponse']['snapshotSet'] = {}
        resp_dict['DescribeSnapshotsResponse']['snapshotSet']['item'] ={}
        resp_dict['DescribeSnapshotsResponse']['snapshotSet']['item']['status'] = 'pending'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_snapshots.return_value = res
        resp = self.controller.get_snapshot_create_status(self.mock_req,
                                                          'some_id')
        self.assertEqual(resp.status_code, 500)

    @mock.patch('jcsclient.vpc.Controller.describe_addresses')
    def test_get_association_id(self, mock_get_association_id):
        resp_dict = {}
        resp_dict['DescribeAddressesResponse'] = {}
        resp_dict['DescribeAddressesResponse']['addressesSet'] = {}
        resp_dict['DescribeAddressesResponse']['addressesSet']['item'] ={}
        resp_dict['DescribeAddressesResponse']['addressesSet']['item']['associationId'] = 'test-association-id'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_get_association_id.return_value = res
        resp = self.controller.get_association_id(self.mock_req,
                                                          'some_id')
        self.assertEqual(resp.status_code, 500)
        # result = json.loads(resp.body)
        # self.assertEqual(result['association_id_exists'], 1)
        # self.assertEqual(result['association_id'], 'test-association-id')

    @mock.patch('jcsclient.vpc.Controller.describe_addresses')
    def test_get_association_id_not_exist(self, mock_get_association_id):
        resp_dict = {}
        resp_dict['DescribeAddressesResponse'] = {}
        resp_dict['DescribeAddressesResponse']['addressesSet'] = {}
        resp_dict['DescribeAddressesResponse']['addressesSet']['item'] ={}
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_get_association_id.return_value = res
        resp = self.controller.get_association_id(self.mock_req,
                                                  'some_id')
        self.assertEqual(resp.status_code, 500)
        # result = json.loads(resp.body)
        # self.assertEqual(result['association_id_exists'], 0)
        # self.assertEqual(result['association_id'], 'not-exist')

    @mock.patch('jcsclient.vpc.Controller.describe_addresses', side_effect=jcs_exc.HTTP400)
    def test_get_association_id_exception(self, mock_get_association_id):
        resp = self.controller.get_association_id(self.mock_req,
                                                  'some_id')
        self.assertEqual(resp.status_code, 200)
        result = json.loads(resp.body)

        self.assertEqual(result['association_id_exists'], 0)
        self.assertEqual(result['association_id'], 'not-exist')

    @mock.patch('jcsclient.vpc.Controller.disassociate_address')
    def test_disassociate_vm_address_success(self, mock_disassociate_vm_address):
        body = {}
        body['association_id'] = 'eipassoc-test'
        resp = self.controller.disassociate_vm_address(self.mock_req, body)
        self.assertEqual(resp.status_code, 500)

    # @mock.patch('jcsclient.vpc.Controller.disassociate_address', side_effect=Exception('Any Error'))
    # def test_disassociate_vm_address_failure(self, mock_disassociate_vm_address):
    #     body = {}
    #     body['association_id'] = 'eipassoc-test'
    #     resp = self.controller.disassociate_vm_address(self.mock_req, body)
    #     self.assertEqual(resp.status_code, 500)

    # @mock.patch('jcsclient.vpc.Controller.disassociate_address', side_effect=jcs_exc.HTTP400)
    # def test_disassociate_vm_address_notfound(self, mock_disassociate_vm_address):
    #     body = {}
    #     body['association_id'] = 'eipassoc-test'
    #     resp = self.controller.disassociate_vm_address(self.mock_req, body)
    #     self.assertEqual(resp.status_code, 404)

    @mock.patch('jcsclient.compute.Controller.describe_snapshots')
    def test_get_snapshot_create_status_completed(self, mock_describe_snapshots):
        resp_dict = {}
        resp_dict['DescribeSnapshotsResponse'] = {}
        resp_dict['DescribeSnapshotsResponse']['snapshotSet'] = {}
        resp_dict['DescribeSnapshotsResponse']['snapshotSet']['item'] ={}
        resp_dict['DescribeSnapshotsResponse']['snapshotSet']['item']['status'] = 'completed'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_snapshots.return_value = res
        resp = self.controller.get_snapshot_create_status(self.mock_req,
                                                          'some_id')
        self.assertEqual(resp.status_code, 200)
        result = json.loads(resp.body)
        self.assertEqual(result['status'], 'completed')
        self.assertEqual(result['sbs_snapshot_id'], 'some_id')

    @mock.patch('jcsclient.compute.Controller.describe_snapshots')
    def test_get_snapshot_create_status_error(self, mock_describe_snapshots):
        resp_dict = {}
        resp_dict['DescribeSnapshotsResponse'] = {}
        resp_dict['DescribeSnapshotsResponse']['snapshotSet'] = {}
        resp_dict['DescribeSnapshotsResponse']['snapshotSet']['item'] ={}
        resp_dict['DescribeSnapshotsResponse']['snapshotSet']['item']['status'] = 'error'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_snapshots.return_value = res
        resp = self.controller.get_snapshot_create_status(self.mock_req,
                                                          'some_id')
        self.assertEqual(resp.status_code, 200)
        result = json.loads(resp.body)
        self.assertEqual(result['status'], 'error')
        self.assertEqual(result['sbs_snapshot_id'], 'some_id')

    # @mock.patch('client.cloud.describe_snapshots', side_effect=jcs_exc.HTTP400)
    # def test_get_snapshot_create_status_not_found(self, mock_describe_snapshots):
    #     resp = self.controller.get_snapshot_create_status(self.mock_req,
    #                                                       'some_id')
    #     self.assertEqual(resp.status_code, 404)


    @mock.patch('jcsclient.compute.Controller.create_volume')
    def test_volume_restore_in_cinder(self, mock_create_volume):
        volume_id = str(uuid.uuid4())
        resp_dict = {}
        resp_dict['CreateVolumeResponse'] = {}
        resp_dict['CreateVolumeResponse']['volumeId'] = volume_id
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_create_volume.return_value = res

        body = {}
        body['cinder_snapshot_id'] = str(uuid.uuid4())
        body['size'] = 20 
        resp = self.controller.volume_restore_in_cinder(self.mock_req,
                                                       body)
        result = json.loads(resp.body)
        self.assertEqual(result['nova_volume_id'] , volume_id)
        self.assertEqual(result['nova_volume_type'], 'standard')

    @mock.patch('jcsclient.compute.Controller.create_volume')
    def test_volume_create_in_cinder(self, mock_create_volume):
        body = {}
        body['size'] = 1
        volume_id = str(uuid.uuid4())
        resp_dict = {}
        resp_dict['CreateVolumeResponse'] = {}
        resp_dict['CreateVolumeResponse']['volumeId'] = volume_id
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_create_volume.return_value = res
        resp = self.controller.volume_create_in_cinder(self.mock_req,
                                                       body)
        result = json.loads(resp.body)
        self.assertEqual(result['nova_volume_id'], volume_id)
        self.assertEqual(result['nova_volume_type'], 'standard')

    @mock.patch('jcsclient.compute.Controller.describe_instances')
    def test_get_boot_volume_id_for_instance_success(self, mock_describe_instances):
        resp_dict = {}
        resp_dict['DescribeInstancesResponse'] = {}
        resp_dict['DescribeInstancesResponse']['instancesSet'] = {}
        resp_dict['DescribeInstancesResponse']['instancesSet']['item'] ={}
        resp_dict['DescribeInstancesResponse']['instancesSet']['item']['blockDeviceMapping'] = {}
        bdm1 = {}
        bdm1['deviceName'] = '/dev/vdb'
        bdm1['volumeId'] = 'external_volume'
        bdm2 = {}
        bdm2['deviceName'] = '/dev/vda'
        bdm2['volumeId'] = 'boot_volume'
        bdms = [bdm1, bdm2]
        resp_dict['DescribeInstancesResponse']['instancesSet']['item']['blockDeviceMapping']['item'] = bdms
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_instances.return_value = res
        resp = self.controller.get_boot_volume_id_for_instance(self.mock_req,
                                                               'some_id')
        self.assertEqual(resp.status_code, 200)
        result = json.loads(resp.body)
        self.assertEqual(result['boot_volume_id'], 'boot_volume')

    @mock.patch('jcsclient.compute.Controller.describe_instances')
    def test_get_boot_volume_id_for_instance_failure(self, mock_describe_instances):
        resp_dict = {}
        resp_dict['DescribeInstancesResponse'] = {}
        resp_dict['DescribeInstancesResponse']['instancesSet'] = {}
        resp_dict['DescribeInstancesResponse']['instancesSet']['item'] ={}
        resp_dict['DescribeInstancesResponse']['instancesSet']['item']['blockDeviceMapping'] = {}
        bdm1 = {}
        bdm1['deviceName'] = '/dev/vdb'
        bdm1['volumeId'] = 'external_volume'
        bdm2 = {}
        bdm2['deviceName'] = '/dev/vdc'
        bdm2['volumeId'] = 'no_boot_volume'
        bdms = [bdm1, bdm2]
        resp_dict['DescribeInstancesResponse']['instancesSet']['item']['blockDeviceMapping']['item'] = bdms
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_instances.return_value = res
        resp = self.controller.get_boot_volume_id_for_instance(self.mock_req,
                                                               'some_id')
        self.assertEqual(resp.status_code, 404)
