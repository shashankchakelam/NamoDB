import json
import mock
import traceback
import requests
import unittest
import uuid

from namo.tests.unit.workflowapi import fake
from namo.workflowapi.v1 import gacontroller

from oslo_config import cfg
CONF = cfg.CONF

# TODO(rushiagr): Just test exception decorator, and after that no need to test
# controller methods raising exceptions
# TODO(rushiagr): call super class's setUp() in all our setUp() methods, just
# like how it's done in this file

class GAControllerTestCase(unittest.TestCase):

    def setUp(self):
        super(GAControllerTestCase, self).setUp()
        self.controller = gacontroller.GuestAgentController()
        self.req = mock.Mock()
        self.mock_context = self.req.context
        CONF.workflow.raga_primary_port = 8080
        CONF.workflow.raga_secondary_port = 8081
        class ReturnClass(object):
            def json(self):
                return {'testkey': 'testvalue'}
        self.requests_retval = ReturnClass()

    def test_get_port(self):
        self.assertEqual('8080', self.controller._get_port('primary'))
        self.assertEqual('8080', self.controller._get_port('PRIMARY'))
        self.assertEqual('8081', self.controller._get_port('secondary'))
        self.assertEqual('8081', self.controller._get_port('SECONDARY'))
        self.assertRaises(Exception, self.controller._get_port, 'junk')
        self.assertRaises(Exception, self.controller._get_port, 123)

    def test_get_full_url(self):
        ip, raga_type, url = '1.2.3.4', 'primary', '/v1/test'
        actual_url = self.controller._get_full_url(ip, url, raga_type)
        expected_url = 'http://1.2.3.4:8080/v1/test'
        self.assertEqual(expected_url, actual_url)

        ip, raga_type, url = '1.2.3.4', 'secondary', '/v1/test'
        actual_url = self.controller._get_full_url(ip, url, raga_type)
        expected_url = 'http://1.2.3.4:8081/v1/test'
        self.assertEqual(expected_url, actual_url)

        # test without passing a raga_type
        ip, url = '1.2.3.4', '/v1/test'
        actual_url = self.controller._get_full_url(ip, url)
        expected_url = 'http://1.2.3.4:8080/v1/test'
        self.assertEqual(expected_url, actual_url)

    @mock.patch.object(requests, 'get')
    def test_do_get_request(self, mock_get):
        ip, raga_type, url = '1.2.3.4', 'primary', '/v1/test'
        self.controller._do_get_request(ip, raga_type, url)
        mock_get.assert_called_once_with('http://1.2.3.4:8080/v1/test')

    @mock.patch.object(requests, 'post')
    def test_do_post_request(self, mock_post):
        mock_post.return_value = fake.ResOk()
        ip, raga_type, url = '1.2.3.4', 'primary', '/v1/test'
        data = {'fakekey': 'fakevalue'}
        self.controller._do_post_request(ip, raga_type, url, data)
        mock_post.assert_called_once_with('http://1.2.3.4:8080/v1/test', data=data)

    @mock.patch('requests.get')
    def test_check_raga_health(self, mock_get):
        mock_get.return_value = fake.Res('""')
        response = self.controller.check_raga_health(self.req,
                                                     '0.0.0.0',
                                                     'primary')
        mock_get.assert_called_once_with('http://0.0.0.0:8080/v1/versioninfo')

    @mock.patch('requests.post')
    def test_create_master_user(self, mock_post):
        mock_post.return_value = fake.ResOk()
        body = {}
        body['ip'] =  '0.0.0.0'
        body['type'] = 'primary'
        response = self.controller.create_master_user(self.req,
                                                     body)
        mock_post.assert_called_once_with('http://0.0.0.0:8080/v1/master_user',
                                          data= json.dumps(body))

    @mock.patch('requests.post')
    def test_set_port_success(self, mock_post):
        mock_post.return_value = fake.ResOk()
        body = {}
        body['ip'] = '0.0.0.0'
        body['type'] = 'primary'
        response = self.controller.set_port(self.req,
                                            body)
        mock_post.assert_called_once_with('http://0.0.0.0:8080/v1/set_port',
                                          data= json.dumps(body))

    @mock.patch('requests.post')
    def test_set_port_failure(self, mock_post):
        mock_post.side_effect = Exception('Internal Server Error')
        body = {}
        body['ip'] = '0.0.0.0'
        body['type'] = 'primary'
        response = self.controller.set_port(self.req,
                                            body)
        mock_post.assert_called_once_with('http://0.0.0.0:8080/v1/set_port',
                                          data= json.dumps(body))

    @mock.patch('requests.post')
    def test_call_raga_to_start_backup(self,
                                       mock_post):
        mock_post.return_value = fake.ResOk()
        body = {}
        cinnder_uuid = str(uuid.uuid4())
        body["backup_id"] = "backup_id"
        body["cinder_uuid"] = cinnder_uuid
        body['ip'] = '0.0.0.0'
        body['type'] = 'primary'
        resp = self.controller.call_raga_to_start_backup(self.req, body)
        data = {}
        data['access_key'] = " "
        data['secret_key'] = " "
        data['compute_url'] = 'https://compute.ind-west-1.staging.jiocloudservices.com/'
        data['backup_id'] = 'backup_id'
        data['vpc_url'] = 'https://vpc.ind-west-1.staging.jiocloudservices.com'
        data['block_time'] = "10"
        data['volume_id'] = cinnder_uuid
        mock_post.assert_called_once_with('http://0.0.0.0:8080/v1/backup',
                                          data=json.dumps(data))
        self.assertEqual(resp.status_code, 200)

    @mock.patch('requests.get')
    def test_check_raga_for_backup_status_empty(self, mock_get):
        mock_get.return_value = fake.Res('""')
        response = self.controller\
                   .check_raga_for_backup_status(self.req,
                                                 '0.0.0.0',
                                                 'primary',
                                                 'b')
        mock_get.assert_called_once_with('http://0.0.0.0:8080/v1/backup/b')
        self.assertEqual(response.status_code, 500)



    @mock.patch('requests.get')
    def test_check_raga_for_backup_status_cinder_snapshot_id_success(self, mock_get):
        mock_get.return_value = fake.create_dict({'backup_id': 'backup_id', 'status': 'BACKUP_SUCCESS'})
        response = self.controller\
                   .check_raga_for_backup_status(self.req,
                                                 '0.0.0.0',
                                                 'primary',
                                                 'b')
        mock_get.assert_called_once_with('http://0.0.0.0:8080/v1/backup/b')
        result = json.loads(response.body)
        self.assertEqual(result['status'], 'BACKUP_SUCCESS')
        self.assertEqual(result['cinder_snapshot_id'], 'backup_id')

    @mock.patch('requests.get')
    def test_check_raga_for_backup_status_cinder_snapshot_id_failure(self, mock_get):
        mock_get.return_value = fake.create_dict({'backup_id': 'backup_id', 'status': 'BACKUP_FAILED'})
        response = self.controller\
                   .check_raga_for_backup_status(self.req,
                                                 '0.0.0.0',
                                                 'primary',
                                                 'b')
        result = json.loads(response.body)
        self.assertEqual(result['status'], 'BACKUP_FAILED')
        self.assertEqual(result['cinder_snapshot_id'], 'backup_id')
        mock_get.assert_called_once_with('http://0.0.0.0:8080/v1/backup/b')

    @mock.patch('requests.get')
    def test_check_raga_for_backup_status_random(self, mock_get):
        mock_get.return_value = fake.create_dict({'backup_id': 'backup_id', 'status': 'RANDOM'})
        response = self.controller\
                   .check_raga_for_backup_status(self.req,
                                                 '0.0.0.0',
                                                 'primary',
                                                 'b')
        self.assertEqual(response.status_code, 500)
        mock_get.assert_called_once_with('http://0.0.0.0:8080/v1/backup/b')

    @mock.patch('requests.get')
    def test_check_ga_active_success(self, mock_get):
        mock_get.return_value = fake.Response('start/running')
        response = self.controller.check_ga_active(self.req, '', 'primary')
        self.assertEqual(response.status_code, 200)
        mock_get.assert_called_once_with('http://:8080/v1/service')

    @mock.patch('requests.get')
    def test_check_ga_active_failure(self, mock_get):
        mock_get.return_value = fake.Response('error')
        response = self.controller.check_ga_active(self.req, '', 'primary')
        self.assertEqual(response.status_code, 500)
        mock_get.assert_called_once_with('http://:8080/v1/service')

    @mock.patch('requests.post')
    def test_ga_service_action_success(self, mock_post):
        mock_post.return_value = fake.ResOk()
        body = {}
        body['ip'] = 'ip'
        body['action'] = 'status'
        body['type'] = 'primary'
        response = self.controller.ga_service_action(self.req, body)
        mock_post.assert_called_once_with('http://ip:8080/v1/service',
                                          data='{"action": "status"}')
        self.assertEqual(response.status_code, 200)

    @mock.patch('requests.post')
    def test_ga_service_action_failure(self, mock_post):
        mock_post.side_effect = Exception('Internal Server Error')
        body = {}
        body['ip'] = 'ip'
        body['action'] = 'status'
        response = self.controller.ga_service_action(self.req, body)
        self.assertEqual(response.status_code, 500)

    @mock.patch('requests.get')
    def test_get_format_success(self, mock_get):
        mock_get.return_value = fake.Response('completed')
        response = self.controller.get_format_status(self.req, '0.0.0.0', 'primary')
        self.assertEqual(response.status_code, 200)
        mock_get.assert_called_once_with('http://0.0.0.0:8080/v1/format')

    @mock.patch('requests.get')
    def test_get_format_failure(self, mock_get):
        mock_get.return_value = fake.Response('error')
        response = self.controller.get_format_status(self.req, '0.0.0.0', 'primary')
        self.assertEqual(response.status_code, 500)
        mock_get.assert_called_once_with('http://0.0.0.0:8080/v1/format')

    @mock.patch('requests.post')
    def test_format_volume_success(self, mock_post):
        mock_post.return_value = fake.ResOk()
        body = {}
        body['ip'] = '0.0.0.0'
        body['type'] = 'primary'
        response = self.controller.format_volume(self.req, body)
        mock_post.assert_called_once_with('http://0.0.0.0:8080/v1/format', data=None)
        self.assertEqual(response.status_code, 200)

    @mock.patch('requests.post')
    def test_format_volume_failure(self, mock_post):
        mock_post.side_effect = Exception('Internal Server Error')
        body = {}
        body['ip'] = 'ip'
        response = self.controller.format_volume(self.req, body)
        self.assertEqual(response.status_code, 500)

    @mock.patch('requests.get')
    def test_wait_for_raga_crash_recovery_done_success(self, mock_get):
        mock_get.return_value = fake.Response('completed')
        response = self.controller\
            .wait_for_raga_crash_recovery_done(self.req, '0.0.0.0', 'primary')
        self.assertEqual(response.status_code, 200)
        mock_get.assert_called_once_with('http://0.0.0.0:8080/v1/crash_recovery')

    @mock.patch('requests.get')
    def test_wait_for_raga_crash_recovery_done_failure(self, mock_get):
        mock_get.return_value = fake.Response('error')
        response = self.controller\
            .wait_for_raga_crash_recovery_done(self.req, '0.0.0.0', 'primary')
        self.assertEqual(response.status_code, 500)
        mock_get.assert_called_once_with('http://0.0.0.0:8080/v1/crash_recovery')

    @mock.patch('requests.post')
    def test_raga_restore_database_on_instance_success(self, mock_post):
        mock_post.return_value = fake.ResOk()
        body = {}
        body['ip'] = '0.0.0.0'
        body['type'] = 'primary'
        response = self.controller.raga_restore_database_on_instance(self.req, body)
        mock_post.assert_called_once_with('http://0.0.0.0:8080/v1/crash_recovery',
                                          data=None)
        self.assertEqual(response.status_code, 200)

    @mock.patch('requests.post')
    def test_raga_restore_database_on_instance_failure(self, mock_post):
        mock_post.side_effect = Exception('Internal Server Error')
        body = {}
        body['ip'] = 'ip'
        response = self.controller.raga_restore_database_on_instance(self.req, body)
        self.assertEqual(response.status_code, 500)

    @mock.patch('requests.post')
    def test_set_parameters_success(self, mock_post):
        mock_post.return_value = fake.ResOk()
        body = {}
        body['ip'] = '0.0.0.0'
        body['type'] = 'primary'
        body['parameters'] = [{'name': 'max_concurrency', 'value': 151, 'type': 'dynamic'}]

        response = self.controller.set_parameters(self.req, body)

        mock_post.assert_called_once_with(
            'http://0.0.0.0:8080/v1/parameters',
            data=json.dumps({'parameters': body['parameters']}))
        self.assertEquals(response.status_code, 200)

    @mock.patch('requests.post')
    def test_set_parameters_failure(self, mock_post):
        mock_post.side_effect = Exception('Internal Server Error')
        body = {}
        body['ip'] = '0.0.0.0'
        body['type'] = 'primary'
        body['parameters'] = [{'name': 'max_concurrency', 'value': 151, 'type': 'dynamic'}]

        response = self.controller.set_parameters(self.req, body)

        mock_post.assert_called_once_with(
            'http://0.0.0.0:8080/v1/parameters',
            data=json.dumps({'parameters': body['parameters']}))
        self.assertEquals(response.status_code, 500)
