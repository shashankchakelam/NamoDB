import json
from random import randint
import unittest
import uuid
import mistralclient
import mock
from mock import patch
from namo.common import exception
from namo.common import httpresponse
import httplib
import json
from namo.db.mysqlalchemy import api
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models
from namo.openclient import session
from namo.tests import constants
from namo.tests.unit.workflowapi import fake
from namo.tests.unit.workflowapi import common
from namo.workflowapi.v1 import masterworkflowcontroller
from namo.workflowapi.v1 import gacontroller

from oslo_config import cfg
CONF = cfg.CONF

class OutputResponse():

    def __init__(self, content, status_code):
        self.content = content
        self.status_code = status_code

class FakeExecution():
    def __init__(self, id):
        self.id = id

class TestWorkflowapi:
    test_owner_id = 12345
    test_instance_id = ""
    test_instance_name = ""
    test_nova_flavor_id = ""
    test_nova_flavor_name = ""
    test_artefact_id = ""
    test_raga_id = ""
    test_os_id = ""
    test_db_engine_id = ""
    test_keypair_id = ""

    def __init__(self):
        self.request = fake.Request()
        self.masterworkflowcontroller = masterworkflowcontroller.MasterworkflowController()
        self.gacontroller = gacontroller.GuestAgentController()

    def _get_dict__from_response(self, response):
        string = '[' +  response.app_iter[0] + ']'
        dictionary = json.loads(string)
        return dictionary[0]

    def save_keypair_local(self):
        body = {}
        body['nova_keypair_id'] = str(uuid.uuid4())
        body['private_key'] = 'Test'
        body['instance_id'] = self.test_instance_id
        response = self.masterworkflowcontroller.keypair_save_in_db(self.request, body)
        dict1 = self._get_dict__from_response(response)
        id = int(dict1['local_keypair_id'])
        response = self.masterworkflowcontroller.keypair_get_from_db(self.request, id)
        dict2 = self._get_dict__from_response(response)
        nova_keypair_id = dict2['nova_keypair_id']
        return nova_keypair_id, body['nova_keypair_id']

    def delete_keypair_local(self):
        body = {}
        body['nova_keypair_id'] = str(uuid.uuid4())
        body['private_key'] = 'Test'
        response = self.masterworkflowcontroller.keypair_save_in_db(self.request, body)
        dict1 = self._get_dict__from_response(response)
        id = int(dict1['local_keypair_id'])
        body2 = {}
        body2['local_keypair_id'] = id
        self.masterworkflowcontroller.keypair_delete_from_db(self.request, body2)
        try:
            self.masterworkflowcontroller.keypair_get_from_db(self.request, id)
        except exception.KeyPairNotFound:
            return 'OK'
        return 'NotOk'

    def create_host_local(self):
        body = {}
        body['instance_id'] = self.test_instance_id
        body['local_keypair_id'] = self.test_keypair_id
        body['az'] = 'az'
        body['nova_vm_id'] = str(uuid.uuid4())
        body['local_flavor_id'] = 1
        body['local_image_id'] = 1
        body['nova_vm_ip'] = str(randint(0,255)) + '.' \
                             + str(randint(0,255)) + '.' \
                             + str(randint(0,255)) + '.' \
                             + str(randint(0,255))
        body['nova_vm_ip_id'] = str(uuid.uuid4())
        body['artefact_id'] = 1
        host = self.masterworkflowcontroller.host_save_in_db(self.request, body)
        result = self._get_dict__from_response(host)
        host = self.masterworkflowcontroller.host_get_from_db(self.request, body['instance_id'])
        result2 = self._get_dict__from_response(host)
        self.masterworkflowcontroller.host_delete_in_db(self.request, result)
        return result['local_host_id'], result2['local_host_id']

    def delete_host_local(self):
        body = {}
        body['instance_id'] = self.test_instance_id
        body['local_keypair_id'] = self.test_keypair_id
        body['az'] = 'az'
        body['nova_vm_id'] = str(uuid.uuid4())
        body['local_flavor_id'] = 1
        body['local_image_id'] = 1
        body['nova_vm_ip'] = str(randint(0,255)) + '.' \
                             + str(randint(0,255)) + '.' \
                             + str(randint(0,255)) + '.' \
                             + str(randint(0,255))
        body['nova_vm_ip_id'] = str(uuid.uuid4())
        body['artefact_id'] = 1
        response = self.masterworkflowcontroller.host_save_in_db(self.request, body)
        dict1 = self._get_dict__from_response(response)
        id = int(dict1['local_host_id'])
        body2 = {}
        body2['local_host_id'] = id
        self.masterworkflowcontroller.host_delete_in_db(self.request, body2)
        try:
            self.masterworkflowcontroller.host_get_from_db(self.request, int(body['instance_id']))
        except exception.HostNotFoundByInstanceId:
            return 'OK'
        return 'NotOk'

    @mock.patch('novaclient.v2.servers.ServerManager.create', fake.create)
    def create_vm(self):
        body = {}
        body['nova_image_id'] = 1
        body['az'] = 'nova'
        body['owner_id'] = 'Testing'
        body['nova_flavor_id'] = 1
        body['nova_keypair_id'] = self.test_keypair_id
        response = self.masterworkflowcontroller.create_vm_in_nova(self.request, body)
        return response

    @mock.patch('novaclient.v2.servers.ServerManager.delete', fake.delete)
    def delete_vm(self):
        body = {}
        body['nova_vm_id'] = ''
        response = self.masterworkflowcontroller.delete_vm(self.request, body)
        return response

    @mock.patch('novaclient.v2.servers.ServerManager.findall', fake.findall_success)
    def check_vm_active_success(self):
        body = {}
        body['nova_vm_id'] = ''
        response = self.masterworkflowcontroller.check_vm_active_in_nova(self.request, body)
        return response

    @mock.patch('novaclient.v2.servers.ServerManager.findall', fake.findall_error)
    def check_vm_active_error(self):
        body = {}
        body['nova_vm_id'] = ''
        response = self.masterworkflowcontroller.check_vm_active_in_nova(self.request, body)
        return response

    @mock.patch('novaclient.v2.servers.ServerManager.findall', fake.findall_deleted_sucess)
    def check_vm_deleted_success(self):
        response = self.masterworkflowcontroller.check_vm_deleted(self.request, '1')
        return response

    @mock.patch('novaclient.v2.servers.ServerManager.findall', fake.findall_deleted_error)
    def check_vm_deleted_error(self):
        body = {}
        body['nova_vm_id'] = ''
        response = self.masterworkflowcontroller.check_vm_deleted(self.request, '1')
        return response

    @mock.patch('cinderclient.v2.volumes.VolumeManager.create', fake.volume_create)
    def create_volume(self):
        body = {}
        body['az'] = 'nova'
        body['size'] = 1
        response = self.masterworkflowcontroller.check_vm_active_in_nova(self.request, body)
        return response

    @mock.patch('cinderclient.v2.volumes.VolumeManager.findall', fake.findall_success)
    def check_volume_available_success(self):
        response = self.masterworkflowcontroller.check_volume_available(self.request, '1')
        return response

    @mock.patch('cinderclient.v2.volumes.VolumeManager.findall', fake.findall_error)
    def check_volume_available_error(self):
        response = self.masterworkflowcontroller.check_volume_available(self.request, '1')
        return response

    @mock.patch('cinderclient.v2.volumes.VolumeManager.findall', fake.findall_attach_sucess)
    def check_volume_attach_success(self):
        response = self.masterworkflowcontroller.check_volume_attached(self.request, '0' , '1')
        return response

    @mock.patch('cinderclient.v2.volumes.VolumeManager.findall', fake.findall_attach_error)
    def check_volume_attach_error(self):
        response = self.masterworkflowcontroller.check_volume_attached(self.request, '0' , '1')
        return response

    @mock.patch('novaclient.v2.volumes.VolumeManager.create_server_volume', fake.attach_volume)
    def attach_volume(self):
        body = {}
        body['nova_vm_id'] = ''
        body['nova_volume_id'] = ''
        response = self.masterworkflowcontroller.attach_volume(self.request, body)
        return response

    @mock.patch('novaclient.v2.volumes.VolumeManager.delete_server_volume', fake.detach_volume)
    def detach_volume(self):
        body = {}
        body['nova_vm_id'] = ''
        body['nova_volume_id'] = ''
        response = self.masterworkflowcontroller.detach_volume(self.request, body)
        return response

    ### Create base objects ###
    def create_base_objects(self):
        self._objects_to_delete = []
        self.create_test_raga()
        print 'Created test raga metdata: %s' % self.test_raga_id
        self.create_test_os()
        print 'Created test os metdata: %s' % self.test_os_id
        self.create_db_engine()
        print 'Created test db_engine metdata: %s' % self.test_db_engine_id
        self.create_test_artefact()
        print 'Created test artefact metdata: %s' % self.test_artefact_id
        self.create_test_nova_flavor()
        print 'Created test nova_flavor metdata: %s' % self.test_nova_flavor_name
        self.create_test_keypair()
        print 'Created test keypair metdata: %s' % self.test_keypair_id
        self.create_test_instance()
        print 'Created test instance metdata: %s' % self.test_instance_name


    def create_test_instance(self):
        test_instance = {}
        test_instance['name'] = str(uuid.uuid4())
        test_instance['storage_size'] = 100
        test_instance['nova_flavor_id'] = self.test_nova_flavor_id
        test_instance['artefact_id'] = self.test_artefact_id
        test_instance['backup_retention_period_days'] = 1
        test_instance['preferred_backup_window_start'] = \
            constants.test_backup_window_start
        test_instance['preferred_backup_window_end'] = \
            constants.test_backup_window_end
        test_instance['preferred_maintenance_window_start_day'] = \
            constants.test_maintenance_window_start_day
        test_instance['preferred_maintenance_window_start_time'] = \
            constants.test_maintenance_window_start_time
        test_instance['preferred_maintenance_window_end_day'] = \
            constants.test_maintenance_window_end_day
        test_instance['preferred_maintenance_window_end_time'] = \
            constants.test_maintenance_window_end_time
        test_instance['master_username'] = constants.test_master_username
        test_instance['master_user_password'] = constants.test_master_user_password
        test_instance['port'] = constants.test_port
        created_instance = api.instance_create(None, self.test_owner_id, test_instance)
        self.test_instance_id = created_instance.id
        self.test_instance_name = created_instance.name

    def create_test_nova_flavor(self):
        nova_flavor = api.nova_flavor_create(None, str(uuid.uuid4()), 'c1.2xlarge', '60', '16')
        self.test_nova_flavor_id = nova_flavor.id
        self.test_nova_flavor_name = nova_flavor.flavor_name

    def create_test_artefact(self):
        artefact = api.create_artefact(None, self.test_raga_id, self.test_db_engine_id, self.test_os_id)
        self.test_artefact_id = artefact.id

    def create_test_raga(self):
        raga = api.create_raga(None, '1.0', 'http://www.jiords.com', 'PREFERRED')
        self.test_raga_id = raga.id
        self._objects_to_delete.append(raga)

    def create_test_os(self):
        os = api.create_os(None, 'ubuntu', '14', '04', 'http://www.ubuntu.com', 'PREFERRED')
        self.test_os_id = os.id
        self._objects_to_delete.append(os)

    def create_db_engine(self):
        db_engine = api.create_db_engine(None, 'MySQL', '5.6', '26', 'Http://www.mysql.com', 'PREFERRED')
        self.test_db_engine_id = db_engine.id
        self._objects_to_delete.append(db_engine)

    def create_test_keypair(self):
        keypair = api.keypair_create(None, str(uuid.uuid4()), str(uuid.uuid4()))
        self.test_keypair_id = keypair.id

    ### Delete base objects ###
    def delete_all_base_objects(self):
        print 'Deleting test instance metdata: %s' % self.test_instance_name
        api.instance_delete(None, self.test_owner_id, self.test_instance_name, None)
        print 'Deleting test nova_flavor metdata: %s' % self.test_nova_flavor_name
        api.nova_flavor_destroy(None, self.test_nova_flavor_name)
        print 'Deleting test artefact metdata: %s' % self.test_artefact_id
        api.delete_artefact(None, self.test_artefact_id)
        print 'Deleting test raga metdata: %s' % self.test_raga_id
        api.delete_raga(None, self.test_raga_id)
        print 'Deleting test os metdata: %s' % self.test_os_id
        api.delete_os(None, self.test_os_id)
        print 'Deleting test db_engine metdata: %s' % self.test_db_engine_id
        api.delete_db_engine(None, self.test_db_engine_id)
        print 'Deleting test keypair metdata: %s' % self.test_keypair_id
        api.keypair_destroy(None, self.test_keypair_id)

        # TODO(rushiagr): above deletions will only mark 'deleted' column to
        # 'True', but won't delete row from db. Deleting actual row from db
        # below. Remove above lines in future
        for i in range(len(self._objects_to_delete)):
            api._delete_db_obj('', self._objects_to_delete.pop())


class WorkflowAPITestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.data = common.set_up_class_helper()

    @classmethod
    def tearDownClass(cls):
        common.tear_down_class_helper(cls.data)

    def setUp(self):
        self.mwf = masterworkflowcontroller.MasterworkflowController()
        self.ga = gacontroller.GuestAgentController()
        self.request = mock.Mock()
        CONF.workflow.workflow_url = 'workflow://url'
        CONF.workflow.get_backup_batch_size = 20
        CONF.workflow.get_instance_batch_size = 20

    def test_check_idempotency(self):
        master = api.masterworkflow_get('')
        data = {}
        if master.execution_id:
            print master.execution_id
            data['execution_id'] = master.execution_id
            self.mwf.set_execution_id_null_for_mfw(self.request, data)
        data['execution_id'] = str(uuid.uuid4())
        resp = self.mwf.check_idempotency(self.request, data)
        result = json.loads(resp.body)
        self.assertEqual(result['id'], 1)

        # Second time it should return 0
        resp = self.mwf.check_idempotency(self.request, data)
        master = api.masterworkflow_get('')
        self.assertEqual(data['execution_id'], master.execution_id)

    def test_set_execution_id_null_for_mfw(self):
        master = api.masterworkflow_get('')
        data = {}
        if master.execution_id is None:
            data['execution_id'] = str(uuid.uuid4())
            resp = self.mwf.check_idempotency(self.request, data)
            result = json.loads(resp.body)
            self.assertEqual(result['id'], 1)
        master = api.masterworkflow_get('')
        data['execution_id'] = master.execution_id
        self.mwf.set_execution_id_null_for_mfw(self.request, data)
        master = api.masterworkflow_get('')
        self.assertEqual(master.execution_id, None)

    @mock.patch('mistralclient.api.v2.executions.ExecutionManager.create')
    def test_start_new_mfw(self, mock_create):
        resp = self.mwf.start_new_mfw(self.request)
        self.assertEqual(resp.status_code, 200)

    @mock.patch('mistralclient.api.v2.executions.ExecutionManager.create')
    def test_start_new_backup_worflow(self, mock_create):
        mistral = fake.Mistral()
        mock_create.return_value = mistral
        old_execution_id = str(uuid.uuid4())
        db.create_workflow_record(old_execution_id,
                                  'CREATE_BACKUP')
        body = {}
        body['execution_id'] = old_execution_id
        body['instance_id'] = None
        body['cinder_uuid'] = str(uuid.uuid4())
        body['backup_id'] = None
        body['owner_id'] = 1
        body['instance_ip'] = '0.0.0.0'
        body['volume_id'] = str(uuid.uuid4())
        resp = self.mwf.start_new_backup_workflow(self.request, body)
        self.assertEqual(resp.status_code, 200)
        workflow = api.get_workflow_record_by_execution_id(old_execution_id)
        self.assertEqual(workflow.state, 'FAILURE')
        workflow = api.get_workflow_record_by_execution_id(mistral.id)
        self.assertEqual(workflow.state, 'IN-PROGRESS')


    def test_volume_update_in_db(self):
        host_id = self.data['host'].id
        body = {
            'nova_volume_id': str(uuid.uuid4()),
            'local_host_id': host_id,
            'nova_volume_type': 'SSD',
            'nova_attach_status': 'NOT ATTACHED',
        }
        resp = self.mwf.volume_save_in_db(self.request, body)

        # TODO(rushiagr): volume_save_in_db doesn't return attach status.
        # Once it starts returning that, we should asssert if it is 'not
        # attached' at this point

        resp_dict = json.loads(resp.body)
        vol_id = resp_dict['local_volume_id']

        body = {
            'local_volume_id': vol_id,
            'attach_status': 'ATTACHED'
            }

        self.mwf.volume_update_in_db(self.request, body)

        # TODO(rushiagr): use volume_get_by_id instead of volume_get_from_db
        # once it's implemented
        resp = self.mwf.volume_get_from_db(self.request, host_id)
        resp_dict = json.loads(resp.body)
        self.assertEqual(resp_dict['attach_status'], 'ATTACHED')

        self.mwf.volume_delete_in_db(self.request, body)
        db._hard_delete_by_id('', models.Volume, vol_id)

    def test_volume_get_from_db(self):
        data = common.set_up_class_helper()
        body = {}
        body['nova_volume_id'] = str(uuid.uuid4())
        body['local_host_id'] = data['host'].id
        body['nova_volume_type'] = 'SSD'
        body['nova_attach_status'] = 'NOT ATTACHED'
        self.mwf.volume_save_in_db(self.request, body)
        resp = self.mwf.volume_get_from_db(self.request, body['local_host_id'])
        volume = json.loads(resp.body)
        db._hard_delete_by_id('', models.Volume, volume['local_volume_id'])
        assert volume['nova_volume_id'] == body['nova_volume_id']
        common.tear_down_class_helper(data)

    def test_volume_save_in_db(self):
        body = {}
        # body['local_volume_id'] = volume.id
        body['nova_volume_id'] = str(uuid.uuid4())
        body['local_host_id'] = self.data['host'].id
        body['nova_volume_type'] = 'SSD'
        body['nova_attach_status'] = 'NOT ATTACHED'
        self.mwf.volume_save_in_db(self.request, body)
        resp = self.mwf.volume_get_from_db(self.request, body['local_host_id'])
        volume = json.loads(resp.body)
        db._hard_delete_by_id('', models.Volume, volume['local_volume_id'])
        assert volume['nova_volume_id'] == body['nova_volume_id']

    def test_volume_delete_in_db(self):
        body = {}
        # body['local_volume_id'] = volume.id
        body['nova_volume_id'] = str(uuid.uuid4())
        body['local_host_id'] = self.data['host'].id
        body['nova_volume_type'] = 'SSD'
        body['nova_attach_status'] = 'NOT ATTACHED'
        resp = self.mwf.volume_save_in_db(self.request, body)
        volume = json.loads(resp.body)
        self.mwf.volume_delete_in_db(self.request, volume)
        delete_volume = db._get_by_id('', models.Volume, volume['local_volume_id'])
        db._hard_delete_by_id('', models.Volume, volume['local_volume_id'])
        assert delete_volume.id == volume['local_volume_id']
        assert delete_volume.deleted == True

    # @patch('mistralclient.api.v2.executions.ExecutionManager.create')
    # def test_find_and_schedule_pending_tasks(self, mock_mistral_call):
    #     # Create four instances in creating/pending, deleting/pending,
    #     # restoring/pending and active/none. Create two backups in
    #     # creating/pending and restoring/pending for instance with status
    #     # active/none. Then check the first three instances go in
    #     # creating/applying, deleting/applying and restoring/applying state,
    #     # and check if restoring/pending backup goes in creating/applying state

    #     # Ensure that there are no instances and backups before starting this test
    #     # else they may interfere with the expectations / validations we have
    #     instances = []
    #     backups = []
    #     try:
    #         instances = db._get_by_filters(None, models.Instance, deleted=False)
    #     except exception.DbEntryNotFound:
    #         pass

    #     try:
    #         backups = db._get_by_filters(None, models.Backup, deleted=False)
    #     except exception.DbEntryNotFound:
    #         pass

    #     for instance in instances:
    #         db.instance_delete(None, instance.owner_id, instance.name, None)
    #     for backup in backups:
    #         db.backup_delete(None, backup.id)

    #     execution_id_1 = str(uuid.uuid4())
    #     execution_id_2 = str(uuid.uuid4())
    #     execution_id_3 = str(uuid.uuid4())
    #     execution_id_4 = str(uuid.uuid4())
    #     execution_id_5 = str(uuid.uuid4())
    #     mock_mistral_call.side_effect = [
    #         FakeExecution(execution_id_1),
    #         FakeExecution(execution_id_2),
    #         FakeExecution(execution_id_3),
    #         FakeExecution(execution_id_4),
    #         FakeExecution(execution_id_5),
    #     ]

    #     i_cp = common.create_test_instance()
    #     i_dp = common.create_test_instance(lifecycle='DELETING')
    #     i_an = common.create_test_instance(lifecycle='ACTIVE',
    #             changestate='NONE')
    #     i_bp = common.create_test_instance(lifecycle='BACKUP')
    #     h_dp = common.create_test_host(i_dp)
    #     h_an = common.create_test_host(i_an)
    #     h_bp = common.create_test_host(i_bp)
    #     v_dp = common.create_test_volume(h_dp)
    #     v_an = common.create_test_volume(h_an)
    #     v_bp = common.create_test_volume(h_bp)
    #     b_cp = common.create_test_backup(i_bp, lifecycle='CREATING')
    #     b_rp = common.create_test_backup(i_an, lifecycle='RESTORING')

    #     i_rp = common.create_test_instance(lifecycle='RESTORING',
    #             restored_from_backup_id=b_rp.id)

    #     i_dp_final_snapshot = common.create_test_instance(lifecycle='DELETING')
    #     b_cp_final_snapshot = common.create_test_backup(i_dp_final_snapshot, lifecycle='CREATING')

    #     self.addCleanup(db._delete_db_obj, '', i_cp)
    #     self.addCleanup(db._delete_db_obj, '', i_dp)
    #     self.addCleanup(db._delete_db_obj, '', i_an)
    #     self.addCleanup(db._delete_db_obj, '', i_bp)
    #     self.addCleanup(db._delete_db_obj, '', h_dp)
    #     self.addCleanup(db._delete_db_obj, '', h_an)
    #     self.addCleanup(db._delete_db_obj, '', h_bp)
    #     self.addCleanup(db._delete_db_obj, '', v_dp)
    #     self.addCleanup(db._delete_db_obj, '', v_an)
    #     self.addCleanup(db._delete_db_obj, '', v_bp)
    #     self.addCleanup(db._delete_db_obj, '', b_cp)
    #     self.addCleanup(db._delete_db_obj, '', b_rp)
    #     self.addCleanup(db._delete_db_obj, '', i_rp)
    #     self.addCleanup(db._delete_db_obj, '', i_dp_final_snapshot)
    #     self.addCleanup(db._delete_db_obj, '', b_cp_final_snapshot)

    #     print "Before find and schedule"
    #     backups = db._get_by_filters(None, models.Backup, deleted=False)
    #     for backup in backups:
    #         common.print_backup_info(backup)

    #     instances = db._get_by_filters(None, models.Instance, deleted=False)
    #     for instance in instances:
    #         common.print_instance_info(instance)

    #     resp = self.mwf.find_and_schedule_pending_tasks(self.request)

    #     print "After find and schedule"
    #     backups = db._get_by_filters(None, models.Backup, deleted=False)
    #     for backup in backups:
    #         common.print_backup_info(backup)

    #     instances = db._get_by_filters(None, models.Instance, deleted=False)
    #     for instance in instances:
    #         common.print_instance_info(instance)

    #     i_cp_now = db._get_by_id('', models.Instance, i_cp.id)
    #     i_dp_now = db._get_by_id('', models.Instance, i_dp.id)
    #     i_rp_now = db._get_by_id('', models.Instance, i_rp.id)
    #     i_bp_now = db._get_by_id('', models.Instance, i_bp.id)
    #     b_cp_now = db._get_by_id('', models.Backup, b_cp.id)
    #     b_rp_now = db._get_by_id('', models.Backup, b_rp.id)
    #     i_dp_final_snapshot_now = db._get_by_id('', models.Instance, i_dp_final_snapshot.id)
    #     b_cp_final_snapshot_now = db._get_by_id('', models.Backup, b_cp_final_snapshot.id)
    #     workflow1 = db.get_workflow_record_by_execution_id(execution_id_1)
    #     workflow2 = db.get_workflow_record_by_execution_id(execution_id_2)
    #     workflow3 = db.get_workflow_record_by_execution_id(execution_id_3)
    #     workflow4 = db.get_workflow_record_by_execution_id(execution_id_4)
    #     workflow5 = db.get_workflow_record_by_execution_id(execution_id_5)
    #     self.assertEqual('APPLYING', i_cp_now.changestate)
    #     self.assertEqual('APPLYING', i_dp_now.changestate)
    #     self.assertEqual('APPLYING', i_rp_now.changestate)
    #     self.assertEqual('APPLYING', i_bp_now.changestate)
    #     self.assertEqual('APPLYING', b_cp_now.changestate)
    #     self.assertEqual('APPLYING', b_rp_now.changestate)
    #     self.assertEqual('APPLYING', i_dp_final_snapshot_now.changestate)
    #     self.assertEqual('APPLYING', b_cp_final_snapshot_now.changestate)

    #     self.assertEqual('IN-PROGRESS', workflow1.state)
    #     self.assertEqual('IN-PROGRESS', workflow2.state)
    #     self.assertEqual('IN-PROGRESS', workflow3.state)
    #     self.assertEqual('IN-PROGRESS', workflow4.state)
    #     self.assertEqual('IN-PROGRESS', workflow5.state)

    #     self.addCleanup(db._delete_db_obj, '', workflow1)
    #     self.addCleanup(db._delete_db_obj, '', workflow2)
    #     self.addCleanup(db._delete_db_obj, '', workflow3)
    #     self.addCleanup(db._delete_db_obj, '', workflow4)
    #     self.addCleanup(db._delete_db_obj, '', workflow5)

    #     self.assertEqual(resp.status_code, 406)

    
    # # @mock.patch('jcsclient.compute.Controller.delete_key_pair')
    # def test_delete_keypair_nova(self, mock_delete):
    #     mock_delete.return_value = {'DeleteKeyPairResponse': {'return': 'true'}}
    #     body = {'nova_keypair_id': ''}
    #     response = self.mwf.delete_keypair_nova(self.request, body)
    #     self.assertEqual(response.status_code, 200)

    # @mock.patch('jcsclient.compute.Controller.create_key_pair')
    # def create_keypair_nova(self, mock_create):
    #     mock_create.return_value = {'CreateKeyPairResponse': {
    #         'keyName': 'mykey',
    #         'keyMaterial': 'keymaterialwhichisabiigkey',
    #     }}
    #     response = self.mwf.keypair_create_in_nova(self.request)
    #     self.assertEqual(response.status_code, 200)

    @patch('mistralclient.api.v2.executions.ExecutionManager.create')
    @patch('namo.db.mysqlalchemy.api.backup_get_all_pending_from_db')
    def test_get_pending_backups_and_schedule(self, mock_db_call, mock_mistral_call):
        execution_id_1 = str(uuid.uuid4())
        execution_id_2 = str(uuid.uuid4())
        mock_mistral_call.side_effect = [FakeExecution(execution_id_1), FakeExecution(execution_id_2)]

        # Create 2 instances, hosts, volumes and backups for setup!
        instance1 = common.create_test_instance(lifecycle='BACKUP', changestate='PENDING')
        host1 = common.create_test_host(instance1)
        volume1 = common.create_test_volume(host1)
        backup1 = common.create_test_backup(instance1)
        self.addCleanup(db._delete_db_obj, '', instance1)
        self.addCleanup(db._delete_db_obj, '', host1)
        self.addCleanup(db._delete_db_obj, '', volume1)
        self.addCleanup(db._delete_db_obj, '', backup1)

        instance2 = common.create_test_instance(lifecycle='BACKUP', changestate='PENDING')
        host2 = common.create_test_host(instance2)
        volume2 = common.create_test_volume(host2)
        backup2 = common.create_test_backup(instance2)
        self.addCleanup(db._delete_db_obj, '', instance2)
        self.addCleanup(db._delete_db_obj, '', host2)
        self.addCleanup(db._delete_db_obj, '', volume2)
        self.addCleanup(db._delete_db_obj, '', backup2)

        # Return the 2 backups in the mock call
        mock_db_call.return_value = [backup1, backup2]

        # Call the method we want to test
        self.mwf._get_pending_backups_and_schedule(self.request)

        # Verifications
        mock_db_call.assert_called_once_with(self.request.context, CONF.workflow.get_backup_batch_size)

        backup1 = db._get_by_id('', models.Backup, backup1.id)
        self.assertEqual(backup1.lifecycle, 'CREATING')
        self.assertEqual(backup1.changestate, 'APPLYING')

        instance1 = db._get_by_id('', models.Instance, instance1.id)
        self.assertEqual(instance1.lifecycle, 'BACKUP')
        self.assertEqual(instance1.changestate, 'APPLYING')

        backup2 = db._get_by_id('', models.Backup, backup2.id)
        self.assertEqual(backup2.lifecycle, 'CREATING')
        self.assertEqual(backup2.changestate, 'APPLYING')

        instance2 = db._get_by_id('', models.Instance, instance2.id)
        self.assertEqual(instance2.lifecycle, 'BACKUP')
        self.assertEqual(instance2.changestate, 'APPLYING')

        workflow1 = db.get_workflow_record_by_execution_id(execution_id_1)
        self.assertEqual(workflow1.state, 'IN-PROGRESS')

        workflow2 = db.get_workflow_record_by_execution_id(execution_id_2)
        self.assertEqual(workflow2.state, 'IN-PROGRESS')

    @patch('mistralclient.api.v2.executions.ExecutionManager.create')
    @patch('namo.db.mysqlalchemy.api.backup_get_all_pending_from_db')
    def test_get_pending_backups_and_schedule_first_exception_seconds_succeed(self, mock_db_call, mock_mistral_call):
        execution_id_1 = str(uuid.uuid4())
        mock_mistral_call.side_effect = [FakeExecution(execution_id_1)]

        # Create 2 instances, hosts, volumes and backups for setup!
        # This instance is in an unexpected state of DELETING
        instance1 = common.create_test_instance(lifecycle='DELETING', changestate='PENDING')
        backup1 = common.create_test_backup(instance1)
        self.addCleanup(db._delete_db_obj, '', instance1)
        self.addCleanup(db._delete_db_obj, '', backup1)

        instance2 = common.create_test_instance(lifecycle='BACKUP', changestate='PENDING')
        host2 = common.create_test_host(instance2)
        volume2 = common.create_test_volume(host2)
        backup2 = common.create_test_backup(instance2)
        self.addCleanup(db._delete_db_obj, '', instance2)
        self.addCleanup(db._delete_db_obj, '', host2)
        self.addCleanup(db._delete_db_obj, '', volume2)
        self.addCleanup(db._delete_db_obj, '', backup2)

        # Return the 2 backups in the mock call
        mock_db_call.return_value = [backup1, backup2]

        # Call the method we want to test
        self.mwf._get_pending_backups_and_schedule(self.request)

        # Verifications
        mock_db_call.assert_called_once_with(self.request.context, CONF.workflow.get_backup_batch_size)

        backup1 = db._get_by_id('', models.Backup, backup1.id)
        self.assertEqual(backup1.lifecycle, 'CREATING')
        self.assertEqual(backup1.changestate, 'PENDING')

        instance1 = db._get_by_id('', models.Instance, instance1.id)
        self.assertEqual(instance1.lifecycle, 'DELETING')
        self.assertEqual(instance1.changestate, 'PENDING')

        backup2 = db._get_by_id('', models.Backup, backup2.id)
        self.assertEqual(backup2.lifecycle, 'CREATING')
        self.assertEqual(backup2.changestate, 'APPLYING')

        instance2 = db._get_by_id('', models.Instance, instance2.id)
        self.assertEqual(instance2.lifecycle, 'BACKUP')
        self.assertEqual(instance2.changestate, 'APPLYING')

        workflow1 = db.get_workflow_record_by_execution_id(execution_id_1)
        self.assertEqual(workflow1.state, 'IN-PROGRESS')

    # @mock.patch('jcsclient.compute.Controller.create_key_pair')
    # def test_keypair_create_in_nova(self, mock_create_key_pair):
    #     resp_dict = {}
    #     resp_dict['CreateKeyPairResponse'] = {}
    #     resp_dict['CreateKeyPairResponse']['keyName'] = 'random_keyName'
    #     resp_dict['CreateKeyPairResponse']['keyMaterial'] = 'random_keyMaterial'
    #     mock_create_key_pair.return_value = resp_dict
    #     response = self.mwf.keypair_create_in_nova(self.request)
    #     self.assertEqual(response.status_code, 200)
    #     result = json.loads(response.body)
    #     self.assertEqual(result['nova_keypair_id'], 'random_keyName')
    #     self.assertEqual(result['private_key'], 'random_keyMaterial')

    @mock.patch('jcsclient.compute.Controller.delete_key_pair')
    def test_delete_keypair_nova_error(self, mock_delete_key_pair):
        id = str(uuid.uuid4())
        body = {}
        body['nova_keypair_id'] = id
        resp_dict = {}
        resp_dict['DeleteKeyPairResponse'] = {}
        resp_dict['DeleteKeyPairResponse']['return'] = 'false'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_delete_key_pair.return_value = res
        try:
            self.mwf.delete_keypair_nova(self.request, body)
        except:
            pass

    @mock.patch('jcsclient.compute.Controller.delete_key_pair')
    def test_delete_keypair_nova_success(self, mock_delete_key_pair):
        id = str(uuid.uuid4())
        body = {}
        body['nova_keypair_id'] = id
        resp_dict = {}
        resp_dict['DeleteKeyPairResponse'] = {}
        resp_dict['DeleteKeyPairResponse']['return'] = 'true'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_delete_key_pair.return_value = res
        response = self.mwf.delete_keypair_nova(self.request, body)
        self.assertEqual(response.status_code, 200)

    @mock.patch('jcsclient.compute.Controller.run_instances')
    def test_create_vm_in_nova(self, mock_run_instances):
        instance_id = str(uuid.uuid4())
        resp_dict = {}
        resp_dict['RunInstancesResponse']= {}
        resp_dict['RunInstancesResponse']['instancesSet'] = {}
        resp_dict['RunInstancesResponse']['instancesSet']['item'] ={}
        resp_dict['RunInstancesResponse']['instancesSet']['item']['instanceId'] = instance_id
        body = {}
        body['nova_image_id'] = 1
        body['az'] = 'nova'
        body['owner_id'] = 'Testing'
        body['nova_flavor_id'] = 1
        body['nova_keypair_id'] = 1
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_run_instances.return_value = res
        response = self.mwf.create_vm_in_nova(self.request, body)
        self.assertEqual(response.status_code, 200)
        result = json.loads(response.body)
        self.assertEqual(result['nova_vm_id'], instance_id)

    @mock.patch('jcsclient.compute.Controller.terminate_instances')
    def test_delete_vm_failure(self, mock_terminate_instances):
        instance_id = str(uuid.uuid4())
        body = {}
        body['nova_vm_id'] = instance_id
        resp_dict = {}
        resp_dict['TerminateInstancesResponse']= {}
        resp_dict['TerminateInstancesResponse']['instancesSet'] = {}
        resp_dict['TerminateInstancesResponse']['instancesSet']['item'] ={}
        resp_dict['TerminateInstancesResponse']['instancesSet']['item']['currentState'] = 'pending'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_terminate_instances.return_value = res
        try:
            response = self.mwf.delete_vm(self.request, body)
        except:
            pass

    @mock.patch('jcsclient.compute.Controller.terminate_instances')
    def test_delete_vm_success(self, mock_terminate_instances):
        instance_id = str(uuid.uuid4())
        body = {}
        body['nova_vm_id'] = instance_id
        resp_dict = {}
        resp_dict['TerminateInstancesResponse']= {}
        resp_dict['TerminateInstancesResponse']['instancesSet'] = {}
        resp_dict['TerminateInstancesResponse']['instancesSet']['item'] ={}

        resp_dict['TerminateInstancesResponse']['instancesSet']['item']['currentState'] = 'shutting-down'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_terminate_instances.return_value = res
        response = self.mwf.delete_vm(self.request, body)
        self.assertEqual(response.status_code, 200)

    @mock.patch('jcsclient.compute.Controller.describe_instances')
    def test_check_vm_active_in_nova_running(self, mock_describe_instances):
        resp_dict = {}
        resp_dict['DescribeInstancesResponse']= {}
        resp_dict['DescribeInstancesResponse']['instancesSet'] = {}
        resp_dict['DescribeInstancesResponse']['instancesSet']['item'] ={}
        resp_dict['DescribeInstancesResponse']['instancesSet']['item']['instanceState'] = 'running'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_instances.return_value = res
        body = {}
        body['nova_vm_id'] = ''
        response = self.mwf.check_vm_active_in_nova(self.request, body)
        self.assertEqual(response.status_code, 200)

    @mock.patch('jcsclient.compute.Controller.describe_instances')
    def test_check_vm_active_in_nova_pending(self, mock_describe_instances):
        resp_dict = {}
        resp_dict['DescribeInstancesResponse']= {}
        resp_dict['DescribeInstancesResponse']['instancesSet'] = {}
        resp_dict['DescribeInstancesResponse']['instancesSet']['item'] ={}
        resp_dict['DescribeInstancesResponse']['instancesSet']['item']['instanceState'] = 'pending'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_instances.return_value = res
        body = {}
        body['nova_vm_id'] = ''
        response = self.mwf.check_vm_active_in_nova(self.request, body)
        self.assertEqual(response.status_code, 404)

    @mock.patch('jcsclient.compute.Controller.describe_instances')
    def test_check_vm_deleted_failure(self, mock_describe_instances):
        resp_dict = {}
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_instances.return_value = res
        response = self.mwf.check_vm_deleted(self.request, '1')
        self.assertEqual(response.status_code, 500)

    # @mock.patch('jcsclient.compute.Controller.describe_instances', side_effect=jcs_exc.HTTP400)
    # def test_check_vm_deleted_success(self, mock_describe_instances):
    #     response = self.mwf.check_vm_deleted(self.request, '1')
    #     self.assertEqual(response.status_code, 200)

    @mock.patch('jcsclient.compute.Controller.create_volume')
    def test_volume_create_in_cinder(self, mock_create_volume):
        volume_id = str(uuid.uuid4())
        body = {}
        body['size'] = 1
        resp_dict = {}
        resp_dict['CreateVolumeResponse'] = {}
        resp_dict['CreateVolumeResponse']['volumeId'] = volume_id
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_create_volume.return_value = res
        response = self.mwf.volume_create_in_cinder(self.request, body)
        self.assertEqual(response.status_code, 200)
        result = json.loads(response.body)
        self.assertEqual(result['nova_volume_id'], volume_id)
        self.assertEqual(result['nova_volume_type'], 'standard')

    # @mock.patch('jcsclient.compute.Controller.delete_volume', side_effect=jcs_exc.HTTP400)
    # def test_delete_volume_failure_bad_request(self, mock_create_volume):
    #     body = {}
    #     body['nova_volume_id'] = 'some_id'
    #     response = self.mwf.delete_volume(self.request, body)
    #     self.assertEqual(response.status_code, 500)

    @mock.patch('jcsclient.compute.Controller.delete_volume')
    def test_delete_volume_failure_call_failed(self, mock_create_volume):
        body = {}
        body['nova_volume_id'] = 'some_id'
        result_dict = {}
        result_dict['DeleteVolumeResponse'] = {}
        result_dict['DeleteVolumeResponse']['return'] = 'false'
        res = OutputResponse(content=result_dict,status_code=httplib.OK)
        mock_create_volume.return_value = res
        response = self.mwf.delete_volume(self.request, body)
        self.assertEqual(response.status_code, 500)

    @mock.patch('jcsclient.compute.Controller.delete_volume')
    def test_delete_volume_success(self, mock_create_volume):
        body = {}
        body['nova_volume_id'] = 'some_id'
        result_dict = dict()
        result_dict['DeleteVolumeResponse'] = dict()
        result_dict['DeleteVolumeResponse']['return'] = 'true'
        res = OutputResponse(content=result_dict,status_code=httplib.OK)
        mock_create_volume.return_value = res
        response = self.mwf.delete_volume(self.request, body)
        self.assertEqual(response.status_code, 200)

    @mock.patch('jcsclient.compute.Controller.attach_volume')
    def test_attach_volume(self, mock_attach_volume):
        body = {}
        body['nova_vm_id'] = ''
        body['nova_volume_id'] = ''
        resp_dict = {}
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_attach_volume.return_value = res
        respone = self.mwf.attach_volume(self.request, body)
        self.assertEqual(respone.status_code, 200)

    # @mock.patch('jcsclient.compute.Controller.describe_volumes', side_effect=jcs_exc.HTTP400)
    # def test_check_volume_attach_error(self, mock_describe_volumes):
    #     response = self.mwf.check_volume_attached(self.request, '0' , '1')
    #     self.assertEqual(response.status_code, 404)

    @mock.patch('jcsclient.compute.Controller.describe_volumes')
    def test_check_volume_attach_failure(self, mock_describe_volumes):
        resp_dict = {}
        resp_dict['DescribeVolumesResponse'] = {}
        resp_dict['DescribeVolumesResponse']['volumeSet'] = {}
        resp_dict['DescribeVolumesResponse']['volumeSet']['item'] ={}
        resp_dict['DescribeVolumesResponse']['volumeSet']['item']['status'] = 'pending'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_volumes.return_value = res
        response = self.mwf.check_volume_attached(self.request, '0' , '1')
        self.assertEqual(response.status_code, 404)

    @mock.patch('jcsclient.compute.Controller.describe_volumes')
    def test_check_volume_attach_success(self, mock_describe_volumes):
        resp_dict = {}
        resp_dict['DescribeVolumesResponse'] = {}
        resp_dict['DescribeVolumesResponse']['volumeSet'] = {}
        resp_dict['DescribeVolumesResponse']['volumeSet']['item'] ={}
        resp_dict['DescribeVolumesResponse']['volumeSet']['item']['status'] = 'in-use'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_volumes.return_value = res
        response = self.mwf.check_volume_attached(self.request, '0' , '1')
        self.assertEqual(response.status_code, 200)

    @mock.patch('jcsclient.compute.Controller.describe_volumes')
    def test_check_volume_available_error(self, mock_describe_volumes):
        resp_dict = {}
        resp_dict['DescribeVolumesResponse'] = {}
        resp_dict['DescribeVolumesResponse']['volumeSet'] = {}
        resp_dict['DescribeVolumesResponse']['volumeSet']['item'] ={}
        resp_dict['DescribeVolumesResponse']['volumeSet']['item']['status'] = 'pending'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_volumes.return_value = res
        response = self.mwf.check_volume_available(self.request, '1')
        self.assertEqual(response.status_code, 404)

    @mock.patch('jcsclient.compute.Controller.describe_volumes')
    def test_check_volume_available_success(self, mock_describe_volumes):
        resp_dict = {}
        resp_dict['DescribeVolumesResponse'] = {}
        resp_dict['DescribeVolumesResponse']['volumeSet'] = {}
        resp_dict['DescribeVolumesResponse']['volumeSet']['item'] ={}
        resp_dict['DescribeVolumesResponse']['volumeSet']['item']['status'] = 'available'
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_volumes.return_value = res
        response = self.mwf.check_volume_available(self.request, '1')
        self.assertEqual(response.status_code, 200)

    @mock.patch('jcsclient.compute.Controller.describe_volumes')
    def test_check_volume_deleted_failure(self, mock_describe_volumes):
        resp_dict = {}
        res = OutputResponse(content=resp_dict,status_code=httplib.OK)
        mock_describe_volumes.return_value = res
        response = self.mwf.check_volume_deleted(self.request, '1')
        self.assertEqual(response.status_code, 500)

    # @mock.patch('jcsclient.compute.Controller.describe_volumes', side_effect=jcs_exc.HTTP404)
    # def test_check_volume_deleted_success(self, mock_describe_volumes):
    #     response = self.mwf.check_volume_deleted(self.request, '1')
    #     self.assertEqual(response.status_code, 200)




