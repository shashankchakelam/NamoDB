import json
from random import randint
import unittest
import uuid
import mistralclient
import mock
from datetime import timedelta
from mock import patch
from namo.common import exception
from namo.db.mysqlalchemy import api
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models
from namo.openclient import session
from namo.tests import constants
from namo.tests.unit.workflowapi import fake
from namo.tests.unit.workflowapi import common
from namo.workflowapi.v1 import cleanautobkpcontroller 
from oslo_utils import timeutils
from oslo_config import cfg

CONF = cfg.CONF

class FakeExecution():
    def __init__(self, id):
        self.id = id

class CleanAutoBkpWorkflowAPITestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.data = common.set_up_class_helper()

    @classmethod
    def tearDownClass(cls):
        common.tear_down_class_helper(cls.data)

    def setUp(self):
        self.clbkp = cleanautobkpcontroller.CleanAutoBkpController()
        self.request = mock.Mock()
        CONF.workflow.workflow_url = 'workflow://url'
        CONF.workflow.get_backup_batch_size = 20
        CONF.workflow.get_instance_batch_size = 20

    def test_check_idempotency(self):
        cleanautobkp = api.infinite_workflow_get('CLEAN_AUTO_BACKUP')
        data = {}
        if cleanautobkp.execution_id:
            print cleanautobkp.execution_id
            data['execution_id'] = cleanautobkp.execution_id
            self.clbkp.set_execution_id_null(self.request, data)
        data['execution_id'] = str(uuid.uuid4())
        resp = self.clbkp.check_idempotency(self.request, data)
        result = json.loads(resp.body)
        self.assertEqual(result['acquired_lock'], 1)
        cleanautobkp = api.infinite_workflow_get('CLEAN_AUTO_BACKUP')
        self.assertEqual(data['execution_id'], cleanautobkp.execution_id)

        # Second time it should return 0
        data['execution_id'] = str(uuid.uuid4())
        resp = self.clbkp.check_idempotency(self.request, data)
        result = json.loads(resp.body)
        self.assertEqual(result['acquired_lock'], 0)

    def _create_bkps(self, i, hours_gap=[]):
        bkp_ids = []
        for h in hours_gap:
            if h==0:
                bkp_ids.append((common.create_test_backup(i, lifecycle='ACTIVE', changestate='NONE')).id)
            else:
                threshold_time = str(timeutils.utcnow() - timedelta(hours=h))
                bkp_ids.append((common.create_test_backup(i, created_at=threshold_time, lifecycle='ACTIVE', changestate='NONE')).id)
        return bkp_ids


    def test_find_and_schedule(self):
        test_cases = {
                'Case#01':{'Ret':2, 'create': [0*24, 1*24],             'should_delete':[2*24, 3*24]},
                'Case#02':{'Ret':0, 'create': [0*24],                   'should_delete':[1*24, 2*24, 3*24]},
                'Case#03':{'Ret':0, 'create': [3*24],                   'should_delete':[5*24, 6*24, 7*24]},
                'Case#04':{'Ret':2, 'create': [3*24],                   'should_delete':[5*24, 6*24, 7*24]},
                'Case#05':{'Ret':6, 'create': [3*24, 5*24],             'should_delete':[6*24, 7*24]},
                'Case#06':{'Ret':8, 'create': [3*24, 5*24, 6*24, 7*24], 'should_delete':[]},
                ## test cases for multiple backups in same day
                'Case#08':{'Ret':1, 'create': [0, 10, 20, 23],          'should_delete':[]},
                'Case#09':{'Ret':1, 'create': [0, 10, 20, 23],          'should_delete':[45, 50]},
                'Case#10':{'Ret':1, 'create': [10, 23],                 'should_delete':[50, 150]},
                'Case#11':{'Ret':0, 'create': [10],                     'should_delete':[15, 23, 45, 55]},
                }
        for case in sorted(test_cases):
            args = test_cases[case]
            i = common.create_test_instance(lifecycle='ACTIVE', changestate='NONE', backup_retention_period_days=args['Ret'])
            self._create_bkps(i, hours_gap=args['create'])
            should_delete = self._create_bkps(i, hours_gap=args['should_delete'])
            resp = self.clbkp.find_and_schedule_clean_auto_bkps(self.request)
            deleted_bkps = api.model_query('', models.Backup, instance_id=str(i.id), type='AUTOMATED', lifecycle='DELETING').all() 
            self.assertEqual(len(deleted_bkps), len(should_delete))
            for d in deleted_bkps:
                self.assertEqual(d.id in should_delete, True)
            print case + " Suceeded"

    def test_set_execution_id_null(self):
        cleanautobkp = api.infinite_workflow_get('CLEAN_AUTO_BACKUP')
        data = {}
        if cleanautobkp.execution_id is None:
            data['execution_id'] = str(uuid.uuid4())
            resp = self.clbkp.check_idempotency(self.request, data)
            result = json.loads(resp.body)
            self.assertEqual(result['acquired_lock'], 1)
        cleanautobkp = api.infinite_workflow_get('CLEAN_AUTO_BACKUP')
        data['execution_id'] = cleanautobkp.execution_id
        self.clbkp.set_execution_id_null(self.request, data)
        cleanautobkp = api.infinite_workflow_get('CLEAN_AUTO_BACKUP')
        self.assertEqual(cleanautobkp.execution_id, None)

    @mock.patch('mistralclient.api.v2.executions.ExecutionManager.create')
    def test_start_new_bkp(self, mock_create):
        resp = self.clbkp.start_new_infinite_workflow(self.request)
        self.assertEqual(resp.status_code, 200)

