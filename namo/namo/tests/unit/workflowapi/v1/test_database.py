# Copyright (c) 2011 X.commerce, a business unit of eBay Inc.
# Copyright 2010 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# Copyright 2014 IBM Corp.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import uuid
import mock

from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models
from namo.common import exception
from namo.tests import constants
from namo.tests.unit import conf_fixture
from namo.tests.unit.workflowapi import common
from oslo_config import cfg
from oslo_db import exception as db_exception
from pprint import pprint

from datetime import timedelta
from oslo_utils import timeutils

import sqlalchemy

CONF = cfg.CONF

conf_fixture.set_defaults()

models.register_models(conf_fixture.connection_str)

import unittest

class DatabaseTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.data = common.set_up_class_helper()

    @classmethod
    def tearDownClass(cls):
        common.tear_down_class_helper(cls.data)

    def setUp(self):
        super(DatabaseTestCase, self).setUp()
        self.mock_req = mock.Mock()
        self.mock_context = self.mock_req.context

    def _create_backup(self, instance, backup_type='MANUAL', backup_start=None,\
            backup_end=None, backup_retention_period_days=1):
        backup = None
        backup = db.backup_create(
            self.mock_context,
            instance.id,
            uuid.uuid4().hex,
            instance.owner_id,
            backup_type,
            23, # storage size
            3306, # port
            'masterusername',
            'masterpassword',
            self.data['flavor'].id,
            self.data['instance_artefact'].id,
            constants.test_backup_window_start,
            constants.test_backup_window_end,
            constants.test_maintenance_window_start_day,
            constants.test_maintenance_window_start_time,
            constants.test_maintenance_window_end_day,
            constants.test_maintenance_window_end_time,
            backup_retention_period_days=backup_retention_period_days)
        return backup

    def _create_test_instance_and_backup(self, owner_id = 1, create_backup=True, backup_start=None, \
            backup_end=None, backup_retention_period_days=1):

        instance = db.instance_create_entry_from_user_supplied_values(
            '',
            owner_id,
            'mysql',
            '5.6',
            uuid.uuid4().hex,
            'c1.4xlarge',
            20,
            constants.test_master_username,
            constants.test_master_user_password,
            backup_start or constants.test_backup_window_start,
            backup_end or constants.test_backup_window_end,
            constants.test_maintenance_window_start_day,
            constants.test_maintenance_window_start_time,
            constants.test_maintenance_window_end_day,
            constants.test_maintenance_window_end_time,
            constants.test_port,
            constants.test_log_retention_period,
            backup_retention_period_days=backup_retention_period_days)
        
        backup = None
        if create_backup:
            backup = self._create_backup(instance, backup_start = (backup_start or constants.test_backup_window_start),\
                backup_end = (backup_end or constants.test_backup_window_end), backup_retention_period_days = backup_retention_period_days)
        return instance, backup

    def test_get_by_id(self):
        # We'll first try to get an instance, first without and then with
        # one level joinedload, and then with two level joinedload

        # Without joinedload
        instance = db._get_by_id('', models.Instance, self.data['instance'].id)
        self.assertEqual(instance.id, self.data['instance'].id)
        self.assertRaises(sqlalchemy.orm.exc.DetachedInstanceError, getattr,
                instance, 'artefact')

        # With joinedload as a list
        instance = db._get_by_id('', models.Instance, self.data['instance'].id,
                session=None, joinedload_tables=['nova_flavor', 'artefact'])
        self.assertEqual(instance.artefact.raga_id, self.data['instance_artefact'].raga_id)

        # With joinedload as a dict -- second level join
        instance = db._get_by_id('', models.Instance, self.data['instance'].id,
                session=None, joinedload_tables={'artefact': ['raga', 'os']})
        self.assertEqual(instance.artefact.raga.id,
                self.data['instance_artefact'].raga_id)

        # Invalid datatype as joinedload_tables
        self.assertRaises(Exception, db._get_by_id, '', models.Instance,
                self.data['instance'].id, session=None, joinedload_tables=('a', 'b',))

        # Invalid table name in joinedload_tables
        self.assertRaises(Exception, db._get_by_id, '', models.Instance,
                self.data['instance'].id, session=None, joinedload_tables=['blah'])

    def test_backup_get_all_pending(self):
        # Create 2 backups, then call 'backup_get_all_pending' to see if it
        # returns that backup.
        backup1 = db.backup_create(
            self.mock_context,
            self.data['instance'].id,
            'backup-'+uuid.uuid4().hex,
            12345, # owner_id
            'MANUAL',
            23, # storage size
            3306, # port
            'masterusername',
            'masterpassword',
            self.data['flavor'].id,
            self.data['instance_artefact'].id,
            constants.test_backup_window_start,
            constants.test_backup_window_end,
            constants.test_maintenance_window_start_day,
            constants.test_maintenance_window_start_time,
            constants.test_maintenance_window_end_day,
            constants.test_maintenance_window_end_time)
        backup2 = db.backup_create(
            self.mock_context,
            self.data['instance'].id,
            'backup-'+uuid.uuid4().hex,
            12345, # owner_id
            'MANUAL',
            23, # storage size
            3306, # port
            'masterusername',
            'masterpassword',
            self.data['flavor'].id,
            self.data['instance_artefact'].id,
            constants.test_backup_window_start,
            constants.test_backup_window_end,
            constants.test_maintenance_window_start_day,
            constants.test_maintenance_window_start_time,
            constants.test_maintenance_window_end_day,
            constants.test_maintenance_window_end_time)
        backups = db.backup_get_all_pending(self.mock_context, batch_size=1)
        self.assertEqual(len(backups), 1)
        # TODO(rushiagr): for now, comment out the following line, until every
        # other test delete the db entry they created
        # self.assertEqual(backups[0]['backup_id'], backup1.id)
        db._delete_db_obj('', backup1)
        db._delete_db_obj('', backup2)

    def test_mark_backup_creation_started(self):
        instance, backup = self._create_test_instance_and_backup()
        db._update_by_id('', models.Instance, instance.id, lifecycle='BACKUP',
                changestate='PENDING')
        backup_dict = {'backup_id': backup.id}
        bkp, inst = db.mark_backup_creation_started(self.mock_context,
                backup_dict)
        self.assertEqual(bkp.lifecycle, 'CREATING')
        self.assertEqual(bkp.changestate, 'APPLYING')
        self.assertEqual(inst.lifecycle, 'BACKUP')
        self.assertEqual(inst.changestate, 'APPLYING')

        db._delete_db_obj('', backup)
        db._delete_db_obj('', instance)

    def test_mark_backup_creation_instance_inconsistent_state(self):
        instance = common.create_test_instance(lifecycle='DELETING', changestate='PENDING')
        backup = common.create_test_backup(instance)
        self.addCleanup(db._delete_db_obj, '', instance)
        self.addCleanup(db._delete_db_obj, '', backup)

        backup_dict = {}
        backup_dict['backup_id'] = backup.id

        db.mark_backup_creation_started(None, backup_dict)

        # Verify unchanged instance and backup states
        self.assertEqual(backup.lifecycle, 'CREATING')
        self.assertEqual(backup.changestate, 'PENDING')
        self.assertEqual(instance.lifecycle, 'DELETING')
        self.assertEqual(instance.changestate, 'PENDING')

    def test_mark_backup_creation_backup_inconsistent_state(self):
        instance = common.create_test_instance(lifecycle='BACKUP', changestate='PENDING')
        backup = common.create_test_backup(instance, lifecycle='DELETING', changestate='PENDING')
        self.addCleanup(db._delete_db_obj, '', instance)
        self.addCleanup(db._delete_db_obj, '', backup)

        backup_dict = {}
        backup_dict['backup_id'] = backup.id

        db.mark_backup_creation_started(None, backup_dict)

        # Verify unchanged instance and backup states
        self.assertEqual(backup.lifecycle, 'DELETING')
        self.assertEqual(backup.changestate, 'PENDING')
        self.assertEqual(instance.lifecycle, 'BACKUP')
        self.assertEqual(instance.changestate, 'PENDING')

    def test_mark_backup_creation_instance_does_not_exist(self):
        instance = common.create_test_instance()
        backup = common.create_test_backup(instance)

        # Delete instance
        db.instance_hard_delete(None, instance.owner_id, instance.name)

        self.addCleanup(db._delete_db_obj, '', backup)

        backup_dict = {}
        backup_dict['backup_id'] = backup.id

        db.mark_backup_creation_started(None, backup_dict)

        # Verify unchanged backup state
        self.assertEqual(backup.lifecycle, 'CREATING')
        self.assertEqual(backup.changestate, 'PENDING')

    def test_mark_backup_creation_backup_does_not_exist(self):
        backup_dict = {}
        backup_dict['backup_id'] = str(uuid.uuid4().hex)

        db.mark_backup_creation_started(None, backup_dict)

    def test_mark_restore_started(self):
        instance, backup = self._create_test_instance_and_backup()

        # lifecycle of instance and backup is 'CREATING' so should raise
        # exception
        self.assertRaises(Exception, db.mark_restore_started)

        # Happy case
        instance = db._update_by_db_object(instance, lifecycle='RESTORING',
                restored_from_backup_id=backup.id)
        backup = db._update_by_db_object(backup, lifecycle='RESTORING')
        inst, bkp = db.mark_restore_started(self.mock_context, instance)
        self.assertEqual('APPLYING', inst.changestate)
        self.assertEqual('APPLYING', bkp.changestate)

    def test_lccs_conditional_update(self):
        # Create instance in creating/pending
        instance = common.create_test_instance()
        self.addCleanup(db._delete_db_obj, '', instance)

        # Happy case -- update when lc and cs are expected
        db.lccs_conditional_update('', models.Instance, instance.id,
        'RESTORING', 'APPLYING', 'CREATING', 'PENDING')

        # Negative case -- update when lc and cs don't have expected values
        self.assertRaises(exception.DbConditionalUpdateFailed, db.lccs_conditional_update, '',
            models.Instance, instance.id, 'ACTIVE', 'NONE', 'RESTORING', 'PENDING')

        # Passing None in any of last four values should raise ValueError
        self.assertRaises(ValueError, db.lccs_conditional_update, '',
            models.Instance, instance.id, 'ACTIVE', None, 'RESTORING',
            'APPLYING')

        # TODO(rushiagr):  This should actually be a db-specific test, where we
        # check any db errors. Here, we're checking if a random value is passed
        # to an enum field
        self.assertRaises(Exception, db.lccs_conditional_update, '', models.Instance, instance.id, 'ACTIVE',
                'JUNKDATA', 'RESTORING', 'APPLYING')

    def test_all_smoke(self):
        # Things which were tested in older db smoke tests which are left to be
        # tested
        db.get_latest_raga('')

    def test_get_user_visible_instance_properties_by_id_not_present(self):
        self.assertRaises(exception.InstanceNotFoundById, db.get_user_visible_instance_properties_by_id, 0)

    def test_instance_owner_id(self):
        # Max size of owner_id is 12 digits
        instance = common.create_test_instance(owner_id=999999999999)
        self.addCleanup(db._delete_db_obj, '', instance)

        # Validate with 13 digit owner_id
        with self.assertRaises(db_exception.DBDataError) as context_manager:
            common.create_test_instance(owner_id=9999999999991)
        print context_manager.exception.message
        self.assertIn('Out of range value for column \'owner_id\' at row 1', context_manager.exception.message)

        # Owner_id is numeric only
        with self.assertRaises(db_exception.DBDataError) as context_manager:
            common.create_test_instance(owner_id='abc1')
        self.assertIn('Incorrect decimal value', context_manager.exception.message)

    def test_backup_owner_id(self):
        instance = common.create_test_instance()
        self.addCleanup(db._delete_db_obj, '', instance)

        # Max size of owner_id is 12 digits
        backup = common.create_test_backup(instance, owner_id=999999999999)
        self.addCleanup(db._delete_db_obj, '', backup)

        # Validate with 13 digit owner_id
        with self.assertRaises(db_exception.DBDataError) as context_manager:
            common.create_test_backup(instance, owner_id=9999999999991)
        print context_manager.exception.message
        self.assertIn('Out of range value for column \'owner_id\' at row 1', context_manager.exception.message)

        # Owner_id is numeric only
        with self.assertRaises(db_exception.DBDataError) as context_manager:
            common.create_test_backup(instance, owner_id='abc1')
        self.assertIn('Incorrect decimal value', context_manager.exception.message)

    def test_get_user_limit_by_owner_id_exception(self):
        # Negative case -- when owner_id is not present
        self.assertRaises(exception.UserLimitNotFoundByOwnerId, db.user_quota_get_by_owner_id_quota_name, '', 0, 'total_snapshots')

    def test_negative_set_execution_id_null_for_infinite_workflow(self):
        execution_id = str(uuid.uuid4().hex)
        self.assertRaises(exception.InfiniteWorkflowNotFound, db.set_execution_id_null_for_infinite_workflow, execution_id, 'AUTO_BACKUP')

    def test_set_execution_id_null_for_infinite_workflow(self):
        execution_id = str(uuid.uuid4().hex)
        update_result = db.infinite_workflow_check_and_update(execution_id, 'AUTO_BACKUP')

        pre_get_result = db.infinite_workflow_get('AUTO_BACKUP')
        execution_id = pre_get_result.execution_id
        db.set_execution_id_null_for_infinite_workflow(execution_id, 'AUTO_BACKUP')
        post_get_result = db.infinite_workflow_get('AUTO_BACKUP')

        self.assertEqual(post_get_result.execution_id, None)

    def test_negative_create_infinite_workflow_record(self):
        self.assertRaises(exception.DBException, db.create_infinite_workflow_record, 'AUTO_BACKUP')

    def test_infinite_workflow_check_and_update(self):
        try:
            pre_get_result = db.infinite_workflow_get('AUTO_BACKUP')
            if pre_get_result.execution_id is not None:
                db.set_execution_id_null_for_infinite_workflow(pre_get_result.execution_id, 'AUTO_BACKUP')
        except exception.InfiniteWorkflowNotFound:
            pass
        update_result = db.infinite_workflow_check_and_update('db-test-123', 'AUTO_BACKUP')
        get_result = db.infinite_workflow_get('AUTO_BACKUP')
        self.assertEqual(get_result.execution_id, 'db-test-123')
        
        second_result = db.infinite_workflow_check_and_update('ERRO123', 'AUTO_BACKUP')
        self.assertEqual(get_result.execution_id, 'db-test-123')


    def _update_instance_details(self, id, lifecycle="ACTIVE", changestate="NONE", created_at = None, updated_at = None):
        
        session = db.get_session()
        with session.begin(subtransactions=True):
            I = db.instance_get_by_id("", id, session)
            I.lifecycle = lifecycle
            I.changestate = changestate
            I.updated_at = updated_at or timeutils.utcnow();
            if created_at:
                I.created_at = created_at
            I.save(session)
            return I

    def _contains_instance_id(self, instances, id):
        if instances is None and id is None:
            return True
        if instances is None:
            return False
        for i in instances:
            if i.id == id:
                return True
        return False

    def _backups_contains_instance_id(self, backups, id):
        if backups is None and id is None:
            return True
        if backups is None:
            return False
        for b in backups:
            if b.instance_id == id:
                return True
        return False

    def _update_backup_details(self, owner_id, name, lifecycle="ACTIVE", changestate="NONE",\
            type="AUTOMATED", created_at=None, updated_at=None):
        session = db.get_session()
        with session.begin(subtransactions=True):
            B = db.backup_get_by_owner_id_and_name("", owner_id, name, session)
            B.lifecycle = lifecycle
            B.changestate = changestate
            B.type = type 
            B.updated_at = updated_at or timeutils.utcnow()
            if created_at:
                B.created_at = created_at
            B.save(session)
            return B

    def test_eligible_instances_in_backup_window(self):
        backup_start = str((timeutils.utcnow() - timedelta(hours=1)).time())
        backup_end   = str((timeutils.utcnow() + timedelta(hours=1)).time())

        # Normal instance, current time lies in backup window
        IB = self._create_test_instance_and_backup(create_backup=False, backup_start=backup_start, \
            backup_end=backup_end, backup_retention_period_days=1)
        I1 = self._update_instance_details(IB[0].id)

        # backup_retention = 0, should not be eligible
        IB = self._create_test_instance_and_backup(create_backup=False, backup_start=backup_start, \
            backup_end=backup_end, backup_retention_period_days=0)
        I2 = self._update_instance_details(IB[0].id)

        # has a recent backup entry, will be eligible but putting in blaclist ids
        IB = self._create_test_instance_and_backup(create_backup=True, backup_start=backup_start, \
            backup_end=backup_end, backup_retention_period_days=2)
        B3 = self._update_backup_details(IB[1].owner_id, IB[1].name)
        I3 = self._update_instance_details(IB[0].id)

        # Test for time roll over 
        backup_start = "23:45:00"
        backup_end   = "23:44:59"
        IB = self._create_test_instance_and_backup(create_backup=False, backup_start=backup_start, \
            backup_end=backup_end, backup_retention_period_days=5)
        I4 = self._update_instance_details(IB[0].id)


        ignore_inst_ids = [I3.id]
        return_instances = db.eligible_instances_in_backup_window_query(ignore_inst_ids=ignore_inst_ids).all()

        self.assertEqual(self._contains_instance_id(return_instances, I1.id), True)
        self.assertEqual(self._contains_instance_id(return_instances, I4.id), True)
        self.assertEqual(self._contains_instance_id(return_instances, I2.id), False)
        self.assertEqual(self._contains_instance_id(return_instances, I3.id), False)

    def test_find_instance_ids_with_recent_auto_backups(self):
        backup_start = str((timeutils.utcnow() - timedelta(hours=1)).time())
        backup_end   = str((timeutils.utcnow() + timedelta(hours=1)).time())

        IB = self._create_test_instance_and_backup(create_backup=True, backup_start=backup_start, \
            backup_end=backup_end, backup_retention_period_days=1)
        I1 = self._update_instance_details(IB[0].id)
        B1 = self._update_backup_details(IB[1].owner_id, IB[1].name)

        return_backups = db.find_instance_ids_with_recent_auto_backups(time_hours=1)
        self.assertEqual(self._backups_contains_instance_id(return_backups, I1.id), True)

        return_backups = db.find_instance_ids_with_recent_auto_backups(time_hours=0)
        self.assertEqual(self._backups_contains_instance_id(return_backups, I1.id), False)

    def test_find_missing_auto_backups(self):
        backup_start = str((timeutils.utcnow() - timedelta(hours=1)).time())
        backup_end   = str((timeutils.utcnow() + timedelta(hours=1)).time())

        # Newly created instance should not be included
        IB = self._create_test_instance_and_backup(create_backup=False, backup_retention_period_days=1)
        I1 = self._update_instance_details(IB[0].id)

        # create an instance in past !
        IB = self._create_test_instance_and_backup(create_backup=False) 
        created_at = timeutils.utcnow() - timedelta(hours=36)
        I2 = self._update_instance_details(IB[0].id, created_at=created_at)

        IB = self._create_test_instance_and_backup(create_backup=True, backup_start=backup_start, \
            backup_end=backup_end, backup_retention_period_days=1)
        B3 = self._update_backup_details(IB[1].owner_id, IB[1].name)
        I3 = self._update_instance_details(IB[0].id)

        return_instances = db.missing_auto_backups_query().all()
        self.assertEqual(self._contains_instance_id(return_instances, I1.id), False)
        self.assertEqual(self._contains_instance_id(return_instances, I2.id), True)
        self.assertEqual(self._contains_instance_id(return_instances, I3.id), False)

    def test_get_available_backups_per_instance(self):
        owner_id = 123
        IB = self._create_test_instance_and_backup(owner_id=owner_id, create_backup=True, backup_retention_period_days=2)
        I1 = self._update_instance_details(IB[0].id)
        B1 = self._update_backup_details(IB[0].owner_id, IB[1].name)
        
        instance = IB[0]
        B2 = self._create_backup(instance) 
        created_at_2 = str((timeutils.utcnow() - timedelta(hours=12)))
        B2 = self._update_backup_details(IB[0].owner_id, B2.name, created_at=created_at_2, updated_at=created_at_2)

        B3 = self._create_backup(instance) 
        created_at_3 = str((timeutils.utcnow() - timedelta(hours=24)))
        B3 = self._update_backup_details(IB[0].owner_id, B3.name, created_at=created_at_3, updated_at=created_at_3)

        B4 = self._create_backup(instance) 
        created_at_4 = str((timeutils.utcnow() - timedelta(hours=48)))
        B4 = self._update_backup_details(IB[0].owner_id, B4.name, created_at=created_at_4, updated_at=created_at_4)

        backups = db.get_available_backups_per_instance(bkp_type='AUTOMATED')
        result_bkp_map = None
        for b in backups:
            if b[0] == instance.id:
                result_bkp_map = b
                break

        self.assertEqual(result_bkp_map[1], 2) # retention days
        filter_bkp_list = result_bkp_map[2]

        time_id_list = result_bkp_map[2].split(',')
        id_list = []
        for created_id in time_id_list:
            i = created_id.split('##')
            id_list.append(i[1])
        self.assertEqual(str(B1.id) in id_list, True) 
        self.assertEqual(str(B2.id) in id_list, True) 
        self.assertEqual(str(B3.id) in id_list, True) 
        self.assertEqual(str(B4.id) in id_list, True) 

    def test_find_all_same_name_instances(self):
        instance = common.create_test_instance()
        C1 = db._find_all_same_name_values(owner_id=instance.owner_id, value_name=instance.name)
        C2 = db._find_all_same_name_values(id=instance.id)
        self.assertEqual(instance.id == C1[0].id, True) 
        self.assertEqual(instance.id == C2[0].id, True) 

        db.instance_delete('', instance.owner_id, instance.name) 
        C3 = db._find_all_same_name_values(owner_id=instance.owner_id, value_name=instance.name)
        self.assertEqual(instance.id == C3[0].id, True) 

        instance2 = common.create_test_instance(owner_id=instance.owner_id, name=instance.name)
        C4 = db._find_all_same_name_values(owner_id=instance.owner_id, value_name=instance.name)
        self.assertEqual(len(C4), 2) 
        cnt = db.get_instance_delete_max_count(owner_id=instance.owner_id, instance_name=instance.name)
        self.assertEqual(cnt, 1) 

        db.instance_delete('', instance2.owner_id, instance2.name) 
        instance3 = common.create_test_instance(owner_id=instance.owner_id, name=instance.name)
        cnt = db.get_instance_delete_max_count(owner_id=instance.owner_id, instance_name=instance.name)
        self.assertEqual(cnt, 2) 
        C5 = db._find_all_same_name_values(owner_id=instance.owner_id, value_name=instance.name)
        C6 = db._find_all_same_name_values(id=instance.id)
        self.assertEqual(len(C5), 3) 
        self.assertEqual(len(C6), 3) 

    def test_default_parameter_create_and_get(self):
        mysql_db_engine = db.create_db_engine('', 'mysql', '5.8', '28', 'll', 'DEPRECATED')
        postgres_db_engine = db.create_db_engine('', 'postgresql', '9.7', '13', 'll', 'ACTIVE')

        mysql_param_dict = {
            'innodb_buffer_pool_size': ['DBInstanceClassMemory*3/4', 'static'],
            'innodb_flush_method': ['O_DIRECT', 'static'],
            'max_connections': ['151', 'dynamic']
        }

        postgres_param_dict = {
            'shared_buffers': ['DBInstanceClassMemory/32768', 'static'],
            'max_stack_depth': ['6144', 'dynamic'],
            'checkpoint_completion_target': ['0.9', 'dynamic']
        }

        for param_name in mysql_param_dict.iterkeys():
            db.create_default_parameter_record(
                mysql_db_engine.id,
                param_name,
                mysql_param_dict[param_name][0],
                mysql_param_dict[param_name][1])

        for param_name in postgres_param_dict.iterkeys():
            db.create_default_parameter_record(
                postgres_db_engine.id,
                param_name,
                postgres_param_dict[param_name][0],
                postgres_param_dict[param_name][1])

        mysql_params_from_db = db.get_default_parameters_for_db_engine_id(mysql_db_engine.id)
        self.assertEquals(len(mysql_params_from_db), 3)

        for param in mysql_params_from_db:
            self.assertEquals(mysql_param_dict[param['name']][0], param['value'])
            self.assertEquals(mysql_param_dict[param['name']][1], param['type'])

        postgres_params_from_db = db.get_default_parameters_for_db_engine_id(postgres_db_engine.id)
        self.assertEquals(len(postgres_params_from_db), 3)

        for param in postgres_params_from_db:
            self.assertEquals(postgres_param_dict[param['name']][0], param['value'])
            self.assertEquals(postgres_param_dict[param['name']][1], param['type'])

