# Copyright 2010 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# Copyright 2011 OpenStack Foundation
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import json
import mock
import unittest
import traceback
import uuid

from sqlalchemy import exc as db_exc
from webob import exc as webob_exc
from namo.common import exception
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models
from namo.tests.unit.workflowapi import common
from namo.workflowapi.v1 import persistence_controller


# TODO(rushiagr): use common.create_test_[instance|backup] methods everywhere,
# and don't use cls.data wala instance.

class TestPersistenceController(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data = common.set_up_class_helper()

    @classmethod
    def tearDownClass(cls):
        common.tear_down_class_helper(cls.data)

    def setUp(self):
        self.controller = persistence_controller.create_resource().controller
        self.mock_req = mock.Mock()
        self.mock_context = self.mock_req.context

    def test_mark_final_backup_completed(self):
        instance = common.create_test_instance(lifecycle='DELETING',
                changestate='APPLYING')
        backup = common.create_test_backup(instance, lifecycle='CREATING',
                changestate='APPLYING')
        self.addCleanup(db._delete_db_obj, '', instance)
        self.addCleanup(db._delete_db_obj, '', backup)
        body = {}
        body['backup_id'] = backup.id
        resp = self.controller.mark_final_backup_completed(self.mock_req, body)
        assert resp.status_code == 200
        updated_backup = db._get_by_id(self.mock_context,
                                       models.Backup,
                                       backup.id)
        assert updated_backup.lifecycle == 'ACTIVE'
        assert updated_backup.changestate == 'NONE'

    # Ideally, each function should be tested in a different test
    def test_backup_mark_completed_manual(self):
        instance = common.create_test_instance(lifecycle='BACKUP',
                changestate='APPLYING')
        backup = common.create_test_backup(instance, lifecycle='CREATING',
                changestate='APPLYING')
        execution_id = str(uuid.uuid4())
        workflow = db.create_workflow_record(execution_id, 'CREATE_BACKUP', instance.id, backup.id)
        self.addCleanup(db._delete_db_obj, '', instance)
        self.addCleanup(db._delete_db_obj, '', backup)
        self.addCleanup(db._delete_db_obj, '', workflow)

        body = {}
        body['backup_id'] = backup.id
        body['execution_id'] = execution_id
        resp = self.controller.backup_mark_completed(self.mock_req, body)
        assert resp.status_code == 200
        updated_backup = db._get_by_id(self.mock_context,
                                       models.Backup,
                                       backup.id)
        assert updated_backup.lifecycle == 'ACTIVE'
        assert updated_backup.changestate == 'NONE'

        updated_instance = db._get_by_id(self.mock_context,
                models.Instance, instance.id)
        assert updated_instance.lifecycle == 'ACTIVE'
        assert updated_instance.changestate == 'NONE'

        updated_workflow = db._get_by_id(self.mock_context,
                                         models.Workflow,
                                         workflow.id)

        assert updated_workflow.state == 'SUCCESS'

    def test_backup_mark_completed_automated(self):
        instance = common.create_test_instance(lifecycle='BACKUP',
                changestate='APPLYING')
        backup = common.create_test_backup(instance, lifecycle='CREATING',
                changestate='APPLYING', type='AUTOMATED')
        execution_id = str(uuid.uuid4())
        workflow = db.create_workflow_record(execution_id, 'CREATE_BACKUP', instance.id, backup.id)
        self.addCleanup(db._delete_db_obj, '', instance)
        self.addCleanup(db._delete_db_obj, '', backup)
        self.addCleanup(db._delete_db_obj, '', workflow)

        body = {}
        body['backup_id'] = backup.id
        body['execution_id'] = execution_id
        resp = self.controller.backup_mark_completed(self.mock_req, body)
        assert resp.status_code == 200
        updated_backup = db._get_by_id(self.mock_context,
                                       models.Backup,
                                       backup.id)
        assert updated_backup.lifecycle == 'ACTIVE'
        assert updated_backup.changestate == 'NONE'

        updated_instance = db._get_by_id(self.mock_context,
                models.Instance, instance.id)
        assert updated_instance.lifecycle == 'ACTIVE'
        assert updated_instance.changestate == 'NONE'
        db._hard_delete_by_id(self.mock_context, models.Backup, backup.id)

        updated_workflow = db._get_by_id(self.mock_context,
                                         models.Workflow,
                                         workflow.id)

        assert updated_workflow.state == 'SUCCESS'

    def test_restore_mark_completed(self):
        # First test happy case
        instance = common.create_test_instance(lifecycle='RESTORING',
        changestate='APPLYING')
        backup = common.create_test_backup(instance, lifecycle='RESTORING',
                changestate='APPLYING')
        execution_id = str(uuid.uuid4())
        workflow = db.create_workflow_record(execution_id, 'RESTORE_INSTANCE', instance.id, backup.id)

        body = {}
        body['backup_id'] = backup.id
        body['instance_id'] = instance.id
        body['execution_id'] = execution_id
        resp = self.controller.restore_mark_completed(self.mock_req, body)

        assert resp.status_code == 200
        inst = db._get_by_id(self.mock_context,
                models.Instance, instance.id)
        bkp = db._get_by_id(self.mock_context,
                               models.Backup,
                               backup.id)
        workflow = db.get_workflow_record_by_execution_id(execution_id)

        self.assertEqual('ACTIVE', inst.lifecycle)
        self.assertEqual('NONE', inst.changestate)
        self.assertEqual('ACTIVE', bkp.lifecycle)
        self.assertEqual('NONE', bkp.changestate)
        self.assertEqual('SUCCESS', workflow.state)

        # If instance or backup is not in restoring/applying state, call should
        # fail
        instance = common.create_test_instance()
        backup = common.create_test_backup(instance)
        self.addCleanup(db._delete_db_obj, '', instance)
        self.addCleanup(db._delete_db_obj, '', backup)
        self.addCleanup(db._delete_db_obj, '', workflow)
        self.assertRaises(Exception, db.restore_mark_completed, '', instance.id,
            backup.id)

        db._delete_db_obj('', inst)
        db._delete_db_obj('', bkp)

    def test_multiple_same_name_backups(self):
        instance = common.create_test_instance()
        bkp_name = uuid.uuid4().hex
        for i in range(0, 5):
            backup = db.backup_create_from_instance('', 'MANUAL', instance, bkp_name)
            updated_bkp = db._update_by_db_object(backup, lifecycle='DELETING', changestate='APPLYING')
            execution_id = str(uuid.uuid4())
            workflow = db.create_workflow_record(execution_id, 'DELETE_BACKUP', instance.id, backup.id)

            body = {}
            body['backup_id'] = backup.id
            body['execution_id'] = execution_id
            resp = self.controller.backup_mark_deletion_completed(self.mock_req, body)
            assert resp.status_code == 200

            bkp = db._get_by_id(self.mock_context, models.Backup, backup.id)
            self.assertEqual('DELETING', bkp.lifecycle)
            self.assertEqual('NONE', bkp.changestate)
            self.assertEqual((i+1), bkp.deleted)

        same_name_backups = db._find_all_same_name_values(table=models.Backup, owner_id=instance.owner_id, value_name=bkp_name)
        self.assertEqual(len(same_name_backups), 5)


    def test_backup_mark_deletion_completed(self):
        # First test happy case
        instance = common.create_test_instance()
        backup = common.create_test_backup(instance, lifecycle='DELETING',
                changestate='APPLYING')
        execution_id = str(uuid.uuid4())
        workflow = db.create_workflow_record(execution_id, 'DELETE_BACKUP', instance.id, backup.id)

        body = {}
        body['backup_id'] = backup.id
        body['execution_id'] = execution_id
        resp = self.controller.backup_mark_deletion_completed(self.mock_req, body)

        assert resp.status_code == 200
        bkp = db._get_by_id(self.mock_context,
                               models.Backup,
                               backup.id)
        workflow = db.get_workflow_record_by_execution_id(execution_id)
        self.assertEqual('DELETING', bkp.lifecycle)
        self.assertEqual('NONE', bkp.changestate)
        self.assertEqual(1, bkp.deleted)
        self.assertEqual('SUCCESS', workflow.state)


        # If backup is not in deleting/applying state, call should fail
        instance = common.create_test_instance()
        backup = common.create_test_backup(instance)
        self.addCleanup(db._delete_db_obj, '', instance)
        self.addCleanup(db._delete_db_obj, '', backup)
        self.addCleanup(db._delete_db_obj, '', workflow)
        self.assertRaises(Exception, db.restore_mark_completed, '', instance.id,
            backup.id)

        db._delete_db_obj('', bkp)

    def test_backup_increment_tries(self):
        # Create a backup entry first
        backup = db.backup_create_from_instance('', 'AUTOMATED',
                                                self.data['instance'],
                                                uuid.uuid4().hex)
        body = {}
        body['backup_id'] = backup.id

        i = 0
        while i < 9:

            resp = self.controller.backup_increment_tries(self.mock_req, body)
            assert resp.status_code == 200
            updated_backup = db._get_by_id(self.mock_context,
                                       models.Backup,
                                       backup.id)
            i = i + 1
            assert updated_backup.num_backup_tries == i
            assert json.loads(resp.body)['retry'] == 1



        resp = self.controller.backup_increment_tries(self.mock_req, body)
        assert resp.status_code == 200
        assert json.loads(resp.body)['retry'] == 0
        updated_backup = db._get_by_id(self.mock_context,
                                       models.Backup,
                                       backup.id)
        assert updated_backup.num_backup_tries == 9
        db._hard_delete_by_id(self.mock_context, models.Backup, backup.id)

    @mock.patch.object(db, 'backup_mark_completed',
            side_effect=db_exc.SQLAlchemyError, autospec=True)
    @mock.patch.object(db, 'backup_increment_tries',
            side_effect=db_exc.SQLAlchemyError, autospec=True)
    @mock.patch.object(traceback, 'print_exc', autospec=True)
    def test_negative(self, mock_print, mock_db_tries,  mock_db_mark):
        # Test all the error paths
        body = {}
        body['backup_id'] = 'bkp_id'
        resp = self.controller.backup_mark_completed(self.mock_req, body)
        isinstance(resp, webob_exc.HTTPInternalServerError)
        mock_db_mark.assert_called_once_with(self.mock_context, 'bkp_id')

        resp = self.controller.backup_increment_tries(self.mock_req, body)
        isinstance(resp, webob_exc.HTTPInternalServerError)
        mock_db_tries.assert_called_once_with(self.mock_context, 'bkp_id', 10)

    def test_create_volume_snapshot_in_db(self):
        data = common.set_up_class_helper()

        data['volume'] = db.volume_create('',
                                      uuid.uuid4(),
                                      data['host'].id,
                                      'SSD',
                                      'ATTACHED')
        backup = db.backup_create_from_instance('', 'AUTOMATED',
                                                data['instance'],
                                                uuid.uuid4().hex)
        body = {}
        body['volume_id'] = data['volume'].id
        body['backup_id'] = backup.id
        body['cinder_snapshot_id'] = str(uuid.uuid4())

        resp = self.controller.create_volume_snapshot_in_db(self.mock_req,
                                                             body)

        self.assertEqual(resp.status_code, 200)
        vs = db.volume_snapshot_get_by_backup_id('', backup.id)

        self.assertEqual(body['cinder_snapshot_id'], vs.cinder_snapshot_id)

        db._hard_delete_by_id('', models.VolumeSnapshot, vs.id)
        db._hard_delete_by_id('', models.Backup, backup.id)

        db._hard_delete_by_id('', models.Volume, data['volume'].id)
        common.tear_down_class_helper(data)

    def test_delete_volume_snapshot_in_db(self):
        data = common.set_up_class_helper()

        data['volume'] = db.volume_create('',
                                          uuid.uuid4(),
                                          data['host'].id,
                                          'SSD',
                                          'ATTACHED')
        backup = db.backup_create_from_instance('', 'AUTOMATED',
                                                data['instance'],
                                                uuid.uuid4().hex)
        body = {}
        body['volume_id'] = data['volume'].id
        body['backup_id'] = backup.id
        body['cinder_snapshot_id'] = str(uuid.uuid4())

        resp = self.controller.create_volume_snapshot_in_db(self.mock_req,
                                                            body)

        self.assertEqual(resp.status_code, 200)
        vs = db.volume_snapshot_get_by_backup_id('', backup.id)
        self.assertEqual(body['cinder_snapshot_id'], vs.cinder_snapshot_id)

        resp = self.controller.delete_volume_snapshot_in_db(self.mock_req,
                                                            backup.id)

        try:
            vs = db.volume_snapshot_get_by_backup_id('', backup.id)
            self.assertEqual("1", "2")
        except exception.VolumeSnapshotNotFoundByBackupId:
            pass
        self.assertEqual(resp.status_code, 200)

        db._hard_delete_by_id('', models.VolumeSnapshot, vs.id)
        db._hard_delete_by_id('', models.Backup, backup.id)
        db._hard_delete_by_id('', models.Volume, data['volume'].id)
        common.tear_down_class_helper(data)

    def test_get_cinder_snapshot_id_by_backup_id(self):
        data = common.set_up_class_helper()

        data['volume'] = db.volume_create('',
                                      uuid.uuid4(),
                                      data['host'].id,
                                      'SSD',
                                      'ATTACHED')
        backup = db.backup_create_from_instance('', 'AUTOMATED',
                                                data['instance'],
                                                uuid.uuid4().hex)
        body = {}
        body['volume_id'] = data['volume'].id
        body['backup_id'] = backup.id
        body['cinder_snapshot_id'] = str(uuid.uuid4())

        resp = self.controller.create_volume_snapshot_in_db(self.mock_req,
                                                             body)

        self.assertEqual(resp.status_code, 200)

        # This is only change from the above test case rest everything is same
        resp = self.controller.get_cinder_snapshot_id_by_backup_id(self.mock_req,
                                                                   backup.id)

        result = json.loads(resp.body)

        self.assertEqual(body['cinder_snapshot_id'], result['cinder_snapshot_id'])

        vs = db.volume_snapshot_get_by_backup_id('', backup.id)
        db._hard_delete_by_id('', models.VolumeSnapshot, vs.id)
        db._hard_delete_by_id('', models.Backup, backup.id)

        db._hard_delete_by_id('', models.Volume, data['volume'].id)
        common.tear_down_class_helper(data)

    def test_populate_endpoint_in_db(self):
        endpoint = uuid.uuid4().hex
        prv_endpoint = self.data['instance'].endpoint
        body = {}
        body['instance_id'] = self.data['instance'].id
        body['endpoint'] = endpoint


        bucket_body = {}
        bucket_body['bucket_name'] = "test_bucket"
        bucket_body['state'] = "ACTIVE"
        bucket_body['customer_id'] = 1
        bucket_body['policy_id'] = "fake-policy-id"

        bucket = common.create_test_bucket(bucket_body)

        body['bucket_id'] = bucket.id


        resp = self.controller.populate_endpoint_bucketid_in_db(self.mock_req,body)
        instance = db._get_by_id('', models.Instance, self.data['instance'].id)
        self.assertEqual(instance.endpoint, endpoint)
        self.assertEqual(instance.bucket_id, body['bucket_id'])

        body['endpoint'] = prv_endpoint
        resp = self.controller.populate_endpoint_bucketid_in_db(self.mock_req,body)
        instance = db._get_by_id('', models.Instance, self.data['instance'].id)
        self.assertEqual(instance.endpoint, prv_endpoint)

        db._hard_delete_by_id('', models.Buckets, body['bucket_id'])


    def test_get_default_parameters(self):
        # Note: This test is tightly tied with the contents of populate_seed_data.py
        # Creates an instance with storage_size = 20 GB, nova_flavor = c1.4xlarge (120 GB RAM)
        instance = common.create_test_instance()
        self.addCleanup(db._delete_db_obj, '', instance)

        resp = self.controller.get_default_parameters(self.mock_req, instance.id)
        resp_dict = json.loads(resp.body)

        for parameter in resp_dict['parameters']:
            if parameter['name'] == 'innodb_buffer_pool_size':
                self.assertEquals(parameter['value'], 120 * 1024 * 1024 * 1024 * 3 / 4)
                self.assertEquals(parameter['type'], 'static')
            if parameter['name'] == 'innodb_flush_method':
                self.assertEquals(parameter['value'], 'O_DIRECT')
                self.assertEquals(parameter['type'], 'static')
            if parameter['name'] == 'max_connections':
                self.assertEquals(parameter['value'], '151')
                self.assertEquals(parameter['type'], 'dynamic')
            if parameter['name'] == 'innodb_log_buffer_size':
                self.assertEquals(parameter['value'], 20 * 1024 * 1024)
                self.assertEquals(parameter['type'], 'dynamic')
