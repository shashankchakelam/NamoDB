from oslo_config import cfg
from oslo_db import options
CONF = cfg.CONF

connection_str = 'mysql://root:kaka123@127.0.0.1/namo?charset=utf8'

def set_defaults():
    options.set_defaults(CONF,
                     connection=connection_str)
