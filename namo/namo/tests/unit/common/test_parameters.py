import unittest

from namo.common import parameters

class ParametersTestCase(unittest.TestCase):
    def test_calculate_param_values_no_formula(self):
        param_value = "256KB"
        calculated_value = parameters.calculate_param_value(param_value, {})
        self.assertEquals(param_value, calculated_value)

    def test_calculate_param_values_with_formula_happy_case(self):
        param_value = "GREATEST({DBInstanceClassMemory / 2}, 10)"
        param_variables = {'DBInstanceClassMemory': '100'}
        calculated_value = parameters.calculate_param_value(param_value, param_variables)
        self.assertEquals(calculated_value, 50)

    def test_calculate_param_values_with_formula_unsupported_chars(self):
        param_value = "GREATEST({DBInstanceClassMemory / 2}, Hello)"
        param_variables = {'DBInstanceClassMemory': '100'}

        with self.assertRaises(ValueError) as context_manager:
            parameters.calculate_param_value(param_value, param_variables)

        message = "Invalid expression for parameter:"
        self.assertIn(message, str(context_manager.exception))

    def test_calculate_param_values_with_formula_multiple_variables(self):
        param_value = "LEAST({DBInstanceClassMemory / 2}, {AllocatedStorage / 100})"
        param_variables = {'DBInstanceClassMemory': '100', 'AllocatedStorage': '5100'}
        calculated_value = parameters.calculate_param_value(param_value, param_variables)
        self.assertEquals(calculated_value, 50)

    def test_calculate_param_values_with_formula_multiple_functions(self):
        param_value = "LEAST({DBInstanceClassMemory / 2}, GREATEST({AllocatedStorage / 100}, 100))"
        param_variables = {'DBInstanceClassMemory': '100', 'AllocatedStorage': '5100'}
        calculated_value = parameters.calculate_param_value(param_value, param_variables)
        self.assertEquals(calculated_value, 50)

    def test_calculate_param_values_truncates_expression_value(self):
        param_value = "{EndPointPort / 2}"
        param_variables = {'DBInstanceClassMemory': '100', 'EndPointPort': '99'}
        calculated_value = parameters.calculate_param_value(param_value, param_variables)
        self.assertEquals(calculated_value, 49)

    def test_calculate_param_values_with_formula_all_arithmetic_operators(self):
        param_value = "{5 + {DBInstanceClassMemory*2/3} + 3*3}"
        param_variables = {'DBInstanceClassMemory': '100'}
        calculated_value = parameters.calculate_param_value(param_value, param_variables)
        self.assertEquals(calculated_value, 80)