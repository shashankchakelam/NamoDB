from datetime import datetime
import os
import uuid

import mock

from namo.common import exception
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models
from namo.tests.unit import conf_fixture

from oslo_config import cfg
from oslo_db import options

import sqlalchemy

CONF = cfg.CONF

conf_fixture.set_defaults()

models.register_models(conf_fixture.connection_str)

import unittest

def populate_seed_data_to_db():
    oses = []
    oses.append(db.create_os('', 'ubuntu', '12.04', '1', 'll', 'ACTIVE'))
    oses.append(db.create_os('', 'ubuntu', '12.04', '2', 'll', 'PREFERRED'))
    oses.append(db.create_os('', 'windows', '8', '1', 'll', 'ACTIVE'))
    oses.append(db.create_os('', 'windows', '8', '2', 'll', 'PREFERRED'))

    db_engines = []
    db_engines.append(db.create_db_engine('', 'mysql', '5.5', '1', 'll', 'ACTIVE'))
    db_engines.append(db.create_db_engine('', 'mysql', '5.5', '2', 'll', 'ACTIVE'))
    db_engines.append(db.create_db_engine('', 'mysql', '5.6', '1', 'll', 'ACTIVE'))
    db_engines.append(db.create_db_engine('', 'mysql', '5.6', '2', 'll', 'PREFERRED'))
    db_engines.append(db.create_db_engine('', 'oracle', '12.1', '2', 'll', 'ACTIVE'))
    db_engines.append(db.create_db_engine('', 'oracle', '12.1', '3', 'll', 'PREFERRED'))
    db_engines.append(db.create_db_engine('', 'postgresql', '9.3', '1', 'll', 'ACTIVE'))
    db_engines.append(db.create_db_engine('', 'postgresql', '9.3', '2', 'll', 'ACTIVE'))
    db_engines.append(db.create_db_engine('', 'postgresql', '9.5', '1', 'll', 'ACTIVE'))
    db_engines.append(db.create_db_engine('', 'postgresql', '9.5', '3', 'll', 'PREFERRED'))

    for i in range(1,4):
        db.create_default_parameter_record(db_engines[i].id, 'innodb_buffer_pool_size', '{DBInstanceClassMemory*3/4}', 'static')
        db.create_default_parameter_record(db_engines[i].id, 'innodb_flush_method', 'O_DIRECT', 'static')
        db.create_default_parameter_record(db_engines[i].id, 'max_connections', '151', 'dynamic')
        db.create_default_parameter_record(db_engines[i].id, 'innodb_log_buffer_size', '{AllocatedStorage / 1024}', 'dynamic')

    for i in range(7, 10):
        db.create_default_parameter_record(db_engines[i].id, 'shared_buffers', '{DBInstanceClassMemory/32768}', 'static')
        db.create_default_parameter_record(db_engines[i].id, 'max_stack_depth', '6144', 'dynamic')
        db.create_default_parameter_record(db_engines[i].id, 'checkpoint_completion_target', '0.9', 'dynamic')

    ragas = []
    ragas.append(db.create_raga('', str(uuid.uuid4()).split('-')[0], 'll', 'ACTIVE'))
    ragas.append(db.create_raga('', str(uuid.uuid4()).split('-')[0], 'll', 'PREFERRED'))

    for raga in ragas:
        for db_engine in db_engines:
            for os in oses:
                if os.name == 'ubuntu' and db_engine.name == 'mysql':
                    artefact = db.create_artefact('', raga.id, db_engine.id, os.id)
                    image = db.image_create('', str(uuid.uuid4()), artefact.id)
                elif os.name == 'ubuntu' and db_engine.name == 'postgresql':
                    artefact = db.create_artefact('',raga.id,db_engine.id,os.id)
                    image = db.image_create('', str(uuid.uuid4()), artefact.id)


    keypair = db.keypair_create('', 'nova_keypair_name', 'nova_private_key_bytes')
    db.nova_flavor_create('', 'c1.xlarge', 'c1.xlarge', '30', '8')
    db.nova_flavor_create('', 'c1.4xlarge', 'c1.4xlarge', '120', '32')

    # giving global values, this entry must always be present in the
    # metadata backend during the actual run

    db.user_quota_create_entry_from_user_supplied_values('', -1, 'total_instances', 5)
    db.user_quota_create_entry_from_user_supplied_values('', -1, 'total_snapshots', 25)
    db.user_quota_create_entry_from_user_supplied_values('', -1, 'total_storage', 5120)
    db.user_quota_create_entry_from_user_supplied_values('', -1, 'total_c1.2xlarge_instances', 0)
    db.user_quota_create_entry_from_user_supplied_values('', -1, 'total_c1.4xlarge_instances', 0)

    #Entering prepoulated data for Dp_operations
    db.create_entry_for_dp_operations(None, 'This operation allows apply immediately modifications to the Db instance','MODIFY_INSTANCE','NOT_SCHEDULED',0,'IN-PROGRESS')

    master = db._mfw_create('')
    auto_bkp = db.create_infinite_workflow_record('AUTO_BACKUP')
    clean_auto_bkp = db.create_infinite_workflow_record('CLEAN_AUTO_BACKUP')

if __name__ == '__main__':
    populate_seed_data_to_db()
