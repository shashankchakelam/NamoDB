You need a database running locally to run tests. SQLite should probably be
fine too. You can edit connection string at namo/tests/unit/conf_fixture.py
before running tests (even if you're running the tests in 'smoke' directory),
in case your local database password is different from 'kaka123'

Running tests
-------------
To run tests. Note that a couple steps might be redundant:

    sudo pip install virtualenv
    cd NamoDB/namo/
    ./run_tests.sh

Alternative, if you want to run a specific test, or tests under a directory/file/class:

    pip install testtools # One time only
    source .venv/bin/activate
    python namo/tests/unit/populate_seed_data.py # One time only
    python -m testtools.run namo.tests.unit.workflowapi.v1.test_persistence_controller.TestPersistenceController.test_positive

Writing tests
------------
* Use mock
* Always provide `autospec=True` in mock. TODO: add the blog post link about autospec
* Add positive and negative tests both
