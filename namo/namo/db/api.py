# Copyright (c) 2011 X.commerce, a business unit of eBay Inc.
# Copyright 2010 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""Defines interface for DB access.

Functions in this module are imported into the namo.db namespace. Call these
functions from namo.db namespace, not the namo.db.api namespace.

All functions in this module return objects that implement a dictionary-like
interface. Currently, many of these objects are sqlalchemy objects that
implement a dictionary interface. However, a future goal is to have all of
these objects be simple dictionaries.


**Related Flags**

:connection:  string specifying the sqlalchemy connection to use, like:
              `sqlite:///var/lib/namo/namo.sqlite`.

:enable_new_services:  when adding a new service to the database, is it in the
                       pool of available hardware (Default: True)

"""

from oslo_config import cfg
from oslo_db import concurrency as db_concurrency
from oslo_db import options as db_options


#from namo.db.sqlalchemy.models import Instance

db_opts = [
    cfg.BoolOpt('enable_new_services',
                default=True,
                help='Services to be added to the available pool on create'),
    cfg.StrOpt('volume_name_template',
               default='volume-%s',
               help='Template string to be used to generate volume names'),
    cfg.StrOpt('snapshot_name_template',
               default='snapshot-%s',
               help='Template string to be used to generate snapshot names'),
    cfg.StrOpt('backup_name_template',
               default='backup-%s',
               help='Template string to be used to generate backup names'), ]


CONF = cfg.CONF
CONF.register_opts(db_opts)
# db_options.set_defaults(CONF)
# CONF.set_default('connection', 'mysql -h admindb.cdjf19y42e59.ap-southeast-1.rds.amazonaws.com AdminDB -P 3306 -u dbaas -pHello123*', group='database')
# CONF.set_default('sqlite_db', 'namo.sqlite', group='database')
# db_options.set_defaults(CONF)
# CONF.set_default('sqlite_db', 'namo.sqlite', group='database')
_BACKEND_MAPPING = {'sqlalchemy': 'namo.db.mysqlalchemy.api'}
IMPL = db_concurrency.TpoolDbapiWrapper(CONF, backend_mapping=_BACKEND_MAPPING)


def dispose_engine():
    """Force the engine to establish new connections."""
    # FIXME(jdg): When using sqlite if we do the dispose
    # we seem to lose our DB here.  Adding this check
    # means we don't do the dispose, but we keep our sqlite DB
    # This likely isn't the best way to handle this
    if 'sqlite' not in IMPL.get_engine().name:
        return IMPL.dispose_engine()
    else:
        return

def masterworkflow_check_and_update(context, id):
    return IMPL.masterworkflow_check_and_update(context, id)

def masterworkflow_get(context):
    return IMPL.masterworkflow_get(context)

# TODO(rushiagr): separate defs by only one line and not two
def instance_create(context, owner_id, data):
    """ Create a instance or raise if it does not create. """
    return IMPL.instance_create(context, owner_id, data)


def instance_destroy(context, owner_id, name):
    """ Destroy the instance or raise if it does not exist. """
    return IMPL.instance_destroy(context, owner_id, name)


def instance_get(context, owner_id, name):
    """ Get a instance or raise if it does not exist. """
    return IMPL.instance_get(context, owner_id, name)


def instance_get_all(context, owner_id, disabled=None):
    """ Get all instances. """
    return IMPL.instance_get_all(context, owner_id, disabled)


def instance_get_n_pending(context , n):
    return IMPL.instance_get_n_pending(context, n)


def instance_update(context, instance_id, values):
    """ Set the given properties on an instance and update it.
        Raises NotFound if instance does not exist.
    """
    return IMPL.instance_update(context, instance_id, values)


def keypair_create(context, nova_key_id, private_key):
    """ Create a key pair or raise if it does not create. """
    return IMPL.keypair_create(context, nova_key_id, private_key)


def keypair_get(context, loca_key_id):
    """ Get a key pair or raise if it does not exist. """
    return IMPL.keypair_get(context, loca_key_id)


def keypair_destroy(context, loca_key_id):
    """ Destroy the key pair or raise if it does not exist. """
    return IMPL.keypair_destroy(context, loca_key_id)


def keypair_update(context, loca_key_id, nova_key_id, private_key):
    """ Set the given properties on an key pair and update it.
        Raises NotFound if key pair does not exist.
    """
    return IMPL.keypair_update(context, loca_key_id, nova_key_id, private_key)


def nova_flavor_create(context, nova_flavor_id, flavor_name):
    """ Create a flavor or raise if it does not create. """
    return IMPL.nova_flavor_create(context, nova_flavor_id, flavor_name)


def nova_flavor_get(context, flavor_name):
    """ Get a flavor or raise if it does not exist. """
    return IMPL.nova_flavor_get(context, flavor_name)


def nova_flavor_destroy(context, flavor_name):
    """ Destroy the flavor or raise if it does not exist. """
    return IMPL.nova_flavor_destroy(context, flavor_name)


def nova_flavor_update(context, nova_flavor_id, flavor_name):
    """Set the given properties on an flavor and update it.
    Raises NotFound if flavor does not exist.

    """
    return IMPL.nova_flavor_update(context, nova_flavor_id, flavor_name)

###################

def image_create(context, nova_key_id, private_key):
    """Create a key pair or raise if it does not create."""
    return IMPL.image_create(context, nova_key_id, private_key)

def image_get(context, database_engine, database_engine_version, os_version):
    """Get a key pair or raise if it does not exist."""
    return IMPL.image_get(context, database_engine, database_engine_version, os_version)

def image_destroy(context, database_engine, database_engine_version, os_version):
    """Destroy the key pair or raise if it does not exist."""
    return IMPL.image_destroy(context, database_engine, database_engine_version, os_version)

def image_update(context, database_engine, database_engine_version, os_version, glance_uuid):
    """Set the given properties on an key pair and update it.
    Raises NotFound if key pair does not exist.

    """
    return IMPL.image_update(context, database_engine, database_engine_version, os_version, glance_uuid)

###################

def host_create(context, instance_id, keypair_id, az, nova_uuid, flavor_id, ip,
        ip_id, artefact_id):
    """Create a key pair or raise if it does not create."""
    return IMPL.host_create(context, instance_id, keypair_id, az, nova_uuid,
            flavor_id, ip, ip_id, artefact_id)

def host_get(context, id):
    """Get a key pair or raise if it does not exist."""
    return IMPL.host_get(context, id)

def host_get_by_instance_id(context, instance_id):
    """Get a key pair or raise if it does not exist."""
    return IMPL.host_get_by_instance_id(context, instance_id)

def host_destroy(context, instance_id):
    """Destroy the key pair or raise if it does not exist."""
    return IMPL.host_destroy(context, instance_id)

def host_update(context, instance_id, keypair_id, az, nova_uuid, flavor_id, ip, ip_id):
    """Set the given properties on an key pair and update it.
    Raises NotFound if key pair does not exist.

    """
    return IMPL.host_update(context, instance_id, keypair_id, az, nova_uuid, flavor_id, ip, ip_id)

###################

def volume_create(context, cinder_uuid, host_id, volume_type, attach_status):
    """Create a key pair or raise if it does not create."""
    return IMPL.volume_create(context, cinder_uuid, host_id, volume_type, attach_status)

def volume_get(context, volume_id, cinder_uuid, host_id, volume_type):
    """
    :param context: http request
    :param volume_id: nova volume id
    :param host_id: nova host id
    :param volume_type: volume type
    :return:
    Get a volume or raise if it does not exist. """
    return IMPL.volume_get(context, volume_id, cinder_uuid, host_id, volume_type)

def volume_get_by_host_id(context, host_id):
    """
    :param context: http request
    :param id: local volume id
    :return: volume object
    """
    return IMPL.volume_get_by_host_id(context, host_id)

def volume_get_by_id(context, id):
    """
    :param context: http request
    :param id: local volume id
    :return: volume object
    """
    return IMPL.volume_get_by_id(context, id)

def volume_destroy(context, id):
    """Destroy the key pair or raise if it does not exist."""
    return IMPL.volume_destroy(context, id)

def volume_update(context, volume_id, cinder_uuid, host_id, volume_type, attach_status):
    """Set the given properties on an key pair and update it.
    Raises NotFound if key pair does not exist.

    """
    return IMPL.volume_update(context, volume_id, cinder_uuid, host_id, volume_type, attach_status)

def volume_change_status(context, id, attach_status):
    return IMPL.volume_change_status(context, id, attach_status)

###################









if __name__ == '__main__':
    print 'ola!'
