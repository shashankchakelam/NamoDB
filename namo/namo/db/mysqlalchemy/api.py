# Copyright (c) 2011 X.commerce, a business unit of eBay Inc.
# Copyright 2010 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# Copyright 2014 IBM Corp.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""Implementation of SQLAlchemy backend."""

import sys
import threading
import warnings

from datetime import timedelta
from namo.db.mysqlalchemy import models
from namo.common import exception
from namo.i18n import _
from oslo_config import cfg
from oslo_db import options
from oslo_db.sqlalchemy import session as db_session
from oslo_log import log as logging
from oslo_utils import timeutils
from sqlalchemy import or_, and_, not_, between, func
from sqlalchemy.sql.expression import literal
from sqlalchemy.orm import joinedload, exc as orm_exc
import json

CONF = cfg.CONF
LOG = logging.getLogger(__name__)

# While running api service or workflow service the below connection is only set
#if default connection is not given in the config file namo-api.conf or namo-workflowapi.conf
# While running test cases the below connection is only set
# if default connection is not give in NamoDB/namo/namo/tests/unit/conf_fixture.py
options.set_defaults(CONF,
                     connection='mysql://root:kaka123@127.0.0.1/namo?charset=utf8')

_LOCK = threading.Lock()
_FACADE = None

def _create_facade_lazily():
    global _LOCK
    with _LOCK:
        global _FACADE
        if _FACADE is None:
            _FACADE = db_session.EngineFacade(
                CONF.database.connection,
                **dict(CONF.database)
            )
#            if CONF.profiler.profiler_enabled:
#                if CONF.profiler.trace_sqlalchemy:
#                    osprofiler.sqlalchemy.add_tracing(sqlalchemy,
#                                                      _FACADE.get_engine(),
#                                                      "db")
        return _FACADE

def get_engine():
    facade = _create_facade_lazily()
    return facade.get_engine()

def get_session(**kwargs):
    facade = _create_facade_lazily()
    return facade.get_session(**kwargs)

def dispose_engine():
    get_engine().dispose()

_DEFAULT_QUOTA_NAME = 'default'

def get_backend():
    """The backend is this module itself."""

    return sys.modules[__name__]

# TODO(rushiagr): do we really need to use this model_query helper method? This
# helper method seems to be handling case when there is a 'deleted' boolean
# column in the table, which we're not going to have


def model_query(context, *args, **kwargs):
    """Query helper that accounts for context's `read_deleted` field.

    :param context: context to query under
    :param *args: main query argument
    :param session: if present, the session to use

    """
    session = kwargs.get('session') or get_session()
    if(kwargs.has_key('session')):
        kwargs.pop('session')

    # As we are calling this again and again from some other methods
    # on which we want sub-transactions to be incorporated

    # Starting sub-transaction here.
    with session.begin(subtransactions=True):
        # use with_for_update so as to lock the row until session is completed.
        if len(kwargs)!=0:
            query = session.query(*args).filter_by(**kwargs).with_for_update()
        else:
            query = session.query(*args).with_for_update()
        return query

####Context Check Method Start####

def is_admin_context(context):
    """Indicates if the request context is an administrator."""
    if not context:
        warnings.warn(_('Use of empty request context is deprecated'),
                      DeprecationWarning)
        raise Exception('die')
    return context.is_admin

def is_user_context(context):
    """Indicates if the request context is a normal user."""
    if not context:
        return False
    if context.is_admin:
        return False
    if not context.user_id or not context.project_id:
        return False
    return True

def require_admin_context(f):
    """Decorator to require admin request context.

    The first argument to the wrapped function must be the context.

    """

    def wrapper(*args, **kwargs):
        if not is_admin_context(args[0]):
            raise exception.AdminRequired()
        return f(*args, **kwargs)
    return wrapper

def require_context(f):
    """Decorator to require *any* user or admin context.

    This does no authorization for user or project access matching, see
    :py:func:`authorize_project_context` and
    :py:func:`authorize_user_context`.

    The first argument to the wrapped function must be the context.

    """

    def wrapper(*args, **kwargs):
        if not is_admin_context(args[0]) and not is_user_context(args[0]):
            raise exception.NotAuthorized()
        return f(*args, **kwargs)
    return wrapper



def get_only_entry(context,
                   model,
                   NotFoundException,
                   session,
                   **filter):
    """
    This function will return only if there is one row in the table based on
    filter variable, if there is no results it will throw custom error,
    if there are more than 1 row it will throw multiple result found error
    :param context: Request context
    :param model: Table name
    :param NotFoundException: custom exception to be thrown
    """
    try:
        result = model_query(context,
                            model,
                            session=session).\
                            filter_by(**filter).one()
    except orm_exc.NoResultFound:
        raise NotFoundException(**filter)
    except orm_exc.MultipleResultsFound:
        raise orm_exc.MultipleResultsFound

    return result

####Context Check Method Ends####

def volume_snapshot_create(context,
                            volume_id,
                            backup_id,
                            cinder_snapshot_id):
    vs_ref = models.VolumeSnapshot()
    vs_ref.backup_id = backup_id
    vs_ref.cinder_snapshot_id = cinder_snapshot_id
    vs_ref.volume_id = volume_id
    vs_ref.created_at = timeutils.utcnow()
    vs_ref.updated_at = timeutils.utcnow()

    session = get_session()
    with session.begin():
        vs_ref.save(session)
        return vs_ref


def volume_snapshot_delete(context,
                           backup_id):
    session = get_session()
    with session.begin():
        vs_ref = volume_snapshot_get_by_backup_id(context,
                                                  backup_id,
                                                  session=session)
        vs_ref.destroy(session=session)
        return vs_ref


def volume_snapshot_get_by_backup_id(context,
                                     backup_id,
                                     session=None):

    return get_only_entry(context,
                          models.VolumeSnapshot,
                          exception.VolumeSnapshotNotFoundByBackupId,
                          session,
                          backup_id=backup_id,
                          deleted=0)

###All Operation on Instance Model starts###


def instance_create(context, owner_id, data, is_restore=False, session=None):
    instance_ref = models.Instance()
    instance_ref.update(data)
    instance_ref.owner_id = owner_id
    instance_ref.changestate = 'PENDING'
    instance_ref.lifecycle = 'RESTORING' if is_restore else 'CREATING'
    instance_ref.created_at = timeutils.utcnow()
    instance_ref.updated_at = timeutils.utcnow()
    # TODO(adarsh): Cross check if Owner Id match data Owner
    # Id if doesnt match reply saying error

    # TODO(varun) Don't know why we have instance_ref.save(session)
    # as instance_ref.update(data) insert data into db. Will remove this next time after confirmation.
    session = session or get_session()
    with session.begin(subtransactions=True):
        instance_ref.save(session)
        return instance_ref

def user_quota_create(context, owner_id, data):
    user_quota_ref = models.UserQuota()
    user_quota_ref.created_at = timeutils.utcnow()
    user_quota_ref.updated_at = timeutils.utcnow()
    user_quota_ref.deleted = 0 
    user_quota_ref.update(data)

    session = get_session()
    with session.begin():
        user_quota_ref.save(session)
        return user_quota_ref

def instance_delete(context, owner_id, name, session=None):
    session = session or get_session()
    with session.begin(subtransactions=True):
        max_deleted_count = get_instance_delete_max_count(owner_id=owner_id, instance_name=name, session=session) + 1
        instance_ref = instance_get(context, owner_id, name, session=session)
        instance_ref.destroy(session=session, delete_count=max_deleted_count)
        return instance_ref

def instance_hard_delete(context, owner_id, name, session=None):
    session = session or get_session()
    with session.begin(subtransactions=True):
        instance_ref = instance_get(context, owner_id, name, session=session)
        instance_ref.hard_delete(session=session)
        return instance_ref

# Maintenance window related
def update_task_runner(runner_id, session=None, task_ids=[], query=None):
    '''
       Either of task_id or a query to fetch task_id/s is required.
    '''
    with session.begin(subtransactions=True):
        T = models.DP_Tasks
        if (not task_ids) and (not query) :
            raise exception.DBException("Either of task_ids or query to get task_ids is required")
        if task_ids:
            session.query(T).filter(T.id.in_(task_ids)).update({'task_runner_id': runner_id}, synchronize_session=False)
        else:
            task_ids = query.all();
            for task_id in task_ids:
                session.query(T).filter(T.id == task_id.id).update({'task_runner_id': runner_id}, synchronize_session=False)
        # session.commit()

def update_task_status(task_id, task_status, session=None):
    session = session or get_session()
    with session.begin(subtransactions=True):
        T = models.DP_Tasks
        session.query(T).filter(T.id == task_id).update({'state': task_status}, synchronize_session=False)

def update_task_status_from_progress_to_success_by_instance_id(instance_id, task_status, session=None):
    session = session or get_session()
    with session.begin(subtransactions=True):
        T = models.DP_Tasks
        session.query(T).filter(T.instance_id == instance_id).filter(T.state == 'IN-PROGRESS').update({'state': task_status}, synchronize_session=False)


def get_dp_operations(session=None, operation_type='MAINTENANCE_WINDOW', status=['IN-PROGRESS']):
    session = session or get_session()
    with session.begin(subtransactions=True):
       O = models.DP_Operations
       query = session.query(O).filter(O.operation_type == operation_type).filter(O.status.in_(status)).order_by(O.priority.desc())
       return query.all()

def get_pending_dp_task_ids_query(operation_id = None, instance_ids = [], session=None):
    '''
        select id from dp_tasks where instance_id in [<input>] AND operation_id = <in> AND state = 'PENDING'
        AND instance_id not in (select distinct instance_id from dp_tasks where task_runner_id IS NOT NULL OR state = 'IN-PROGRESS')
    '''
    session = session or get_session()
    with session.begin(subtransactions=True):
       T = models.DP_Tasks
       query = session.query(T.id)
       query = query.filter(T.state == 'PENDING')
       if instance_ids:
           query = query.filter(T.instance_id.in_(instance_ids))
       if operation_id:
           query = query.filter(T.operation_id == operation_id)
       subquery = session.query(T.instance_id).distinct(T.instance_id)
       # subquery = subquery.filter(or_(T.state == 'IN-PROGRESS', T.task_runner_id != None)).subquery()
       subquery = subquery.filter(T.state == 'IN-PROGRESS').subquery()
       final_query = query.filter(T.instance_id.notin_(subquery))
       LOG.debug('Query for get_pending_dp_task_ids: %s'%(str(final_query)))
       return final_query

def insert_into_dp_tasks(instance,new_values_for_workflow_list,session=None):

    operations_info = get_dp_operations(session=session, operation_type='NOT_SCHEDULED')
    dp_tasks_ref = models.DP_Tasks()
    for operation in operations_info:
        dp_tasks_ref.operation_id = operation.id
    dp_tasks_ref.instance_id = instance.id
    dp_tasks_ref.created_at = timeutils.utcnow()
    dp_tasks_ref.updated_at = timeutils.utcnow()
    dp_tasks_ref.task_data = str(json.dumps(new_values_for_workflow_list))
    dp_tasks_ref.state = 'PENDING'

    session = session or get_session()
    with session.begin(subtransactions=True):
        dp_tasks_ref.save(session)
        return dp_tasks_ref

def create_entry_for_dp_operations(session, description, operation_name, operation_type, priority, status):
    dp_operations_ref = models.DP_Operations()
    dp_operations_ref.created_at = timeutils.utcnow()
    dp_operations_ref.updated_at = timeutils.utcnow()
    dp_operations_ref.desciption = description
    dp_operations_ref.operation_name = operation_name
    dp_operations_ref.operation_type = operation_type
    dp_operations_ref.status = status
    dp_operations_ref.priority = priority

    session = session or get_session()
    with session.begin(subtransactions=True):
        dp_operations_ref.save(session)
        return dp_operations_ref

### Backups related 

def add_backup_window_query(query):
    I = models.Instance
    if query is None:
        return query
    filtered_query = query.filter(\
     or_(\
        and_(\
             I.preferred_backup_window_start < I.preferred_backup_window_end,\
             between(func.UTC_TIME(), I.preferred_backup_window_start, I.preferred_backup_window_end)\
        ),\
        and_(\
             I.preferred_backup_window_start > I.preferred_backup_window_end,\
             or_(
                between(func.UTC_TIME(), I.preferred_backup_window_start, "24:00:00"),\
                between(func.UTC_TIME(), "00:00:00", I.preferred_backup_window_end)\
            )
        )\
     )\
    )
    return filtered_query

def eligible_instances_in_backup_window_query(ignore_inst_ids=[], session=None):
    '''
        SELECT * 
        FROM instances 
        WHERE instances.lifecycle = :lifecycle_1 AND instances.changestate = :changestate_1 AND \
            instances.backup_retention_period_days != :backup_retention_period_days_1 AND \
            instances.deleted = :deleted_1 AND \
                (instances.preferred_backup_window_start < instances.preferred_backup_window_end AND\
                 UTC_TIME() BETWEEN instances.preferred_backup_window_start AND instances.preferred_backup_window_end OR\
                 instances.preferred_backup_window_start > instances.preferred_backup_window_end AND \
                 (UTC_TIME() BETWEEN instances.preferred_backup_window_start AND :UTC_TIME_1 OR\
                     UTC_TIME() BETWEEN :UTC_TIME_2 AND instances.preferred_backup_window_end
                 )
                )
        Mysql order of preference: Between > AND > OR

        Need indexing on Instances(lifecycle, changestate, preferred_backup_start, preferred_backup_end)
    '''
    session = session or get_session()
    with session.begin(subtransactions=True):
       I = models.Instance
       query = session.query(I)
       filter_query = query.filter(I.lifecycle == 'ACTIVE').\
                            filter(I.changestate == 'NONE').\
                            filter(I.backup_retention_period_days != 0).\
                            filter(I.deleted == 0)
       if ignore_inst_ids:
           filter_query = filter_query.filter(not_(I.id.in_(ignore_inst_ids)))
       filter_query = add_backup_window_query(filter_query)
       LOG.debug('Query for eligible_instances_in_backup_window: %s'%(str(filter_query)))
       return filter_query
                
def find_instance_ids_with_recent_auto_backups(session=None, time_hours=1):
    '''
        Function to find instance_ids for which automated backups were completed in the last 't'(default=1) hours.
        1 hour is the maximum backup window permissible as of now.

        select I.id from instances I, backups B where I.id = B.instance_id AND\
        B.created_at > :time
    '''
    threshold_time = timeutils.utcnow() - timedelta(hours=time_hours)
    session = session or get_session()
    with session.begin(subtransactions=True):
        B = models.Backup
        query = session.query(B.instance_id).\
                  filter(B.type == 'AUTOMATED').\
                  filter(threshold_time < B.created_at)
        instance_ids = query.distinct()
        return instance_ids

def missing_auto_backups_query(session=None, time_hours=26):
    '''
        Function to find instances for which auto backup has not been taken for last 26 hours & 
        the instances are in ACTIVE/NONE

        SELECT * FROM instances 
        WHERE instances.lifecycle = :lifecycle_1 AND instances.changestate = :changestate_1 AND instances.id NOT IN (
            SELECT DISTINCT backups.instance_id AS backups_instance_id 
            FROM backups 
            WHERE backups.type = :type_1 AND backups.created_at > :created_at_1
        )
    '''
    threshold_time = timeutils.utcnow() - timedelta(hours=time_hours)
    # a backup can be missing only if a day has elapsed since the creation time.
    one_day_back_time = timeutils.utcnow() - timedelta(hours=24)
    session = session or get_session()
    with session.begin(subtransactions=True):
        B = models.Backup
        I = models.Instance
        backup_query = session.query(B.instance_id).distinct()
        filter_query = backup_query.filter(B.type == 'AUTOMATED').filter(threshold_time < B.created_at)
        backups_subquery = filter_query.subquery()
        final_query = session.query(I).\
                        filter(I.lifecycle == 'ACTIVE').\
                        filter(I.changestate == 'NONE').\
                        filter(I.backup_retention_period_days > 0).\
                        filter(I.deleted == 0).\
                        filter(I.created_at < one_day_back_time).\
                        filter(I.id.notin_(filter_query))
        LOG.debug('Query for missing_auto_backups: %s'%(str(final_query)))
        return final_query 

# TODO paginate the api before instance count grows or accept a filter on list of instance_ids
def get_available_backups_per_instance(session=None, bkp_type='AUTOMATED', bkp_lifecycle='ACTIVE', bkp_changestate='NONE'):
    '''
        Function to find existing backups group_by instance id, order ascending by created_at
        which are not deleted and satisfy the input conditions
        
        @returns list of <instance_id, backup_retention_period_days, concat(created_at,'##','backup_id')

        select I.instance_id, I.backup_retention_period_days, group_concat(B.id order by created_at ASC) 
        from backups B , instances I
        where B.type = 'AUTOMATED' and B.deleted = 0 and B.lifecycle = 'ACTIVE' 
        and B.changestate = 'NONE' and B.instance_id = I.id and I.deleted = 0 
        group by I.id ;

    '''
    session = session or get_session()
    with session.begin(subtransactions=True):
        B = models.Backup
        I = models.Instance
        # Note: Current limit on backup_retention is 35 (sorting in memory will not be performance intense)
        final_query = session.query(\
                        I.id, I.backup_retention_period_days, func.group_concat(B.created_at, '##', B.id)).\
                        filter(B.type == bkp_type).\
                        filter(B.lifecycle == bkp_lifecycle).\
                        filter(B.changestate == bkp_changestate).\
                        filter(B.deleted == 0).\
                        filter(B.instance_id == I.id).\
                        filter(I.deleted == 0).\
                        group_by(I.id)
        LOG.debug('Query for available_backups_per_instance: %s'%(str(final_query)))
    return final_query.all()

### Backups related end here

def instance_get(context, owner_id, name, session=None):

    return get_only_entry(context,
                          models.Instance,
                          exception.InstanceNotFoundByOwnerIdAndName,
                          session,
                          owner_id=owner_id,
                          name=name,
                          deleted=0)


def instance_get_by_id(context, id, session=None):
    return get_only_entry(context,
                          models.Instance,
                          exception.InstanceNotFoundById,
                          session,
                          id=id,
                          deleted=0)
def bucket_get_by_id(context, id, session=None):
    return get_only_entry(context,
                          models.Buckets,
                          exception.BucketNotFoundById,
                          session,
                          id=id,
                          deleted=0)

def dp_task_get_by_id(context, id, session=None):
    return get_only_entry(context,
                          models.DP_Tasks,
                          exception.DP_TaskNotFoundById,
                          session,
                          id=id,
                          deleted=0)

def instance_get_all(context, owner_id, session=None):
    result = model_query(
        context,
        models.Instance,
        session=session).\
        options(joinedload('nova_flavor')).\
        options(joinedload('artefact').joinedload('db_engine')).\
        filter_by(owner_id=owner_id,
                  deleted=0)
    return result

def instance_update(context, instance_id, values, session=None):
    session = session or get_session()
    with session.begin(subtransactions=True):
        instance_ref = instance_get(context, values['owner_id'], values['name'], session=session)
        # new_name for modifying the instance's name, we first use the old name
        # to get the instance_ref and then change the value of values['name']
        # to new_name
        if 'new_name' in values:
            values['name'] = values['new_name']
            del values['new_name']
        instance_ref.updated_at = timeutils.utcnow()
        instance_ref.update(values)
        return instance_ref

def instance_update_by_id(context, instance_id, values, session=None):
    session = session or get_session()
    with session.begin(subtransactions=True):
        instance_ref = instance_get_by_id(context, instance_id, session=session)
        # new_name for modifying the instance's name, we first use the old name
        # to get the instance_ref and then change the value of values['name']
        # to new_name
        if 'new_name' in values:
            values['name'] = values['new_name']
            del values['new_name']
        instance_ref.updated_at = timeutils.utcnow()
        instance_ref.update(values)
        return instance_ref

def instance_get_n_pending(context, n, session=None):
    #query = model_query(context, models.Instance)
    session = session or get_session()

    with session.begin(subtransactions=True):
        query = model_query(
        context,
        models.Instance,
        session=session).\
        options(joinedload('nova_flavor')).\
        options(joinedload('artefact')).\
        options(joinedload('bucket')).\
        filter_by(changestate='PENDING',
                  deleted=0).limit(n)
        return query

def get_instance_delete_max_count(owner_id=None, instance_name=None, instance_id=None, session=None):
    '''
        returns the maximum value for the column name 'deleted' in instances table
    '''
    same_name_instances = _find_all_same_name_values(table=models.Instance, owner_id=owner_id,\
            value_name=instance_name, id=instance_id, session=session)
    return _get_max_deleted_count(same_name_instances)

def _get_max_deleted_count(values):
    max_deleted_count = 0
    for v in values:
        if v.deleted  > max_deleted_count:
            max_deleted_count = v.deleted 
    return max_deleted_count

def _find_all_same_name_values(table=models.Instance, owner_id=None, value_name=None, id=None, session=None):
    if ((owner_id is None or value_name is None) and (id is None)):
        raise exception.DBException("Either of owner_id+value_name or id is required")

    if owner_id is None or value_name is None:
        ret_obj = _get_by_id("", table, id)
        value_name = ret_obj.name
        owner_id = ret_obj.owner_id

    session = session or get_session()
    with session.begin(subtransactions=True):
        query = session.query(table.id, table.owner_id, table.name, table.deleted, table.created_at)
        query = query.filter(table.name == value_name).filter(table.owner_id == owner_id)
        LOG.debug('Query for find_all_same_name_values: %s'%(str(query)))
    return query.all()

### All Operation on Instance Model ends###

### All operation on Masterworkflow Model starts###

def masterworkflow_get(context, session=None):
    return get_only_entry(context,
                          models.MasterWorkflow,
                          exception.MasterWorkflowNotFound,
                          session,
                          deleted=0)


def masterworkflow_check_and_update(context, id):
    session = get_session()
    with session.begin():
        # Here we are trying to check if id is null,
        # if it is null then we are updating with
        # execution id of the master workflow. In this way
        # we wont be having write skew.
        result= model_query(
        context,
        models.MasterWorkflow,
        session=session).\
        filter_by(execution_id=None,
                  deleted=0,
                  id=1).\
                  update({'execution_id': id},
                  synchronize_session=False)
        return result

def set_execution_id_null_for_mfw(context, id):
    session = get_session()
    with session.begin():
        # Here we are trying to check if id is null,
        # if it is null then we are updating with
        # execution id of the master workflow. In this way
        # we wont be having write skew.
        result= model_query(
        context,
        models.MasterWorkflow,
        session=session).\
        filter_by(execution_id=id,
                  deleted=0,
                  id=1).\
                  update({'execution_id': None},
                  synchronize_session=False)
        return result

def _mfw_create(context):
    mfw_ref = models.MasterWorkflow()
    mfw_ref.created_at = timeutils.utcnow()
    mfw_ref.updated_at = timeutils.utcnow()
    session = get_session()
    with session.begin():
        mfw_ref.save(session)
        return mfw_ref
### All operation on Masterworkflow model ends###

###All Operation on Key pair Model starts###

def keypair_create(context, nova_key_id, private_key):

    keypair_ref = models.Keypair()
    keypair_ref.nova_key_id = nova_key_id
    keypair_ref.private_key = private_key
    keypair_ref.created_at = timeutils.utcnow()
    keypair_ref.updated_at = timeutils.utcnow()

    session = get_session()
    with session.begin():
        keypair_ref.save(session)
        return keypair_ref


def keypair_get(context, id, session=None):
    return get_only_entry(context,
                          models.Keypair,
                          exception.KeyPairNotFound,
                          session,
                          id=id,
                          deleted=0)


def keypair_update(context, loca_key_id, nova_key_id, private_key):

    session = get_session()
    with session.begin():
        keypair_ref = keypair_get(context, loca_key_id,session=session)
        keypair_ref.private_key = private_key
        keypair_ref.nova_key_id = nova_key_id
        keypair_ref.updated_at = timeutils.utcnow()
        return keypair_ref

def keypair_destroy(context, loca_key_id):
    session = get_session()
    with session.begin():
        keypair_ref = keypair_get(context, loca_key_id, session=session)
        keypair_ref.destroy(session=session)
        return keypair_ref

###All Operation on Key pair Model ends###

###All Operation on nova_flavor Model starts###

def nova_flavor_create(context, nova_flavor_id, flavor_name, ram, vcpus):

    nova_flavor_ref = models.Flavor()
    nova_flavor_ref.nova_flavor_id = nova_flavor_id
    nova_flavor_ref.flavor_name = flavor_name
    nova_flavor_ref.ram = ram
    nova_flavor_ref.vcpus = vcpus
    nova_flavor_ref.created_at = timeutils.utcnow()
    nova_flavor_ref.updated_at = timeutils.utcnow()
    session = get_session()
    with session.begin():
        nova_flavor_ref.save(session)
        return nova_flavor_ref


def nova_flavor_get(context, flavor_name, session=None):

    return get_only_entry(context,
                          models.Flavor,
                          exception.FlavorNotFound,
                          session,
                          flavor_name=flavor_name,
                          deleted=0)

def nova_flavor_get_by_id(id, session=None):
    return _get_by_id("", models.Flavor, id, session=session)

def nova_flavor_update(context, nova_flavor_id, flavor_name):

    session = get_session()
    with session.begin():
        nova_flavor_ref = nova_flavor_get(context, flavor_name,session=session)
        nova_flavor_ref.nova_flavor_id = nova_flavor_id
        nova_flavor_ref.flavor_name = flavor_name
        nova_flavor_ref.updated_at = timeutils.utcnow()
        return nova_flavor_ref

def nova_flavor_destroy(context, flavor_name):
    session = get_session()
    with session.begin():
        nova_flavor_ref = nova_flavor_get(context, flavor_name, session=session)
        nova_flavor_ref.destroy(session=session)
        return nova_flavor_ref

###All Operation on flavor Model ends###

###All Operation on image Model starts###

def image_create(context, glance_uuid, artefact_id):

    image_ref = models.Image()
    image_ref.glance_uuid = glance_uuid
    image_ref.artefact_id=artefact_id
    image_ref.created_at = timeutils.utcnow()
    image_ref.updated_at = timeutils.utcnow()

    session = get_session()
    with session.begin():
        image_ref.save(session)
        return image_ref

# TODO(rushiagr): need to pass session object here?
def image_get_for_artefact(context, artefact_id, session=None):
    return get_only_entry(context,
                          models.Image,
                          exception.ImageNotFoundByArtefactId,
                          session,
                          artefact_id=artefact_id,
                          deleted=0)


# TODO(rushiagr): remove this function. Image table doesn't have db and os
# information at all now. This function is being called in two more functions
# in this file, so need to be removed from there first
def image_get(context, database_engine, database_engine_version, os_version, session=None):
    return get_only_entry(context,
                          models.Image,
                          exception.ImageNotFoundByDbEngineVersionAndOS,
                          session,
                          database_engine=database_engine,
                          database_engine_version=database_engine_version,
                          os_version=os_version,
                          deleted=0)


def image_update(context,
                 database_engine,
                 database_engine_version,
                 os_version, glance_uuid):

    session = get_session()
    with session.begin():
        image_ref = image_get(context, database_engine,
                                database_engine_version,
                                os_version,session=session)
        image_ref.glance_uuid = glance_uuid
        image_ref.updated_at = timeutils.utcnow()
        return image_ref

def image_destroy(context, database_engine,
                  database_engine_version, os_version):
    session = get_session()
    with session.begin():
        image_ref = image_get(context, database_engine,
                                database_engine_version,
                                os_version,session=session)
        image_ref.destroy(session=session)
        return image_ref

###All Operation on flavor Model ends###

#----------------Artefacts------------------#

# TODO(rushiagr): maybe split this file into multiple files for easier readability?
# TODO(rushiagr): make sure we write enough checks everywhere so that we don't
# have to write 'first()' sqlalchemy method everywhere, and just simply use
# 'get()' method. We should also add 'unique constraints' wherever we want a
# unique entry to be returned, if that makes sense

def create_os(context, name, major_version, minor_version, url, state):
    # NOTE(rushiagr): Alternative way to pass parameters is to pass them
    #   key-value arguments, but the current approach makes for better
    #   enforcement of non-null values
    ref = models.Os()
    ref.update({
        'name': name,
        'major_version': major_version,
        'minor_version': minor_version,
        'url': url,
        'state': state,
        'created_at': timeutils.utcnow(),
        'updated_at': timeutils.utcnow(),
        })

    session = get_session()
    with session.begin():
        ref.save(session)
        return ref

def create_db_engine(context, name, major_version, minor_version, url, state):
    # NOTE(rushiagr): Alternative way to pass parameters is to pass them
    #   key-value arguments, but the current approach makes for better
    #   enforcement of non-null values
    ref = models.DbEngine()
    ref.update({
        'name': name,
        'major_version': major_version,
        'minor_version': minor_version,
        'url': url,
        'state': state,
        'created_at': timeutils.utcnow(),
        'updated_at': timeutils.utcnow(),
        })

    session = get_session()
    with session.begin():
        ref.save(session)
        return ref

def create_raga(context, version, url, state):
    # NOTE(rushiagr): Alternative way to pass parameters is to pass them
    #   key-value arguments, but the current approach makes for better
    #   enforcement of non-null values
    ref = models.Raga()
    ref.update({
        'version': version,
        'url': url,
        'state': state,
        'created_at': timeutils.utcnow(),
        'updated_at': timeutils.utcnow(),
        })

    session = get_session()
    with session.begin():
        ref.save(session)
        return ref


def create_artefact(context, raga_id, db_engine_id, os_id):
    # NOTE(rushiagr): Alternative way to pass parameters is to pass them
    #   key-value arguments, but the current approach makes for better
    #   enforcement of non-null values
    ref = models.Artefact()
    ref.update({
        'raga_id': raga_id,
        'db_engine_id': db_engine_id,
        'os_id': os_id,
        'created_at': timeutils.utcnow(),
        'updated_at': timeutils.utcnow(),
        })

    session = get_session()
    with session.begin():
        ref.save(session)
        return ref

# TODO(rushiagr): Use sqlalchemy's get() method when accessing via
#   primary key, for all the older code

def update_os(context, id, **kwargs):
    return _update_by_id(context, models.Os, id, kwargs)

def update_db_engine(context, id, **kwargs):
    return _update_by_id(context, models.DbEngine, id, kwargs)

def update_raga(context, id, **kwargs):
    return _update_by_id(context, models.Raga, id, kwargs)

def update_artefact(context, id, **kwargs):
    return _update_by_id(context, models.Artefact, id, kwargs)

def delete_os(context, id):
    return _delete_by_id(context, models.Os, id)

def delete_db_engine(context, id):
    return _delete_by_id(context, models.DbEngine, id)

def delete_raga(context, id):
    return _delete_by_id(context, models.Raga, id)

def delete_artefact(context, id):
    return _delete_by_id(context, models.Artefact, id)

def get_os(context, id):
    return _get_by_id(context, models.Os, id)

def get_db_engine(context, id):
    return _get_by_id(context, models.DbEngine, id)

def get_raga(context, id):
    return _get_by_id(context, models.Raga, id)

def get_artefact(context, id):
    return _get_by_id(context, models.Artefact, id)

def get_os_by_filters(context, **filters):
    return _get_by_filters(context, models.Os, **filters)

def get_db_engine_by_filters(context, **filters):
    return _get_by_filters(context, models.DbEngine, **filters)

def get_raga_by_filters(context, **filters):
    return _get_by_filters(context, models.Raga, **filters)

def get_artefact_by_filters(context, **filters):
    return _get_by_filters(context, models.Artefact, **filters)

def _get_by_id(context, table, id, session=None, joinedload_tables=None):
    """
    Get by ID.

    If 'joinedload_tables' is not None (e.g. joinedload_tables=['table1',
    'table2']), then do a joinedload on the query.

    List entries can be a string or a dict with one key whose value as list of
    strings. If its a dict like {'table1': ['table2', 'table3']}, then table2
    and table3 are one level deeper tables, joined on table1.
    """
    query = model_query(context, table, session=session)
    if joinedload_tables:
        for table in joinedload_tables:
            if type(table) == str:
                query = query.options(joinedload(table))
            elif type(table) == dict:
                first_level_table = table.keys()[0]
                if type(table[first_level_table]) != list:
                    # TODO(rushiagr): raise a more specific exception?
                    raise Exception
                jl = joinedload(first_level_table)
                for second_level_table in table[first_level_table]:
                    jl = jl.joinedload(second_level_table)
                query = query.options(jl)
            else:
                raise Exception
    result = query.get(id)

    if not result:
        # TODO(rushiagr): actually create this exception class
        raise exception.DbEntryNotFound(table=table.__tablename__, id=id)
    return result

def get_artefact_joinedload(context, id):
    result = model_query(context, models.Artefact).\
            options(joinedload('raga')).\
            options(joinedload('os')).\
            options(joinedload('db_engine')).\
            get(id)
    return result

def _get_all(context, table):
    session = get_session()
    with session.begin():
        return model_query(context, table).all()

# TODO(rushiagr): name it _get_all_by_filters(), and create another one _get_one_by_filters()
def _get_by_filters(context, table, session=None, **filters):
    """
    Return row/rows from table based on filters.

    Note that this function can return multiple rows, unless you specify
    only_get_first=True in the method
    """

    if 'only_get_first' in filters and filters['only_get_first']:
        filters.pop('only_get_first')
        result = model_query(context, table, session=session).filter_by(**filters).first()
    else:
        result = model_query(context, table, session=session).filter_by(**filters).all()
    if not result:
        raise exception.DbEntryNotFound(table=table.__tablename__, id=id)
    return result

def _update_by_id(context, table, id, joinedload_tables=None, session=None, **kwargs):
    """
    Update a row in a table where primary key is provided.

    Note that the table's primary key should be called 'id'.
    Note that the table must have an update_at column.

    See docstring of _get_by_id for more information on joinedload_tables
    """
    session = session or get_session()

    with session.begin(subtransactions=True):
        # TODO(rushiagr): understand sqlalchemy more. Check if our 'if ref is
        # none' condition (in _get_by_id() function) will be executed before
        # ref.update() happens.
        # Alternatively, find out a way in sqlalchemy to get and update a row
        # in a single query, and avoid making an additional call to database.
        # Same goes for all the other 'update_*' methods in this file
        ref = _get_by_id(context, table, id, session=session,
                joinedload_tables=joinedload_tables)
        # TODO(rushiagr): write a test case to check what happens if some
        # additional key-values are present in the kwargs dict. If it ignores
        # them, then ok, otherwise we need to write a test case to 'except'
        # that exception and handle that. Also, write test to check if not
        # enough key-values are provided in the kwargs dict
        kwargs['updated_at'] = timeutils.utcnow()
        ref.update(kwargs)
        ref.save(session)
    return ref

def _update_by_db_object(db_obj, **kwargs):
    session = get_session()
    with session.begin():
        kwargs['updated_at'] = timeutils.utcnow()
        db_obj.update(kwargs)
        db_obj.save(session)
    return db_obj
    
def _delete_by_id(context, table, id):
    """
    Soft-delete a row in a table where primary key is provided.

    By soft delete, it means that it will just mark the 'deleted' boolean
    column as True. Note that the table's primary key should be called 'id'.
    """
    session = get_session()
    with session.begin():
        ref = _get_by_id(context, table, id, session=session)
        ref.destroy(session=session)
        return ref

def _hard_delete_by_id(context, table, id):
    """
    Hard-delete a row in a table where primary key is provided.

    Note that the table's primary key should be called 'id'.
    """
    session = get_session()
    with session.begin():
        ref = _get_by_id(context, table, id, session=session)
        ref.hard_delete(session=session)
        return ref

def _delete_db_obj(context, db_obj):
    """
    Delete db object from database.
    """
    session = get_session()
    with session.begin():
        db_obj.hard_delete(session=session)
    return db_obj


def get_artefact_info(context, db_engine, db_engine_version=None, session=None):
    '''
    Get all the information for the user-supplied information for database.

    A user can just specify a database name (e.g. mysql) and no version in
    which case we'll have to use the 'preferred' version for that type of DB.

    This function should return the preferred image which should be spawned
    for the corresponding information. The image will contain preferred OS
    with preferred database version with some version of raga installed.
    Also, separately, the preferred
    raga version and that package's information will also be returned by this
    function.


    Note that flavor information is not fetched in this API call.
    '''
    # TODO(rushiagr): handle the case when there is actually no preferred
    # image uploaded for the given artefact. Or maybe, add the helper
    # method which to update tables in such a way that there will never be
    # such a case -- we can do so by first adding the image to the glance table, and
    # only then adding an entry to the artefacts table

    # TODO(rushiagr): In future, check if more than one rows are
    # returned here, and if yes, raise an error. Alternatively, we
    # can make a large number of checks while adding data to the
    # tables such that while retrieving data from database here in
    # this query, there will be exactly one row returned always.
    # We'll need to do a very good amount of testing to achieve
    # that

    # TODO(rushiagr): instead of making two database calls, make only one

    # TODO(rushiagr): see if joinedload_all makes more sense in our case
    # since we're wanting two levels of tables

    try:
        if db_engine_version is None:
            # The harder case
            preferred_artefact = model_query(context, models.Artefact, session=session).\
                    options(joinedload('raga')).\
                    options(joinedload('os')).\
                    options(joinedload('db_engine')).\
                    join(models.Artefact.raga).\
                    join(models.Artefact.os).\
                    join(models.Artefact.db_engine).\
                    filter(models.Os.state=='PREFERRED').\
                    filter(models.DbEngine.name==db_engine).\
                    filter(models.DbEngine.state=='PREFERRED').\
                    filter(models.Raga.state=='PREFERRED').\
                    one()
                    # NOTE(rushiagr): maybe the check DbEngine.state=preferred is
                    # redundant
            return preferred_artefact
        else:
            preferred_artefact = model_query(context, models.Artefact, session=session).\
                    options(joinedload('raga')).\
                    options(joinedload('os')).\
                    options(joinedload('db_engine')).\
                    join(models.Artefact.raga).\
                    join(models.Artefact.os).\
                    join(models.Artefact.db_engine).\
                    filter(models.Os.state=='PREFERRED').\
                    filter(models.DbEngine.name==db_engine).\
                    filter(models.DbEngine.major_version==db_engine_version).\
                    filter(models.DbEngine.state=='PREFERRED').\
                    filter(models.Raga.state=='PREFERRED').\
                    one()
            return preferred_artefact
    except orm_exc.NoResultFound:
        raise exception.ArtefactNotFound()

def instance_create_entry_from_user_supplied_values(
        context,
        owner_id,
        database_engine,
        database_engine_version,
        database_name,
        database_flavor,
        database_storage_size,
        master_username,
        master_user_password,
        preferred_backup_window_start,
        preferred_backup_window_end,
        preferred_maintenance_window_start_day,
        preferred_maintenance_window_start_time,
        preferred_maintenance_window_end_day,
        preferred_maintenance_window_end_time,
        port,
        log_retention_period,
        backup_retention_period_days=1,
        session=None
    ):
        """
        Method to add entry to instances table from user input.

        NOTE: if database_engine_version is not specified by user, you should
        pass None to this method.
        """
        # TODO(rushiagr): if preferred_*_window_*_ param is none, add a default
        # values here

        # TODO(rushiagr): use same session across all these database calls? Not
        # sure if this will avoid multiple trips to database. Need to learn
        # sqlalchemy more
        # TODO(rushiagr): or what if I write a giant query which does all these
        # query things in one single query? *wicked smile*
        artefact_joinedload = get_artefact_info(context, database_engine,
                database_engine_version, session=session)

        nova_flavor = nova_flavor_get(context, database_flavor, session=session)

        instance_dict = {
            'name': database_name,
            'storage_size': database_storage_size,
            'nova_flavor_id': nova_flavor.id,
            'artefact_id': artefact_joinedload.id,
            'backup_retention_period_days': backup_retention_period_days,
            'log_retention_period': log_retention_period,
            'preferred_backup_window_start': preferred_backup_window_start,
            'preferred_backup_window_end': preferred_backup_window_end,
            'preferred_maintenance_window_start_day': preferred_maintenance_window_start_day,
            'preferred_maintenance_window_start_time': preferred_maintenance_window_start_time,
            'preferred_maintenance_window_end_day': preferred_maintenance_window_end_day,
            'preferred_maintenance_window_end_time': preferred_maintenance_window_end_time,
            'master_username': master_username,
            'master_user_password': master_user_password,
            'port': port}
        return instance_create(context, owner_id, instance_dict, session=session)


def get_bucket_details_for_owner_id(context, owner_id, session=None):
    '''
    Get the bucket details for an owner_id
    :param owner_id: Specific Owner_id to get bucket  for
    :param session: Current SQLAlchemy session
    :return:
    bucket information.
    '''
    session = session or get_session()
    with session.begin(subtransactions=True):
        bucket_ref = get_only_entry(context,
                          models.Buckets,
                          exception.BucketNotFoundByOwnerId,
                          session,
                          owner_id=owner_id,
                          deleted=0)
        return bucket_ref

def bucket_create_entry(
        context,
        name,
        owner_id,
        policy_id,
        state,
        session=None
                ):
    """
    Method is to add entry to buckets table from user input.

    """
    bucket_ref = models.Buckets()
    bucket_ref.owner_id = owner_id
    bucket_ref.attached_policy_id = policy_id
    bucket_ref.name = name
    bucket_ref.state = state
    bucket_ref.created_at = timeutils.utcnow()
    bucket_ref.updated_at = timeutils.utcnow()
    session = session or get_session()
    with session.begin(subtransactions=True):
        bucket_ref.save(session)
        return bucket_ref

def update_bucket_by_bucketid(context, bucket_id, values, session=None):
    """
    Update the bucket information by bucket_id .

    """
    session = session or get_session()
    with session.begin(subtransactions=True):
        bucket_ref = bucket_get_by_id(context, bucket_id, session=session)
        bucket_ref.updated_at = timeutils.utcnow()
        bucket_ref.update(values)
        return bucket_ref

def delete_bucket(context,bucket_id, session=None):
    session = session or get_session()
    with session.begin(subtransactions=True):
        bucket_ref = bucket_get_by_id(context, bucket_id, session)
        bucket_ref.state = 'DELETED'
        bucket_ref.destroy(session=session)
        return bucket_ref

def user_quota_create_entry_from_user_supplied_values(
        context,
        owner_id,
        quota_name,
        quota_limit):
    '''
    Method to add entry to UserQuota table from user input.
    '''

    user_quota_dict = {
        'owner_id':owner_id,
        'quota_name': quota_name,
        'quota_limit': quota_limit}
    return user_quota_create(context, owner_id, user_quota_dict)

def restore_db_instance_from_snapshot(context,
        owner_id,
        database_name,
        database_flavor,
        database_storage_size,
        master_username,
        master_user_password,
        preferred_backup_window_start,
        preferred_backup_window_end,
        preferred_maintenance_window_start_day,
        preferred_maintenance_window_start_time,
        preferred_maintenance_window_end_day,
        preferred_maintenance_window_end_time,
        port,
        nova_flavor_id,
        artefact_id,
        backup_id,
        log_retention_period,
        backup_retention_period_days=1,
        session=None
       ):

        """
        Method to add entry to instances table from user input.

        NOTE: if database_engine_version is not specified by user, you should
        pass None to this method.
        """

        if database_flavor!=None:
            nova_flavor = nova_flavor_get(context, database_flavor)
            nova_flavor_id = nova_flavor.id

        instance_dict = {
            'name': database_name,
            'storage_size': database_storage_size,
            'nova_flavor_id': nova_flavor_id,
            'artefact_id': artefact_id,
            'backup_retention_period_days': backup_retention_period_days,
            'preferred_backup_window_start': preferred_backup_window_start,
            'preferred_backup_window_end': preferred_backup_window_end,
            'preferred_maintenance_window_start_day': preferred_maintenance_window_start_day,
            'preferred_maintenance_window_start_time': preferred_maintenance_window_start_time,
            'preferred_maintenance_window_end_day': preferred_maintenance_window_end_day,
            'preferred_maintenance_window_end_time': preferred_maintenance_window_end_time,
            'master_username': master_username,
            'master_user_password': master_user_password,
            'restored_from_backup_id': backup_id,
            'log_retention_period': log_retention_period,
            'port': port}
        return instance_create(context, owner_id, instance_dict, True, session=session)


def get_user_visible_instance_properties(owner_id, instance_name, session=None):
    try:
        result = model_query(
            None,
            models.Instance,
            session=session).\
            options(joinedload('nova_flavor')).\
            options(joinedload('artefact').joinedload('db_engine')).\
            options(joinedload('bucket')).\
            filter_by(
                owner_id=owner_id,
                name=instance_name,
                deleted=0).\
            one()
    except orm_exc.NoResultFound:
        raise exception.InstanceNotFoundByOwnerIdAndName(owner_id=owner_id, name=instance_name)

    return result

def get_user_visible_instance_properties_by_id(id, session=None):
    try:
        result = model_query(
            None,
            models.Instance,
            session=session).\
            options(joinedload('nova_flavor')).\
            options(joinedload('artefact').joinedload('db_engine')).\
            filter_by(
                id=id,
                deleted=0).\
            one()
    except orm_exc.NoResultFound:
        raise exception.InstanceNotFoundById(id=id)
    return result

# TODO(rushiagr): Fill these
def get_latest_raga(context):
    return get_raga_by_filters(context, state='PREFERRED', only_get_first=True)


def get_current_raga_for_host_ip(context, ip):
    # TODO(rushiagr): do a joinedload here to fetch raga row directly
    # while fetching the host row
    host = _get_by_filters(context, models.Host, vm_ip=ip, only_get_first=True)
    # TODO(rushiagr): instead of making THREE db calls, make one
    artefact = get_artefact(context, host.artefact_id)
    raga = get_raga(context, artefact.raga_id)
    return raga

###All Operation on Key pair Model starts###

def host_create(context, instance_id, keypair_id, az, nova_uuid,
        nova_flavor_id, ip, ip_id, artefact_id):

    host_ref = models.Host()
    host_ref.instance_id = instance_id
    host_ref.keypair_id = keypair_id
    host_ref.az = az
    host_ref.nova_uuid = nova_uuid
    host_ref.nova_flavor_id = nova_flavor_id
    host_ref.created_at = timeutils.utcnow()
    host_ref.updated_at = timeutils.utcnow()
    host_ref.vm_ip = ip
    host_ref.vm_ip_id = ip_id
    host_ref.artefact_id = artefact_id

    session = get_session()
    with session.begin():
        host_ref.save(session)
        return host_ref

def host_get(context, id, session=None):

    return get_only_entry(context,
                          models.Host,
                          exception.HostNotFoundById,
                          session,
                          id=id,
                          deleted=0)


def host_get_by_instance_id(context, instance_id, session=None):
    return get_only_entry(context,
                          models.Host,
                          exception.HostNotFoundByInstanceId,
                          session,
                          instance_id=instance_id,
                          deleted=0)


# NOTE(rushiagr): This method is not used anywhere in the code. Hopefully we'll
# start using it in future :) Remove this comment once we start using this
def host_update(context, instance_id, az, nova_uuid, nova_flavor_id):

    session = get_session()
    with session.begin():
        host_ref = host_get_by_instance_id(context, instance_id,session=session)
        values = {}
        values['instance_id']= instance_id
        values['az'] = az
        values['nova_uuid'] = nova_uuid
        values['nova_flavor_id'] = nova_flavor_id
        values['updated_at'] = timeutils.utcnow()
        host_ref.update(values)
        return host_ref

def host_destroy(context, local_host_id):
    session = get_session()
    with session.begin():
        host_ref = host_get(context, local_host_id, session=session)
        host_ref.destroy(session=session)
        return host_ref

###All Operation on Key pair Model ends###

###All Operation on volume Model starts###

def volume_create(context, cinder_uuid, host_id, volume_type, attach_status):

    volume_ref = models.Volume()
    volume_ref.cinder_uuid = cinder_uuid
    volume_ref.host_id = host_id
    volume_ref.volume_type = volume_type
    volume_ref.attach_status = attach_status
    volume_ref.created_at = timeutils.utcnow()
    volume_ref.updated_at = timeutils.utcnow()
    session = get_session()
    with session.begin():
        volume_ref.save(session)
        return volume_ref

def volume_get(context, volume_id, cinder_uuid, host_id, volume_type, session=None):

    return get_only_entry(context,
                          models.Volume,
                          exception.VolumeNotFoundById,
                          session,
                          volume_id=volume_id,
                          cinder_uuid=cinder_uuid,
                          host_id=host_id,
                          volume_type=volume_type,
                          deleted=0)


def volume_get_by_host_id(context, host_id, session=None):

    return get_only_entry(context,
                          models.Volume,
                          exception.VolumeNotFoundByHostId,
                          session,
                          host_id=host_id,
                          deleted=0)


def volume_get_by_id(context, id, session=None):
    return get_only_entry(context,
                          models.Volume,
                          exception.VolumeNotFoundById,
                          session,
                          id=id,
                          deleted=0)


def volume_update(context, volume_id, cinder_uid, host_id, volume_type, attach_status):

    session = get_session()
    with session.begin():
        volume_ref = volume_get(context, volume_id, cinder_uid, host_id, volume_type,session=session)
        volume_ref.cinder_uid = cinder_uid
        volume_ref.host_id = host_id
        volume_ref.volume_type = volume_type
        volume_ref.attach_status = attach_status
        volume_ref.updated_at = timeutils.utcnow()
        return volume_ref

def volume_update_status(context, id , attach_status):
    session = get_session()
    with session.begin():
        volume_ref = volume_get_by_id(context, id,session=session)
        volume_ref.attach_status = attach_status
        volume_ref.updated_at = timeutils.utcnow()
        return volume_ref

def volume_destroy(context, id):
    session = get_session()
    with session.begin():
        volume_ref = volume_get_by_id(context, id, session=session)
        volume_ref.destroy(session=session)
        return volume_ref

def volume_change_status(context, id, change_status):
    session = get_session()
    with session.begin():
        volume_ref = volume_get_by_id(context, id,session=session)
        volume_ref.attach_status = change_status
        volume_ref.updated_at = timeutils.utcnow()
        return  volume_ref

###All Operation on volume Model ends###





## ---- Backup table operations---- ##

# TODO(rushiagr): It's better to organize db operations this way I think. Do
# the same for all other tables

def backup_create(
        context,
        instance_id,
        name,
        owner_id,
        type,
        storage_size,
        port,
        master_user_name,
        master_user_password,
        nova_flavor_id,
        artefact_id,
        preferred_backup_window_start,
        preferred_backup_window_end,
        preferred_maintenance_window_start_day,
        preferred_maintenance_window_start_time,
        preferred_maintenance_window_end_day,
        preferred_maintenance_window_end_time,
        backup_retention_period_days=1,
        num_backup_tries=0,
        session=None):
    """Generic create backup."""
    ref = models.Backup()
    update_dict = {
        'instance_id': instance_id,
        'name': name,
        'owner_id': owner_id,
        'type': type,
        'lifecycle': 'CREATING',
        'changestate': 'PENDING',
        'storage_size': storage_size,
        'port': port,
        'master_user_name': master_user_name,
        'master_user_password': master_user_password,
        'nova_flavor_id': nova_flavor_id,
        'artefact_id': artefact_id,
        'preferred_backup_window_start': preferred_backup_window_start,
        'preferred_backup_window_end': preferred_backup_window_end,
        'preferred_maintenance_window_start_day': preferred_maintenance_window_start_day,
        'preferred_maintenance_window_start_time': preferred_maintenance_window_start_time,
        'preferred_maintenance_window_end_day': preferred_maintenance_window_end_day,
        'preferred_maintenance_window_end_time': preferred_maintenance_window_end_time,
        'backup_retention_period_days': backup_retention_period_days,
        'num_backup_tries': num_backup_tries,

        # TODO(rushiagr): write generic _db_update_row() and _db_create_row()
        # methods update method which will just take parameters from function
        # and insert/update into the database, including setting created_at and
        # updated_at whenever applicable. See
        # http://stackoverflow.com/questions/582056/ for how to use 'inspect'
        # to do the same thing
        'created_at': timeutils.utcnow(),
        'updated_at': timeutils.utcnow(),
        }
    ref.update(update_dict)

    session = session or get_session()

    with session.begin(subtransactions=True):
        ref.save(session)
        return ref
    pass


def backup_create_from_instance(
        context,
        backup_type,
        instance,
        backup_name,
        session=None):
    """Create backup entry for an instance"""
    ref = models.Backup()
    return backup_create(
        context,
        instance.id,
        backup_name,
        instance.owner_id,
        backup_type,
        instance.storage_size,
        instance.port,
        instance.master_username,
        instance.master_user_password,
        instance.nova_flavor_id,
        instance.artefact_id,
        instance.preferred_backup_window_start,
        instance.preferred_backup_window_end,
        instance.preferred_maintenance_window_start_day,
        instance.preferred_maintenance_window_start_time,
        instance.preferred_maintenance_window_end_day,
        instance.preferred_maintenance_window_end_time,
        instance.backup_retention_period_days,
        0,  # no of backup tries
        session)


def backup_update(context, id, **kwargs):
    return _update_by_id(context, models.Backup, id, kwargs)


def backup_delete(context, id, session=None):
    '''
        Soft deletes the backup by incrementing value for deleted
        Note: db has unique key constraint on {owner_id, name, deleted}
    '''
    session = session or get_session()
    with session.begin():
        same_name_backups = _find_all_same_name_values(table=models.Backup, id=id, session=session)
        max_deleted_count = _get_max_deleted_count(same_name_backups) + 1
        ref = _get_by_id("", models.Backup, id, session=session)
        ref.destroy(session=session, delete_count=max_deleted_count)
        return ref

def backup_get_by_filters(context, session=None, **filters):
    return _get_by_filters(context, models.Backup, session=session, **filters)

def bucket_get_by_filters(context, session=None, **filters):
    return _get_by_filters(context, models.Buckets, session=session, **filters)

def backup_get_all_for_owner(context, owner_id, snapshot_type=None, session= None):
    if snapshot_type:
        result = model_query(
            context,
            models.Backup,
            session=session).\
            options(joinedload('nova_flavor')).\
            options(joinedload('artefact').joinedload('db_engine')).\
            filter_by(owner_id=owner_id,
                    deleted=0,
                    type=snapshot_type).\
            all()
    else:
        result = model_query(
            context,
            models.Backup,
            session=session).\
            options(joinedload('nova_flavor')).\
            options(joinedload('artefact').joinedload('db_engine')).\
            filter_by(owner_id=owner_id,
                    deleted=0).\
            all()
    return result

def backup_get_all_for_instance(context, instance_id, snapshot_type=None, session=None):
    if snapshot_type:
        result = model_query(
            context,
            models.Backup,
            session=session).\
            options(joinedload('nova_flavor')).\
            options(joinedload('artefact').joinedload('db_engine')).\
            filter_by(instance_id=instance_id,
                    deleted=0,
                    type=snapshot_type).\
            all()
    else:
        result = model_query(
            context,
            models.Backup,
            session=session).\
            options(joinedload('nova_flavor')).\
            options(joinedload('artefact').joinedload('db_engine')).\
            filter_by(instance_id=instance_id,
                    deleted=0).\
            all()
    return result

def backup_get_by_owner_id_and_name(context, owner_id, backup_name, session=None):
    return get_only_entry(context,
                          models.Backup,
                          exception.BackupNotFoundByOwnerIdAndName,
                          session,
                          name=backup_name,
                          owner_id=owner_id,
                          deleted=0)

def user_quota_get_by_owner_id_quota_name(context, owner_id, quota_name, session=None):
    return get_only_entry(context,
                    models.UserQuota,
                    exception.UserLimitNotFoundByOwnerId,
                    session,
                    owner_id=owner_id,
                    quota_name=quota_name,
                    deleted=0)

def lccs_conditional_update(
        context,
        table,
        id,
        lifecycle,
        changestate,
        expected_lifecycle,
        expected_changestate,
        session=None):
    """
    Conditionally update lifecycle and changestate of row.

    The method will only update lifecycle and changestate to 'lifecycle' and
    'changestate' arguments if the existing values are 'existing_lifecycle' and
    'existing_changestate' respectively.

    Input:
        table: model class for the table to update, e.g. models.Instance
        id: row/model id
    Return value:
        updated model object
    """
    # TODO(rushiagr): In future, all the operations here should be done
    # atomically/in one transaction.

    # TODO(rushiagr): If no lifecycle, changestate is passed, nothing will be
    # updated. Should we throw an error instead?

    # Last four arguments cannot be 'None'
    if not lifecycle or not changestate or not expected_lifecycle or not expected_changestate:
        raise ValueError

    session = session or get_session()

    with session.begin(subtransactions=True):
        db_obj = _get_by_id(context, table, id, session=session)

        if db_obj.lifecycle != expected_lifecycle:
            raise exception.DbConditionalUpdateFailed(
                table=str(table), id=id, column='lifecycle',
                expected=expected_lifecycle, actual=db_obj.lifecycle)

        if db_obj.changestate != expected_changestate:
            raise exception.DbConditionalUpdateFailed(
                table=str(table), id=id, column='changestate',
                expected=expected_changestate, actual=db_obj.changestate)

        new_attributes = {
            'lifecycle': lifecycle,
            'changestate': changestate,
            }

        return _update_by_id(context, table, id, session=session, **new_attributes)

def final_backup_completed(context, id):
    backup = lccs_conditional_update(context, models.Backup, id,
            'ACTIVE', 'NONE', 'CREATING', 'APPLYING')
    return backup

def backup_mark_completed(context, id):
    backup = lccs_conditional_update(context, models.Backup, id,
            'ACTIVE', 'NONE', 'CREATING', 'APPLYING')
    instance = lccs_conditional_update(context, models.Instance, backup.instance_id,
            'ACTIVE', 'NONE', 'BACKUP', 'APPLYING')

    return backup, instance

def restore_mark_completed(context, instance_id, backup_id):
    instance = lccs_conditional_update(context, models.Instance, instance_id,
            'ACTIVE', 'NONE', 'RESTORING', 'APPLYING')
    backup = lccs_conditional_update(context, models.Backup, backup_id,
            'ACTIVE', 'NONE', 'RESTORING', 'APPLYING')
    return instance, backup

def backup_mark_deletion_completed(context, id):
    # TODO(shank): Make transactional
    backup_delete(context, id)
    backup = lccs_conditional_update(context, models.Backup, id,
            'DELETING', 'NONE', 'DELETING', 'APPLYING')
    return backup

def backup_increment_tries(context,
                           id,
                           max_retry_count=10):
    bkp = _get_by_id(context, models.Backup, id)
    if bkp.num_backup_tries < max_retry_count - 1:
        tries = bkp.num_backup_tries + 1
        return _update_by_id(context, models.Backup, id, **{'num_backup_tries': tries})
    raise exception.MaxRetryCount(max_retry_count=max_retry_count)


#### Get set of customer visible supported values for nova flavors####
def supported_nova_flavors_get_all_names():
    results = model_query(
        None,
        models.Flavor,
        session=None).\
        filter_by(deleted=0).\
        with_entities(models.Flavor.flavor_name).\
        all()

    # Formatting so that instead of getting a tuple, I get just a single value
    results = [result[0] for result in results]
    return results

#### Get set of customer visible supported values for engines ####
def supported_engines_get_all_names():
    results = model_query(
        None,
        models.DbEngine,
        session=None).\
        filter_by(deleted=0).\
        filter(models.DbEngine.state=='PREFERRED').\
        with_entities(models.DbEngine.name).\
        all()

    # Formatting so that instead of getting a tuple, I get just a single value
    results = [result[0] for result in results]
    return results

#### Get set of customer visible supported values for engine version for a given engine ####
def supported_engine_versions_get_all_values(engine_name):
    results = model_query(
        None,
        models.DbEngine,
        session=None).\
        filter_by(deleted=0).\
        filter(models.DbEngine.state != 'DEPRECATED').\
        filter_by(name=engine_name).\
        with_entities(models.DbEngine.major_version).\
        all()

    # Formatting so that instead of getting a tuple, I get just a single value
    results = [result[0] for result in results]
    return results

#### Get set of customer visible supported values for all engine versions ####
def supported_db_types_get_all():
    db_type_names=supported_engines_get_all_names()
    results = []
    for db_type_name in db_type_names:
        result= get_user_visible_db_type_properties(db_type_name)
        results.append(result)
    return results

def get_user_visible_db_type_properties(db_type_name):
    try:
        result={}
        result['name']=db_type_name
        result['db_type_versions'] = model_query(
            None,
            models.DbEngine,
            session=None).\
            filter_by(deleted=False).\
            filter(models.DbEngine.state != 'DEPRECATED').\
            filter_by(name=db_type_name)
    except orm_exc.NoResultFound:
        raise exception.DBTypeNotFoundByName(name=db_type_name)
    return result

#### Get preferred engine version for a given engine ####
def get_preferred_engine_version_for_engine(engine_name, session=None):

    return get_only_entry(None,
                          models.DbEngine,
                          exception.PreferedGlobalEngineNotFound,
                          session,
                          state='PREFERRED',
                          name=engine_name,
                          deleted=0).major_version


def backup_get_all_pending(context, batch_size=None):
    """
    Higher level db api method, which returns batch of pending backups.

    Return value: list of a dict, where each dict contains 'backup_id',
    'owner_id' and 'lifecycle' keys and corresponding values.
    """
    # TODO(rushiagr): don't allow anybody to not pass a batch size?
    backups = backup_get_all_pending_from_db(context, batch_size)
    return_backups = []
    for backup in backups:
        return_backup = {
                'backup_id': backup.id,
                'lifecycle': backup.lifecycle,
                'owner_id': backup.owner_id, # not used presently
                'instance_id': backup.instance_id
        }
        return_backups.append(return_backup)

    return return_backups

def backup_get_all_pending_from_db(context, batch_size):
    """
    Get backups from db whose changestate='PENDING'.

    Also get corresponding instance entries.
    """

    session = get_session()
    with session.begin():
        query = model_query(context, models.Backup, session=session).\
            filter_by(changestate='PENDING', deleted=0).\
            order_by(models.Backup.created_at)
        if batch_size is not None:
            backups = query.limit(batch_size)
        else:
            backups = query.all()

        return backups

def construct_mistral_create_backup_input(context, backup, instance):
    """Generates input data to be passed to mistral."""
    # Get instance table with joinedloaded information
    instance = _get_by_id(context, models.Instance, instance.id, session=None,
            joinedload_tables=[{'hosts': ['volumes']}])
    return_dict = {
        'backup_id': backup.id,
        'owner_id': str(backup.owner_id),
        'instance_id': instance.id,
        'instance_ip': instance.hosts[0].vm_ip,
        'cinder_uuid': instance.hosts[0].volumes[0].cinder_uuid,
        'volume_id': instance.hosts[0].volumes[0].id,
    }
    return return_dict

def construct_mistral_delete_backup_input(context, backup_id):
    """Generates input data to be passed to mistral."""
    # Get backups row with joinedloaded information
    backup = _get_by_id(None, models.Backup, backup_id, session=None,
            joinedload_tables=['volume_snapshots'])
    return_dict = {
        'backup_id': backup_id,
        #'volume_snapshot_id': backup.volume_snapshots[0].id,
        'sbs_snapshot_id': backup.volume_snapshots[0].cinder_snapshot_id,
    }
    return return_dict


def mark_backup_creation_started(context, backup_dict):
    """
    Mark backup CREATING/APPLYING and corresponding instance BACKUP/APPLYING.
    """

    # Do a best effort at this. If we find an inconsistent state, we don't want
    # to throw an error and prevent scheduling of other pending backups. So, we
    # catch the exception, log it and proceed further. It's expected that we have
    # monitoring in place for "Pending Backups too long" and are alerted by this
    # so that someone can manually dig in and figure it out
    # TODO(shank): Consider if we want to place the backup and instance in an
    # 'ERROR' changestate so that we don't need the above mentioned monitoring
    # Opened https://jira.ril.com/browse/JDB-56 to track this
    try:
        session = get_session()

        # Performing both operations in the same transaction
        with session.begin(subtransactions=True):
            backup = lccs_conditional_update(
                context,
                models.Backup,
                backup_dict['backup_id'],
                'CREATING',
                'APPLYING',
                'CREATING',
                'PENDING',
                session
            )
            instance = lccs_conditional_update(
                context,
                models.Instance,
                backup.instance_id,
                'BACKUP',
                'APPLYING',
                'BACKUP',
                'PENDING',
                session
            )
            return backup, instance
    except (exception.DbConditionalUpdateFailed, exception.DbEntryNotFound) as e:
        LOG.exception('Failed mark_backup_creation_started')
        return None, None

def mark_restore_started(context, instance_obj):
    """
    Mark restore operation as started.

    Do this by setting instance changestate to 'APPLYING', and corresponding
    backup changestate to 'APPLYING'. Before that, check if instance and backup
    state is both restoring/pending..
    """
    instance = lccs_conditional_update(context, models.Instance, instance_obj.id,
            'RESTORING', 'APPLYING', 'RESTORING', 'PENDING')
    backup = lccs_conditional_update(context, models.Backup, instance_obj.restored_from_backup_id,
            'RESTORING', 'APPLYING', 'RESTORING', 'PENDING')

    return instance, backup

def mark_backup_deletion_started(context, backup_dict):
    """
    Mark backup DELETING/APPLYING.
    """

    # Do a best effort at this. If we find an inconsistent state, we don't want
    # to throw an error and prevent scheduling of other pending backups. So, we
    # catch the exception, log it and proceed further. It's expected that we
    # have monitoring in place for "Pending Backups too long" and are alerted
    # by this so that someone can manually dig in and figure it out
    # TODO(shank): Consider if we want to place the backup in an 'ERROR'
    # changestate so that we don't need the above mentioned monitoring Opened
    # https://jira.ril.com/browse/JDB-56 to track this
    try:
        session = get_session()

        # TODO(rushiagr): not sure if subtransactions thing is required, but
        # adding anyway as it's not harmful
        with session.begin(subtransactions=True):
            backup = lccs_conditional_update(context, models.Backup,
                backup_dict['backup_id'], 'DELETING', 'APPLYING',
                'DELETING', 'PENDING', session)
            return backup
    except (exception.DbConditionalUpdateFailed, exception.DbEntryNotFound) as e:
        LOG.exception('Failed mark_backup_deletion_started')
        return None

def mark_backup_deletion_started_with_final_snapshot(context, backup_id):
    try:
        session = get_session()

        # Performing both operations in the same transaction
        with session.begin(subtransactions=True):
            backup = lccs_conditional_update(
                context,
                models.Backup,
                backup_id,
                'CREATING',
                'APPLYING',
                'CREATING',
                'PENDING',
                session
            )
            instance = lccs_conditional_update(
                context,
                models.Instance,
                backup.instance_id,
                'DELETING',
                'APPLYING',
                'DELETING',
                'PENDING',
                session
            )
            return backup, instance
    except (exception.DbConditionalUpdateFailed, exception.DbEntryNotFound) as e:
        LOG.exception('Failed to update Delete Instance with Final Snapshot')
        return None, None

### Operations on InfiniteWorkflow table starts ###
'''
    workflow_type:
    AUTO_BACKUP = "AUTO_BACKUP"
    CLEAN_AUTO_BACKUP = "CLEAN_AUTO_BACKUP"
'''

def infinite_workflow_check_and_update(execution_id, workflow_type, session=None):
    session = session or get_session()
    with session.begin(subtransactions=True):
        # use with_for_update so as to lock the row (FIXME it locks the whole table) until session is completed.
        I = models.InfiniteWorkflow
        query = session.query(I).with_for_update().filter(I.workflow_type == workflow_type)
        result = query.one_or_none()
        if result is None:
            create_infinite_workflow_record(workflow_type, execution_id, session=session)
        elif result.execution_id is None:
            result.updated_at = timeutils.utcnow()
            result.update({'execution_id': execution_id})
        return result

def set_execution_id_null_for_infinite_workflow(execution_id, workflow_type, session=None):
    '''
        @input : execution_id, workflow_type
            fetches the execution_id for workflow_type and compares with the input.
            Sets the execution_id to null if everything goes fine
        @response : None. 
        @throws : exception.InfiniteWorkflowNotFound
    '''
    session = session or get_session()
    with session.begin():
        I = models.InfiniteWorkflow
        query = session.query(I).with_for_update().filter(I.workflow_type == workflow_type)
        result = query.one()
        if result is None or result.execution_id is None:
            return
        if result is None or str(result.execution_id)!=execution_id :
            exception_str = "Unable to set execution id for workflow_type:%s ,as the execution id : %s doesnot exist."\
            %(workflow_type, execution_id)
            LOG.exception(exception_str)
            raise exception.InfiniteWorkflowNotFound(exception_str)
        
        result.updated_at = timeutils.utcnow()
        result.update({'execution_id': None})

def infinite_workflow_get(workflow_type, session=None):
    return get_only_entry('',
                          models.InfiniteWorkflow,
                          exception.InfiniteWorkflowNotFound,
                          session,
                          workflow_type=workflow_type)

def create_infinite_workflow_record(workflow_type, execution_id=None, session=None):
    session = session or get_session()
    with session.begin(subtransactions=True):
        query = session.query(models.InfiniteWorkflow).with_for_update().filter(models.InfiniteWorkflow.workflow_type == workflow_type)
        result = query.one_or_none()
        if result is not None:
            raise exception.DBException("Unable to create infinite workflow record for \
            workflow_type %s. The record already exists."%(workflow_type))

        I = models.InfiniteWorkflow()
        I.execution_id = execution_id
        I.workflow_type = workflow_type 
        I.created_at = timeutils.utcnow()
        I.updated_at = timeutils.utcnow()
        I.save(session)
        return I 

#### Workflow related functions start ####

def get_workflow_record_by_execution_id(execution_id, session=None):
    return get_only_entry(None,
                          models.Workflow,
                          exception.WorkflowNotFoundByExecutionId,
                          session,
                          execution_id=execution_id)

def create_workflow_record(execution_id, workflow_name, instance_id=None, backup_id=None, session=None):
    workflow_ref = models.Workflow()
    workflow_ref.execution_id = execution_id
    workflow_ref.workflow_name = workflow_name
    workflow_ref.instance_id = instance_id
    workflow_ref.backup_id = backup_id
    workflow_ref.created_at = timeutils.utcnow()
    workflow_ref.updated_at = timeutils.utcnow()

    session = session or get_session()
    with session.begin(subtransactions=True):
        workflow_ref.save(session)
        return workflow_ref

def update_workflow_record(execution_id, values, session=None):
    session = session or get_session()
    with session.begin(subtransactions=True):
        workflow_ref = get_workflow_record_by_execution_id(execution_id, session)
        workflow_ref.updated_at = timeutils.utcnow()
        workflow_ref.update(values)
        return workflow_ref

def workflow_mark_completed(execution_id, session=None):
    values = {}
    values['state'] = 'SUCCESS'
    return update_workflow_record(execution_id, values, session)

#### DefaultParameter related functions start ####

def get_default_parameters_for_db_engine_id(db_engine_id, session=None):
    '''
    Get all default parameters for a specific db_engine_id

    Parameters
    ----------
    db_engine_id: Specific DbEngine to get default parameters for
    session: Current SQLAlchemy session

    Returns
    -------
    List of default parameters
    '''
    session = session or get_session()
    with session.begin(subtransactions=True):
        query = model_query(None, models.DefaultParameter, session=session).\
            filter_by(db_engine_id=db_engine_id, deleted=0)
        parameters = query.all()

    formatted_params = []
    for parameter in parameters:
        formatted_param = {
            'name': parameter.param_name,
            'value': parameter.param_value,
            'type': parameter.param_type
        }
        formatted_params.append(formatted_param)

    return formatted_params


def create_default_parameter_record(db_engine_id, param_name, param_value, param_type, session=None):
    '''
    Create a DefaultParameter record

    Parameters
    ----------
    db_engine_id: DbEngine whose default parameter record we're creating
    param_name: Name of the default parameter
    param_value: Value of the default parameter
    param_type: Type of the default parameter (Static / Dynamic)
    session: Current SQLAlchemy session

    Returns
    -------
    Created DefaultParameter object
    '''
    parameter_ref = models.DefaultParameter()
    parameter_ref.db_engine_id = db_engine_id
    parameter_ref.param_name = param_name
    parameter_ref.param_value = param_value
    parameter_ref.param_type = param_type
    parameter_ref.created_at = timeutils.utcnow()
    parameter_ref.updated_at = timeutils.utcnow()

    session = session or get_session()
    with session.begin(subtransactions=True):
        parameter_ref.save(session)
        return parameter_ref

def get_default_parameters_for_instance(instance_id, session=None):
    '''
    Get all default parameters for a specific instance

    Parameters
    ----------
    instance_id: Specific Instance to get default parameters for
    session: Current SQLAlchemy session

    Returns
    -------
    List of default parameters
    '''
    session = session or get_session()
    with session.begin(subtransactions=True):
        instance = model_query(None,
                    models.Instance,
                    session=session). \
                    options(joinedload('artefact').joinedload('db_engine')). \
                    filter_by(id=instance_id). \
                    one()

        return get_default_parameters_for_db_engine_id(instance.artefact.db_engine.id)

def get_parameter_variable_values_for_instance(instance_id, session=None):
    '''
    Get Parameter Variable corresponding to a particular Instance

    Parameters
    ----------
    instance_id: Specific instance to get parameter variables for
    session: Current SQLAlchemy session

    Returns
    -------
    Dictionary of parameter variables
    '''
    session = session or get_session()
    with session.begin(subtransactions=True):
        instance = model_query(None,
                    models.Instance,
                    session=session). \
                    options(joinedload('nova_flavor')). \
                    filter_by(id=instance_id). \
                    one()

    parameter_variables = {
        'AllocatedStorage': instance.storage_size * 1024 * 1024 * 1024,               # Providing value in bytes
        'DBInstanceClassMemory': instance.nova_flavor.ram * 1024 * 1024 * 1024,       # Providing value in bytes
        'EndPointPort': instance.port
    }

    return parameter_variables


def get_parameter_variable_values_for_scale_instance(context, instance_id, target_nova_flavor_id, session=None):
    '''
    Get Parameter Variable corresponding to a particular Instance

    Parameters
    ----------
    instance_id: Specific instance to get parameter variables for
    session: Current SQLAlchemy session

    Returns
    -------
    Dictionary of parameter variables
    '''
    session = session or get_session()
    with session.begin(subtransactions=True):

        instance = model_query(None,
                               models.Instance,
                               session=session). \
            options(joinedload('nova_flavor')). \
            filter_by(id=instance_id). \
            one()

        nova_flavor = nova_flavor_get(context,target_nova_flavor_id,session)

    parameter_variables = {
        'AllocatedStorage': instance.storage_size * 1024 * 1024 * 1024,               # Providing value in bytes
        'DBInstanceClassMemory': nova_flavor.ram * 1024 * 1024 * 1024,       # Providing value in bytes
        'EndPointPort': instance.port
    }

    return parameter_variables
