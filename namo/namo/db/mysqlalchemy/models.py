# Copyright (c) 2011 X.commerce, a business unit of eBay Inc.
# Copyright 2010 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# Copyright 2011 Piston Cloud Computing, Inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
"""
SQLAlchemy models for metadata backend.
"""

from oslo_config import cfg
from oslo_db.sqlalchemy import models as lib_models
from oslo_utils import timeutils
from sqlalchemy import Column, DateTime, Enum, Integer, String, Text
from sqlalchemy import UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey 
from sqlalchemy.orm import relationship
from sqlalchemy.orm import backref

from sqlalchemy.dialects import mysql

CONF = cfg.CONF
BASE = declarative_base()

class NamoBase(#lib_models.TimestampMixin,
                 lib_models.ModelBase):
    """Base class for Namo Models."""

    __table_args__ = {'mysql_engine': 'InnoDB'}

    # TODO(rpodolyaka): reuse models.SoftDeleteMixin in the next stage
    #                   of implementing of BP db-cleanup
    #deleted_at = Column(DateTime)
    # TODO(rushiagr): remove deleted boolean once we add the *_deleted tables
    deleted = Column(Integer, nullable=False, default=0)
    created_at = Column(mysql.DATETIME(fsp=3), nullable=False)
    updated_at = Column(mysql.DATETIME(fsp=3), nullable=False)
    metadata = None

    def destroy(self, session, delete_count=1):
        #""Delete this object.""
        self.deleted = delete_count
        self.updated_at = timeutils.utcnow()
        with session.begin(subtransactions=True):
            session.add(self)
            session.flush

    # TODO(rushiagr): we should call this delete, and the above method as
    # soft_delete
    def hard_delete(self, session):
        # Read this to know why we're using substansactions. Basically looks
        # like a bug in sqlalchemy, which might or might not have been fixed by
        # now.
        # https://github.com/openstack/oslo.db/blob/2e79681670f7b8e6843abf29d6e95ffda0c3147c/oslo_db/sqlalchemy/models.py#L38
        with session.begin(subtransactions=True):
            session.delete(self)
            session.flush

# TODO(rushiagr): make database __tablename__ variables either singular or
# plural for all tables


class Instance(BASE, NamoBase):
    """Represents a running service on a host."""

    __tablename__ = 'instances'
    __table_args__ = (
        UniqueConstraint('owner_id', 'name', 'deleted',
            name='uniq_instance0owner_id0name0deleted'),
        {'mysql_engine': 'InnoDB'}
        )

    id = Column(Integer, primary_key=True, autoincrement=True)
    # TODO(rushiagr): an owner cannot have two backups with same name I think.
    # In that case, we can create a unique index. BUT only in case when we're
    # moving deleted columns to a different *_deleted table.
    name = Column(String(50), nullable=False,)
    owner_id = Column(
        mysql.DECIMAL(precision=12, scale=0, unsigned=True, zerofill=True),
        nullable=False, index=True)
    storage_size = Column(Integer, nullable=False)
    nova_flavor_id = Column(Integer, ForeignKey('nova_flavors.id'), nullable=False)
    port= Column(Integer, nullable=False)
    lifecycle = Column(
            Enum('ACTIVE', 'CREATING', 'DELETING', 'PATCHING', 'RESTORING', 'BACKUP','MODIFYING'),
            nullable=False,
            default='CREATING')
    changestate = Column(
            Enum('NONE', 'PENDING', 'APPLYING'),
            nullable=False,
            default='PENDING')
    artefact_id = Column(Integer, ForeignKey('artefact.id'), nullable=False)
    bucket_id = Column(Integer, ForeignKey('buckets.id'), nullable=True)
    master_username = Column(String(128), nullable=False)
    master_user_password = Column(String(128), nullable=False)
    backup_retention_period_days = Column(Integer, nullable=False, default=1)
    # Backup Window format "HH:MM-HH:MM" e.g. 12:15-12:50
    preferred_backup_window_start = Column(mysql.TIME, nullable=False)
    preferred_backup_window_end = Column(mysql.TIME, nullable=False)
    # Maintenance Window format "DDD:HH:MM-DDD:HH:MM" e.g. Sun:23:31-Mon:00:06
    preferred_maintenance_window_start_day = Column(
            Enum('SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'),
            nullable=False)
    preferred_maintenance_window_start_time = Column(mysql.TIME, nullable=False)
    preferred_maintenance_window_end_day = Column(
            Enum('SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'),
            nullable=False)
    preferred_maintenance_window_end_time = Column(mysql.TIME, nullable=False)
    # NOTE(rushiagr): restored_from_backup_id is not a foreign key to backups table. This
    # is because the backup row can be deleted but we'll still need
    # restored_from_backup_id entry -- something which a foreign key won't allow us to do.
    restored_from_backup_id = Column(Integer, nullable=True)

    # This should record the actual time when customer's instance was created.
    # Might be helpful for metering the customer in future
    instance_creation_time = Column(mysql.DATETIME(fsp=3), nullable=True)
    endpoint = Column(String(255), nullable=True)
    # This retention time of objects inside the bucket would be 7 days by default.
    log_retention_period = Column(Integer, default=7, nullable=False)

    nova_flavor = relationship("Flavor", backref="instances",
            foreign_keys=nova_flavor_id)
    artefact = relationship("Artefact", backref="instances",
            foreign_keys=artefact_id)
    bucket = relationship("Buckets", backref="instances", foreign_keys=bucket_id)

    # TODO(rushiagr): write a generic to_dict in the parent class, to avoid
    #   duplication
    def to_dict(self):
        return {'id': self.id,
                'name': self.name,
                'owner_id': int(str(self.owner_id)),
                'storage_size': self.storage_size,
                'state': self.lifecycle,
                'port':self.port}

    # adding column modified_at to contain timestamp
    # for manual enable/disable of cinder services
    # updated_at column will now contain timestamps for
    # periodic updates

class Host(BASE, NamoBase):
    __tablename__ = 'hosts'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer,primary_key=True, autoincrement=True)
    instance_id = Column(Integer, ForeignKey('instances.id'), nullable=False)
    keypair_id = Column(Integer, ForeignKey('keypairs.id'), nullable=False)
    az = Column(String(20), nullable=False)
    nova_uuid = Column(String(36), nullable=False)
    nova_flavor_id = Column(Integer, ForeignKey('nova_flavors.id'), nullable=False)
    # TODO(rushiagr): use BINARY(16) for image_id column?
    vm_ip = Column(String(20), nullable=True)
    vm_ip_id = Column(String(36), nullable=True)
    artefact_id = Column(Integer, ForeignKey('artefact.id'), nullable=False)

    instance = relationship("Instance", backref="hosts",
            foreign_keys=instance_id)
    keypair = relationship("Keypair", backref="hosts",
            foreign_keys=keypair_id)
    nova_flavor = relationship("Flavor", backref="hosts",
            foreign_keys=nova_flavor_id)
    artefact = relationship("Artefact", backref="hosts",
            foreign_keys=artefact_id)

class Image(BASE, NamoBase):
    __tablename__ = 'images'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer,primary_key=True, autoincrement=True)
    glance_uuid = Column(String(36), nullable=False, unique=True)
    artefact_id = Column(Integer, ForeignKey('artefact.id'))

    artefact = relationship("Artefact", backref="images",
            foreign_keys=artefact_id)
    # TODO(rushiagr): need to create index on (artefact_id, glance_uuid), in
    # that order, as we're writing 'get glance uuid for artefact'
    # TODO(rushiagr): need to create unique constraint on (artefact_id,
    # glance_uuid), so that we avoid adding two entries with different
    # glance_uuids but same artefact_id. For now just assume that there's going
    # to be exactly one artefact id value in the entire table

class Flavor(BASE, NamoBase):
    __tablename__ = 'nova_flavors'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer,primary_key=True, autoincrement=True)
    nova_flavor_id = Column(String(36), nullable=False, unique=True)
    flavor_name = Column(String(20), nullable=False)
    ram = Column(mysql.DECIMAL(precision=7, scale=2, unsigned=True), nullable=False)
    vcpus = Column(Integer, nullable=False)

class Volume(BASE, NamoBase):
    __tablename__ = 'volumes'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer,primary_key=True, autoincrement=True)
    cinder_uuid = Column(String(36), nullable=False, unique=True)
    host_id = Column(Integer, ForeignKey('hosts.id'), nullable=False)
    volume_type = Column(String(20), nullable=False)
    attach_status = Column(Enum('NOT ATTACHED','ATTACHED'), nullable=False, default='NOT ATTACHED')

    host = relationship("Host", backref="volumes", foreign_keys=host_id)

class Keypair(BASE, NamoBase):
    __tablename__ = 'keypairs'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer,primary_key=True, autoincrement=True)
    nova_key_id = Column(String(36), nullable=False, unique=True)
    private_key = Column(String(4200), nullable=False)

# TODO(rushiagr): nnullable = true makes foreign key removable?

class Artefact(BASE, NamoBase):
    """
    Represents an 'artefact'.

    An artefact is nothing but a tuple of Raga, and Glance image information.
    Glance image in turn is a tuple of OS and database engine information
    """
    __tablename__ = 'artefact'
    # NOTE: __table_args__ should be a tuple, so don't forget comma
    __table_args__ = (
        UniqueConstraint('raga_id', 'os_id', 'db_engine_id',
            name='uniq_artefact0raga_id0os_id0db_engine_id'),
        {'mysql_engine': 'InnoDB'}
        )
    id = Column(Integer, primary_key=True, autoincrement=True)
    raga_id = Column(Integer, ForeignKey('raga.id'))
    os_id = Column(Integer, ForeignKey('os.id'))
    db_engine_id = Column(Integer, ForeignKey('db_engine.id'))

    raga = relationship("Raga", backref="artefacts", foreign_keys=raga_id)
    os = relationship("Os", backref="artefacts", foreign_keys=os_id)
    db_engine = relationship("DbEngine", backref="artefacts",
            foreign_keys=db_engine_id)

    def to_dict(self):
        return {
                'id': self.id,
                'raga_id': self.raga_id,
                'os_id': self.os_id,
                'db_engine_id': self.db_engine_id,
                }

class Raga(BASE, NamoBase):
    """
    Represents information about a Raga package.

    'version' is an 8-char unique hex ID, like 'a8c23dfb'
    'url' is the URL of the raga package in object store
    'state' can be 'active', 'preferred', or 'deprecated'

    """
    __tablename__ = 'raga'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    version = Column(String(8), nullable=False, unique=True)
    url = Column(String(254), nullable=False)
    state = Column(
                Enum('ACTIVE', 'PREFERRED', 'DEPRECATED'),
                nullable=False,
                index=True,
                default='ACTIVE',
    )

    def to_dict(self):
        return {
                'id': self.id,
                'name': self.name,
                'major_version': self.major_version,
                'minor_version': self.minor_version,
                'url': self.url,
                'state': self.state,
                }

class DbEngine(BASE, NamoBase):
    """
    Represents database engine information.

    'name' can be 'mysql', 'oracle', etc..
    'major_version' can be '5.5', etc
    'minor_version' can be '1', '2', etc.
    'url' is the URL of the place to get database binaries, if any
    'state' can be 'active', 'preferred', or 'deprecated'
    'preferred' will help us find out which db engine version to use in case the
    user just specifies database name, and no version. For each database, there
    will be exactly one entry with preferred as state

    Using free-form strings for major_version and minor_version to allow flexibility
    if in case in future we get non-integer version numbers.
    """
    __tablename__ = 'db_engine'
    __table_args__ = (
        UniqueConstraint('name', 'major_version', 'minor_version', 'deleted',
            name='uniq_db_engine0name0major_version0minor_version0deleted'),
        {'mysql_engine': 'InnoDB'}
        )
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    major_version = Column(String(20), nullable=False)
    minor_version = Column(String(20), nullable=False)
    url = Column(String(254), nullable=False)
    state = Column(
                Enum('ACTIVE', 'PREFERRED', 'DEPRECATED'),
                nullable=False,
                index=True,
                default='ACTIVE',
    )

    def to_dict(self):
        return {
                'id': self.id,
                'name': self.name,
                'major_version': self.major_version,
                'minor_version': self.minor_version,
                'url': self.url,
                'state': self.state,
                }

class Os(BASE, NamoBase):
    """
    Represents operating system information.

    'name' can be 'ubuntu', 'windows', etc..
    'major_version' can be '12.04', , '98', etc
    'minor_version' can be '1', '2', etc.
    'url' is the URL of the operating system image in object store
    'state' can be 'active', 'preferred', or 'deprecated'

    Using free-form strings for major_version and minor_version to allow flexibility
    if in case in future we get non-integer version numbers.
    """
    __tablename__ = 'os'
    __table_args__ = (
        UniqueConstraint('name', 'major_version', 'minor_version', 'deleted',
            name='uniq_db_engine0name0major_version0minor_version0deleted'),
        {'mysql_engine': 'InnoDB'}
        )
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    major_version = Column(String(20), nullable=False)
    minor_version = Column(String(20), nullable=False)
    url = Column(String(254), nullable=False)
    state = Column(
                Enum('ACTIVE', 'PREFERRED', 'DEPRECATED'),
                nullable=False,
                index=True,
                default='ACTIVE',
    )

    def to_dict(self):
        return {
                'id': self.id,
                'name': self.name,
                'major_version': self.major_version,
                'minor_version': self.minor_version,
                'url': self.url,
                'state': self.state,
                }

class Backup(BASE, NamoBase):
    __tablename__ = 'backups'
    __table_args__ = (
        UniqueConstraint('owner_id', 'name', 'deleted',
            name='uniq_backup0owner_id0name0deleted'),
        {'mysql_engine': 'InnoDB'}
        )

    id = Column(Integer, primary_key=True, autoincrement=True)
    # NOTE(rushiagr): instance_id is not a foreign key to instances table. This
    # is because the instances row can be deleted but we'll still need backup
    # row -- something which a foreign key won't allow us to do.
    instance_id = Column(Integer, nullable=False)
    name = Column(String(255), nullable=False)
    owner_id = Column(
            mysql.DECIMAL(precision=12, scale=0, unsigned=True, zerofill=True),
            nullable=False)
    # TODO(rushiagr): add a default entry to type?
    type = Column(Enum('AUTOMATED', 'MANUAL'), nullable=False)
    # TODO(rushiagr): DELETED? Why's this for?
    lifecycle = Column(Enum('ACTIVE', 'CREATING', 'DELETING', 'RESTORING'),
            nullable=False, default='CREATING')
    changestate = Column(Enum('NONE', 'PENDING', 'APPLYING'), nullable=False,
            default='PENDING')
    # TODO(rushiagr): use mysql-specific tiny/medium int?
    num_backup_tries = Column(Integer, default=0, nullable=False)

    # Instance info is stored in these columns
    storage_size = Column(Integer, nullable=False)
    # TODO(rushiagr): use mysql-specific UNSIGNED SMALLINT? Range of ports ==
    # range of mysql's unsigned smallint.
    port = Column(Integer, nullable=False)
    master_user_name = Column(String(255), nullable=False)
    master_user_password = Column(String(255), nullable=False)
    nova_flavor_id = Column(Integer, ForeignKey('nova_flavors.id'),
            nullable=False)
    artefact_id = Column(Integer, ForeignKey('artefact.id'), nullable=False)
    backup_retention_period_days = Column(Integer, nullable=False, default=1)
    # Backup Window format "HH:MM-HH:MM" e.g. 12:15-12:50
    preferred_backup_window_start = Column(mysql.TIME, nullable=False)
    preferred_backup_window_end = Column(mysql.TIME, nullable=False)
    # Maintenance Window format "DDD:HH:MM-DDD:HH:MM" e.g. Sun:23:31-Mon:00:06
    preferred_maintenance_window_start_day = Column(
            Enum('SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'),
            nullable=False)
    preferred_maintenance_window_start_time = Column(mysql.TIME, nullable=False)
    preferred_maintenance_window_end_day = Column(
            Enum('SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'),
            nullable=False)
    preferred_maintenance_window_end_time = Column(mysql.TIME, nullable=False)

    # This retention time of objects inside the bucket would be 7 days by default.
    log_retention_period = Column(Integer, default=7, nullable=False)

    artefact = relationship("Artefact", backref="backups",
            foreign_keys=artefact_id)
    nova_flavor = relationship("Flavor", backref="backups",
            foreign_keys=nova_flavor_id)

class VolumeSnapshot(BASE, NamoBase):
    __tablename__ = 'volume_snapshots'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    # TODO Add unique key contrain for cinder snapshot id volumeid backupid
    # Dont add foreign key constrain on volume id
    id = Column(Integer, primary_key=True, autoincrement=True)
    cinder_snapshot_id = Column(String(36), nullable=False) # is a UUID
    # NOTE(rushiagr): volume_id is not a foreign key to volumes table. This
    # is because the volumes row can be deleted but we'll still need
    # volume_snapshots row -- something which a foreign key won't allow us to
    # do.
    volume_id = Column(Integer, nullable=False)
    backup_id = Column(Integer, ForeignKey('backups.id'), nullable=False)

    backup = relationship("Backup", backref="volume_snapshots",
            foreign_keys=backup_id)


class Buckets(BASE, NamoBase):
    __tablename__ = 'buckets'
    __table_args__ = (
        UniqueConstraint('owner_id', 'deleted',
                         name='uniq_bucket0owner_id0deleted'),
        {'mysql_engine': 'InnoDB'}
    )
    # primary key of the table and also the foreign key for the
    id = Column(Integer, primary_key=True, autoincrement=True)
    # name would be unique for the bucket and can be updated at later stage as well.
    name = Column(String(255), nullable=False)
    # to record the state of bucket.
    state = Column(
        Enum('PENDING', 'ACTIVE', 'DELETED'),
        nullable=False, default='PENDING')
    owner_id = Column(
        mysql.DECIMAL(precision=12, scale=0, unsigned=True, zerofill=True),
        nullable=False, index=True)
    attached_policy_id = Column(String(255), nullable=True)


# TODO(shank): In the future, considering removing this table and using the
# infiniteworkflow table for MasterWorkflow idempotency too
class MasterWorkflow(BASE, NamoBase):
    __tablename__ = 'masterworkflow'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer,primary_key=True, autoincrement=True)
    execution_id = Column(String(36), nullable=True)

# TODO(shm): Using Enum in mysql is a bad idea. Create corresponding tables instead
class InfiniteWorkflow(BASE, NamoBase):
    __tablename__ = 'infiniteworkflows'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    id = Column(Integer,primary_key=True, autoincrement=True)
    execution_id = Column(String(36), nullable=True)
    workflow_type = Column(
        Enum('AUTO_BACKUP', 'CLEAN_AUTO_BACKUP'),
        nullable=False, unique=True)

class DP_Operations(BASE, NamoBase):
    __tablename__ = 'dp_operations'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer,primary_key=True, autoincrement=True)
    priority = Column(Integer, nullable=False )
    description= Column(String(4200))
    operation_name = Column(
        Enum('UPGRADE_SOFTWARE','MODIFY_INSTANCE'), nullable=False)
    operation_type= Column(
        Enum('MAINTENANCE_WINDOW','NOT_SCHEDULED'), nullable=False)
    status= Column(
        Enum('PENDING', 'IN-PROGRESS', 'SUCCESS', 'PARTIAL-SUCCESS', 'FAILED'),
        nullable=False)

class DP_Tasks(BASE, NamoBase):
    __tablename__ = 'dp_tasks'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer,primary_key=True, autoincrement=True)
    operation_id = Column(Integer, ForeignKey('dp_operations.id'), nullable=False)
    instance_id = Column(Integer, ForeignKey('instances.id'), nullable=False) # TODO(shm) host_id ?
    task_runner_id = Column(String(36), nullable=True)
    available_after = Column(mysql.DATETIME(fsp=3), nullable=True)
    task_data = Column(Text, nullable=True)
    state = Column(
        Enum('PENDING', 'IN-PROGRESS', 'SUCCESS', 'FAILED'),
        nullable=False)

class Workflow(BASE, NamoBase):
    __tablename__ = 'workflows'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    id = Column(Integer,primary_key=True, autoincrement=True)
    execution_id = Column(String(36), nullable=False, unique=True)
    workflow_name = Column(
        Enum('CREATE_INSTANCE', 'DELETE_INSTANCE', 'RESTORE_INSTANCE', 'CREATE_BACKUP', 'DELETE_BACKUP', 'MASTER','MODIFY_INSTANCE'),
        nullable=False)
    instance_id = Column(Integer, nullable=True)
    backup_id = Column(Integer, nullable=True)
    state = Column(Enum('IN-PROGRESS', 'SUCCESS', 'FAILURE'), nullable=False, default='IN-PROGRESS')

class UserQuota(BASE, NamoBase):
    __tablename__ = 'user_quotas'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    # the default global values will be contained in the metadata with owner_id = -1
    # the metadata must contain an entry permanently with owner_id = -1
    # overriding the inherited attributes because they are not required
    deleted = Column(Integer, nullable=False, default=0)
    created_at = Column(mysql.DATETIME(fsp=3), nullable=True)
    updated_at = Column(mysql.DATETIME(fsp=3), nullable=True)

    id = Column(Integer, primary_key=True, autoincrement=True)
    owner_id = Column(
        mysql.DECIMAL(precision=12, scale=0),
        nullable=False)
    quota_name = Column(String(255), Enum('total_snapshots', 'total_instances', 'total_storage'), nullable=False)
    quota_limit = Column(Integer, nullable=False)

class DefaultParameter(BASE, NamoBase):
    __tablename__ = 'default_parameters'
    __table_args__ = (
        UniqueConstraint('db_engine_id', 'param_name', 'deleted',
            name='uniq_default_parameter0db_engine_id0param_name0deleted'),
        {'mysql_engine': 'InnoDB'}
    )

    id = Column(Integer, primary_key=True, autoincrement=True)
    db_engine_id = Column(Integer, ForeignKey('db_engine.id'), nullable=False)
    param_name = Column(String(255), nullable=False)
    param_value = Column(String(255), nullable=False)
    param_type = Column(String(7), Enum('dynamic', 'static'), nullable=False)

    db_engine = relationship("DbEngine", backref="default_parameters",
            foreign_keys=db_engine_id)

def register_models(connection_str=None):
    from sqlalchemy import create_engine
    # TODO(rushiagr): interestingly, I have not added any artefacts related
    # table here, but those are still getting created. So does that mean we
    # don't need these lines here? :)
    models = [Instance, Keypair, Flavor, Image, Host, Volume, Backup, VolumeSnapshot, MasterWorkflow, UserQuota, Workflow, InfiniteWorkflow]

    # This is a way of consolidating Oslo db config parameters with pecan ones
    # We load pecan config parameters in *_pecan_config_override options and
    # if they exist, replace with the appropriate parameter. Directly setting
    # these options in pecan causes a conflict because the Oslo Config gets
    # initialized much later and it throws a duplicate key exception
    try:
        if CONF.database.connection_pecan_config_override:
            CONF.database.connection = CONF.database.connection_pecan_config_override
    except cfg.NoSuchOptError:
        pass

    connection_str = connection_str if connection_str is not None else CONF.database.connection
    engine = create_engine(connection_str, echo=True)
    for model in models:
        model.metadata.create_all(engine)

if __name__ == '__main__':
    register_models()
