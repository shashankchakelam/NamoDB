ALTER TABLE artefact MODIFY deleted INT;
ALTER TABLE backups MODIFY deleted INT;
ALTER TABLE db_engine MODIFY deleted INT;
ALTER TABLE hosts MODIFY deleted INT;
ALTER TABLE images MODIFY deleted INT;
ALTER TABLE infiniteworkflows MODIFY deleted INT;
ALTER TABLE instances MODIFY deleted INT;
ALTER TABLE keypairs MODIFY deleted INT;
ALTER TABLE masterworkflow MODIFY deleted INT;
ALTER TABLE nova_flavors MODIFY deleted INT;
ALTER TABLE os MODIFY deleted INT;
ALTER TABLE raga MODIFY deleted INT;
ALTER TABLE user_quotas MODIFY deleted INT;
ALTER TABLE volume_snapshots MODIFY deleted INT;
ALTER TABLE volumes MODIFY deleted INT;
ALTER TABLE workflows MODIFY deleted INT;

