-- This is tied to deploying the default parameters feature
-- This script modifies the nova_flavors table to introduce 2 non-nullable columns

-- There's no need for seperate DDL for the default_parameters table since
-- SQLAlchemy's create_all() method takes care of creating it (called during register_models)
-- after checking that the table doesn't exist in the database
-- http://docs.sqlalchemy.org/en/latest/core/metadata.html#sqlalchemy.schema.MetaData.create_all

-- Create Column but keeping them nullable
ALTER TABLE nova_flavors ADD ram DECIMAL(7, 2) NULL;
ALTER TABLE nova_flavors ADD vcpus INT NULL;

-- Fill in appropriate values
UPDATE nova_flavors SET ram = '3.75', vcpus = '1' WHERE flavor_name = 'c1.small';
UPDATE nova_flavors SET ram = '7.5', vcpus = '2' WHERE flavor_name = 'c1.medium';
UPDATE nova_flavors SET ram = '15', vcpus = '4' WHERE flavor_name = 'c1.large';
UPDATE nova_flavors SET ram = '30', vcpus = '8' WHERE flavor_name = 'c1.xlarge';

-- Alter columns to be non-nullable
ALTER TABLE nova_flavors MODIFY ram DECIMAL(7, 2) NOT NULL;
ALTER TABLE nova_flavors MODIFY vcpus INT NOT NULL;
