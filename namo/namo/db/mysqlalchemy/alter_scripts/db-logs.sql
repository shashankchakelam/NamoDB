use namo;

CREATE TABLE `buckets` (
  `deleted` int(11) NOT NULL,
  `created_at` datetime(3) NOT NULL,
  `updated_at` datetime(3) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `state` enum('PENDING','ACTIVE','DELETED') NOT NULL,
  `owner_id` decimal(32,0) unsigned NOT NULL,
  `attached_policy_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_bucket0owner_id0deleted` (`owner_id`,`deleted`),
  KEY `ix_buckets_owner_id` (`owner_id`)
);

ALTER TABLE namo.instances ADD bucket_id INT NULL;
ALTER TABLE namo.instances ADD log_retention_period INT NULL;
ALTER TABLE namo.instances
ADD CONSTRAINT instances_buckets_id_fk
FOREIGN KEY (bucket_id) REFERENCES buckets (id);

ALTER TABLE namo.backups ADD log_retention_period INT NULL;