-- MySQL dump 10.13  Distrib 5.6.29, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: namo
-- ------------------------------------------------------
-- Server version	5.6.29-1trusty

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `default_parameters`
--

DROP TABLE IF EXISTS `default_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `default_parameters` (
  `deleted` int(11) NOT NULL,
  `created_at` datetime(3) NOT NULL,
  `updated_at` datetime(3) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `db_engine_id` int(11) NOT NULL,
  `param_name` varchar(255) NOT NULL,
  `param_value` varchar(255) NOT NULL,
  `param_type` varchar(7) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_default_parameter0db_engine_id0param_name0deleted` (`db_engine_id`,`param_name`,`deleted`),
  CONSTRAINT `default_parameters_ibfk_1` FOREIGN KEY (`db_engine_id`) REFERENCES `db_engine` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `default_parameters`
--

LOCK TABLES `default_parameters` WRITE;
/*!40000 ALTER TABLE `default_parameters` DISABLE KEYS */;
INSERT INTO `default_parameters` VALUES (0,'2016-07-21 14:39:55.000','2016-07-21 14:39:55.000',1,1,'innodb_log_file_size','536870912','static'),(0,'2016-07-21 14:40:01.000','2016-07-21 14:40:01.000',4,1,'innodb_buffer_pool_size','{DBInstanceClassMemory*3/4}','static'),(0,'2016-07-21 14:40:01.000','2016-07-21 14:40:01.000',7,1,'innodb_flush_method','O_DIRECT','static'),(0,'2016-07-21 14:40:02.000','2016-07-21 14:40:02.000',10,1,'max_connections','{DBInstanceClassMemory/12582880}','dynamic'),(0,'2016-07-21 14:41:14.000','2016-07-21 14:41:14.000',13,2,'shared_buffers','{DBInstanceClassMemory/32768}','static'),(0,'2016-07-21 14:41:14.000','2016-07-21 14:41:14.000',16,2,'max_connections','{DBInstanceClassMemory/12582880}','static'),(0,'2016-07-21 14:41:14.000','2016-07-21 14:41:14.000',19,2,'max_stack_depth','6144','dynamic'),(0,'2016-07-21 14:41:14.000','2016-07-21 14:41:14.000',22,2,'effective_cache_size','{DBInstanceClassMemory/16384}','dynamic'),(0,'2016-07-21 14:41:14.000','2016-07-21 14:41:14.000',25,2,'checkpoint_completion_target','0.9','dynamic'),(0,'2016-07-21 14:41:15.000','2016-07-21 14:41:15.000',28,2,'checkpoint_segments','16','dynamic');
/*!40000 ALTER TABLE `default_parameters` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-30 12:48:44
