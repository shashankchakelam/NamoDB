#!/bin/bash
set -ex

# If 'mistral-tmux' tmux session is already running, do nothing. Else, create it
if [[ $(tmux list-sessions | grep -c mistral-tmux) != "1" ]]; then
    # Run mistral service inside 'mistral-tmux' tmux session
    tmux new-session -d -s mistral-tmux -n one
    tmux new-window -t mistral-tmux:3 -n mistral
    tmux send-keys -t mistral-tmux:3 "/usr/bin/mistral-server --config-file /etc/mistral/mistral.conf" C-m
    tmux detach-client -s mistral-tmux

    # Wait for the mistral service to come up
    sleep 10
fi

# Clean up workbooks if they're left over by any chance
WORKBOOKS=$(mistral workbook-list | head -n -1 | tail -n +4 | awk '{print $2}')
if [[ $WORKBOOKS != '<none>' ]]; then
    for wb in $WORKBOOKS; do
        mistral workbook-delete $wb
    done
fi

YAML_FILES=$(ls conf | grep yaml$)

for file in $YAML_FILES; do
    mistral workbook-create conf/$file
done

# Clean up workbooks
WORKBOOKS=$(mistral workbook-list | head -n -1 | tail -n +4 | awk '{print $2}')
if [[ $WORKBOOKS != '<none>' ]]; then
    for wb in $WORKBOOKS; do
        mistral workbook-delete $wb
    done
fi
