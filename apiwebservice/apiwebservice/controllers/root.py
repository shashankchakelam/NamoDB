import logging
import pecan

from apiwebservice import responses
from apiwebservice import utils
from apiwebservice.auth import auth
from apiwebservice.constants import actions
from apiwebservice import exceptions
from apiwebservice.validators import apis
from apiwebservice.apihandlers import handler_factory
from oslo_config import cfg
from pecan import expose

"""
Refer to https://docs.google.com/document/d/1lxNtGEAucXLxBGV2aORmBxaPMoEwu7-6TcnANuj6EjE/
for the API spec that we're implementing. We support only 2 HTTP methods: GET and POST.
The APIs follow the Query Style paradigm that AWS follows.
"""

LOG = logging.getLogger(__name__)
CONF = cfg.CONF

class RootController(object):

    @expose(generic=True)
    def index(self):
        self._log_request()
        return responses.generate_error_response(
            exceptions.UnsupportedHTTPMethod(name=pecan.request.method)
        )

    @index.when(method='GET')
    def index_get(self):
        self._log_request()
        return self._handle_api_call(actions.get_actions)

    @index.when(method='POST')
    def index_post(self):
        self._log_request()

        # This is a ***HACK*** to support cURL requests!!
        # With cURL 7.35.0 we noticed that POST requests made with no body
        # specified were sent with header "Content-Length" set as blank instead
        # of 0. This resulted in anomolous behaviour in our apiwebservice and
        # request would get hung. So, the user would have to either specify a
        # dummy body or explicitly that Content-Length: 0. To avoid imposing
        # this on the user, explicitly setting this Header to 0 if we get blank
        if not 'Content-Length' in pecan.request.headers or \
            not pecan.request.headers['Content-Length']:
                LOG.debug("Explicitly setting Content-Length header to 0")
                pecan.request.headers['Content-Length'] = 0

        return self._handle_api_call(actions.post_actions)

    def _handle_api_call(self, supported_actions):
        try:
            # Extract query parameters from the URL
            query_params = utils.get_query_params(pecan.request.url)

            # Verify that Action is valid for this HTTP method
            action = self._get_and_validate_action(query_params, supported_actions)

            # Validate the parsed query parameters and get back the API specific
            # Parameter object
            validated_params = self._validate_and_get_parameters(action, query_params)

            # Authenticate and Authorize the API call
            customer_id = auth.handle_auth(action, query_params)
            LOG.debug('Customer id is: ' + str(customer_id))

            # If success, process the request
            return self._handle_action(action, validated_params, customer_id)
        except exceptions.APIException as exc:
            LOG.exception("Handled a known error case:")
            return responses.generate_error_response(exc)
        except Exception:
            LOG.exception("Unknown failure occurred")
            return responses.generate_error_response(exceptions.InternalFailure())

    def _get_and_validate_action(self, query_params, action_list):
        # Verify that an Action has been passed
        action = ''
        if 'Action' in query_params:
            action = query_params['Action']

        if not action:
            LOG.debug("No action found in URL")
            raise exceptions.MissingAction

        # Verify if specified Action is a supported one
        if action not in action_list:
            LOG.debug(
                "Parsed action: %s not supported. Valid actions: %s",
                action,
                action_list)
            raise exceptions.InvalidAction

        LOG.debug("Valid action: %s parse from URL", action)
        return action

    def _validate_and_get_parameters(self, action, query_parameters):
        # dynamic dispatching - the function to be called would be chosen at runtime.
        # for example for modify we will call validate_modify_db_instance, similarly
        # for create we will call valdate_create_db_instance
        # Construct validate method name and dynamically call it
        action_name = utils.convert_camel_case_to_underscore_seperated(action)
        validation_method_name = 'validate_' + action_name
        validate_params_method = getattr(apis, validation_method_name)
        return validate_params_method(query_parameters)

    def _handle_action(self, action, request_params, customer_id):
        # Construct handle method name and dynamically call it
        action_name = utils.convert_camel_case_to_underscore_seperated(action)
        request_handler = handler_factory.get_action_handler(action_name)

        # Sending the request to specific Action Handler for processing the request.
        return request_handler.handle_request(request_params, customer_id)

    def _log_request(self):
        LOG.debug(
            'METHOD: %s, CONTENT_TYPE: %s, URL: %s, HEADERS: %s, BODY: %s',
            pecan.request.method,
            pecan.request.content_type,
            pecan.request.url,
            pecan.request.headers.items(),
            pecan.request.body
        )
