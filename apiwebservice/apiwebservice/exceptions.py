class APIException(Exception):
    """
    Base API Exception class

    To correctly use this class, inherit from it and define
    a 'message' property. That message will get printf'd
    with the keyword arguments provided to the constructor.
    """

    message = "An unknown exception occurred"
    code = "Unknown"
    status_code = 500

    def __init__(self, message=None, *args, **kwargs):
        if not message:
            message = self.message

        if kwargs:
            message = message % kwargs

        self.msg = message
        self.code = self.__class__.__name__
        super(APIException, self).__init__(message)

# Derived Exceptions
# Modelling these to be as close to AWS RDS errors as we can
# http://docs.aws.amazon.com/AmazonRDS/latest/APIReference/CommonErrors.html

class MissingAction(APIException):
    message = "The request is missing an action or a required parameter."
    status_code = 400

class InvalidAction(APIException):
    message = "The action or operation requested is invalid. Verify that the action is typed correctly."
    status_code = 400

class MissingParameter(APIException):
    message = "A required parameter for the specified action is not supplied: %(name)s"
    status_code = 400

class InvalidParameterValue(APIException):
    message = "An invalid or out-of-range value was supplied for the input parameter: %(name)s"
    status_code = 400

class InvalidParameterCombination(APIException):
    message = "Parameters that must not be used together were used together: %(name)s"
    status_code = 400

class InvalidQueryParameter(APIException):
    message = "The JCS query string is malformed or does not adhere to JCS standards."
    status_code = 400

class MalformedQueryString(APIException):
    message = "The query string contains a syntax error."
    status_code = 400

class ValidationError(APIException):
    message = "The input fails to satisfy the constraints specified by a JCS service."
    status_code = 400

class DBInstanceNotFound(APIException):
    message = "%(name)s does not refer to an existing DB instance."
    status_code = 404

class DBSnapshotAlreadyExists(APIException):
    message = "%(name)s is already used by an existing snapshot."
    status_code = 400

class InvalidDBInstanceState(APIException):
    message = "The specified DB instance is not in the %(state)s state."
    status_code = 400

class AuthFailure(APIException):
    message = "Auth failed for this request: %(reason)s"
    status_code = 403

class DBInstanceAlreadyExists(APIException):
    message = "User already has a DB instance with the given identifier %(name)s"
    status_code = 400

class StorageQuotaExceeded(APIException):
    message = "Request would result in user exceeding the allowed quota %(quota)d GB of storage available across " + \
        "all DB instances. Please contact JCS customer service to increase your total data storage quota."
    status_code = 400

class InstanceQuotaExceeded(APIException):
    message = "Request would result in user exceeding the allowed quota %(quota)d of total DB instances. " + \
        "Please contact JCS customer service to increase your total DB instances quota."
    status_code = 400

class InstanceClassQuotaExceeded(APIException):
    message = "Request would result in user exceeding the allowed quota %(quota)d of DB instances for the instance " + \
        "class - %(name)s. Please contact JCS customer service to increase your total DB instances quota for this " + \
        "instance class."
    status_code = 400

class SnapshotQuotaExceeded(APIException):
    message = "Request would result in user exceeding the allowed quota %(quota)d of total DB snapshots across all " + \
        "DB instances. Please delete your old snapshots or contact JCS customer service to increase your total DB " + \
        "snapshots quota."
    status_code = 400

class DBSnapshotNotFound(APIException):
    message = "%(name)s does not refer to an existing DB snapshot."
    status_code = 404

class InvalidDBSnapshotState(APIException):
    message = "The specified DB snapshot is not in the %(state)s state."
    status_code = 400

class InternalFailure(APIException):
    message = "The request processing has failed because of an unknown error, exception or failure."
    status_code = 500

class UnsupportedHTTPMethod(APIException):
    message = "Unsupported HTTP method %(name)s specified"
    status_code = 405

class DBTypeNotFoundByName(APIException):
    message = "DBType Not Found By name %(name)s ."
    status_code = 400
