"""
Policy Engine Middleware
"""
from apiwebservice import exceptions
from oslo_serialization import jsonutils
import pecan
import re

class PolicyEngine():
    """Middleware that sets up environment for authorizing client calls."""

    map_file_contents = {}
    action_key = "action"
    rsrc_list_key = "resources"
    rsrc_key = "resource"
    rsrc_val_rqrd_key = "isResourceValueRequired"
    rsrc_value_key = "resourcePath"
    secondary_actions_key = "secondary_actions"
    indices_regex = r'(\w+[\w.]*)\.N\.?([\w.]*)'

    @classmethod
    def handle_params(cls, action, query_params):
        """
        Given query_parameters and action, return back the Action-Resource list

        Parameters
        ----------
        action: Action that we want to generate mapping for
        query_params: Query Parameters passed by user

        Returns
        -------
        Action-Resource list
        """
        cls.map_file_contents = cls._read_policy_json()
        action_dict = cls.map_file_contents.get(action)

        if not action_dict:
            raise exceptions.InvalidAction(name=action)

        resource_action_list = []
        cls._populate_ra_list(action_dict, resource_action_list, query_params)
        return resource_action_list

    @classmethod
    def _get_resource_value(cls, rsrc_dict, params):
        rsrc_values = []
        path_string = rsrc_dict.get(cls.rsrc_value_key)
        if not path_string:
            return None
        if path_string.find('params.') != 0:
            raise exceptions.InternalFailure()
        path_string = path_string[len('params.') : ]
        # Check if resource path is of the form <keyword>.N
        # This would be for cases like DescribeImages
        match = re.match(cls.indices_regex, path_string)
        if match:
            idx = 1
            path = match.group(1) + '.' + str(idx)
            if match.group(2): path += '.' + match.group(2)

            while params.get(path):
                rsrc_values.append(params.get(path))
                idx += 1
                path = match.group(1) + '.' + str(idx)
                if match.group(2): path += '.' + match.group(2)
        elif params.get(path_string):
            rsrc_values.append(params.get(path_string))
        return rsrc_values

    @classmethod
    def _populate_ra_list(
            cls,
            action_dict,
            ra_list,
            params,
            implicit_allow=False):
        action = action_dict.get(cls.action_key)
        resource_list = action_dict.get(cls.rsrc_list_key)
        secondary_actions = action_dict.get(cls.secondary_actions_key)
        if not action or not resource_list:
            raise exceptions.InternalFailure()
        if not isinstance(resource_list, list):
            raise exceptions.InternalFailure()
        for resource in resource_list:
            rsrc = resource.get(cls.rsrc_key) + ':'
            rsrc_values = []
            if resource.get(cls.rsrc_value_key):
                rsrc_values = cls._get_resource_value(resource, params)
            rsrc_value_rqrd = resource.get(cls.rsrc_val_rqrd_key)
            rsrc_value_rqrd = rsrc_value_rqrd.lower()
            if rsrc_values:
                for value in rsrc_values:
                    ra_entry = {'action': action,
                                'resource': rsrc + value,
                                'implicit_allow': str(implicit_allow)}
                    ra_list.append(ra_entry)
            elif rsrc_value_rqrd == 'true':
                raise exceptions.AuthFailure(message="The request is missing " +
                    "required paramters for authorization.")
            else:
                ra_entry = {'action': action, 'resource': rsrc,
                        'implicit_allow': str(implicit_allow)}
                ra_list.append(ra_entry)
        if secondary_actions:
            for sa in secondary_actions:
                action_dict = cls.map_file_contents.get(sa)
                if not action_dict:
                    raise exceptions.InternalFailure()
                cls._populate_ra_list(action_dict, ra_list, params,
                                      implicit_allow=True)

    @classmethod
    def _read_policy_json(cls):
        filepath = pecan.conf.auth_params['mapping_file']
        with open(filepath) as fap:
            data = fap.read()
            return jsonutils.loads(data)
