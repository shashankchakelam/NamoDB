from apiwebservice import exceptions
from apiwebservice import utils
from apiwebservice.auth import policy_engine
import hashlib
import logging
import pecan
import requests
from oslo_serialization import jsonutils
LOG = logging.getLogger(__name__)

"""
File that contains all Authentication and Authorization methods
"""

def handle_auth(action, params):
    """
    Makes a call to IAM for Authentication and Authorization. We handle 2 cases:
    1. Console will call us with an AWS-style Query URL except that it will pass
    in a token (UUID) instead of AccessKey and Signature parameters. Hence, we
    will pass this token to IAM's token-auth API for AuthN and AuthZ
    2. Regular customers will call us passing AccessKey and Signature params.
    We will pass these to IAM's ec2-auth API for AuthN and AuthZ

    For AuthZ, since we support resource based policies, we need pass IAM a list
    of Action:Resource pairs so that IAM can verify if the users policy allows
    this pair.

    IAM responds back with the UserId, AccountId and keystone-style token.

    Parameters
    ----------
    action: The user-specified action we want to Authorize
    params: Dictionary of parameters that customer specified in URL

    Returns
    -------
    UserID
    """

    # We have a different URL we call based on whether we want to Auth via token
    # or via AccessKey / Signature
    keystone_validation_url = ""

    # pecan.conf.iam_authentication['authorize'] flag is set to false for testing/developing
    if pecan.conf.iam_authentication['authorize'] == 'false':
        LOG.debug('Skipping Authorization for API call')
        return str(pecan.conf.iam_authentication['dummy_customer_id'])
    LOG.debug('Handling Authorization for API call')

    # This will be blank for us
    body_hash = hashlib.sha256(pecan.request.body).hexdigest()
    data = {}
    headers = {'Content-Type': 'application/json'}
    fwd_header = '000.000.0.0'
    if 'X-Forwarded-For' in pecan.request.headers:
        fwd_header = pecan.request.headers['X-Forwarded-For']
    headers['X-Forwarded-For'] = fwd_header
    auth_token = utils.get_auth_token()
    rsrc_action_list = policy_engine.PolicyEngine.handle_params(action, params)

    if auth_token:
        # This handles the console use case where we get a token header
        headers['X-Auth-Token'] = auth_token
        data['action_resource_list'] = rsrc_action_list
        data = jsonutils.dumps(data)
        keystone_validation_url = pecan.conf.keystone['token_url']
    else:
        # Signature parameter shouldn't be part of this dict as this dict is
        # used for calculating the signature!
        signature = params['Signature']
        params.pop('Signature')

        cred_dict = {
            'access': params['JCSAccessKeyId'],
            'signature': signature,
            'host': pecan.request.host,
            'verb': pecan.request.method,
            'path': pecan.request.path,
            'params': params,
            'headers': pecan.request.headers,
            'body_hash': body_hash,
            'action_resource_list': rsrc_action_list,
        }
        creds = {'ec2Credentials': cred_dict}
        data = jsonutils.dumps(creds)
        keystone_validation_url = pecan.conf.keystone['ec2_auth_url']

    response = requests.request(
        'POST',
        keystone_validation_url,
        verify=False,
        data=data,
        headers=headers)

    # Verify if we got a SUCCESS back
    if response.status_code != 200:
        msg = response.text
        raise exceptions.AuthFailure(reason=msg)

    result = response.json()
    LOG.debug(result)

    # We are interested in only the account_id that's returned back. We ignore the
    # remaining fields (token_id, user_id, user_type)
    if 'account_id' not in result:
        # TODO(shank); This should NOT be shown to the customer. Temporarily
        # having it for testing and debugging purposes
        raise exceptions.AuthFailure(message="account_id missing from IAM response")

    user_id = result.get('account_id')
    return user_id
