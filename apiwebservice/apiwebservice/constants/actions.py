"""
File to hold Actions and Resources supported by the API Web Service
"""

"""
Actions we support on GET HTTP requests. Should be consistent with:
https://docs.google.com/document/d/1lxNtGEAucXLxBGV2aORmBxaPMoEwu7-6TcnANuj6EjE/
"""
get_actions = [
    'DescribeDBInstances',
    'DescribeDBSnapshots',
    'DescribeDBTypes'
]

"""
Actions we support on POST HTTP requests. Should be consistent with:
https://docs.google.com/document/d/1lxNtGEAucXLxBGV2aORmBxaPMoEwu7-6TcnANuj6EjE/
"""
post_actions = [
    'CreateDBInstance',
    'DeleteDBInstance',
    'ModifyDBInstance',
    'CreateDBSnapshot',
    'DeleteDBSnapshot',
    'RestoreDBInstanceFromDBSnapshot',
    'UploadDBInstanceLogs'
]

"""
Resource Types we support. Should be consistent with:
https://docs.google.com/document/d/1lxNtGEAucXLxBGV2aORmBxaPMoEwu7-6TcnANuj6EjE/
"""
resource_types = [
    'DBInstance',
    'DBSnapshot'
]