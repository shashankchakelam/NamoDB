from pecan import conf

"""
File to hold parameters supported by the API Web Service
"""

class Parameter(object):
    """
    Parameter class used to model all user passed parameters
    """

    def __init__(self, name, optional=False, default_value=None, value=None, supported_values=None):
        self.name = name
        self.optional = optional
        self.default_value = default_value
        self.value = value
        self.supported_values = supported_values

class BaseParams(object):
    """
    Common query parameters that will be passed with each request
    """
    def __init__(self):
        self.version = Parameter('Version', optional=False, supported_values=['2016-03-01'])
        self.access_key_id = Parameter('JCSAccessKeyId', optional=False)
        self.signature = Parameter('Signature', optional=False)
        self.signature_method = Parameter(
                'SignatureMethod',
                optional=False,
                supported_values=['HmacSHA256'])
        self.signature_version = Parameter(
                'SignatureVersion',
                optional=False,
                supported_values=['2'])
        self.timestamp = Parameter('Timestamp', optional=False)

    def get_base_param_names(self):
        """
        Get names of all the base parameters we support

        Returns
        -------
        List of base parameter names
        """
        return [
            self.version.name,
            self.access_key_id.name,
            self.signature.name,
            self.signature_method.name,
            self.signature_version.name,
            self.timestamp.name
    ]

    def get_mandatory_param_names_except_base_params(self):
        """
        Return names of all mandatory parameters except base parameters
        This is useful for subclasses which actually have other mandatory params

        Returns
        -------
        List of mandatory parameter names
        """
        mandatory_param_names = []
        base_param_names = self.get_base_param_names()

        for key, value in self.__dict__.items():
            # Ignore all attributes in the namespace that have __ as prefix
            # These attributes are default ones associated with all classes
            if not key.startswith("__") and value.optional is False and value.name not in base_param_names:
                mandatory_param_names.append(value.name)

        return mandatory_param_names

    def get_optional_param_names(self):
        """
        Get names of all optional parameters
        This is useful for subclasses which actually have optional params

        Returns
        -------
        List of optional parameter names
        """
        optional_param_names = []

        for key, value in self.__dict__.items():
            # Ignore all attributes in the namespace that have __ as prefix
            # These attributes are default ones associated with all classes
            if not key.startswith("__") and value.optional is True:
                optional_param_names.append(value.name)

        return optional_param_names

class CreateDBInstanceParams(BaseParams):
    """
    CreateDBInstance API Parameters. Inherits all base parameters from BaseParams
    This models all the parameters passed by the customer as input for this API
    """
    def __init__(self):
        super(CreateDBInstanceParams, self).__init__()

        # Mandatory Parameters
        self.db_instance_identifier = Parameter('DBInstanceIdentifier', optional=False)
        self.db_instance_class = Parameter('DBInstanceClass', optional=False)
        self.engine = Parameter('Engine', optional=False)
        self.allocated_storage = Parameter('AllocatedStorage', optional=False)
        self.master_username = Parameter('MasterUsername', optional=False)
        self.master_user_password = Parameter('MasterUserPassword', optional=False)

        # Optional Parameters
        self.port = Parameter('Port', optional=True)
        self.engine_version = Parameter('EngineVersion', optional=True)
        self.preferred_maintenance_window = Parameter('PreferredMaintenanceWindow', optional=True)
        self.preferred_backup_window = Parameter('PreferredBackupWindow', optional=True)
        self.log_retention_period = Parameter(
            'LogRetentionPeriod',
            optional=True,
            default_value=conf.user_parameter_defaults['log_retention_period'])
        self.backup_retention_period = Parameter(
                                        'BackupRetentionPeriod',
                                        optional=True,
                                        default_value=conf.user_parameter_defaults['backup_retention_period'])

class ModifyDbInstanceParams(BaseParams):
    """
    ModifyDbInstanceParams API Parameters. Inherits all base parameters from BaseParams
    This models all the parameters passed by the customer as input for this API
    """
    def __init__(self):
        super(ModifyDbInstanceParams, self).__init__()

        # Mandatory Parameter
        self.db_instance_identifier = Parameter('DBInstanceIdentifier', optional=False)
        # Optional Parameters
        self.new_db_instance_identifier = Parameter('NewDBInstanceIdentifier', optional=True)
        self.db_instance_class = Parameter('DBInstanceClass', optional=True)
        self.preferred_maintenance_window = Parameter('PreferredMaintenanceWindow', optional=True)
        self.preferred_backup_window = Parameter('PreferredBackupWindow', optional=True)
        self.backup_retention_period = Parameter('BackupRetentionPeriod', optional=True)

class DescribeDBInstancesParams(BaseParams):
    """
    DescribeDBInstances API supported parameters. Inherits all base parameters from BaseParams
    This models all the parameters passed by the customer as input for this API
    """

    def __init__(self):
        super(DescribeDBInstancesParams, self).__init__()

        # Optional Parameters
        self.db_instance_identifier = Parameter('DBInstanceIdentifier', optional=True)

class DeleteDBInstanceParams(BaseParams):
    """
    DeleteDBInstance API supported parameters. Inherits all base parameters from BaseParams
    This models all the parameters passed by the customer as input for this API
    """

    def __init__(self):
        super(DeleteDBInstanceParams, self).__init__()

        # Mandatory Parameters
        self.db_instance_identifier = Parameter('DBInstanceIdentifier', optional=False)

        # Optional Parameters
        self.final_db_snapshot_identifier = Parameter('FinalDBSnapshotIdentifier', optional=True)
        self.skip_final_snapshot = Parameter(
                                    'SkipFinalSnapshot',
                                    optional=True,
                                    default_value=False)

class UploadDBInstanceLogsParams(BaseParams):
    """
    UploadDBInstanceLogs API supported parameters. Inherits all base parameters from BaseParams
    This models all the parameters passed by the customer as input for this API
    """

    def __init__(self):
        super(UploadDBInstanceLogsParams, self).__init__()

        # Mandatory Parameters
        self.db_instance_identifier = Parameter('DBInstanceIdentifier', optional=False)

class DescribeDBSnapshotsParams(BaseParams):
    """
    DescribeDBSnapshots API Parameters. Inherits all base parameters from BaseParams
    This models all the parameters passed by the customer as input for this API
    """
    def __init__(self):
        super(DescribeDBSnapshotsParams, self).__init__()

        # Optional Parameters
        self.db_instance_identifier = Parameter('DBInstanceIdentifier', optional=True)
        self.db_snapshot_identifier = Parameter('DBSnapshotIdentifier', optional=True)
        self.snapshot_type = Parameter('SnapshotType', optional=True, supported_values=['manual', 'automated'])

class CreateDBSnapshotParams(BaseParams):
    """
    CreateDBSnapshot API Parameters. Inherits all base parameters from BaseParams
    This models all the parameters passed by the customer as input for this API
    """
    def __init__(self):
        super(CreateDBSnapshotParams, self).__init__()

        # Mandatory Parameters
        self.db_instance_identifier = Parameter('DBInstanceIdentifier', optional=False)
        self.db_snapshot_identifier = Parameter('DBSnapshotIdentifier', optional=False)


class RestoreDBInstanceFromDBSnapshotParams(BaseParams):
    """
    RestoreDBInstanceFromDBSnapshot API supported parameters. Inherits all base parameters from BaseParams
    This models all the parameters passed by the customer as input for this API
    """

    def __init__(self):
        super(RestoreDBInstanceFromDBSnapshotParams, self).__init__()

        # Mandatory Parameter
        self.db_instance_identifier = Parameter('DBInstanceIdentifier', optional=False)
        self.db_snapshot_identifier = Parameter('DBSnapshotIdentifier', optional=False)

        # Optional Parameters
        self.allocated_storage = Parameter('AllocatedStorage', optional=True)
        self.db_instance_class = Parameter('DBInstanceClass', optional=True)
        self.master_username = Parameter('MasterUsername', optional=True)
        self.master_user_password = Parameter('MasterUserPassword', optional=True)
        self.port = Parameter('Port', optional=True)
        self.preferred_maintenance_window = Parameter('PreferredMaintenanceWindow', optional=True)
        self.preferred_backup_window = Parameter('PreferredBackupWindow', optional=True)
        self.log_retention_period = Parameter(
            'LogRetentionPeriod',
            optional=True,
            default_value=conf.user_parameter_defaults['log_retention_period'])
        self.backup_retention_period = Parameter(
            'BackupRetentionPeriod',
            optional=True,
            default_value=conf.user_parameter_defaults['backup_retention_period'])

class DeleteDBSnapshotParams(BaseParams):
    """
    DeleteDBSnapshot API supported parameters. Inherits all base parameters from BaseParams
    This models all the parameters passed by the customer as input for this API
    """

    def __init__(self):
        super(DeleteDBSnapshotParams, self).__init__()

        # Mandatory Parameters
        self.db_snapshot_identifier = Parameter('DBSnapshotIdentifier', optional=False)

class DescribeDBTypesParams(BaseParams):
    """
    DescribeDBEngines API supported parameters. Inherits all base parameters from BaseParams
    This models all the parameters passed by the customer as input for this API
    """

    def __init__(self):
        super(DescribeDBTypesParams,self).__init__()

        #Optional Parameters
        self.db_type_identifier = Parameter('DBTypeId', optional=True)
