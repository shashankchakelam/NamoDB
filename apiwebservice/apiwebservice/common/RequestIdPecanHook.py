import pecan
from pecan.hooks import PecanHook
from apiwebservice.common import CustomThreadLocal as tl

class RequestIdHook(PecanHook):

    # Pecan Hook needs a priority based on which it sorts the list of hooks
    priority = 100

    def before(self, state):
        # Add anything that needs to be done before the api call here
        tl.CustomThreadLocalClass().create_req_id()
    
    def after(self, state):
        # Clear the reqid from ThreadLocal so that next request (by the same thread) does not get the same request id
        tl.CustomThreadLocalClass().clear_req_id()

