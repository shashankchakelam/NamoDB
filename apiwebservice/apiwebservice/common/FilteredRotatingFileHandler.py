import logging

from apiwebservice.common import CustomThreadLocal
from logging import handlers
from logging.handlers import RotatingFileHandler

class FilteredRotatingHandlerClass(RotatingFileHandler):
    """
    Adds the request Id filter and uses all other features of RotatingFileHandler
    """
    def __init__(self, *args, **kwargs):
        RotatingFileHandler.__init__(self, *args, **kwargs)
        self.addFilter(ReqIdFilter())

class ReqIdFilter(logging.Filter):
    """
    This is a filter which injects Request Id into the log.
    """

    def filter(self, record):
        record.req_id = CustomThreadLocal.CustomThreadLocalClass().get_req_id()
        return True

