import time

import apiwebservice.common.CustomThreadLocal as tl
import pecan
from jinja2 import Environment, PackageLoader
from namo.common import exception
from namo.db.mysqlalchemy import api as db
import datetime

"""
File that generates responses for the various API calls
"""

env = Environment(loader=PackageLoader('apiwebservice', 'templates'))

def generate_create_db_instance_body(instance):
    """
    Generate response body for CreateDBInstance API using a Jinja2 template

    Parameters
    ----------
    instance: model.Instance object that was created

    Returns
    -------
    XML body for CreateDbInstance API response
    """
    template = env.get_template('CreateDBInstanceResponse.xml')
    template_formatted_instance = _populate_instance_template(instance)
    return template.render(
        namespace = pecan.conf.xml_namespace['current'],
        instance = template_formatted_instance,
        metadata = _generate_metadata())

def generate_restore_db_instance_from_db_snapshot_body(instance):
    """
    Generate response body for RestoreDBInstanceFromDBSnapshot API using a Jinja2 template

    Parameters
    ----------
    instance: model.Instance object that was created

    Returns
    -------
    XML body for RestoreDBInstanceFromDBSnapshot API response
    """
    template = env.get_template('RestoreDBInstanceFromDBSnapshotResponse.xml')
    template_formatted_instance = _populate_instance_template(instance)
    return template.render(
        namespace = pecan.conf.xml_namespace['current'],
        instance = template_formatted_instance,
        metadata = _generate_metadata())

def generate_create_db_snapshot_body(snapshot):
    """
    Generate response body for CreateDBSnapshotInstance API using a Jinja2
    template

    Parameters
    ----------
    snapshot: model.Backup object that was created

    Returns
    -------
    XML body for CreateDbSnapshot API response
    """
    template = env.get_template('CreateDBSnapshotResponse.xml')
    template_formatted_snapshot = _populate_snapshot_template(snapshot)
    return template.render(
        namespace = pecan.conf.xml_namespace['current'],
        snapshot = template_formatted_snapshot,
        metadata = _generate_metadata())

def generate_delete_db_snapshot_body(snapshot):
    """
    Generate response body for DeleteDBSnapshot API using a Jinja2
    template

    Parameters
    ----------
    snapshot: model.Backup object that was created

    Returns
    -------
    XML body for DeleteDbSnapshot API response
    """
    template = env.get_template('DeleteDBSnapshotResponse.xml')
    template_formatted_snapshot = _populate_snapshot_template(snapshot)
    return template.render(
        namespace = pecan.conf.xml_namespace['current'],
        snapshot = template_formatted_snapshot,
        metadata = _generate_metadata())

def generate_describe_db_instances_body(instances):
    """
    Generate response body for DescribeDBInstances API using a Jinja2 template

    Parameters
    ----------
    instances: List of model.Instance objects that are being described

    Returns
    -------
    XML body for DescribeDbInstances API response
    """
    template = env.get_template('DescribeDBInstancesResponse.xml')
    template_formatted_instances = []

    for instance in instances:
        template_formatted_instance = _populate_instance_template(instance)
        template_formatted_instances.append(template_formatted_instance)
    return template.render(
        namespace = pecan.conf.xml_namespace['current'],
        instances = template_formatted_instances,
        metadata = _generate_metadata())

def generate_delete_db_instance_body(instance):
    """
    Generate response body for DeleteDBInstance API using a Jinja2 template

    Parameters
    ----------
    instance: model.Instance object that was deleted

    Returns
    -------
    XML body for DeleteDbInstance API response
    """
    template = env.get_template('DeleteDBInstanceResponse.xml')
    template_formatted_instance = _populate_instance_template(instance)
    return template.render(
        namespace = pecan.conf.xml_namespace['current'],
        instance = template_formatted_instance,
        metadata = _generate_metadata())

def generate_modify_db_instance_body(instance):
    """
    Generate response body for ModifyDbInstance API using a Jinja2 template

    Parameters
    ----------
    instance that needs to be modified

    Returns
    -------
    XML body for ModifyDbInstance API response
    """
    template = env.get_template('ModifyDBInstanceResponse.xml')

    template_formatted_instance = _populate_instance_template(instance)

    return template.render(
        namespace = pecan.conf.xml_namespace['current'],
        instance = template_formatted_instance,
        metadata = _generate_metadata())

def generate_describe_db_snapshots_body(snapshots):
    """
    Generate response body for DescribeDBSnapshots API using a Jinja2 template

    Parameters
    ----------
    snapshots: List of model.Backup objects that are being described

    Returns
    -------
    XML body for DescribeDbSnapshots API response
    """
    template = env.get_template('DescribeDBSnapshotsResponse.xml')
    template_formatted_snapshots = []

    for snapshot in snapshots:
        template_formatted_snapshot = _populate_snapshot_template(snapshot)
        template_formatted_snapshots.append(template_formatted_snapshot)

    return template.render(
        namespace = pecan.conf.xml_namespace['current'],
        snapshots = template_formatted_snapshots,
        metadata = _generate_metadata())

def generate_error_response(exception):
    """
    Generate user-facing XML error response

    Parameters
    ----------
    exception: An exception that is a sub-class of APIException

    Returns
    -------
    User-facing XML response
    """
    template = env.get_template('ErrorResponse.xml')
    error = {}
    error["message"] = exception.msg
    error["code"] = exception.code
    xml_body = template.render(error = error, metadata = _generate_metadata())

    return generate_response(xml_body, exception.status_code)

def generate_upload_db_instance_logs_body():
    """
    Generate response body for UploadDBInstanceLogs API using a Jinja2 template

    Returns
    -------
    XML body for UploadDBInstanceLogs API response
    """
    template = env.get_template('UploadDBInstanceLogsResponse.xml')
    upload_db_logs = {}
    upload_db_logs['message'] = "Request to upload logs to DSS is submitted successfully"
   

    return template.render(
        namespace=pecan.conf.xml_namespace['current'],
        upload_db_logs=upload_db_logs,
        metadata=_generate_metadata())

def generate_response(body=None, status_code=200, content_type='application/xml'):
    """
    Generate API-WS HTTP Response

    Parameters
    ----------
    body: HTTP Response Body
    status_code: HTTP Response Status Code
    content_type: HTTP Response Content Type

    Returns
    -------
    Formatted HTTP Response
    """

    return pecan.Response(
        body,
        status_code,
        None,
        None,
        content_type
    )

def _get_customer_visible_instance_status(lifecycle):
    """
    Given the instance lifecycle, generate the customer visible instance status
    :param lifecycle: Instance lifecycle
    :return: Customer visible instance status
    """

    if lifecycle.lower() == 'active':
        return 'available'

    return lifecycle.lower()


def _datetime_to_string_converter(instance):

    if not isinstance(instance.preferred_backup_window_start, basestring):
        instance.preferred_backup_window_start = instance.preferred_backup_window_start.strftime('%H:%M');

    if not isinstance(instance.preferred_backup_window_end, basestring):
        instance.preferred_backup_window_end = instance.preferred_backup_window_end.strftime('%H:%M');

    if not isinstance(instance.preferred_maintenance_window_start_time, basestring):
        instance.preferred_maintenance_window_start_time = instance.preferred_maintenance_window_start_time.strftime('%H:%M');

    if not isinstance(instance.preferred_maintenance_window_end_time, basestring):
        instance.preferred_maintenance_window_end_time = instance.preferred_maintenance_window_end_time.strftime('%H:%M');

    return instance


def _populate_instance_template(instance):
    """
    Populate a dictionary that will be applied to the instance template

    Parameters
    ----------
    instance: A model.Instance object

    Returns
    -------
    Formatted instance dictionary
    """
    # HOT FIX to convert the start and end times for backup and maintenance from datetime to str due to
    # disparity in db query. TODO Needs to be fixed
    instance = _datetime_to_string_converter(instance)

    backup_starttime = time.strptime(instance.preferred_backup_window_start,'%H:%M')
    backup_endtime = time.strptime(instance.preferred_backup_window_end,'%H:%M')
    maintenance_start_time = time.strptime(instance.preferred_maintenance_window_start_time,'%H:%M')
    maintenance_end_time = time.strptime(instance.preferred_maintenance_window_end_time,'%H:%M')

    template_formatted_instance = {}
    template_formatted_instance['backup_retention_period'] = \
        instance.backup_retention_period_days
    template_formatted_instance['status'] = \
        _get_customer_visible_instance_status(instance.lifecycle)
    template_formatted_instance['name'] = instance.name
    template_formatted_instance['backup_window'] = \
        time.strftime("%H:%M", backup_starttime) + '-' + \
        time.strftime("%H:%M", backup_endtime)
    template_formatted_instance['maintenance_window'] = \
        instance.preferred_maintenance_window_start_day + ':' + \
        time.strftime("%H:%M", maintenance_start_time) + '-' + \
        instance.preferred_maintenance_window_end_day + ':' + \
        time.strftime("%H:%M", maintenance_end_time)
    template_formatted_instance['engine'] = instance.artefact.db_engine.name
    template_formatted_instance['engine_version'] = \
        instance.artefact.db_engine.major_version + "." + \
        instance.artefact.db_engine.minor_version
    template_formatted_instance['instance_creation_time'] = instance.instance_creation_time
    template_formatted_instance['allocated_storage'] = instance.storage_size
    template_formatted_instance['flavor'] = instance.nova_flavor.flavor_name
    template_formatted_instance['master_username'] = instance.master_username
    template_formatted_instance['port'] = instance.port
    template_formatted_instance['endpoint_address'] = instance.endpoint
    template_formatted_instance['bucket_exists'] = False
    if instance.bucket is not None:
        template_formatted_instance['bucket_exists'] = True
        template_formatted_instance['bucket_name'] = instance.bucket.name
        template_formatted_instance['bucket_prefix_name'] = "DbLogs/" + instance.name
        template_formatted_instance['log_retention_period'] = instance.log_retention_period
        template_formatted_instance['bucket_status'] = instance.bucket.state

    return template_formatted_instance


def _populate_snapshot_template(snapshot):
    """
    Populate a dictionary that will be applied to the snapshot template

    Parameters
    ----------
    snapshot: A model.Backup object

    Returns
    -------
    Formatted snapshot dictionary
    """
    template_formatted_snapshot = {}

    try:
        # Try getting properties from parent instance if it exists. Else, skip
        instance_ref = db.get_user_visible_instance_properties_by_id(snapshot.instance_id)
        template_formatted_snapshot['instance_exists'] = True
        template_formatted_snapshot['instance_creation_time'] = instance_ref["instance_creation_time"]
        template_formatted_snapshot['db_instance_identifier'] = instance_ref["name"]
    except exception.InstanceNotFoundById:
        template_formatted_snapshot['instance_exists'] = False
        pass

    artefact_info = db.get_artefact('', snapshot.artefact_id)
    engine_info = db.get_db_engine('', artefact_info.db_engine_id)

    template_formatted_snapshot['engine_version'] = engine_info.major_version + '.' + engine_info.minor_version
    template_formatted_snapshot['engine'] = engine_info.name
    template_formatted_snapshot['backup_retention_period'] = \
        snapshot.backup_retention_period_days
    template_formatted_snapshot['status'] = \
        _get_customer_visible_instance_status(snapshot.lifecycle)
    template_formatted_snapshot['name'] = snapshot.name
    template_formatted_snapshot['allocated_storage'] = snapshot.storage_size
    template_formatted_snapshot['master_username'] = snapshot.master_user_name
    template_formatted_snapshot['port'] = snapshot.port
    template_formatted_snapshot['type'] = snapshot.type
    return template_formatted_snapshot


def _populate_db_type_template(db_type):
    """
        Populate a dictionary that will be applied to the DBType template

        Parameters
        ----------
        db_type: A dictionary object containing "name" referring to the DBEngine name and
        "db_type_versions" referring to list of models.DBEngine objects,
        basically all versions for a DBEngine as specified in db_type["name"]

        Returns
        -------
        Formatted db_type dictionary
        """
    template_formatted_db_type = {}
    template_formatted_db_type['name'] = db_type['name']
    template_formatted_db_type['db_type_versions'] = []

    for db_type_version in db_type['db_type_versions']:
        template_formatted_db_type_version = {}
        template_formatted_db_type_version['version'] = db_type_version.major_version + '.' + db_type_version.minor_version
        template_formatted_db_type_version['state'] = 'Preferred' if db_type_version.state=='PREFERRED' else 'Available'
        template_formatted_db_type['db_type_versions'].append(template_formatted_db_type_version)
    return template_formatted_db_type


def _generate_metadata():
    """
        Generates a metadata dict for most of the response handlers. 
        It populates fields like request_id.

        Returns
        -------
        Metadata dictionary with RequestId populated
    """
    metadata = {}
    metadata["request_id"] = tl.CustomThreadLocalClass().get_req_id()
    return metadata


def generate_describe_db_types_body(db_types):
    """
    Generate response body for DescribeDBTypes API using a Jinja2 template

    Parameters
    ----------
    db_types: A list of dictionary objects each containing "name" referring to the DBEngine name and
    "db_type_versions" referring to list of models.DBEngine objects,
    basically all versions for a DBEngine as specified in db_type["name"]

    Returns
    -------
    XML body for DescribeDBTypes API response
    """
    template = env.get_template('DescribeDBTypesResponse.xml')
    template_formatted_db_types = []

    for db_type in db_types:
        template_formatted_db_type = _populate_db_type_template(db_type)
        template_formatted_db_types.append(template_formatted_db_type)

    return template.render(
        namespace = pecan.conf.xml_namespace['current'],
        db_types = template_formatted_db_types,
        metadata = _generate_metadata())
