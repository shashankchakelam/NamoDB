import logging

from apiwebservice import exceptions
from apiwebservice import utils
from apiwebservice.constants import reserved_words
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
from namo.db.mysqlalchemy import api as db
from pecan import conf
from apiwebservice.constants import parameters
from namo.common import exception

LOG = logging.getLogger(__name__)

"""
File that contains all common helper validate methods that are used by validate_<API>
"""

def validate_and_set_common_parameters(params, cust_input_params):
    """
    Validate all mandatory parameters that every request is expected to have

    Parameters
    ----------
    params: The Parameters instance of the corresponding API Parameters class
    cust_input_params: Query Parameters parsed from the URL sent by user

    Returns
    -------
    Nothing
    """
    if conf.iam_authentication['authorize'] == 'true':
        auth_token = utils.get_auth_token()
    elif conf.iam_authentication['authorize'] == 'false':
        auth_token = None

    if auth_token:
        # This is the Console use case and is a temporary one
        return

    for base_param_name in params.get_base_param_names():
        if base_param_name not in cust_input_params:
            raise exceptions.MissingParameter(name=base_param_name)

    if cust_input_params[params.version.name] in params.version.supported_values:
        params.version.value = cust_input_params[params.version.name]
    else:
        raise exceptions.InvalidParameterValue(name=params.version.name)

    if cust_input_params[params.signature_method.name] in \
            params.signature_method.supported_values:
        params.signature_method.value = cust_input_params[params.signature_method.name]
    else:
        raise exceptions.InvalidParameterValue(name=params.signature_method.name)

    if cust_input_params[params.signature_version.name] in \
            params.signature_version.supported_values:
        params.signature_version.value = cust_input_params[params.signature_version.name]
    else:
        raise exceptions.InvalidParameterValue(name=params.signature_version.name)

    # Following guideline set in http://docs.aws.amazon.com/IAM/latest/APIReference/API_AccessKey.html
    validate_param_is_string(
            params.access_key_id.name,
            cust_input_params[params.access_key_id.name],
            min_length=conf.user_parameter_constraints['access_key_id_min_length'],
            max_length=conf.user_parameter_constraints['access_key_id_max_length'],
            alphanumeric=True)
    params.access_key_id.value = cust_input_params[params.access_key_id.name]

    validate_param_is_string(params.signature.name, cust_input_params[params.signature.name])
    params.signature.value = cust_input_params[params.signature.name]

    # TODO(shank): Currently enforcing that we get UTC time by using literal 'Z' at the end
    # Need to confirm with IAM folks if they support IST and if we'll need a proper TZD
    validate_param_is_date_time(
            params.timestamp.name,
            cust_input_params[params.timestamp.name],
            '%Y-%m-%dT%H:%M:%SZ')
    params.timestamp.value = cust_input_params[params.timestamp.name]

def validate_param_is_integer_and_cast(param_name, param_value, min_size=None, max_size=None):
    """
    Validate if a given parameter is an integer

    Parameters
    ----------
    param_name: Name of the parameter that's being validated
    param_value: Value of the parameters that's being validated
    min_size: Minimum value of the parameter that we allow. Note: min and max are exclusive
    max_size: Maximum value of the parameter that we allow

    Returns
    -------
    Integer Parameter
    """
    if min_size and max_size and min_size >= max_size:
        raise ValueError("min_size must be less that max_size")

    if not param_value:
        raise exceptions.InvalidParameterValue(name=param_name)

    if not utils.is_integer(param_value):
        raise exceptions.InvalidParameterValue(name=param_name)

    param_value = int(param_value)

    if min_size is not None and param_value < min_size:
        raise exceptions.InvalidParameterValue(name=param_name)

    if max_size is not None and param_value > max_size:
        raise exceptions.InvalidParameterValue(name=param_name)

    return param_value

def validate_param_is_string(param_name, param_value, min_length=None, max_length=None, alphanumeric=False):
    """
    Validate if a given parameter is a string

    Parameters
    ----------
    param_name: Name of the parameter that's being validated
    param_value: Value of the parameters that's being validated
    min_length: Minimum length of the string parameter
    max_length: Maxmium length of the string parameter
    alphanumeric: Validate if string is alphanumeric

    Returns
    -------
    Nothing
    """
    if not param_value:
        raise exceptions.InvalidParameterValue(name=param_name)

    if not isinstance(param_value, basestring):
        raise exceptions.InvalidParameterValue(name=param_name)

    if min_length and len(param_value) < min_length:
        raise exceptions.InvalidParameterValue(name=param_name)

    if max_length and len(param_value) > max_length:
        raise exceptions.InvalidParameterValue(name=param_name)

    if alphanumeric and not param_value.isalnum():
        raise exceptions.InvalidParameterValue(name=param_name)

def validate_param_is_bool_string_and_return_bool(param_name, value):
    """
    Validate that a parameter is a boolean string and return the boolean value

    Parameters
    ----------
    param_name: Name of the parameter that we're validating
    value: Customer specified vallue that we're validating

    Returns
    -------
    Boolean value
    """
    validate_param_is_string(param_name, value)

    if value.lower() == 'true':
        return True

    if value.lower() == 'false':
        return False

    raise exceptions.InvalidParameterValue(name=param_name)

def validate_param_is_date_time(param_name, param_value, date_time_format):
    """
    Validate if a given parameter is a datetime in the specified format and
    return back the corresponding datetime

    Parameters
    ----------
    param_name: Name of the parameter that's being validated
    param_value: Value of the parameters that's being validated
    date_time_format: Format that you expect the date time string int e.g. %Y-%m-%dT%H

    Returns
    -------
    datetime object corresponding to the param_value
    """
    if not param_value:
        raise exceptions.InvalidParameterValue(name=param_name)

    # Validate format of the date_time_string that's passed. Formats are specified as per:
    # https://docs.python.org/2/library/datetime.html#strftime-and-strptime-behavior
    try:
        return datetime.strptime(param_value, date_time_format)
    except ValueError:
        raise exceptions.InvalidParameterValue(name=param_name)

def validate_db_instance_identifier(db_instance_identifier_param, customer_value):
    """
    Validate the customer specified DBInstanceIdentifier value and set the formatted value
    in our DBInstanceIdentifier parameter

    Parameters
    ----------
    db_instance_identifier_param: DBInstanceIdentifier Parameter
    customer_value: Customer specified value

    Returns
    -------
    Nothing
    """
    # Validate is string and check length specifications
    validate_param_is_string(db_instance_identifier_param.name, customer_value, 1, 63)

    # Validate that it doesn't end with a hyphen
    if customer_value.endswith('-'):
        raise exceptions.InvalidParameterValue(name=db_instance_identifier_param.name)

    # Validate that it doesn't contain two consecutive hyphens
    if '--' in customer_value:
        raise exceptions.InvalidParameterValue(name=db_instance_identifier_param.name)

    # Format the identifier
    db_instance_identifier_value = customer_value.lower()
    db_instance_identifier_value = db_instance_identifier_value.strip()

    # Validate that all non-hyphen characters are alphanumeric
    stripped_db_instance_identifier = db_instance_identifier_value.replace('-', '')
    if not stripped_db_instance_identifier.isalnum():
        raise exceptions.InvalidParameterValue(name=db_instance_identifier_param.name)

    db_instance_identifier_param.value = db_instance_identifier_value

def validate_db_instance_class(db_instance_class_param, customer_value):
    """
    Validate the customer specified DBInstanceClass value and set the formatted
    value in our DBInstanceClass parameter

    Parameters
    ----------
    db_instance_class_param: DBInstanceClass Parameter
    customer_value: Customer specified value

    Returns
    -------
    Nothing
    """

    # Validate is string
    validate_param_is_string(db_instance_class_param.name, customer_value)

    # Format the value
    db_instance_class_value = customer_value.lower()
    db_instance_class_value = db_instance_class_value.strip()

    # Verify if the specified value is valid
    supported_instance_classes = db.supported_nova_flavors_get_all_names()

    if db_instance_class_value not in supported_instance_classes:
        LOG.debug(
            "User supplied instance class: %s. Valid instance classes: %s",
            db_instance_class_value,
            str(supported_instance_classes))
        raise exceptions.InvalidParameterValue(name=db_instance_class_param.name)

    db_instance_class_param.value = db_instance_class_value

def validate_engine(engine_param, customer_value):
    """
    Validate the customer specified Engine value and set the formatted value
    in our Engine parameter

    Parameters
    ----------
    engine_param: Engine Parameter
    engine: Customer specified value

    Returns
    -------
    Nothing
    """

    # Validate is string
    validate_param_is_string(engine_param.name, customer_value)

    # Format the value
    engine_value = customer_value.lower()
    engine_value = engine_value.strip()

    # Verify if the specified value is valid
    supported_engines = db.supported_engines_get_all_names()
    if engine_value not in supported_engines:
        raise exceptions.InvalidParameterValue(name=engine_param.name)

    engine_param.value = engine_value

def validate_engine_version(engine_version_param, customer_value, engine):
    """
    Validate the customer specified Engine Version value and set the formatted value
    in our EngineVersion parameter

    Parameters
    ----------
    engine_version_param: Engine Version Parameter
    customer_value: Customer specified value
    engine: The database engine that the customer specified

    Returns
    -------
    Nothing
    """

    # Validate is string
    engine_version_value = customer_value.strip()
    validate_param_is_string(engine_version_param.name, engine_version_value)

    # Verify if the specified value is valid
    supported_engine_versions = db.supported_engine_versions_get_all_values(engine)
    if engine_version_value not in supported_engine_versions:
        raise exceptions.InvalidParameterValue(name=engine_version_param.name)

    engine_version_param.value = engine_version_value

def validate_allocated_storage(allocated_storage_param, customer_value):
    """
    Validate the customer specified AllocatedStorage value and set the formatted value
    in our AllocatedStorage parameter

    Parameters
    ----------
    allocated_storage_param: Allocated Storage Parameter
    customer_value: Customer specified value

    Returns
    -------
    Nothing
    """

    allocated_storage_param.value = validate_param_is_integer_and_cast(
            allocated_storage_param.name,
            customer_value,
            conf.user_parameter_constraints['min_storage_size_supported'],
            conf.user_parameter_constraints['max_storage_size_supported']
    )

def validate_master_username(master_username_param, customer_value, engine='mysql'):
    """
    Validate the customer specified MasterUsername parameter

    Parameters
    ----------
    master_username_param: MasterUsername Parameter
    customer_value: Customer specified master username
    engine: Engine specified by the customer

    Returns
    -------
    Nothing
    """
    customer_value = customer_value.strip()

    # The MasterUsername constraints are enforced by the database
    # engine and, hence, we check what engine we're dealing with
    if engine.lower() in ['mysql','postgresql']:
        # MasterUserame is a string of length 2 - 16 and containing only alphanumeric values
        validate_param_is_string(
            master_username_param.name,
            customer_value,
            min_length=conf.user_parameter_constraints[engine.lower()+'_master_username_min_length'],
            max_length=conf.user_parameter_constraints[engine.lower()+'_master_username_max_length'],
            alphanumeric=True
        )

        # Masterusername cannot be a reserved word in MySQL
        if customer_value.upper() in reserved_words.all_reserved_words[engine.lower()+'_reserved_words']:
            raise exceptions.InvalidParameterValue(name=master_username_param.name)

        master_username_param.value = customer_value
    else:
        raise NotImplementedError

def validate_master_user_password(master_user_password_param, customer_value, engine='mysql'):
    """
    Validate the customer specified MasterUserPassword parameter

    Parameters
    ----------
    master_user_password_param: MasterUserPassword Parameter
    customer_value: Customer specified master username
    engine: Engine specified by the customer

    Returns
    -------
    Nothing
    """
    customer_value = customer_value.strip()

    # The MasterUserPassword constraints are enforced by the database
    # engine and, hence, we check what engine we're dealing with
    if engine.lower() in ['mysql','postgresql']:
        # MasterUserPassword is a string of bounded length
        validate_param_is_string(
                master_user_password_param.name,
                customer_value,
                min_length=conf.user_parameter_constraints[engine.lower()+'_master_user_pwd_min_length'],
                max_length=conf.user_parameter_constraints[engine.lower()+'_master_user_pwd_max_length'],
        )

        # MasterUserPassword can be any printable ASCII character except "/", """, "@"
        unsupported_characters = conf.user_parameter_constraints[engine.lower()+'_master_user_pwd_unsupported_chars']
        if any(unsupported_character in customer_value for unsupported_character in unsupported_characters):
            raise exceptions.InvalidParameterValue(name=master_user_password_param.name)

        master_user_password_param.value = customer_value
    else:
        raise NotImplementedError

def validate_port(port_param, customer_value):
    """
    Validate the customer specified Port parameter

    Parameters
    ----------
    port_param: Port Parameter
    customer_value: Customer specified port

    Returns
    -------
    Nothing
    """

    # Supporting valid port range 1150-65535
    port_param.value = validate_param_is_integer_and_cast(
            port_param.name,
            customer_value,
            min_size=conf.user_parameter_constraints['port_range_min'],
            max_size=conf.user_parameter_constraints['port_range_max'])

def validate_backup_retention_period(backup_retention_period_param, customer_value):
    """
    Validate the customer specified backup retention period

    Parameters
    ----------
    backup_retention_period_param: Backup Retention Period Parameter
    customer_value: Customer specified backup retention period in days

    Returns
    -------
    Nothing
    """

    # Valid range of values: 0 - 35
    backup_retention_period_param.value = validate_param_is_integer_and_cast(
            backup_retention_period_param.name,
            customer_value,
            conf.user_parameter_constraints['min_backup_retention_period'],
            conf.user_parameter_constraints['max_backup_retention_period'])

def validate_log_retention_period(log_retention_period_param, customer_value):
    """
    Validate the customer specified log retention period

    Parameters
    ----------
    log_retention_period_param: Log Retention Period Parameter
    customer_value: Customer specified log retention period in days

    Returns
    -------
    Nothing
    """

    # Valid range of values: 0 - 7
    log_retention_period_param.value = validate_param_is_integer_and_cast(
            log_retention_period_param.name,
            customer_value,
            conf.user_parameter_constraints['min_log_retention_period'],
            conf.user_parameter_constraints['max_log_retention_period'])

def validate_preferred_maintenance_window(preferred_maintenance_window_param, customer_value):
    """
    Validate the customer specified preferred maintenance window

    Parameters
    ----------
    preferred_maintenance_window_param: Preferred Maintenance Window Parameter
    customer_value: Customer specified preferred maintenance window

    Returns
    -------
    Nothing
    """

    # Validate that specified value is a date of format: ddd:hh24:mi-ddd:hh24:mi
    maintenance_window = customer_value.strip()

    if '-' not in maintenance_window:
        raise exceptions.InvalidParameterValue(name=preferred_maintenance_window_param.name)

    maintenance_window_list = maintenance_window.split('-')
    maintenance_window_start = maintenance_window_list[0]
    maintenance_window_end = maintenance_window_list[1]

    start_datetime = validate_param_is_date_time(
        preferred_maintenance_window_param.name,
        maintenance_window_start,
        '%a:%H:%M')

    # Get a correct datetime with weekday set relative to the validated one
    start_datetime = _get_maintenance_window_relative_to_datetime(
                        maintenance_window_start,
                        start_datetime)

    validate_param_is_date_time(
        preferred_maintenance_window_param.name,
        maintenance_window_end,
        '%a:%H:%M')

    # Get a correct datetime with weekday set relative to the start datetime
    end_datetime = _get_maintenance_window_relative_to_datetime(
                        maintenance_window_end,
                        start_datetime)

    # Verify constraints on window size
    min_window_size = conf.user_parameter_constraints['min_maintenance_window_size']
    max_window_size = conf.user_parameter_constraints['max_maintenance_window_size']

    if (end_datetime - start_datetime) < timedelta(minutes=min_window_size) or \
        (end_datetime - start_datetime) > timedelta(minutes=max_window_size):
        raise exceptions.InvalidParameterValue(name=preferred_maintenance_window_param.name)

    preferred_maintenance_window_param.value = maintenance_window

def validate_preferred_backup_window(preferred_backup_window_param, customer_value):
    """
    Validate the customer specified preferred backup window

    Parameters
    ----------
    preferred_backup_window_param: Preferred Backup Window Parameter
    customer_value: Customer specified preferred backup window

    Returns
    -------
    Nothing
    """

    # Validate that specified value is a date of format: hh24:mi-hh24:mi
    backup_window = customer_value.strip()

    if '-' not in backup_window:
        raise exceptions.InvalidParameterValue(name=preferred_backup_window_param.name)

    backup_window_list = backup_window.split('-')
    backup_window_start = backup_window_list[0]
    backup_window_end = backup_window_list[1]

    start_datetime = validate_param_is_date_time(
        preferred_backup_window_param.name,
        backup_window_start,
        '%H:%M')

    end_datetime = validate_param_is_date_time(
        preferred_backup_window_param.name,
        backup_window_end,
        '%H:%M')

    # Verify constraints on window size
    min_window_size = conf.user_parameter_constraints['min_backup_window_size']
    max_window_size = conf.user_parameter_constraints['max_backup_window_size']

    timediff = end_datetime - start_datetime

    # Handles cases where midnight falls in between the start and end datetimes
    if timediff < timedelta(minutes=0):
        timediff += timedelta(hours=24)

    if timediff < timedelta(minutes=min_window_size) or \
        timediff > timedelta(minutes=max_window_size):
        raise exceptions.InvalidParameterValue(name=preferred_backup_window_param.name)

    preferred_backup_window_param.value = backup_window

def validate_snapshot_identifier(snapshot_identifier_param, customer_value):
    """
    Validate a customer specified snapshot identifier

    Parameters
    ----------
    snapshot_identifier_param: Parameter being validated
    customer_value: Name of the snapshot (specified by the customer)

    Returns
    -------
    Nothing
    """
    # Validate is string and check length specifications
    validate_param_is_string(snapshot_identifier_param.name, customer_value, 1, 255)

    # Validate that it doesn't end with a hyphen
    if customer_value.endswith('-'):
        raise exceptions.InvalidParameterValue(name=snapshot_identifier_param.name)

    # Validate that it doesn't contain two consecutive hyphens
    if '--' in customer_value:
        raise exceptions.InvalidParameterValue(name=snapshot_identifier_param.name)

    # Format the identifier
    snapshot_identifier_param_value = customer_value.lower()
    snapshot_identifier_param_value = snapshot_identifier_param_value.strip()

    # Validate that all non-hyphen characters are alphanumeric
    stripped_snapshot_identifier = snapshot_identifier_param_value.replace('-', '')
    if not stripped_snapshot_identifier.isalnum():
        raise exceptions.InvalidParameterValue(name=snapshot_identifier_param.name)

    snapshot_identifier_param.value = snapshot_identifier_param_value

def _get_maintenance_window_relative_to_datetime(mw_datetime_string, relative_date_time):
    # This helper function is specific to maintenance window
    # The reason we have this method is because datetime.strftime() doesn't set
    # weekday correctly when passed. It sets hour and minute but defaults the
    # date to 1900-01-01 and weekday as Monday regardless of what you pass as
    # the %a. Hence, I'm explicitly parsing out the weekday string and using the
    # relativedelta() method to set it correctly

    if not relative_date_time or not mw_datetime_string:
        raise ValueError("Invalid arguments")

    # Gets a dictionary of Abbreviated day names to Integer
    # Mon:0, Tue:1, Wed:2, etc
    day_of_week_map =  dict(zip(['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'], range(7)))
    # 0 - Abbreviated day name
    # 1 - hour
    # 2 - minute
    mw_datetime_components = mw_datetime_string.split(':')

    return (relative_date_time +
                relativedelta(
                    weekday=day_of_week_map[mw_datetime_components[0].lower()],
                    hour=int(mw_datetime_components[1]),
                    minute=int(mw_datetime_components[2])))

def validate_modify_atleast_one_optional_parameter_present(query_parameters):
    """
    At least one optional parameter must be passed in order to make any
    modification.
    If no parameter is passed then we'll raise an error
    """

    modify_db_instance_params = parameters.ModifyDbInstanceParams()
    optional_parameter_found = False

    for option_param in modify_db_instance_params.get_optional_param_names():
        if option_param in query_parameters:
            optional_parameter_found = True
            break

    if (optional_parameter_found == False):
        raise exceptions.ValidationError()

def validate_db_instance_not_present(owner_id, db_identifier, session=None):
    """
    Need to validate that the new identifier is already present with the owner_id or not
    :param owner_id:
    :param new_db_identifier:

    :return:

    an error if the new_db_identifier is already present
    """
    try:
        instance = db.get_user_visible_instance_properties(owner_id, db_identifier, session)
        new_instance_already_exists = True
    except exception.InstanceNotFoundByOwnerIdAndName:
        new_instance_already_exists = False

    if (new_instance_already_exists == True):
        raise exceptions.DBInstanceAlreadyExists(name=db_identifier)

def validate_instance_lc_cs(instance_lc, instance_cs, expected_lc, expected_cs):
    """
    checks if the instance state is in the state that we require it to be in
    returns error if ins't.
    """
    print instance_lc
    expected_state = expected_lc
    if (expected_lc == 'ACTIVE'):
        expected_state = 'available'
    if (instance_lc != expected_lc):
        raise exceptions.InvalidDBInstanceState(state = expected_state)
    if (instance_cs != expected_cs):
        raise exceptions.InvalidDBInstanceState(state = expected_state)


def validate_modifying_parameter(parameter_name, new_values, instance):


    changeable_parameters = {'PreferredMaintenanceWindow':'preferred_maintenance_window_start_day:preferred_maintenance_window_start_time:'
        'preferred_maintenance_window_end_day:preferred_maintenance_window_end_time','PreferredBackupWindow':'preferred_backup_window_start:preferred_backup_window_end',
                                       'BackupRetentionPeriod':'backup_retention_period_days','DBInstanceClass':'nova_flavor_id'}

    if parameter_name in changeable_parameters.keys():
        parameter_string = changeable_parameters[parameter_name]
        parameter_string_arr = parameter_string.split(":")
        parameter_string_val = ''
        instance_string_val = ''
        for val in parameter_string_arr:
            parameter_string_val += str(new_values[val])
            instance_string_val += str(instance[val])
        if parameter_string_val == instance_string_val:
            return False
            #raise exceptions.DbInstanceParameterNotModifiedException(parameter=parameter_name)

    return True

def validate_snapshot_type(snapshot_type_param, customer_value):
    customer_value = customer_value.lower()
    if customer_value not in ('manual', 'automated'):
        raise exceptions.InvalidParameterValue(name=snapshot_type_param.name)

    snapshot_type_param.value = customer_value

def validate_snapshot_not_present(owner_id, snapshot_id, session=None):
        try:
            instance = db.backup_get_by_owner_id_and_name('', owner_id, snapshot_id, session)
            raise exceptions.DBSnapshotAlreadyExists(name=snapshot_id)
        except exception.BackupNotFoundByOwnerIdAndName:
            pass

def validate_instance_present(owner_id, instance_name, session=None):
    try:
        instance = db.instance_get(
            None,
            owner_id,
            instance_name,
            session)
    except exception.InstanceNotFoundByOwnerIdAndName:
        LOG.exception(
            "Invalid instance: {0} specified by customer",
           instance_name)
        raise exceptions.DBInstanceNotFound(name=instance_name)
    return instance

def validate_both_parameter_not_present(parameter_1, parameter_2, query_parameters):
    if parameter_1.name in query_parameters and parameter_2.name in query_parameters:
        raise exceptions.InvalidParameterCombination(name=str(parameter_1.name) + ' and ' + str(parameter_2.name))

def validate_snapshot_present(owner_id, snapshot_name, session=None):
        try:
            snapshot = db.backup_get_by_owner_id_and_name('',
                                                          owner_id=owner_id,
                                                          backup_name=snapshot_name,
                                                          session=session)
            return snapshot
        except exception.BackupNotFoundByOwnerIdAndName:
            LOG.exception("Invalid snapshot_name specified by customer")
            raise exceptions.DBSnapshotNotFound(name=snapshot_name)

def validate_db_type_identifier(db_type_identifier,customer_value):
    """
    Validates db type identifier. the function validates if the input customer_value 
    is a supported database engine or not. 
    If it is not it will throw Invalid Parameter Value Exception 
    """
    customer_value = customer_value.strip().lower()
    if customer_value not in db.supported_engines_get_all_names():
        raise exceptions.InvalidParameterValue(name=db_type_identifier.name)

    db_type_identifier.value =customer_value
