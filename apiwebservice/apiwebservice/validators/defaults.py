from datetime import datetime
from datetime import timedelta
from namo.db.mysqlalchemy import api as db
from pecan import conf
from random import randrange

"""
File that stores and/or generates default values for various user-facing parameters
"""

def get_preferred_engine_version(engine):
    """
    Get preferred engine version for a given engine

    Parameters
    ----------
    engine: The engine that we want preferred engine version for

    Returns
    -------
    An engine version
    """

    return db.get_preferred_engine_version_for_engine(engine)

def get_default_port_by_engine(engine):
    """
    Get default port number for database engine

    Parameters
    ----------
    engine: The engine that we want the default port for

    Returns
    -------
    Port number
    """

    if engine.lower() == 'mysql':
        return conf.user_parameter_defaults['port_mysql']
    elif engine.lower() == 'postgresql':
        return conf.user_parameter_defaults['port_postgresql']
    else:
        raise NotImplementedError

def generate_default_maintenance_window():
    """
    Generate default maintenance window. This is a 30 minute window selected at random from the "night" time
    8 hour block, on a random day of the week

    Returns
    -------
    Maintenance Window String
    """
    mw_times = _generate_random_start_end_time_in_given_block(
        conf.user_parameter_defaults['night_block_start_hour'],
        conf.user_parameter_defaults['night_block_size_mins'],
        conf.user_parameter_defaults['maintenance_window_size_mins'])

    start_datetime = mw_times[0]
    start_datetime.replace(day=randrange(1, 7))
    end_datetime = mw_times[1]

    start_time_string = start_datetime.strftime('%a:%H:%M')
    end_time_string = end_datetime.strftime('%a:%H:%M')

    return start_time_string + "-" + end_time_string

def generate_default_backup_window():
    """
    Generate a default backup window. This is a 30 minute window selected at random from the "night" time block.

    Returns
    -------
    Backup Window String
    """
    # Initialize values
    bw_times = _generate_random_start_end_time_in_given_block(
        conf.user_parameter_defaults['night_block_start_hour'],
        conf.user_parameter_defaults['night_block_size_mins'],
        conf.user_parameter_defaults['backup_window_size_mins'])
    start_time_string = bw_times[0].strftime('%H:%M')
    end_time_string = bw_times[1].strftime('%H:%M')

    return start_time_string + "-" + end_time_string

def _generate_random_start_end_time_in_given_block(
        block_start_hour,
        block_size_mins,
        start_end_diff_mins):
    start_datetime = datetime.now()
    start_datetime.replace(hour=block_start_hour)
    start_datetime.replace(minute=0)

    rand_minutes = randrange(0, block_size_mins - start_end_diff_mins)
    start_datetime = start_datetime + timedelta(minutes=rand_minutes)
    end_datetime = start_datetime + timedelta(minutes=start_end_diff_mins)
    return [start_datetime, end_datetime]