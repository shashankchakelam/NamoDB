import logging

from apiwebservice import exceptions
from namo.db.mysqlalchemy import api as db
from namo.common import exception

LOG = logging.getLogger(__name__)

"""
File contains methods to validate that user isn't exceeding his/her quota.
So far, these validations occur only during the create API calls. In the
future, it would be extended to modify API calls too.
"""

def validate_number_of_snapshots_does_not_exceed_limit(owner_id, session=None):
    number_of_snapshots = len(db.backup_get_all_for_owner('', owner_id, session=session))

    try:
        user_limit = db.user_quota_get_by_owner_id_quota_name('', owner_id, 'total_snapshots', session=session)
        number_of_snapshots_limit = user_limit["quota_limit"]
    except exception.UserLimitNotFoundByOwnerId:
        # the global value
        global_limit = db.user_quota_get_by_owner_id_quota_name('', -1, 'total_snapshots', session=session)
        number_of_snapshots_limit = global_limit["quota_limit"]

    if number_of_snapshots >= number_of_snapshots_limit:
        raise exceptions.SnapshotQuotaExceeded(quota=number_of_snapshots_limit)

def validate_instances_quotas(owner_id, instance_class, session=None):
    _validate_number_of_instances_does_not_exceed_limit(owner_id, session)

    # (shank): An alternative way to do this could be to parse the user_quotas table per owner_id for
    # quotas of the type total_{0}_instances and use that. This seems too much for now given that we have
    # only 2 known exceptions. If we see that this is going to increase in the future then we should take
    # this up.
    if instance_class in ['c1.2xlarge', 'c1.4xlarge']:
        _validate_number_of_instances_of_class_does_not_exceed_limit(owner_id, instance_class, session)

def validate_storage_size_of_instances_does_not_exceed_limit(owner_id, current_storage_requirement, session=None):
    instances = db.instance_get_all('', owner_id, session=session)
    storage_already_allocated = 0
    for instance in instances:
        storage_already_allocated = storage_already_allocated + instance["storage_size"]
    try:
        # the global value
        user_limit = db.user_quota_get_by_owner_id_quota_name('', owner_id, 'total_storage', session=session)
        storage_limit = user_limit["quota_limit"]
    except exception.UserLimitNotFoundByOwnerId:
        # the global value
        global_limit = db.user_quota_get_by_owner_id_quota_name('', -1, 'total_storage', session=session)
        storage_limit = global_limit["quota_limit"]

    if storage_already_allocated + current_storage_requirement > storage_limit:
        raise exceptions.StorageQuotaExceeded(quota=storage_limit)

def _validate_number_of_instances_does_not_exceed_limit(owner_id, session=None):
    number_of_instances = db.instance_get_all('', owner_id, session=session).count()

    try:
        user_limit = db.user_quota_get_by_owner_id_quota_name('', owner_id, 'total_instances', session=session)
        number_of_instances_limit = user_limit["quota_limit"]
    except exception.UserLimitNotFoundByOwnerId:
        # the global value
        global_limit = db.user_quota_get_by_owner_id_quota_name('', -1, 'total_instances', session=session)
        number_of_instances_limit = global_limit["quota_limit"]

    if number_of_instances >= number_of_instances_limit:
        raise exceptions.InstanceQuotaExceeded(quota=number_of_instances_limit)

def _validate_number_of_instances_of_class_does_not_exceed_limit(owner_id, instance_class, session=None):
    nova_flavor = db.nova_flavor_get('', instance_class, session)
    LOG.debug(
        "Validating owner_id %s for instance class %s quota",
        owner_id,
        instance_class)

    number_of_instances_of_class = db.instance_get_all('', owner_id, session).filter_by(nova_flavor_id=nova_flavor.id).count()
    quota_name = 'total_{0}_instances'.format(instance_class)

    try:
        user_limit = db.user_quota_get_by_owner_id_quota_name('', owner_id, quota_name, session=session)
        number_of_instances_limit = user_limit["quota_limit"]
    except exception.UserLimitNotFoundByOwnerId:
        # Use the global value
        global_limit = db.user_quota_get_by_owner_id_quota_name('', -1, quota_name, session=session)
        number_of_instances_limit = global_limit["quota_limit"]

    if number_of_instances_of_class >= number_of_instances_limit:
        raise exceptions.InstanceClassQuotaExceeded(name=instance_class, quota=number_of_instances_limit)
