from apiwebservice import exceptions
from apiwebservice.constants import parameters
from apiwebservice.validators import defaults
from apiwebservice.validators import helpers
from apiwebservice.constants.parameters import CreateDBInstanceParams
from apiwebservice.constants.parameters import DescribeDBInstancesParams

"""
File that contains validator methods for all APIs exposed by API Web Service
"""

def validate_create_db_instance(query_parameters):
    """
    Validates all parameters passed in the CreateDBInstance API call

    Parameters
    ----------
    query_parameters: Query parameters parsed from the URL sent by the user

    Returns
    -------
    CreateDBInstanceParams object with values set
    """

    create_db_instance_params = parameters.CreateDBInstanceParams()

    # Validate common parameters
    helpers.validate_and_set_common_parameters(create_db_instance_params, query_parameters)

    # Validate mandatory parameters
    for mandatory_param in create_db_instance_params.get_mandatory_param_names_except_base_params():
        if mandatory_param not in query_parameters:
            raise exceptions.MissingParameter(name=mandatory_param)

    helpers.validate_db_instance_identifier(
            create_db_instance_params.db_instance_identifier,
            query_parameters['DBInstanceIdentifier'])
    helpers.validate_db_instance_class(
            create_db_instance_params.db_instance_class,
            query_parameters['DBInstanceClass'])
    helpers.validate_engine(
            create_db_instance_params.engine,
            query_parameters['Engine'])
    helpers.validate_allocated_storage(
            create_db_instance_params.allocated_storage,
            query_parameters['AllocatedStorage'])
    helpers.validate_master_username(
            create_db_instance_params.master_username,
            query_parameters['MasterUsername'],
            create_db_instance_params.engine.value)
    helpers.validate_master_user_password(
            create_db_instance_params.master_user_password,
            query_parameters['MasterUserPassword'],
            create_db_instance_params.engine.value)

    # Validate optional parameters
    if 'EngineVersion' in query_parameters:
        helpers.validate_engine_version(
            create_db_instance_params.engine_version,
            query_parameters['EngineVersion'],
            create_db_instance_params.engine.value)
    else:
        create_db_instance_params.engine_version.value = \
            defaults.get_preferred_engine_version(create_db_instance_params.engine.value)

    if 'Port' in query_parameters:
        helpers.validate_port(
            create_db_instance_params.port,
            query_parameters['Port'])
    else:
        create_db_instance_params.port.value = \
            defaults.get_default_port_by_engine(create_db_instance_params.engine.value)

    if 'BackupRetentionPeriod' in query_parameters:
        helpers.validate_backup_retention_period(
            create_db_instance_params.backup_retention_period,
            query_parameters['BackupRetentionPeriod'])
    else:
        create_db_instance_params.backup_retention_period.value = \
            create_db_instance_params.backup_retention_period.default_value

    if 'LogRetentionPeriod' in query_parameters:
        helpers.validate_log_retention_period(
            create_db_instance_params.log_retention_period,
            query_parameters['LogRetentionPeriod'])
    else:
        create_db_instance_params.log_retention_period.value = \
            create_db_instance_params.log_retention_period.default_value

    if 'PreferredMaintenanceWindow' in query_parameters:
        helpers.validate_preferred_maintenance_window(
            create_db_instance_params.preferred_maintenance_window,
            query_parameters['PreferredMaintenanceWindow'])
    else:
        create_db_instance_params.preferred_maintenance_window.value = \
            defaults.generate_default_maintenance_window()

    if 'PreferredBackupWindow' in query_parameters:
        helpers.validate_preferred_backup_window(
            create_db_instance_params.preferred_backup_window,
            query_parameters['PreferredBackupWindow'])
    else:
        create_db_instance_params.preferred_backup_window.value = \
            defaults.generate_default_backup_window()

    return create_db_instance_params

def validate_describe_db_instances(query_parameters):
    """
    Validates all parameters passed in the DescribeDBInstances API call

    Parameters
    ----------
    query_parameters: Query Parameters parsed from the URL sent by user

    Returns
    -------
    DescribeDBInstancesParams object with values set
    """

    describe_db_instances_params = parameters.DescribeDBInstancesParams()

    # Validate common parameters
    helpers.validate_and_set_common_parameters(describe_db_instances_params, query_parameters)

    # Validate optional parameters
    if 'DBInstanceIdentifier' in query_parameters:
        helpers.validate_db_instance_identifier(
            describe_db_instances_params.db_instance_identifier,
            query_parameters['DBInstanceIdentifier'])

    return describe_db_instances_params

def validate_upload_db_instance_logs(query_parameters):
    """
    Validates all parameters passed in the UploadDBInstanceLogs API call

    Parameters
    ----------
    query_parameters: Query Parameters parsed from the URL sent by user

    Returns
    -------
    UploadDBInstanceLogs object with values set
    """

    upload_db_instance_logs_params = parameters.UploadDBInstanceLogsParams()

    # Validate common parameters
    helpers.validate_and_set_common_parameters(upload_db_instance_logs_params, query_parameters)

    # Validate mandatory parameters
    for mandatory_param in upload_db_instance_logs_params.get_mandatory_param_names_except_base_params():
        if mandatory_param not in query_parameters:
            raise exceptions.MissingParameter(name=mandatory_param)

    helpers.validate_db_instance_identifier(
            upload_db_instance_logs_params.db_instance_identifier,
            query_parameters['DBInstanceIdentifier'])

    return upload_db_instance_logs_params

def validate_delete_db_instance(query_parameters):
    """
    Validates all parameters passed in the DeleteDBInstance API call

    Parameters
    ----------
    query_parameters: Query Parameters parsed from the URL sent by user

    Returns
    -------
    DeleteDBInstanceParams object with values set
    """

    delete_db_instance_params = parameters.DeleteDBInstanceParams()

    # Validate common parameters
    helpers.validate_and_set_common_parameters(delete_db_instance_params, query_parameters)

    # Validate mandatory parameters
    for mandatory_param in delete_db_instance_params.get_mandatory_param_names_except_base_params():
        if mandatory_param not in query_parameters:
            raise exceptions.MissingParameter(name=mandatory_param)

    helpers.validate_db_instance_identifier(
            delete_db_instance_params.db_instance_identifier,
            query_parameters['DBInstanceIdentifier'])

    # Validate optional parameters
    if 'SkipFinalSnapshot' in query_parameters:
        delete_db_instance_params.skip_final_snapshot.value = \
            helpers.validate_param_is_bool_string_and_return_bool(
                delete_db_instance_params.skip_final_snapshot.name,
                query_parameters['SkipFinalSnapshot'])
    else:
        delete_db_instance_params.skip_final_snapshot.value = \
            delete_db_instance_params.skip_final_snapshot.default_value

    # If user wants us to take a final snapshot, validate that he has given
    # us the name he wants to assign the snapshot
    if not delete_db_instance_params.skip_final_snapshot.value:
        if 'FinalDBSnapshotIdentifier' in query_parameters:
            helpers.validate_snapshot_identifier(
                delete_db_instance_params.final_db_snapshot_identifier,
                query_parameters['FinalDBSnapshotIdentifier'])
        else:
            raise exceptions.MissingParameter(name='FinalDBSnapshotIdentifier')
    else:
        # If FinalDBSnapshotIdentifier is specified but the users wants to skip
        # final snapshot, this is a contradictory input that's been provided. So
        # raise an excpetion so that the user can correct this
        if 'FinalDBSnapshotIdentifier' in query_parameters:
            raise exceptions.InvalidParameterCombination()

    return delete_db_instance_params

def validate_modify_db_instance(query_parameters):
    """
    Validates all parameters passed in the ModifyDBInstance API call

    Parameters
    ----------
    query_parameters: Query Parameters parsed from the URL sent by user

    Returns
    -------
    ModifyDBInstanceParams object with values set
    """
    modify_db_instance_params = parameters.ModifyDbInstanceParams()

    # Validate common parameters
    helpers.validate_and_set_common_parameters(modify_db_instance_params, query_parameters)

    # Validate mandatory parameters
    for mandatory_param in modify_db_instance_params.get_mandatory_param_names_except_base_params():
        if mandatory_param not in query_parameters:
            raise exceptions.MissingParameter(name=mandatory_param)

    # Validate mandatory parameters
    helpers.validate_db_instance_identifier(
            modify_db_instance_params.db_instance_identifier,
            query_parameters['DBInstanceIdentifier'])
    # Validate optional parameters

    # At least one optional parameter must be passed in order to make any modification.
    # If no parameter is passed then we'll raise an error
    helpers.validate_modify_atleast_one_optional_parameter_present(query_parameters)

    if 'NewDBInstanceIdentifier' in query_parameters:
        # check if the new name is valid
        helpers.validate_db_instance_identifier(
                modify_db_instance_params.new_db_instance_identifier,
                query_parameters['NewDBInstanceIdentifier'])

    if 'BackupRetentionPeriod' in query_parameters:
        helpers.validate_backup_retention_period(
            modify_db_instance_params.backup_retention_period,
            query_parameters['BackupRetentionPeriod'])

    if 'PreferredMaintenanceWindow' in query_parameters:
        helpers.validate_preferred_maintenance_window(
            modify_db_instance_params.preferred_maintenance_window,
            query_parameters['PreferredMaintenanceWindow'])

    if 'PreferredBackupWindow' in query_parameters:
        helpers.validate_preferred_backup_window(
            modify_db_instance_params.preferred_backup_window,
            query_parameters['PreferredBackupWindow'])

    if 'DBInstanceClass' in query_parameters:
        helpers.validate_db_instance_class(
            modify_db_instance_params.db_instance_class,
            query_parameters['DBInstanceClass'])

    return modify_db_instance_params

def validate_create_db_snapshot(query_parameters):
    """
    Validates all parameters passed in the CreateDBSnapshot API call
    Returns
    create_snapshot object with values set
    """
    create_snapshot_params = parameters.CreateDBSnapshotParams()

    for mandatory_param in create_snapshot_params.get_mandatory_param_names_except_base_params():
        if mandatory_param not in query_parameters:
            raise exceptions.MissingParameter(name=mandatory_param)

    # Validate common parameters
    helpers.validate_and_set_common_parameters(create_snapshot_params, query_parameters)

    helpers.validate_db_instance_identifier(
        create_snapshot_params.db_instance_identifier,
        query_parameters['DBInstanceIdentifier'])

    helpers.validate_snapshot_identifier(
        create_snapshot_params.db_snapshot_identifier,
        query_parameters['DBSnapshotIdentifier'])

    return create_snapshot_params


def validate_restore_db_instance_from_db_snapshot(query_parameters):

    """
    Validates all parameters passed in the RestoreDBInstanceFromDBSnapshot API call

    Parameters
    ----------
    query_parameters: Query Parameters parsed from the URL sent by user

    Returns
    -------
    RestoreDBInstanceFromDBSnapshot object with values set
    """
    restore_db_instance_from_snapshot_params = parameters.RestoreDBInstanceFromDBSnapshotParams()

    # Validate common parameters
    helpers.validate_and_set_common_parameters(restore_db_instance_from_snapshot_params, query_parameters)

    # Validate mandatory parameters
    for mandatory_param in restore_db_instance_from_snapshot_params.get_mandatory_param_names_except_base_params():
        if mandatory_param not in query_parameters:
            raise exceptions.MissingParameter(name=mandatory_param)

    # Validate mandatory parameters
    helpers.validate_db_instance_identifier(
            restore_db_instance_from_snapshot_params.db_instance_identifier,
            query_parameters['DBInstanceIdentifier'])

    helpers.validate_snapshot_identifier(
        restore_db_instance_from_snapshot_params.db_snapshot_identifier,
        query_parameters['DBSnapshotIdentifier'])

     # Validate optional parameters

    if 'DBInstanceClass' in query_parameters :
        helpers.validate_db_instance_class(
            restore_db_instance_from_snapshot_params.db_instance_class,
            query_parameters['DBInstanceClass'])

    if 'AllocatedStorage' in query_parameters :
        helpers.validate_allocated_storage(
            restore_db_instance_from_snapshot_params.allocated_storage,
            query_parameters['AllocatedStorage'])

    if 'MasterUsername' in query_parameters :
        helpers.validate_master_username(
            restore_db_instance_from_snapshot_params.master_username,
            query_parameters['MasterUsername'])

    if 'MasterUserPassword' in query_parameters :
        helpers.validate_master_user_password(
            restore_db_instance_from_snapshot_params.master_user_password,
            query_parameters['MasterUserPassword'])

    if 'Port' in query_parameters:
        helpers.validate_port(
            restore_db_instance_from_snapshot_params.port,
            query_parameters['Port'])

    if 'BackupRetentionPeriod' in query_parameters:
        helpers.validate_backup_retention_period(
            restore_db_instance_from_snapshot_params.backup_retention_period,
            query_parameters['BackupRetentionPeriod'])

    if 'LogRetentionPeriod' in query_parameters:
        helpers.validate_log_retention_period(
            restore_db_instance_from_snapshot_params.log_retention_period,
            query_parameters['LogRetentionPeriod'])

    if 'PreferredMaintenanceWindow' in query_parameters:
        helpers.validate_preferred_maintenance_window(
            restore_db_instance_from_snapshot_params.preferred_maintenance_window,
            query_parameters['PreferredMaintenanceWindow'])

    if 'PreferredBackupWindow' in query_parameters:
        helpers.validate_preferred_backup_window(
            restore_db_instance_from_snapshot_params.preferred_backup_window,
            query_parameters['PreferredBackupWindow'])

    return restore_db_instance_from_snapshot_params

def validate_describe_db_snapshots(query_parameters):
    """
    Validates all parameters passed in the DescribeDBSnapshots API call

    Parameters
    ----------
    query_parameters: Query Parameters parsed from the URL sent by user

    Returns
    -------
    DescribeDBSnapshotsParams object with values set
    """
    describe_db_snapshots_params = parameters.DescribeDBSnapshotsParams()

    # Validate common parameters
    helpers.validate_and_set_common_parameters(describe_db_snapshots_params, query_parameters)

    # Validate optional parameters
    if 'DBInstanceIdentifier' in query_parameters:
        helpers.validate_db_instance_identifier(
            describe_db_snapshots_params.db_instance_identifier,
            query_parameters['DBInstanceIdentifier'])

    if 'DBSnapshotIdentifier' in query_parameters:
        helpers.validate_snapshot_identifier(
            describe_db_snapshots_params.db_snapshot_identifier,
            query_parameters['DBSnapshotIdentifier'])

    if 'SnapshotType' in query_parameters:
        helpers.validate_snapshot_type(
            describe_db_snapshots_params.snapshot_type,
            query_parameters['SnapshotType'])

    helpers.validate_both_parameter_not_present(describe_db_snapshots_params.db_snapshot_identifier, describe_db_snapshots_params.snapshot_type, query_parameters)
    helpers.validate_both_parameter_not_present(describe_db_snapshots_params.db_snapshot_identifier, describe_db_snapshots_params.db_instance_identifier, query_parameters)

    return describe_db_snapshots_params


def validate_delete_db_snapshot(query_parameters):
    """
    Validates all parameters passed in the DeleteDBSnapshot API call

    Parameters
    ----------
    query_parameters: Query Parameters parsed from the URL sent by user

    Returns
    -------
    DeleteDBSnapshotParams object with values set
    """

    delete_db_snapshot_params = parameters.DeleteDBSnapshotParams()

    # Validate common parameters
    helpers.validate_and_set_common_parameters(delete_db_snapshot_params, query_parameters)

    # Validate mandatory parameters
    for mandatory_param in delete_db_snapshot_params.get_mandatory_param_names_except_base_params():
        if mandatory_param not in query_parameters:
            raise exceptions.MissingParameter(name=mandatory_param)

    helpers.validate_snapshot_identifier(
        delete_db_snapshot_params.db_snapshot_identifier,
        query_parameters['DBSnapshotIdentifier'])

    return delete_db_snapshot_params


def validate_describe_db_types(query_parameters):
    """
    Validates all parameters passed in the DescribeDBTypes API call

    Parameters
    ----------
    query_parameters: Query Parameters parsed from the URL sent by user

    Returns
    -------
    DescribeDBTypesParams object with values set
    """

    describe_db_types_params = parameters.DescribeDBTypesParams()

    # Validate common parameters
    helpers.validate_and_set_common_parameters(describe_db_types_params, query_parameters)

    # Validate mandatory parameters
    for mandatory_param in describe_db_types_params.get_mandatory_param_names_except_base_params():
        if mandatory_param not in query_parameters:
            raise exceptions.MissingParameter(name=mandatory_param)

    if 'DBTypeId' in query_parameters:
        helpers.validate_db_type_identifier(
            describe_db_types_params.db_type_identifier,query_parameters['DBTypeId']
        )

    return describe_db_types_params
