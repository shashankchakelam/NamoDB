import pecan
import re

from apiwebservice import exceptions
from urlparse import parse_qs
from urlparse import urlparse

"""
File that contains all utility/helper methods for API Web Service
"""

def get_query_params(url):
    """
    Extract out the Query Parameters from the Request URL
    Parameters
    ----------
    url: URL of the Request

    Returns
    -------
    Dictionary of Query Parameters
    """
    query_parameters = _extract_query_params(url)

    # Urlparse.parse_qs gives back a dictionary of <key, list>. Dealing
    # with this list value is a pain, so this method parses out the list
    # and makes the query_parameters dictionary <key, value>
    parsed_params = {}

    for key, value in query_parameters.iteritems():
        parsed_params[key] = _get_only_param_value(key, value).strip()

    return parsed_params

def is_integer(value):
    """ Check if a value is an integer """
    if isinstance(value, basestring):
        if value.isdigit():
            return True
        else:
            return False

    if isinstance(value, int):
        return True
    else:
        return False

# Pre-compiling the regexes as they will be used often
first_cap_regex = re.compile('(.)([A-Z][a-z]+)')
all_cap_regex = re.compile('([a-z0-9])([A-Z])')

def convert_camel_case_to_underscore_seperated(camel_case_name):
    """Convert CamelCase name to underscore_seperated name"""

    underscore_name = first_cap_regex.sub(r'\1_\2', camel_case_name)
    return all_cap_regex.sub(r'\1_\2', underscore_name).lower()

def get_maintenance_window_components(mw_string):
    """
    Get the various maintenance window components given user inpur string

    Parameters
    ----------
    mw_string: User input string for maintenance window

    Returns
    -------
    Components of the maintenance window
    0 - maintenance window start day
    1 - maintenance window start time
    2 - maintenance window end day
    3 - maintenance window end time
    """
    # Split using the '-' delimiter
    mw_components = mw_string.split('-')

    # Split at the first ':'
    mw_start_components = mw_components[0].split(':', 1)
    mw_end_components = mw_components[1].split(':', 1)

    mw_start_day = mw_start_components[0].upper()
    mw_start_time = mw_start_components[1]
    mw_end_day = mw_end_components[0].upper()
    mw_end_time = mw_end_components[1]

    return [mw_start_day, mw_start_time, mw_end_day, mw_end_time]

def get_auth_token():
    """
    Extract the Auth token from the request. We expect this for requests coming
    from Console. This is the header X-Auth-Token present in the request

    Returns
    -------
    Auth Token
    """
    token_header_name = 'X-Auth-Token'
    if token_header_name not in pecan.request.headers:
        return None

    return pecan.request.headers[token_header_name]


def _extract_query_params(url):
    parsed_url = urlparse(url)

    # https://docs.python.org/2/library/urlparse.html
    # https://pymotw.com/2/urlparse/
    query_string = parsed_url.query
    query_params = parse_qs(query_string)
    return query_params

def _get_only_param_value(param, param_list):
    if not param_list:
        raise exceptions.MissingParameter(name=param)

    # We expect only one parameter value
    if len(param_list) is not 1:
        raise exceptions.InvalidParameterValue(name=param)

    return param_list[0]
