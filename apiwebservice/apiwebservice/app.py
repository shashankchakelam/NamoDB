import pecan

from namo.common import utils
from oslo_config import cfg
from pecan import make_app
from paste.translogger import TransLogger

CONF = cfg.CONF

def setup_app(config):
    app_conf = dict(config.app)
    _make_app_dirs()
    _register_database_conf()

    app = make_app(
        app_conf.pop('root'),
        logging=getattr(config, 'logging', {}),
        **app_conf
    )

    app = TransLogger(app, setup_console_handler=False)
    return app

def _make_app_dirs():
     # Ensure app folder locations exist, else create
    app_folder = pecan.conf.app_dirs['home']
    app_log_folder = pecan.conf.app_dirs['logs']

    utils.safe_mkdirs(app_folder)
    utils.safe_mkdirs(app_log_folder)

def _register_database_conf():
    # Mainly using the oslo global ConfigOpts so that we can make use of the
    # mysqlalchemy/api.py. Translating pecan.conf values into oslo config
    database_opts = [
        cfg.StrOpt('connection_pecan_config_override', default=pecan.conf.database['connection'])
    ]

    database_group = cfg.OptGroup(name='database')

    CONF.register_group(database_group)
    CONF.register_opts(database_opts, group='database')
