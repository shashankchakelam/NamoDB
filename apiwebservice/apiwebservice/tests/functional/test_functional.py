import os
import pecan
import uuid
import re
import random
import unittest

import datetime
from mock import patch
from pecan.testing import load_test_app
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models
from unittest import TestCase
from namo.common import exception

class BaseTestRootController:
    """
    Functional tests for the API Web Service project
    All these tests target the root controller as the starting point
    """
    @classmethod
    def setUpClass(cls):
        cls.app = load_test_app(os.path.join(
            os.path.dirname(__file__),
            '../config.py'
        ))
        cls.url_with_base_params = \
            "/?Version=2016-03-01&" + \
            "JCSAccessKeyId=abcdefghijklmnopqrstuvwxyz123456&" + \
            "Signature=1234567890abcdefghijklmnopqrstuvwxyz&" + \
            "SignatureMethod=HmacSHA256&" + \
            "SignatureVersion=2&" + \
            "Timestamp=2016-01-01T12:00:00Z&"
        cls.owner_id = 1
        # Not specifying database engine specific parameters as they are specified in derived classes
        cls.database_flavor = 'c1.xlarge'
        cls.database_storage_size = 100
        cls.master_username = "shank"
        cls.master_user_password = "abcde12345"
        cls.preferred_backup_window_start = '23:45'
        cls.preferred_backup_window_end = "00:15"
        cls.preferred_maintenance_window_start_day = 'SUN'
        cls.preferred_maintenance_window_start_time = "22:45"
        cls.preferred_maintenance_window_end_day = 'SUN'
        cls.preferred_maintenance_window_end_time = "23:40"
        cls.backup_retention_period_days = 3
        cls.log_retention_period = 4

        try:
            db.user_quota_get_by_owner_id_quota_name('', 1, 'total_snapshots')
        except exception.UserLimitNotFoundByOwnerId:
            db.user_quota_create_entry_from_user_supplied_values('', 1, 'total_instances', 2500)
            db.user_quota_create_entry_from_user_supplied_values('', 1, 'total_snapshots', 2500)
            db.user_quota_create_entry_from_user_supplied_values('', 1, 'total_storage', 250000)

###################################################################
################# Test Unsupported HTTP methods ###################
###################################################################
    def test_put_http_method_call(self):
        response = self.app.put('/', expect_errors=True)
        self._helper_validate_unsupported_http_method(response, 'PUT')

    def test_delete_http_method_call(self):
        response = self.app.delete('/', expect_errors=True)
        self._helper_validate_unsupported_http_method(response, 'DELETE')

    def test_patch_http_method_call(self):
        response = self.app.patch('/', expect_errors=True)
        self._helper_validate_unsupported_http_method(response, 'PATCH')

###################################################################
######################### Test Actions ############################
###################################################################
    def test_invalid_get_method_action(self):
        response = self.app.get('/?Action=CreateDBInstance', expect_errors=True)
        self._helper_validate_error_response(
            response,
            400,
            'InvalidAction',
            'The action or operation requested is invalid'
        )

    def test_invalid_post_method_action(self):
        response = self.app.post('/?Action=DescribeDBInstances', expect_errors=True)
        self._helper_validate_error_response(
            response,
            400,
            'InvalidAction',
            'The action or operation requested is invalid'
        )

    def test_missing_action(self):
        response = self.app.get('/?', expect_errors=True)
        self._helper_validate_error_response(
            response,
            400,
            'MissingAction',
            'The request is missing an action or a required parameter.'
        )

    def test_blank_action(self):
        response = self.app.get('/?Action=', expect_errors=True)
        self._helper_validate_error_response(
            response,
            400,
            'MissingAction',
            'The request is missing an action or a required parameter.'
        )

###################################################################
################# Test DescribeDBInstances API ####################
###################################################################
    def test_describe_db_instances_api_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        describe_url = self.url_with_base_params + \
            "Action=DescribeDBInstances" + \
            "&DBInstanceIdentifier=" + instance_name
        response = self.app.get(describe_url)

        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'creating',
            self.port,
            retention_period=self.backup_retention_period_days
        )

    def test_describe_db_instances_api_instance_creating_time(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        describe_url = self.url_with_base_params + \
            "Action=DescribeDBInstances" + \
            "&DBInstanceIdentifier=" + instance_name

        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['instance_creation_time'] = '2016-03-02 09:57:45.001'
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        instance = db.instance_update('', '', values)
        response = self.app.get(describe_url)

        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'available',
            self.port,
            retention_period=self.backup_retention_period_days,
            instance_creation_time='2016-03-02 09:57:45.001000'
        )
        db._hard_delete_by_id('', models.Instance, instance.id)

    def test_describe_db_instances_api_happy_case_without_db_instace_identifier(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        instance_name_2 = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        instance_2 = self._helper_create_test_instance_in_db(instance_name_2)
        endpoint_name = 'endpoint-' + str(uuid.uuid4())
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name_2
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['endpoint'] = endpoint_name
        db.instance_update('', '', values)
        describe_url = self.url_with_base_params + \
            "Action=DescribeDBInstances"
        response = self.app.get(describe_url)

        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'creating',
            self.port,
            retention_period=self.backup_retention_period_days,
            endpoint=endpoint_name
        )

        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name_2,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'available',
            self.port,
            retention_period=self.backup_retention_period_days,
            endpoint=endpoint_name
        )

    def test_describe_db_instances_with_endpoint_api_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        endpoint_name = 'endpoint-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['endpoint'] = endpoint_name
        db.instance_update('', 'instance_id?', values)

        describe_url = self.url_with_base_params + \
            "Action=DescribeDBInstances" + \
            "&DBInstanceIdentifier=" + instance_name
        response = self.app.get(describe_url)

        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'available',
            self.port,
            retention_period=3,
            endpoint=endpoint_name
        )

    def test_describe_db_instances_without_port(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        describe_url = self.url_with_base_params + \
            "Action=DescribeDBInstances" + \
            "&DBInstanceIdentifier=" + instance_name
        response = self.app.get(describe_url)

        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'creating',
            self.port,
            retention_period=self.backup_retention_period_days
        )

    def test_describe_db_instances_api_instance_does_not_exist_error(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        describe_url = self.url_with_base_params + \
            "Action=DescribeDBInstances" + \
            "&DBInstanceIdentifier=" + instance_name
        response = self.app.get(describe_url, expect_errors=True)

        self._helper_validate_error_response(
            response,
            404,
            'DBInstanceNotFound',
            "%s does not refer to an existing DB instance." % instance_name
        )

###################################################################
################### Test ModifyDBInstance API #####################
###################################################################

    def test_modify_db_instances_api_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        endpoint_name = 'endpoint-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['endpoint'] = endpoint_name
        db.instance_update('', 'instance_id?', values)

        modify_url = self.url_with_base_params + \
            "Action=ModifyDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&PreferredMaintenanceWindow=Sun:23:35-Mon:00:06"
        response = self.app.post(modify_url)

        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'available',
            self.port,
            retention_period=self.backup_retention_period_days,
            endpoint=endpoint_name
        )

    def test_modify_db_instances_with_endpoint_api_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        endpoint_name = 'endpoint-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['endpoint'] = endpoint_name
        db.instance_update('', 'instance_id?', values)

        modify_url = self.url_with_base_params + \
            "Action=ModifyDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&PreferredMaintenanceWindow=Sun:23:35-Mon:00:06"
        response = self.app.post(modify_url)

        self._helper_validate_response_instance_xml(
             1,
             response,
             instance_name,
             self.database_flavor,
             self.database_engine,
             self.database_storage_size,
             self.master_username,
             'available',
             self.port,
             retention_period=3,
             endpoint=endpoint_name
        )

    def test_modify_db_instances_invalid_parameter_error(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        endpoint_name = 'endpoint-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['endpoint'] = endpoint_name
        db.instance_update('', 'instance_id?', values)

        modify_url = self.url_with_base_params + \
            "Action=ModifyDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&PreferredMaintenanceWindow=hello"
        response = self.app.post(modify_url, expect_errors=True)

        self._helper_validate_error_response(
            response,
            400,
            'InvalidParameterValue',
            "An invalid or out-of-range value was supplied for the input parameter: PreferredMaintenanceWindow"
        )

    def test_delete_db_instance_with_endpoint_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        endpoint_name = 'endpoint-' + str(uuid.uuid4())

        # Create instance entry in metadata backend
        instance = self._helper_create_test_instance_in_db(instance_name)
        self.addCleanup(db._delete_db_obj, '', instance)

        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['endpoint'] = endpoint_name
        db.instance_update('', 'instance_id?', values)

        # Make instance lccs ACTIVE/NONE
        db.lccs_conditional_update(
            None,
            models.Instance,
            instance.id,
            'ACTIVE',
            'NONE',
            'CREATING',
            'PENDING',
            None)

        # Construct DeleteDBInstance API request and call it
        delete_url = self.url_with_base_params + \
            "Action=DeleteDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&SkipFinalSnapshot=TruE"
        response = self.app.post(delete_url)

        # Validate HTTP Response
        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'deleting',
            self.port,
            retention_period=self.backup_retention_period_days,
            endpoint=endpoint_name
        )

        # Validate instance lccs
        instance = db.instance_get(None, self.owner_id, instance_name, None)
        self.assertEqual(instance.lifecycle, 'DELETING', "Invalid lifecycle for instance: " + instance.name)
        self.assertEqual(instance.changestate, 'PENDING', "Invalid changestate for instance: " + instance.name)

    def test_restore_db_instance_from_snapshot_api_mandatory_params_only_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        old_instance_name = 'old-' + instance_name
        old_instance = self._helper_create_test_instance_in_db(old_instance_name)

        values = {}
        values['owner_id'] = 1
        values['name'] = old_instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        old_instance = db.instance_update('', '', values)

        snapshot = self._helper_create_test_snapshot_in_db(old_instance, snapshot_name)
        values['name'] = snapshot_name
        db.lccs_conditional_update(
            None,
            models.Backup,
            snapshot['id'],
            'ACTIVE',
            'NONE',
            'CREATING',
            'PENDING',
            None)

        restore_url = self.url_with_base_params + \
            "Action=RestoreDBInstanceFromDBSnapshot" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&DBSnapshotIdentifier=" + snapshot_name
        response = self.app.post(restore_url)

        self._helper_validate_restore_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'restoring'
        )

        snapshot = db.backup_get_by_owner_id_and_name(None, self.owner_id, snapshot_name)
        self.assertEqual(snapshot.lifecycle, 'RESTORING', "Unexpected Lifecycle: " + snapshot.lifecycle)
        self.assertEqual(snapshot.changestate, 'PENDING', "Unexpected Changestate: " + snapshot.changestate)

    def test_restore_db_instance_from_snapshot_api_all_optional_params_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        old_instance_name = 'old-' + instance_name
        old_instance = self._helper_create_test_instance_in_db(old_instance_name)

        values = {}
        values['owner_id'] = 1
        values['name'] = old_instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        old_instance = db.instance_update('', '', values)

        snapshot = self._helper_create_test_snapshot_in_db(old_instance, snapshot_name)
        values['name'] = snapshot_name
        db.lccs_conditional_update(
            None,
            models.Backup,
            snapshot['id'],
            'ACTIVE',
            'NONE',
            'CREATING',
            'PENDING',
            None)

        maintenance_window = "{0}:{1}-{2}:{3}".format(
            self.preferred_maintenance_window_start_day,
            self.preferred_maintenance_window_start_time,
            self.preferred_maintenance_window_end_day,
            self.preferred_maintenance_window_end_time
        )
        backup_window = "{0}-{1}".format(
            self.preferred_backup_window_start,
            self.preferred_backup_window_end
        )
        restore_url = self.url_with_base_params + \
            "Action=RestoreDBInstanceFromDBSnapshot" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&DBSnapshotIdentifier=" + snapshot_name +\
            "&DBInstanceClass=" + self.database_flavor + \
            "&AllocatedStorage=" + str(self.database_storage_size) + \
            "&MasterUsername=" + self.master_username + \
            "&MasterUserPassword=" + self.master_user_password + \
            "&PreferredMaintenanceWindow=" + maintenance_window + \
            "&PreferredBackupWindow=" + backup_window + \
            "&BackupRetentionPeriod=" + str(self.backup_retention_period_days) + \
            "&Port=" + str(self.port)
        response = self.app.post(restore_url)

        self._helper_validate_restore_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'restoring',
            self.port,
            self.database_major_engine_version + '.' + self.database_minor_engine_version,
            self.backup_retention_period_days,
            backup_window,
            maintenance_window
        )

        snapshot = db.backup_get_by_owner_id_and_name(None, self.owner_id, snapshot_name)
        self.assertEqual(snapshot.lifecycle, 'RESTORING', "Unexpected Lifecycle: " + snapshot.lifecycle)
        self.assertEqual(snapshot.changestate, 'PENDING', "Unexpected Changestate: " + snapshot.changestate)

    def test_create_db_snapshot_api_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        db.instance_update('', '', values)

        modify_url = self.url_with_base_params + \
            "Action=CreateDBSnapshot" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&DBSnapshotIdentifier=" + snapshot_name
        response = self.app.post(modify_url)
        engine_version = self.database_major_engine_version + "." + self.database_minor_engine_version

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            instance_name,
            snapshot_name,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

    def test_create_db_snapshot_instance_in_backup_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        db.instance_update('', '', values)

        modify_url = self.url_with_base_params + \
            "Action=CreateDBSnapshot" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&DBSnapshotIdentifier=" + snapshot_name
        response = self.app.post(modify_url)
        instance_ref = db.instance_get('', 1, instance_name)

        self.assertEquals(instance_ref["lifecycle"], 'BACKUP')
        self.assertEquals(instance_ref["changestate"], 'PENDING')

    def test_describe_db_snapshots_api_happy_case_with_snapshot_name(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        db.instance_update('', '', values)
        self._helper_create_test_snapshot_in_db(instance, snapshot_name)


        describe_snapshot_url = self.url_with_base_params + \
            "Action=DescribeDBSnapshots" + \
            "&DBSnapshotIdentifier=" + snapshot_name


        response = self.app.get(describe_snapshot_url)
        engine_version = self.database_major_engine_version + "." + self.database_minor_engine_version

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            instance_name,
            snapshot_name,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

    def test_describe_db_snapshots_api_happy_case_with_instance_name(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        snapshot_name_2 = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        db.instance_update('', '', values)
        self._helper_create_test_snapshot_in_db(instance, snapshot_name)
        self._helper_create_test_snapshot_in_db(instance, snapshot_name_2)


        describe_url = self.url_with_base_params + \
            "Action=DescribeDBSnapshots" + \
            "&DBInstanceIdentifier=" + instance_name
        response = self.app.get(describe_url)
        engine_version = self.database_major_engine_version + "." + self.database_minor_engine_version

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            instance_name,
            snapshot_name,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            instance_name,
            snapshot_name_2,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

    def test_describe_db_snapshots_api_happy_case_with_snapshot_type(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        snapshot_name_2 = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        db.instance_update('', '', values)
        self._helper_create_test_snapshot_in_db(instance, snapshot_name)
        self._helper_create_test_snapshot_in_db(instance, snapshot_name_2)


        describe_url = self.url_with_base_params + \
            "Action=DescribeDBSnapshots" + \
            "&SnapshotType=MANUAL"
        response = self.app.get(describe_url)
        engine_version = self.database_major_engine_version + "." + self.database_minor_engine_version

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            instance_name,
            snapshot_name,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            instance_name,
            snapshot_name_2,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

    def test_describe_db_snapshots_api_happy_case_with_owner_id(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        snapshot_name_2 = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        db.instance_update('', '', values)
        self._helper_create_test_snapshot_in_db(instance, snapshot_name)
        self._helper_create_test_snapshot_in_db(instance, snapshot_name_2)


        describe_url = self.url_with_base_params + \
            "Action=DescribeDBSnapshots"
        response = self.app.get(describe_url)
        engine_version = self.database_major_engine_version + "." + self.database_minor_engine_version

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            instance_name,
            snapshot_name,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            instance_name,
            snapshot_name_2,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

    def test_describe_db_snapshots_api_happy_case_with_instance_deleted(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        #values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        db.instance_update('', '', values)

        self._helper_create_test_snapshot_in_db(instance, snapshot_name)

        db.instance_delete(None, self.owner_id, instance_name)

        describe_snapshot_url = self.url_with_base_params + \
            "Action=DescribeDBSnapshots" + \
            "&DBSnapshotIdentifier=" + snapshot_name
        response = self.app.get(describe_snapshot_url)

        engine_version = self.database_major_engine_version + "." + self.database_minor_engine_version

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            None,
            snapshot_name,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

    def test_describe_db_snapshots_api_happy_case_with_snapshot_type_DBInstanceIdentifier(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        instance_name_2 = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        snapshot_name_2 = 'snapshot-' + str(uuid.uuid4())
        snapshot_name_3 = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        instance_2 = self._helper_create_test_instance_in_db(instance_name_2)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        db.instance_update('', '', values)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name_2
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['instance_create_time'] = '2016-03-02 09:57:45.000'
        # the first two argumnets in the db.instace_update are context and instance_id which are not required
        db.instance_update('', '', values)
        self._helper_create_test_snapshot_in_db(instance, snapshot_name)
        self._helper_create_test_snapshot_in_db(instance, snapshot_name_2)
        self._helper_create_test_snapshot_in_db(instance_2, snapshot_name_3)

        describe_url = self.url_with_base_params + \
            "Action=DescribeDBSnapshots" + \
            "&SnapshotType=MANUAL" + \
            "&DBInstanceIdentifier=" + instance_name
        response = self.app.get(describe_url)
        engine_version = self.database_major_engine_version + "." + self.database_minor_engine_version

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            instance_name,
            snapshot_name,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'creating',
            'MANUAL',
            instance_name,
            snapshot_name_2,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )
        self.assertNotIn(
            "<DBSnapshotIdentifier>{0}</DBSnapshotIdentifier>".format(snapshot_name_3),
            response,
            "Incorrect DBSnapshotIdentifier found in response body"
        )

    def test_modify_db_maintenance_window_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        endpoint_name = 'endpoint-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        values['endpoint'] = endpoint_name
        db.instance_update('', 'instance_id?', values)

        modify_url = self.url_with_base_params + \
            "Action=ModifyDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&PreferredMaintenanceWindow=Sun:23:35-Mon:00:06"
        response = self.app.post(modify_url)
        instance_ref = db.instance_get('', 1, instance_name)

        self.assertEquals(instance_ref["preferred_maintenance_window_start_time"], datetime.time(23,35))
        self.assertEquals(instance_ref["preferred_maintenance_window_start_day"], 'SUN')
        self.assertEquals(instance_ref["preferred_maintenance_window_end_time"], datetime.time(00,06))
        self.assertEquals(instance_ref["preferred_maintenance_window_end_day"], 'MON')

    def test_modify_db_backup_retention_period_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        db.instance_update('', 'instance_id?', values)

        modify_url = self.url_with_base_params + \
            "Action=ModifyDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&BackupRetentionPeriod=4"
        response = self.app.post(modify_url)
        instance_ref = db.instance_get('', 1, instance_name)

        self.assertEquals(instance_ref["backup_retention_period_days"], 4)

    def test_modify_db_name_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        new_instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        db.instance_update('', 'instance_id?', values)

        modify_url = self.url_with_base_params + \
            "Action=ModifyDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&NewDBInstanceIdentifier=" + new_instance_name
        response = self.app.post(modify_url)
        instance_ref = db.instance_get('', 1, new_instance_name)
        self.assertEquals(instance_ref["name"], new_instance_name)

    def test_modify_backup_window_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        db.instance_update('', 'instance_id?', values)

        modify_url = self.url_with_base_params + \
            "Action=ModifyDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&PreferredBackupWindow=23:44-00:14"
        response = self.app.post(modify_url)
        instance_ref = db.instance_get('', 1, instance_name)

        self.assertEquals(instance_ref["preferred_backup_window_start"], datetime.time(23,44))
        self.assertEquals(instance_ref["preferred_backup_window_end"], datetime.time(0,14))


    @patch('apiwebservice.auth.auth.handle_auth')
    def test_total_instances_custom_quota(self, mock_authorize_call):
        # Set total_instances quota to 1 for this new user
        custom_owner_id = str(self.custom_owner_id)
        # custom_owner_id = self.custom_owner_id
        user_quota = db.user_quota_create_entry_from_user_supplied_values(None, custom_owner_id, 'total_instances', 1)
        mock_authorize_call.return_value = custom_owner_id

        instance1_name = 'instance-' + str(uuid.uuid4())
        create_url = self.url_with_base_params + \
            "Action=CreateDBInstance" + \
            "&DBInstanceIdentifier=" + instance1_name + \
            "&DBInstanceClass=" + self.database_flavor + \
            "&Engine=" + self.database_engine + \
            "&AllocatedStorage=" + str(self.database_storage_size) + \
            "&MasterUsername=" + self.master_username + \
            "&MasterUserPassword=" + self.master_user_password

        response = self.app.post(create_url)

        self._helper_validate_response_instance_xml(
            custom_owner_id,
            response,
            instance1_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'creating'
        )

        instance2_name = 'instance-' + str(uuid.uuid4())
        create_url = self.url_with_base_params + \
            "Action=CreateDBInstance" + \
            "&DBInstanceIdentifier=" + instance2_name + \
            "&DBInstanceClass=" + self.database_flavor + \
            "&Engine=" + self.database_engine + \
            "&AllocatedStorage=" + str(self.database_storage_size) + \
            "&MasterUsername=" + self.master_username + \
            "&MasterUserPassword=" + self.master_user_password
        response = self.app.post(create_url, expect_errors=True)

        message = "Request would result in user exceeding the allowed quota {0} of total DB instances. " + \
        "Please contact JCS customer service to increase your total DB instances quota."
        message = message.format(1)

        self._helper_validate_error_response(
            response,
            400,
            'InstanceQuotaExceeded',
            message)
        db._hard_delete_by_id('', models.UserQuota , user_quota.id)

    @patch('apiwebservice.auth.auth.handle_auth')
    def test_total_instances_custom_quota_restore_scenario(self, mock_authorize_call):
        # Set total_instances quota to 1 for this new user
        custom_owner_id = str(1234567+random.randint(1,10000))
        user_quota = db.user_quota_create_entry_from_user_supplied_values(None, custom_owner_id, 'total_instances', 1)
        mock_authorize_call.return_value = custom_owner_id

        instance1_name = 'instance-' + str(uuid.uuid4())
        create_url = self.url_with_base_params + \
            "Action=CreateDBInstance" + \
            "&DBInstanceIdentifier=" + instance1_name + \
            "&DBInstanceClass=" + self.database_flavor + \
            "&Engine=" + self.database_engine + \
            "&AllocatedStorage=" + str(self.database_storage_size) + \
            "&MasterUsername=" + self.master_username + \
            "&MasterUserPassword=" + self.master_user_password
        response = self.app.post(create_url)

        self._helper_validate_response_instance_xml(
            custom_owner_id,
            response,
            instance1_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'creating'
        )

        # Create snapshot from this instance
        instance = db.instance_get('', custom_owner_id, instance1_name)
        backup_name = 'backup-' + str(uuid.uuid4())
        backup = db.backup_create_from_instance('', 'MANUAL', instance, backup_name)

        instance2_name = 'instance-' + str(uuid.uuid4())
        restore_url = self.url_with_base_params + \
            "Action=RestoreDBInstanceFromDBSnapshot" + \
            "&DBInstanceIdentifier=" + instance2_name + \
            "&DBSnapshotIdentifier=" + backup_name
        response = self.app.post(restore_url, expect_errors=True)

        message = "Request would result in user exceeding the allowed quota {0} of total DB instances. " + \
        "Please contact JCS customer service to increase your total DB instances quota."
        message = message.format(1)

        self._helper_validate_error_response(
            response,
            400,
            'InstanceQuotaExceeded',
            message)
        db._hard_delete_by_id('', models.UserQuota , user_quota.id)


    @patch('apiwebservice.auth.auth.handle_auth')
    def test_total_instances_of_instance_class_custom_quota(self, mock_authorize_call):
        custom_owner_id = str(self.custom_owner_id)
        # custom_owner_id = self.custom_owner_id
        # custom_owner_id = self.owner_id
        user_quota = db.user_quota_create_entry_from_user_supplied_values(None, custom_owner_id, 'total_c1.4xlarge_instances', 1)
        self.addCleanup(db._delete_db_obj, '', user_quota)
        mock_authorize_call.return_value = custom_owner_id
        instance_class = 'c1.4xlarge'

        instance1_name = 'instance-' + str(uuid.uuid4())
        create_url = self.url_with_base_params + \
                     "Action=CreateDBInstance" + \
                     "&DBInstanceIdentifier=" + instance1_name + \
                     "&DBInstanceClass=" + instance_class + \
                     "&Engine=" + self.database_engine + \
                     "&AllocatedStorage=" + str(self.database_storage_size) + \
                     "&MasterUsername=" + self.master_username + \
                     "&MasterUserPassword=" + self.master_user_password
        response = self.app.post(create_url)

        self._helper_validate_response_instance_xml(
            custom_owner_id,
            response,
            instance1_name,
            instance_class,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'creating'
        )

        instance2_name = 'instance-' + str(uuid.uuid4())
        create_url = self.url_with_base_params + \
                     "Action=CreateDBInstance" + \
                     "&DBInstanceIdentifier=" + instance2_name + \
                     "&DBInstanceClass=" + instance_class + \
                     "&Engine=" + self.database_engine + \
                     "&AllocatedStorage=" + str(self.database_storage_size) + \
                     "&MasterUsername=" + self.master_username + \
                     "&MasterUserPassword=" + self.master_user_password
        response = self.app.post(create_url, expect_errors=True)

        message = "Request would result in user exceeding the allowed quota {0} of DB instances for the instance " + \
        "class - {1}. Please contact JCS customer service to increase your total DB instances quota for this " + \
        "instance class."
        message = message.format(user_quota['quota_limit'], instance_class)

        self._helper_validate_error_response(
            response,
            400,
            'InstanceClassQuotaExceeded',
            message)


    @patch('apiwebservice.auth.auth.handle_auth')
    def test_total_instances_of_instance_class_custom_quota_restore_scenrio(self, mock_authorize_call):

        custom_owner_id = str(1234567 + random.randint(1, 10000))
        user_quota = db.user_quota_create_entry_from_user_supplied_values(None, custom_owner_id, 'total_c1.4xlarge_instances', 1)
        self.addCleanup(db._delete_db_obj, '', user_quota)
        mock_authorize_call.return_value = custom_owner_id
        instance_class = 'c1.4xlarge'

        instance1_name = 'instance-' + str(uuid.uuid4())
        create_url = self.url_with_base_params + \
                     "Action=CreateDBInstance" + \
                     "&DBInstanceIdentifier=" + instance1_name + \
                     "&DBInstanceClass=" + instance_class + \
                     "&Engine=" + self.database_engine + \
                     "&AllocatedStorage=" + str(self.database_storage_size) + \
                     "&MasterUsername=" + self.master_username + \
                     "&MasterUserPassword=" + self.master_user_password
        response = self.app.post(create_url)

        self._helper_validate_response_instance_xml(
            custom_owner_id,
            response,
            instance1_name,
            instance_class,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'creating'
        )

        # Create snapshot from this instance
        instance = db.instance_get('', custom_owner_id, instance1_name)
        backup_name = 'backup-' + str(uuid.uuid4())
        backup = db.backup_create_from_instance('', 'MANUAL', instance, backup_name)

        instance2_name = 'instance-' + str(uuid.uuid4())
        create_url = self.url_with_base_params + \
                     "Action=RestoreDBInstanceFromDBSnapshot" + \
                     "&DBInstanceIdentifier=" + instance2_name + \
                     "&DBSnapshotIdentifier=" + backup_name
        response = self.app.post(create_url, expect_errors=True)

        message = "Request would result in user exceeding the allowed quota {0} of DB instances for the instance " + \
        "class - {1}. Please contact JCS customer service to increase your total DB instances quota for this " + \
        "instance class."
        message = message.format(user_quota['quota_limit'], instance_class)

        self._helper_validate_error_response(
            response,
            400,
            'InstanceClassQuotaExceeded',
            message)


    def test_create_db_instance_api_only_mandatory_params_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        create_url = self.url_with_base_params + \
            "Action=CreateDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&DBInstanceClass=" + self.database_flavor + \
            "&Engine=" + self.database_engine + \
            "&AllocatedStorage=" + str(self.database_storage_size) + \
            "&MasterUsername=" + self.master_username + \
            "&MasterUserPassword=" + self.master_user_password
        response = self.app.post(create_url)

        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'creating'
        )

    def test_create_db_instance_api_all_optional_params_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        maintenance_window = "{0}:{1}-{2}:{3}".format(
            self.preferred_maintenance_window_start_day,
            self.preferred_maintenance_window_start_time,
            self.preferred_maintenance_window_end_day,
            self.preferred_maintenance_window_end_time
        )
        backup_window = "{0}-{1}".format(
            self.preferred_backup_window_start,
            self.preferred_backup_window_end
        )
        create_url = self.url_with_base_params + \
            "Action=CreateDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&DBInstanceClass=" + self.database_flavor + \
            "&Engine=" + self.database_engine + \
            "&AllocatedStorage=" + str(self.database_storage_size) + \
            "&MasterUsername=" + self.master_username + \
            "&MasterUserPassword=" + self.master_user_password + \
            "&EngineVersion=" + self.database_major_engine_version + \
            "&PreferredMaintenanceWindow=" + maintenance_window + \
            "&PreferredBackupWindow=" + backup_window + \
            "&BackupRetentionPeriod=" + str(self.backup_retention_period_days) + \
            "&LogRetentionPeriod=" + str(self.log_retention_period) + \
            "&Port=" + str(self.port)
        response = self.app.post(create_url)

        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'creating',
            self.port,
            self.database_major_engine_version + '.' + self.database_minor_engine_version,
            self.backup_retention_period_days,
            backup_window,
            maintenance_window
        )

    def test_create_db_instance_api_missing_mandatory_param(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        create_url = self.url_with_base_params + \
            "Action=CreateDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&DBInstanceClass=" + self.database_flavor + \
            "&Engine=" + self.database_engine + \
            "&AllocatedStorage=" + str(self.database_storage_size) + \
            "&MasterUsername=" + self.master_username
        response = self.app.post(create_url, expect_errors=True)

        self._helper_validate_error_response(
            response,
            400,
            'MissingParameter',
            'A required parameter for the specified action is not supplied: MasterUserPassword'
        )

    def test_delete_db_instance_api_no_final_snapshot_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())

        # Create instance entry in metadata backend
        instance = self._helper_create_test_instance_in_db(instance_name)
        self.addCleanup(db._delete_db_obj, '', instance)
        endpoint_name = 'endpoint-' + str(uuid.uuid4())

        # Make instance lccs ACTIVE/NONE
        db.lccs_conditional_update(
            None,
            models.Instance,
            instance.id,
            'ACTIVE',
            'NONE',
            'CREATING',
            'PENDING',
            None)

        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['endpoint'] = endpoint_name
        db.instance_update('', 'instance_id?', values)

        # Construct DeleteDBInstance API request and call it
        delete_url = self.url_with_base_params + \
            "Action=DeleteDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&SkipFinalSnapshot=TruE"
        response = self.app.post(delete_url)

        # Validate HTTP Response
        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'deleting',
            self.port,
            retention_period=self.backup_retention_period_days,
            endpoint=endpoint_name
        )

        # Validate instance lccs
        instance = db.instance_get(None, self.owner_id, instance_name, None)
        self.assertEqual(instance.lifecycle, 'DELETING', "Invalid lifecycle for instance: " + instance.name)
        self.assertEqual(instance.changestate, 'PENDING', "Invalid changestate for instance: " + instance.name)

    def test_delete_db_snapshot_api_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())

        # Create instance entry in metadata backend
        instance = self._helper_create_test_instance_in_db(instance_name)
        self._helper_create_test_snapshot_in_db(instance, snapshot_name)
        snapshot = db.backup_get_by_owner_id_and_name('', owner_id=self.owner_id, backup_name=snapshot_name)

        # Make snapshot lccs ACTIVE/NONE
        snapshot = db.lccs_conditional_update(
            None,
            models.Backup,
            snapshot.id,
            'ACTIVE',
            'NONE',
            'CREATING',
            'PENDING',
            None)

        # Construct DeleteDBInstance API request and call it
        delete_url = self.url_with_base_params + \
            "Action=DeleteDBSnapshot" + \
            "&DBSnapshotIdentifier=" + snapshot_name
        response = self.app.post(delete_url)
        engine_version = self.database_major_engine_version + "." + self.database_minor_engine_version
        # Validate HTTP Response
        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'deleting',
            'MANUAL',
            instance_name,
            snapshot_name,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

        # Validate instance lccs
        snapshot = db.backup_get_by_owner_id_and_name('', owner_id=self.owner_id, backup_name=snapshot_name)
        self.assertEqual(snapshot.lifecycle, 'DELETING')
        self.assertEqual(snapshot.changestate, 'PENDING')

    def test_delete_db_snapshots_api_happy_case_with_instance_deleted(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        db.instance_update('', '', values)

        snapshot = self._helper_create_test_snapshot_in_db(instance, snapshot_name)

        # Make instance lccs ACTIVE/NONE
        db.lccs_conditional_update(
            None,
            models.Backup,
            snapshot.id,
            'ACTIVE',
            'NONE',
            'CREATING',
            'PENDING',
            None)

        db.instance_delete(None, self.owner_id, instance_name)

        delete_snapshot_url = self.url_with_base_params + \
            "Action=DeleteDBSnapshot" + \
            "&DBSnapshotIdentifier=" + snapshot_name
        response = self.app.post(delete_snapshot_url)

        engine_version = self.database_major_engine_version + "." + self.database_minor_engine_version

        self._helper_validate_response_snapshot_xml(
            response,
            self.port,
            self.backup_retention_period_days,
            'deleting',
            'MANUAL',
            None,
            snapshot_name,
            self.database_engine,
            'general-public-license',
            engine_version,
            self.database_storage_size,
            self.master_username
        )

    def test_unique_request_id(self):
       instance_name = 'instance-' + str(uuid.uuid4())
       instance = self._helper_create_test_instance_in_db(instance_name)
       describe_url = self.url_with_base_params + \
            "Action=DescribeDBInstances" + \
            "&DBInstanceIdentifier=" + instance_name
       req_dict = {} 
       for x in range(0, 100):
            response = self.app.get(describe_url)            
            req_id = self._helper_validate_req_id(response)
            if req_dict.get(req_id) is None:
                req_dict[req_id] = x
            else:
                self.fail("In iteration %s : A request id [%s] already exists from iteration # %s"%(x, req_id, req_dict[req_id]))

    def test_delete_db_instance_api_with_final_snapshot_happy_case(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        final_snapshot_name = 'snap-' + str(uuid.uuid4())
        endpoint_name = 'endpoint-' + str(uuid.uuid4())

        # Create instance entry in metadata backend
        instance = self._helper_create_test_instance_in_db(instance_name)
        self.addCleanup(db._delete_db_obj, '', instance)

        # Make instance lccs ACTIVE/NONE
        db.lccs_conditional_update(
            None,
            models.Instance,
            instance.id,
            'ACTIVE',
            'NONE',
            'CREATING',
            'PENDING',
            None)

        values = {}
        values['owner_id'] = 1
        values['name'] = instance_name
        values['endpoint'] = endpoint_name
        db.instance_update('', 'instance_id?', values)

        # Construct DeleteDBInstance API request and call it
        delete_url = self.url_with_base_params + \
            "Action=DeleteDBInstance" + \
            "&DBInstanceIdentifier=" + instance_name + \
            "&SkipFinalSnapshot=fAlSe" + \
            "&FinalDBSnapshotIdentifier=" + final_snapshot_name
        response = self.app.post(delete_url)

        # Validate HTTP Response
        self._helper_validate_response_instance_xml(
            1,
            response,
            instance_name,
            self.database_flavor,
            self.database_engine,
            self.database_storage_size,
            self.master_username,
            'deleting',
            self.port,
            retention_period=self.backup_retention_period_days,
            endpoint=endpoint_name
        )

        # Validate instance lccs
        instance = db.instance_get(None, self.owner_id, instance_name, None)
        self.assertEqual(instance.lifecycle, 'DELETING', "Invalid lifecycle for instance: " + instance.name)
        self.assertEqual(instance.changestate, 'PENDING', "Invalid changestate for instance: " + instance.name)

        # Validate that a backup row has been created
        backup = db.backup_get_by_owner_id_and_name(None, self.owner_id, final_snapshot_name)
        self.addCleanup(db._delete_db_obj, '', backup)
        self.assertEqual(backup.owner_id, self.owner_id, "Invalid owner_id for backup: " + backup.name)
        self.assertEqual(backup.instance_id, instance.id, "Invalid instance_id for backup: " + backup.name)
        self.assertEqual(backup.lifecycle, 'CREATING', "Invalid lifecycle for backup: " + backup.name)
        self.assertEqual(backup.changestate, 'PENDING', "Invalid changestate for backup: " + backup.name)
        self.assertEqual(backup.type, 'MANUAL', "Invalid type for backup: " + backup.name)

###################################################################
################### Test DescribeDBTypes API ######################
###################################################################

    def test_describe_db_types_api_happy_case(self):
        describe_url = self.url_with_base_params + \
                       "Action=DescribeDBTypes" + \
                       "&DBTypeId="+self.database_engine
        response = self.app.get(describe_url)

        database_engines = [db.get_user_visible_db_type_properties(self.database_engine)]
        self._helper_validate_response_db_type_xml(
            response,
            database_engines,
        )

    def test_describe_db_types_api_happy_case_without_db_type_identifier(self):
        describe_url = self.url_with_base_params + \
                       "Action=DescribeDBTypes"

        response = self.app.get(describe_url)

        database_types_names = db.supported_engines_get_all_names()
        database_types = []
        for database_type_name in database_types_names:
            database_type = db.get_user_visible_db_type_properties(database_type_name)
            database_types.append(database_type)
        self._helper_validate_response_db_type_xml(
            response,
            database_types
        )


###################################################################
######################### Helper methods ##########################
###################################################################
    def _helper_validate_unsupported_http_method(self, response, method):
        self._helper_validate_error_response(
            response,
            405,
            'UnsupportedHTTPMethod',
            'Unsupported HTTP method %s specified' % method
        )
        self._helper_validate_req_id(
            response)

    def _helper_create_test_instance_in_db(self, name):
        instance = db.instance_create_entry_from_user_supplied_values(
            context=None,
            owner_id=self.owner_id,
            database_engine=self.database_engine,
            database_engine_version=self.database_major_engine_version,
            database_name=name,
            database_flavor=self.database_flavor,
            database_storage_size=self.database_storage_size,
            master_username=self.master_username,
            master_user_password=self.master_user_password,
            preferred_backup_window_start=self.preferred_backup_window_start,
            preferred_backup_window_end=self.preferred_backup_window_end,
            preferred_maintenance_window_start_day=self.preferred_maintenance_window_start_day,
            preferred_maintenance_window_start_time=self.preferred_maintenance_window_start_time,
            preferred_maintenance_window_end_day=self.preferred_maintenance_window_end_day,
            preferred_maintenance_window_end_time=self.preferred_maintenance_window_end_time,
            backup_retention_period_days=self.backup_retention_period_days,
            log_retention_period=self.log_retention_period,
            port=self.port
        )
        return instance


    def _helper_create_test_snapshot_in_db(self, instance, snapshot_name):
        snapshot = db.backup_create_from_instance(
            context='',
            backup_type='MANUAL',
            instance=instance,
            backup_name=snapshot_name,
            session=None)
        return snapshot

    def _helper_validate_restore_response_instance_xml(
            self,
            owner_id,
            response,
            name,
            instance_class,
            engine,
            storage,
            username,
            status,
            port=None,
            engine_version=None,
            retention_period=None,
            backup_window=None,
            maintenance_window=None
            ):
        self.assertEqual(
            response.status_int,
            200,
            "Status Code: Expected: {0}, Actual: {1}".format(200, response.status_int)
        )
        self.assertIn(
            "<DBInstanceIdentifier>{0}</DBInstanceIdentifier>".format(name),
            response,
            "Incorrect DBInstanceIdentifier found in response body"
        )
        self.assertIn(
            "<DBInstanceClass>{0}</DBInstanceClass>".format(instance_class),
            response,
            "Incorrect DBInstanceClass found in response body"
        )
        self.assertIn(
            "<Engine>{0}</Engine>".format(engine),
            response,
            "Incorrect Engine found in response body"
        )
        self.assertIn(
            "<AllocatedStorage>{0}</AllocatedStorage>".format(storage),
            response,
            "Incorrect AllocatedStorage found in response body"
        )
        self.assertIn(
            "<MasterUsername>{0}</MasterUsername>".format(username),
            response,
            "Incorrect MasterUsername found in response body"
        )
        self.assertIn(
            "<DBInstanceStatus>{0}</DBInstanceStatus>".format(status),
            response,
            "Incorrect DBInstanceStatus found in response body"
        )
        self.assertIn(
            "<LicenseModel>general-public-license</LicenseModel>",
            response,
            "Incorrect LicenseModel found in response body"
        )
        self.assertIn(
            "<PubliclyAccessible>false</PubliclyAccessible>",
            response,
            "Incorrect PubliclyAccessible field found in response body"
        )

        if port is None:
           pass
        else:
            self.assertIn(
                "<Port>{0}</Port>".format(port),
                response,
                "Incorrect Port found in response body"
            )

        if retention_period is None:
            pass
        else:
            self.assertIn(
                "<BackupRetentionPeriod>{0}</BackupRetentionPeriod>".format(retention_period),
                response,
                "Incorrect BackupRetentionPeriod found in response body"
            )

        if backup_window is None:
            pass
        else:
            self.assertIn(
                "<PreferredBackupWindow>{0}</PreferredBackupWindow>".format(backup_window),
                response,
                "Incorrect PreferredBackupWindow found in response body"
            )

        if maintenance_window is None:
           pass
        else:
            self.assertIn(
                "<PreferredMaintenanceWindow>{0}</PreferredMaintenanceWindow>".format(maintenance_window),
                response,
                "Incorrect PreferredMaintenanceWindow found in response body"
            )

        if engine_version is None:
            pass
        else:
            self.assertIn(
                "<EngineVersion>{0}</EngineVersion>".format(engine_version),
                response,
                "Incorrect EngineVersion found in response body"
            )

        self.assertNotIn(
            "MasterUserPassword",
            response,
            "MasterUserPassword must not be returned back in response!!"
        )
        
        self._helper_validate_req_id(
            response)

    def _helper_validate_response_instance_xml(
            self,
            owner_id,
            response,
            name,
            instance_class,
            engine,
            storage,
            username,
            status,
            port=None,
            engine_version=None,
            retention_period=None,
            backup_window=None,
            maintenance_window=None,
            endpoint=None,
            instance_creation_time=None
            ):
        self.assertEqual(
            response.status_int,
            200,
            "Status Code: Expected: {0}, Actual: {1}".format(200, response.status_int)
        )
        self.assertIn(
            "<DBInstanceIdentifier>{0}</DBInstanceIdentifier>".format(name),
            response,
            "Incorrect DBInstanceIdentifier found in response body"
        )
        self.assertIn(
            "<DBInstanceClass>{0}</DBInstanceClass>".format(instance_class),
            response,
            "Incorrect DBInstanceClass found in response body"
        )
        self.assertIn(
            "<Engine>{0}</Engine>".format(engine),
            response,
            "Incorrect Engine found in response body"
        )
        self.assertIn(
            "<AllocatedStorage>{0}</AllocatedStorage>".format(storage),
            response,
            "Incorrect AllocatedStorage found in response body"
        )
        self.assertIn(
            "<MasterUsername>{0}</MasterUsername>".format(username),
            response,
            "Incorrect MasterUsername found in response body"
        )
        self.assertIn(
            "<DBInstanceStatus>{0}</DBInstanceStatus>".format(status),
            response,
            "Incorrect DBInstanceStatus found in response body"
        )
        self.assertIn(
            "<LicenseModel>general-public-license</LicenseModel>",
            response,
            "Incorrect LicenseModel found in response body"
        )
        self.assertIn(
            "<PubliclyAccessible>false</PubliclyAccessible>",
            response,
            "Incorrect PubliclyAccessible field found in response body"
        )

        if port is None:
            port = pecan.conf.user_parameter_defaults['port_'+self.database_engine]
        self.assertIn(
            "<Port>{0}</Port>".format(port),
            response,
            "Incorrect Port found in response body"
        )

        if retention_period is None:
            retention_period = pecan.conf.user_parameter_defaults['backup_retention_period']
        self.assertIn(
            "<BackupRetentionPeriod>{0}</BackupRetentionPeriod>".format(retention_period),
            response,
            "Incorrect BackupRetentionPeriod found in response body"
        )

        if backup_window is None:
            instance = db.instance_get(None, owner_id, name, None)
            bw_start = instance.preferred_backup_window_start.strftime('%H:%M')
            bw_end = instance.preferred_backup_window_end.strftime('%H:%M')
            backup_window = bw_start + '-' + bw_end
        self.assertIn(
            "<PreferredBackupWindow>{0}</PreferredBackupWindow>".format(backup_window),
            response,
            "Incorrect PreferredBackupWindow found in response body"
        )

        if maintenance_window is None:
            instance = db.instance_get(None, owner_id, name, None)
            maintenance_window = "{0}:{1}-{2}:{3}".format(
                instance.preferred_maintenance_window_start_day,
                instance.preferred_maintenance_window_start_time.strftime('%H:%M'),
                instance.preferred_maintenance_window_end_day,
                instance.preferred_maintenance_window_end_time.strftime('%H:%M'),
            )
        self.assertIn(
            "<PreferredMaintenanceWindow>{0}</PreferredMaintenanceWindow>".format(maintenance_window),
            response,
            "Incorrect PreferredMaintenanceWindow found in response body"
        )

        if endpoint:
            self.assertIn(
                "<Address>{0}</Address>".format(endpoint),
                response,
                "Incorrect endpoint_address found in response body %r" %endpoint
            )
        else:
            self.assertNotIn(
                "<Address>",
                response,
                "Incorrect endpoint_address found in response body %r" %endpoint
            )

        if instance_creation_time is not None:
            self.assertIn(
                "<InstanceCreateTime>{0}</InstanceCreateTime>".format(instance_creation_time),
                response,
                "Incorrect InstanceCreateTime found in response body %r" %response
            )
        if engine_version is None:
            engine_version = self.database_major_engine_version+'.'+self.database_minor_engine_version
        self.assertIn(
            "<EngineVersion>{0}</EngineVersion>".format(engine_version),
            response,
            "Incorrect EngineVersion found in response body"
        )

        self.assertNotIn(
            "MasterUserPassword",
            response,
            "MasterUserPassword must not be returned back in response!!"
        )

        self._helper_validate_req_id(
            response)

    def _helper_validate_response_snapshot_xml(
            self,
            response,
            port,
            retention_period,
            status,
            snapshotType,
            db_instance_identifier,
            db_snapshot_identifier,
            engine,
            general_license,
            engine_version,
            storage,
            username
        ):

        instance_exists = True
        instance = None
        try:
            instance = db.instance_get(None, self.owner_id, db_instance_identifier)
        except exception.InstanceNotFoundByOwnerIdAndName:
            instance_exists = False

        if instance_exists:
            self.assertIn(
                "<DBInstanceIdentifier>{0}</DBInstanceIdentifier>".format(db_instance_identifier),
                response,
                "Incorrect DBInstanceIdentifier found in response body the DB identifier"
            )
            self.assertIn(
                "<InstanceCreateTime>{0}</InstanceCreateTime>".format(instance.instance_creation_time),
                response,
                "Incorrect InstanceCreateTime found in response body the DB identifier"
            )
        else:
            self.assertNotIn(
                "<DBInstanceIdentifier>",
                response,
                "Incorrect DBInstanceIdentifier found in response body the DB identifier"
            )
            self.assertNotIn(
                "<InstanceCreateTime>",
                response,
                "Instance Create Time not expected in response body"
            )

        self.assertIn(
            "<Status>{0}</Status>".format(status),
            response,
            "Incorrect Status found in response body, the status is %r" %status
        )

        self.assertIn(
            "<SnapshotType>{0}</SnapshotType>".format(snapshotType),
            response,
            "Incorrect SnapshotType found in response body"
        )

        self.assertIn(
            "<DBSnapshotIdentifier>{0}</DBSnapshotIdentifier>".format(db_snapshot_identifier),
            response,
            "Incorrect DBSnapshotIdentifier found in response body"
        )

        self.assertIn(
            "<Engine>{0}</Engine>".format(engine),
            response,
            "Incorrect Engine found in response body"
        )

        self.assertIn(
            "<AllocatedStorage>{0}</AllocatedStorage>".format(storage),
            response,
            "Incorrect AllocatedStorage found in response body"
        )

        self.assertIn(
            "<MasterUsername>{0}</MasterUsername>".format(username),
            response,
            "Incorrect MasterUsername found in response body"
        )

        self.assertIn(
            "<LicenseModel>general-public-license</LicenseModel>",
            response,
            "Incorrect LicenseModel found in response body"
        )
        '''
        # TODO sid, write the code for the time
        self.assertIn(
            "<InstanceCreateTime>{0}</InstanceCreateTime>".format(instance_create_time),
            response,
            "Incorrect InstanceCreateTime found in response body %r" %response
        )
        '''

        self.assertIn(
            "<Port>{0}</Port>".format(port),
            response,
            "Incorrect Port found in response body"
        )

        self.assertIn(
            "<BackupRetentionPeriod>{0}</BackupRetentionPeriod>".format(retention_period),
            response,
            "Incorrect BackupRetentionPeriod found in response body"
        )

        self.assertIn(
            "<EngineVersion>{0}</EngineVersion>".format(engine_version),
            response,
            "Incorrect EngineVersion found in response body"
        )

        self.assertNotIn(
            "MasterUserPassword",
            response,
            "MasterUserPassword must not be returned back in response!!"
        )

        self._helper_validate_req_id(
            response)

    def _helper_validate_error_response(
            self,
            response,
            status_code,
            code,
            message):
        self.assertEqual(
            response.status_int,
            status_code,
            "Invalid Status Code. Expected: %s, Actual: %s" % (status_code, response.status_int))
        self.assertIn(
            "<Message>%s" % message,
            response.body,
            "Message not found. Expected message: %s, in the response body: %s" % (message, response.body))
        self.assertIn(
            "<Code>%s</Code>" % code,
            response.body,
            "Code not found. Expected code: %s, in the response body: %s" % (code, response.body))
        self._helper_validate_req_id(
            response)

    def _helper_validate_req_id(
            self,
            response):
        m = re.search('<RequestId>(.*)</RequestId>', response.body)
        req_id = m.group(1)
        self.assertTrue(len(req_id) >= 16, "Length of request_id %s is less than 16"%(req_id))
        return req_id

    def _helper_validate_response_db_type_xml(
            self,
            response,
            db_types):
        for db_type in db_types:
            self.assertIn(
                "<DBTypeId>{0}</DBTypeId>".format(db_type['name']),
                response,
                "Incorrect DBTypeId found in DB identifier."
                )
            for db_type_version in db_type['db_type_versions']:
                self.assertIn(
                    '<DBTypeVersion>{0}</DBTypeVersion>'.format(db_type_version.major_version+'.'+db_type_version.minor_version),
                    response,
                    "Version information not found."
                )
                self.assertIn(
                    '<DBTypeState>{0}</DBTypeState>'.format('Preferred' if db_type_version.state=='PREFERRED' else 'Available'),
                    response,
                    "State information not found"
                )

        self._helper_validate_req_id(response)

#################################################################################################
##################### FOR RUNNING TEST CASES FOR ANY DATABASE ENGINE#############################
#################################################################################################
_custom_owner_id=1234567+random.randint(1,10000)
class MysqlTestRootController(BaseTestRootController,TestCase):
    database_engine='mysql'
    database_major_engine_version='5.6'
    database_minor_engine_version='2'
    port=3307
    custom_owner_id=_custom_owner_id

class PostgresqlTestRootController(BaseTestRootController, TestCase):
    database_engine ='postgresql'
    database_major_engine_version = '9.5'
    database_minor_engine_version = '3'
    port = 5432
    custom_owner_id = _custom_owner_id+1


