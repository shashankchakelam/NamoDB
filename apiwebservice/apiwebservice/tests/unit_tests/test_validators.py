import os
import unittest
import uuid

from namo.common import exception
from apiwebservice.validators import quota_validators
from apiwebservice.constants import reserved_words
from apiwebservice.constants import parameters
from apiwebservice.validators import apis
from apiwebservice.validators import helpers
from apiwebservice.apihandlers.delete_db_snapshot_handler import DeleteDBSnapshotHandler
from apiwebservice import exceptions
from datetime import datetime
from pecan import conf
from pecan import set_config
from namo.db.mysqlalchemy import api as db
from mock import patch


class BaseTestValidators:
    """
    Unit Tests for all apiwebservice.validators methods
    """
    @classmethod
    def setUpClass(cls):
        set_config(os.path.join(os.path.dirname(__file__), '../config.py'))
        cls.query_parameters = {}
        cls.base_params = parameters.BaseParams()
        cls.create_db_instance_params = parameters.CreateDBInstanceParams()
        cls.delete_db_instance_params = parameters.DeleteDBInstanceParams()
        cls.describe_db_snapshots_params = parameters.DescribeDBSnapshotsParams()
        cls.delete_db_snapshot_params = parameters.DeleteDBSnapshotParams()
        cls.describe_db_types_params = parameters.DescribeDBTypesParams()
        # giving owner_id=1 a lot of snapshots to run all the test cases

        cls.database_flavor = 'c1.xlarge'

        try:
            db.user_quota_get_by_owner_id_quota_name('', 1, 'total_snapshots')
        except exception.UserLimitNotFoundByOwnerId:
            db.user_quota_create_entry_from_user_supplied_values('', 1, 'total_instances', 2500)
            db.user_quota_create_entry_from_user_supplied_values('', 1, 'total_snapshots', 2500)
            db.user_quota_create_entry_from_user_supplied_values('', 1, 'total_storage', 250000)

    def setUp(self):
        self.query_parameters.clear()
        self.query_parameters[self.base_params.version.name] = \
            self.base_params.version.supported_values[0]
        self.query_parameters[self.base_params.access_key_id.name] = \
            'abcdefghijklmnopqrstuvwxyz1234'
        self.query_parameters[self.base_params.signature.name] = \
            'abcdefghiJKLMNOPQRST12345='
        self.query_parameters[self.base_params.signature_method.name] = \
            self.base_params.signature_method.supported_values[0]
        self.query_parameters[self.base_params.signature_version.name] = \
            self.base_params.signature_version.supported_values[0]
        self.query_parameters[self.base_params.timestamp.name] = \
            datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')

###################################################################
################ Test Common Parameter Validation #################
###################################################################
    # Happy case validating common parameters
    def test_validate_common_parameters_happy_case(self):
        helpers.validate_and_set_common_parameters(self.base_params, self.query_parameters)

    ####### Test common params missing in query params
    def test_missing_version_param(self):
        self._helper_test_missing_common_param(self.base_params.version.name)

    def test_missing_access_key_id_param(self):
        self._helper_test_missing_common_param(self.base_params.access_key_id.name)

    def test_missing_signature_param(self):
        self._helper_test_missing_common_param(self.base_params.signature.name)

    def test_missing_signature_method_param(self):
        self._helper_test_missing_common_param(self.base_params.signature_method.name)

    def test_missing_signature_version_param(self):
        self._helper_test_missing_common_param(self.base_params.signature_version.name)

    def test_missing_timestamp_param(self):
        self._helper_test_missing_common_param(self.base_params.timestamp.name)

    ####### Test Unsupported values for Common Params
    def test_unsupported_version_param(self):
        self._helper_test_unsupported_common_param_value(self.base_params.version.name)

    def test_unsupported_signature_method_param(self):
        self._helper_test_unsupported_common_param_value(self.base_params.signature_method.name)

    def test_unsupported_signature_version_param(self):
        self._helper_test_unsupported_common_param_value(self.base_params.signature_version.name)

    ####### Test Blank Values for Common Params
    def test_blank_version_param(self):
        self._helper_test_blank_common_param(self.base_params.version.name)

    def test_blank_access_key_id_param(self):
        self._helper_test_blank_common_param(self.base_params.access_key_id.name)

    def test_blank_signature_param(self):
        self._helper_test_blank_common_param(self.base_params.signature.name)

    def test_blank_signature_method_param(self):
        self._helper_test_blank_common_param(self.base_params.signature_method.name)

    def test_blank_signature_version_param(self):
        self._helper_test_blank_common_param(self.base_params.signature_version.name)

    def test_blank_timestamp_param(self):
        self._helper_test_blank_common_param(self.base_params.timestamp.name)

    ####### Test Access Key ID validation a little more
    def test_access_key_id_too_short(self):
        # Min length is 16 characters
        self.query_parameters[self.base_params.access_key_id.name] = 'abcd'
        self._helper_validate_common_param_expect_exception(
                self.base_params.access_key_id.name,
                exceptions.InvalidParameterValue)

    def test_access_key_id_too_long(self):
        # Max length is 32 characters
        self.query_parameters[self.base_params.access_key_id.name] = 'abcdefghijklmnopqrsetuvwxyz123456789'
        self._helper_validate_common_param_expect_exception(
                self.base_params.access_key_id.name,
                exceptions.InvalidParameterValue)

    def test_access_key_id_not_alphanumeric(self):
        self.query_parameters[self.base_params.access_key_id.name] = 'abcdefghijklmnopqrstuvwxyz@#!$'
        self._helper_validate_common_param_expect_exception(
                self.base_params.access_key_id.name,
                exceptions.InvalidParameterValue)

    ####### Test timestamp validation a little more
    def test_timestamp_incorrect_format(self):
        # Expected format for timestamp is YYYY-mm-ddTHH-MM-ssZ
        self.query_parameters[self.base_params.timestamp.name] = "27-02-2016T14:09:16Z"
        self._helper_validate_common_param_expect_exception(
                self.base_params.timestamp.name,
                exceptions.InvalidParameterValue)

###################################################################
############## Test DBInstanceIdentifier Validation ###############
###################################################################
    def test_db_instance_identifier_validation_happy_case(self):
        helpers.validate_db_instance_identifier(
                self.create_db_instance_params.db_instance_identifier,
                " Shank-Instance-12345  ")
        self.assertEqual(
                self.create_db_instance_params.db_instance_identifier.value,
                "shank-instance-12345",
                "Incorrectly formatted")

    def test_db_instance_identifier_too_long(self):
        identifier = "123456789abcdefghijklmnopqrstuvwxyz123456789abcdefghijklmnopqrstuvwxyz"
        self._helper_validate_db_instance_identifier_expect_exception(identifier, exceptions.InvalidParameterValue)

    def test_db_instance_identifier_blank(self):
        self._helper_validate_db_instance_identifier_expect_exception('', exceptions.InvalidParameterValue)

    def test_db_instance_identifier_ends_with_hyphen(self):
        self._helper_validate_db_instance_identifier_expect_exception("abc123-", exceptions.InvalidParameterValue)

    def test_db_instance_identifier_contains_consecutive_hyphens(self):
        self._helper_validate_db_instance_identifier_expect_exception("abc--123", exceptions.InvalidParameterValue)

    def test_db_instance_identifier_has_special_characters(self):
        self._helper_validate_db_instance_identifier_expect_exception("abc123@!#$", exceptions.InvalidParameterValue)

###################################################################
############### Test SnapshotIdentifier Validation ################
###################################################################
    def test_snapshot_identifier_validation_happy_case(self):
        helpers.validate_snapshot_identifier(
                self.delete_db_instance_params.final_db_snapshot_identifier,
                " Shank-Snapshot-12345  ")
        self.assertEqual(
                self.delete_db_instance_params.final_db_snapshot_identifier.value,
                "shank-snapshot-12345",
                "Incorrectly formatted")

    def test_snapshot_identifier_too_long(self):
        identifier = "123456789abcdefghijklmnopqrstuvwxyz" * 10
        self._helper_validate_snapshot_identifier_expect_exception(identifier, exceptions.InvalidParameterValue)

    def test_snapshot_identifier_blank(self):
        self._helper_validate_snapshot_identifier_expect_exception('', exceptions.InvalidParameterValue)

    def test_snapshot_identifier_ends_with_hyphen(self):
        self._helper_validate_snapshot_identifier_expect_exception("abc123-", exceptions.InvalidParameterValue)

    def test_snapshot_identifier_contains_consecutive_hyphens(self):
        self._helper_validate_snapshot_identifier_expect_exception("abc--123", exceptions.InvalidParameterValue)

    def test_snapshot_identifier_has_special_characters(self):
        self._helper_validate_snapshot_identifier_expect_exception("abc123@!#$", exceptions.InvalidParameterValue)

###################################################################
################ Test DBInstanceClass Validation ##################
###################################################################
    @patch('namo.db.mysqlalchemy.api.supported_nova_flavors_get_all_names')
    def test_db_instance_class_validation_happy_case(self, mock_db_call):
        mock_db_call.return_value = ['m1.small', 'm1.medium']
        helpers.validate_db_instance_class(
            self.create_db_instance_params.db_instance_class,
            " m1.small       ")
        self.assertEqual(self.create_db_instance_params.db_instance_class.value, "m1.small", "Incorrectly formatted")
        mock_db_call.assert_called_once_with()

    @patch('namo.db.mysqlalchemy.api.supported_nova_flavors_get_all_names')
    def test_db_instance_class_invalid_class_specified(self, mock_db_call):
        mock_db_call.return_value = ['m1.medium', 'm1.large']

        with self.assertRaises(exceptions.InvalidParameterValue) as context_manager:
            helpers.validate_db_instance_class(
                self.create_db_instance_params.db_instance_class,
                "m1.small")

        self.assertIn('DBInstanceClass', context_manager.exception.msg)
        mock_db_call.assert_called_once_with()

    def test_db_instance_class_blank(self):
        with self.assertRaises(exceptions.InvalidParameterValue) as context_manager:
            helpers.validate_db_instance_class(
                self.create_db_instance_params.db_instance_class,
                '')
        self.assertIn('DBInstanceClass', context_manager.exception.msg)

###################################################################
##################### Test Engine Validation ######################
###################################################################
    @patch('namo.db.mysqlalchemy.api.supported_engines_get_all_names')
    def test_engine_validation_happy_case(self, mock_db_call):
        mock_db_call.return_value = ['oracle', 'mysql','postgresql']
        helpers.validate_engine(
            self.create_db_instance_params.engine,
            '  '+self.database_engine+'      ')
        self.assertEqual(self.create_db_instance_params.engine.value, self.database_engine, "Incorrectly formatted")
        mock_db_call.assert_called_once_with()

    @patch('namo.db.mysqlalchemy.api.supported_engines_get_all_names')
    def test_engine_invalid_engine_specified(self, mock_db_call):
        mock_db_call.return_value = ['oracle', 'mysql','postgresql']

        with self.assertRaises(exceptions.InvalidParameterValue) as context_manager:
            helpers.validate_engine(
                self.create_db_instance_params.engine,
                'sqlserver')

        self.assertIn('Engine', context_manager.exception.msg)
        mock_db_call.assert_called_once_with()

    def test_engine_blank(self):
        with self.assertRaises(exceptions.InvalidParameterValue) as context_manager:
            helpers.validate_engine(
                self.create_db_instance_params.engine,
                '')
        self.assertIn('Engine', context_manager.exception.msg)

###################################################################
################# Test EngineVersion Validation ###################
###################################################################
    @patch('namo.db.mysqlalchemy.api.supported_engine_versions_get_all_values')
    def test_engine_version_validation_happy_case(self, mock_db_call):
        mock_db_call.return_value = self.supported_engine_versions_list
        helpers.validate_engine_version(
            self.create_db_instance_params.engine_version,
            '   '+self.database_engine_major_version+'           ',
            self.database_engine)
        self.assertEqual(self.create_db_instance_params.engine_version.value, self.database_engine_major_version, "Incorrectly formatted")
        mock_db_call.assert_called_once_with(self.database_engine)

    @patch('namo.db.mysqlalchemy.api.supported_engine_versions_get_all_values')
    def test_engine_version_invalid_value(self, mock_db_call):
        mock_db_call.return_value = self.supported_engine_versions_list

        with self.assertRaises(exceptions.InvalidParameterValue) as context_manager:
            helpers.validate_engine_version(
                self.create_db_instance_params.engine_version,
                self.unsupported_database_engine_major_version,
                self.database_engine)

        self.assertIn('EngineVersion', context_manager.exception.msg)
        mock_db_call.assert_called_once_with(self.database_engine)

    def test_engine_version_blank(self):
        with self.assertRaises(exceptions.InvalidParameterValue) as context_manager:
            helpers.validate_engine_version(
                self.create_db_instance_params.engine_version,
                '',
                self.database_engine)
        self.assertIn('EngineVersion', context_manager.exception.msg)

###################################################################
############### Test AllocatedStorage Validation ##################
###################################################################
    def test_allocated_storage_validation_happy_case(self):
        test_value = (conf.user_parameter_constraints['min_storage_size_supported'] +
                      conf.user_parameter_constraints['max_storage_size_supported']) / 2
        helpers.validate_allocated_storage(
            self.create_db_instance_params.allocated_storage,
            test_value)
        self.assertEqual(
            self.create_db_instance_params.allocated_storage.value,
            test_value,
            "Incorrect value set for allocated storage parameter")

    def test_allocated_storage_string_number(self):
        test_value = "100"
        helpers.validate_allocated_storage(
            self.create_db_instance_params.allocated_storage,
            test_value)
        self.assertEqual(
            self.create_db_instance_params.allocated_storage.value,
            int(test_value),
            "Incorrect value set for allocated storage parameter")

    def test_allocated_storage_blank(self):
        self._helper_validate_allocated_storage_expect_exception(
            '',
            exceptions.InvalidParameterValue)

    def test_allocated_storage_too_small(self):
        test_value = conf.user_parameter_constraints['min_storage_size_supported'] - 1
        self._helper_validate_allocated_storage_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_allocated_storage_too_large(self):
        test_value = conf.user_parameter_constraints['max_storage_size_supported'] + 1
        self._helper_validate_allocated_storage_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_allocated_storage_not_number(self):
        test_value = "hundred"
        self._helper_validate_allocated_storage_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_allocated_storage_decimal_number(self):
        test_value = 100.2
        self._helper_validate_allocated_storage_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

###################################################################
################ Test MasterUsername Validation ###################
###################################################################
    def test_master_username_validation_happy_case(self):
        test_username = 'shank'
        helpers.validate_master_username(
            self.create_db_instance_params.master_username,
            test_username,
            self.database_engine)
        self.assertEqual(
            self.create_db_instance_params.master_username.value,
            test_username,
            "Incorrect value set for master username")

    def test_master_username_blank(self):
        self._helper_validate_master_username_expect_exception(
            '',
            exceptions.InvalidParameterValue)

    def test_master_username_too_short(self):
        length = conf.user_parameter_constraints[self.database_engine+'_master_username_min_length'] - 1
        test_username = 'a' * length
        self._helper_validate_master_username_expect_exception(
            test_username,
            exceptions.InvalidParameterValue)

    def test_master_username_too_long(self):
        length = conf.user_parameter_constraints[self.database_engine+'_master_username_max_length'] + 1
        test_username = 'a' * length
        self._helper_validate_master_username_expect_exception(
            test_username,
            exceptions.InvalidParameterValue)

    def test_master_username_not_alphanumeric(self):
        test_username = 'abc-123@#'
        self._helper_validate_master_username_expect_exception(
            test_username,
            exceptions.InvalidParameterValue)

    def test_master_username_is_reserved_word(self):
        test_username = reserved_words.all_reserved_words[self.database_engine+'_reserved_words'][0]
        self._helper_validate_master_username_expect_exception(
            test_username,
            exceptions.InvalidParameterValue)

    def test_master_username_not_mysql(self):
        with self.assertRaises(NotImplementedError):
            helpers.validate_master_username(
                self.create_db_instance_params.master_username,
                'abc123',
                'oracle')

###################################################################
############## Test MasterUserPassword Validation #################
###################################################################
    def test_master_user_password_validation_happy_case(self):
        helpers.validate_master_user_password(
            self.create_db_instance_params.master_user_password,
            'hello123',
            self.database_engine)
        self.assertEqual(
            self.create_db_instance_params.master_user_password.value,
            'hello123',
            "Incorrect value set for master user password"
        )

    def test_master_user_password_blank(self):
        test_password = ''
        self._helper_validate_master_user_password_expect_exception(
            test_password,
            exceptions.InvalidParameterValue)

    def test_master_user_password_too_short(self):
        length = conf.user_parameter_constraints[self.database_engine+'_master_user_pwd_min_length'] - 1
        test_password = 'a' * length
        self._helper_validate_master_user_password_expect_exception(
            test_password,
            exceptions.InvalidParameterValue)

    def test_master_user_password_too_long(self):
        length = conf.user_parameter_constraints[self.database_engine+'_master_user_pwd_max_length'] + 1
        test_password = 'a' * length
        self._helper_validate_master_user_password_expect_exception(
            test_password,
            exceptions.InvalidParameterValue)

    def test_master_user_password_contains_slash(self):
        test_password = 'abc/123'
        self._helper_validate_master_user_password_expect_exception(
            test_password,
            exceptions.InvalidParameterValue)

    def test_master_user_password_contains_at_symbol(self):
        test_password = 'abc@123'
        self._helper_validate_master_user_password_expect_exception(
            test_password,
            exceptions.InvalidParameterValue)

    def test_master_user_password_contains_quote(self):
        test_password = 'abc"123'
        self._helper_validate_master_user_password_expect_exception(
            test_password,
            exceptions.InvalidParameterValue)

    def test_master_user_password_not_mysql(self):
        with self.assertRaises(NotImplementedError):
            helpers.validate_master_user_password(
                self.create_db_instance_params.master_user_password,
                'abc123',
                'oracle')

###################################################################
##################### Test Port Validation ########################
###################################################################
    def test_port_validation_happy_case(self):
        test_value = (conf.user_parameter_constraints['port_range_min'] +
                      conf.user_parameter_constraints['port_range_max'])/2
        helpers.validate_port(
            self.create_db_instance_params.port,
            test_value)
        self.assertEqual(
            self.create_db_instance_params.port.value,
            test_value,
            "Incorrect value set for port"
        )

    def test_port_string_number(self):
        test_value = self.port
        helpers.validate_port(
            self.create_db_instance_params.port,
            test_value)
        self.assertEqual(
            self.create_db_instance_params.port.value,
            int(test_value),
            "Incorrect value set for port")

    def test_port_too_small(self):
        test_value = conf.user_parameter_constraints['port_range_min'] - 1
        self._helper_validate_port_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_port_too_large(self):
        test_value = conf.user_parameter_constraints['port_range_max'] + 1
        self._helper_validate_port_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_port_blank(self):
        test_value = ''
        self._helper_validate_port_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_port_not_number(self):
        test_value = 'thousand'
        self._helper_validate_port_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_port_decimal_number(self):
        test_value = 10000.0
        self._helper_validate_port_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

###################################################################
############### Test BackupRetention Validation ###################
###################################################################
    def test_backup_retention_validation_happy_case(self):
        test_value = (conf.user_parameter_constraints['min_backup_retention_period'] +
                      conf.user_parameter_constraints['max_backup_retention_period'])/2
        helpers.validate_backup_retention_period(
            self.create_db_instance_params.backup_retention_period,
            test_value)
        self.assertEqual(
            self.create_db_instance_params.backup_retention_period.value,
            test_value,
            "Incorrect value set for backup retention period")

    def test_backup_retention_string_number(self):
        test_value = str((conf.user_parameter_constraints['min_backup_retention_period'] +
                      conf.user_parameter_constraints['max_backup_retention_period'])/2)
        helpers.validate_backup_retention_period(
            self.create_db_instance_params.backup_retention_period,
            test_value)
        self.assertEqual(
            self.create_db_instance_params.backup_retention_period.value,
            int(test_value),
            "Incorrect value set for backup retention period")

    def test_backup_retention_too_small(self):
        test_value = conf.user_parameter_constraints['min_backup_retention_period'] - 1
        self._helper_validate_backup_retention_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_retention_too_large(self):
        test_value = conf.user_parameter_constraints['max_backup_retention_period'] + 1
        self._helper_validate_backup_retention_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_retention_blank(self):
        test_value = ''
        self._helper_validate_backup_retention_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_retention_not_number(self):
        test_value = "thirty"
        self._helper_validate_backup_retention_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_retention_decimal_number(self):
        test_value = 20.0
        self._helper_validate_backup_retention_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

###################################################################
######### Test PreferredMaintenanceWindow Validation ##############
###################################################################
    def test_maintenance_window_validation_happy_case_same_day(self):
        test_value = 'Sun:22:31-Sun:23:06'
        helpers.validate_preferred_maintenance_window(
            self.create_db_instance_params.preferred_maintenance_window,
            test_value)
        self.assertEqual(
            self.create_db_instance_params.preferred_maintenance_window.value,
            test_value)

    def test_maintenance_window_validation_happy_case_different_day(self):
        test_value = 'Sun:23:31-Mon:00:06'
        helpers.validate_preferred_maintenance_window(
            self.create_db_instance_params.preferred_maintenance_window,
            test_value)
        self.assertEqual(
            self.create_db_instance_params.preferred_maintenance_window.value,
            test_value)

    def test_maintenance_window_start_date_invalid_day_format(self):
        test_value = 'Sunday:23:31-Mon:00:06'
        self._helper_validate_maintenance_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_maintenance_window_start_date_invalid_hour(self):
        test_value = 'Sun:25:31-Mon:00:06'
        self._helper_validate_maintenance_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_maintenance_window_start_date_invalid_minute(self):
        test_value = 'Sun:23:61-Mon:00:06'
        self._helper_validate_maintenance_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_maintenance_window_invalid_format_missing_hyphen(self):
        test_value = 'Sun:23:31:Mon:00:06'
        self._helper_validate_maintenance_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_maintenance_window_invalid_format_datetime(self):
        test_value = 'Sun-23-31-Mon:00:06'
        self._helper_validate_maintenance_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_maintenance_window_blank(self):
        test_value = ''
        self._helper_validate_maintenance_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_maintenance_window_too_short(self):
        test_value = 'Sun:23:51-Mon:00:06'
        self._helper_validate_maintenance_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_maintenance_window_too_long(self):
        test_value = 'Sun:00:51-Mon:00:06'
        self._helper_validate_maintenance_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_maintenance_window_start_after_end_same_day(self):
        test_value = 'Sun:23:51-Sun:23:06'
        self._helper_validate_maintenance_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_maintenance_window_start_after_end_different_day(self):
        test_value = 'Mon:00:06-Sun:23:31'
        self._helper_validate_maintenance_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

###################################################################
############## Test PreferredBackupWindow Validation ##############
###################################################################
    def test_backup_window_validation_happy_case_same_day(self):
        test_value = '22:31-23:06'
        helpers.validate_preferred_backup_window(
            self.create_db_instance_params.preferred_backup_window,
            test_value)
        self.assertEqual(
            self.create_db_instance_params.preferred_backup_window.value,
            test_value)

    def test_backup_window_validation_happy_case_different_day(self):
        test_value = '23:31-00:06'
        helpers.validate_preferred_backup_window(
            self.create_db_instance_params.preferred_backup_window,
            test_value)
        self.assertEqual(
            self.create_db_instance_params.preferred_backup_window.value,
            test_value)

    def test_backup_window_start_date_invalid_hour(self):
        test_value = '25:31-00:06'
        self._helper_validate_backup_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_window_start_date_invalid_minute(self):
        test_value = '23:61-00:06'
        self._helper_validate_backup_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_window_invalid_format_missing_hyphen(self):
        test_value = '23:31:00:06'
        self._helper_validate_backup_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_window_invalid_format_datetime(self):
        test_value = '23-31-00-06'
        self._helper_validate_backup_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_window_blank(self):
        test_value = ''
        self._helper_validate_backup_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_window_too_short(self):
        test_value = '23:51-00:06'
        self._helper_validate_backup_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_window_too_long(self):
        test_value = '00:31-00:06'
        self._helper_validate_backup_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_window_start_after_end_same_day(self):
        test_value = '23:31-23:06'
        self._helper_validate_backup_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_backup_window_start_after_end_different_day(self):
        test_value = '00:06-23:31'
        self._helper_validate_backup_window_expect_exception(
            test_value,
            exceptions.InvalidParameterValue)

    def test_modify_atleast_one_optional_parameter_present_exception(self):
        test_value = []
        self._helper_validate_atleast_one_optional_parameter_present_exception(
            test_value,
            exceptions.ValidationError)

    def test_modify_instance_in_active_state_exception(self):
        test_lifecycle_value = 'creating'
        test_changestate_value = 'pending'
        self._helper_validate_instance_in_active_none_exception(
                test_lifecycle_value,
                test_changestate_value,
                exceptions.InvalidDBInstanceState)

    def test_snapshot_already_present_exception(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        snapshot = self._helper_create_test_snapshot_in_db(instance, snapshot_name)
        self._helper_validate_snapshot_already_present_exception(
            1,
            snapshot_name,
            exceptions.DBSnapshotAlreadyExists)

    def test_modify_instance_already_present_exception(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        self._helper_modify_instance_already_present_exception(
             1,
             instance_name,
             exceptions.DBInstanceAlreadyExists)

    def test_instance_not_present_exception(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        self._helper_validate_instance_present_exception(
             1,
             instance_name,
             exceptions.DBInstanceNotFound)

    def test_validate_snapshot_type_exception(self):
        snapshot_type = 'random'
        self._helper_validate_snapshot_type_exception(
            snapshot_type,
            exceptions.InvalidParameterValue)

    def test_validate_snapshot_present_exception(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        self._helper_validate_snapshot_present_exception(
            1,
            snapshot_name,
            exceptions.DBSnapshotNotFound)

    def test_validate_both_parameter_not_present_exception(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        self._helper_validate_both_parameter_not_present_exception(
            instance_name,
            snapshot_name,
            exceptions.InvalidParameterCombination)
    # TODO complete the next two functions after implimenting delete
    def test_validate_number_of_instances_does_not_exceed_limit(self):
        count = 0
        owner_id = 2
        while count < 5:
            count = count + 1
            instance_name = 'instance-' + str(uuid.uuid4())
            instance = self._helper_create_test_instance_in_db(instance_name, owner_id=2)
            self.addCleanup(db._delete_db_obj, '', instance)

        self._helper_validate_number_of_instances_does_not_exceed_limit(
            owner_id,
            self.database_flavor,
            exceptions.InstanceQuotaExceeded,
            5)

    def test_validate_number_of_snapshots_does_not_exceed_limit(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name, owner_id=2)
        count = 0
        while count < 25:
            count = count + 1
            snapshot_name = 'snapshot-' + str(uuid.uuid4())
            snapshot = self._helper_create_test_snapshot_in_db(instance, snapshot_name)
            self.addCleanup(db._delete_db_obj, '', snapshot)

        self._helper_validate_number_of_snapshots_does_not_exceed_limit(2, exceptions.SnapshotQuotaExceeded, 25)

    def test_validate_storage_size_of_instances_does_not_exceed_limit(self):
        self._helper_validate_storage_size_of_instances_does_not_exceed_limit(2, 100000, exceptions.StorageQuotaExceeded, 5120)

    def test_validate_number_of_c1_4xlarge_instances_does_not_exceed_limit_global_limit(self):
        owner_id = 2
        self._helper_validate_number_of_instances_of_class_does_not_exceed_limit(owner_id, 'c1.4xlarge', exceptions.InstanceClassQuotaExceeded, 0)

    def test_validate_number_of_c1_4xlarge_instances_does_not_exceed_limit_user_limit(self):
        owner_id = 3
        instance_class = 'c1.4xlarge'

        # Create Quota to be = 1
        user_quota = db.user_quota_create_entry_from_user_supplied_values('', 3, 'total_c1.4xlarge_instances', 1)
        self.addCleanup(db._delete_db_obj, '', user_quota)

        # Expect no error message as number of instances = 0
        quota_validators.validate_instances_quotas(owner_id, instance_class)

        # Create one test instance (equal to the quota for this owner_id)
        instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name, owner_id=owner_id, database_flavor=instance_class)
        self.addCleanup(db._delete_db_obj, '', instance)

        # Expect error to be thrown
        self._helper_validate_number_of_instances_of_class_does_not_exceed_limit(owner_id, 'c1.4xlarge', exceptions.InstanceClassQuotaExceeded, 1)

    def test_validate_instance_quotas(self):
        owner_id = 3

        instance_quota = db.user_quota_create_entry_from_user_supplied_values('', 3, 'total_instances', 5)
        self.addCleanup(db._delete_db_obj, '', instance_quota)
        class_4xlarge_quota = db.user_quota_create_entry_from_user_supplied_values('', 3, 'total_c1.4xlarge_instances', 1)
        self.addCleanup(db._delete_db_obj, '', class_4xlarge_quota)

        # Create 3 regular instances
        count = 0
        while count < 3:
            count = count + 1
            instance_name = 'instance-' + str(uuid.uuid4())
            instance = self._helper_create_test_instance_in_db(instance_name, owner_id=owner_id)
            self.addCleanup(db._delete_db_obj, '', instance)

        # Create a 4xlarge instance
        instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name, owner_id=owner_id, database_flavor='c1.4xlarge')
        self.addCleanup(db._delete_db_obj, '', instance)

        # Expect exception from quota validator
        self._helper_validate_number_of_instances_of_class_does_not_exceed_limit(owner_id, 'c1.4xlarge',
                                                                                 exceptions.InstanceClassQuotaExceeded,
                                                                                 1)
        # Create one more regular instance
        instance_name = 'instance-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name, owner_id=owner_id)
        self.addCleanup(db._delete_db_obj, '', instance)

        # Validate that both quota validators would throw an error if invoked
        self._helper_validate_number_of_instances_does_not_exceed_limit(owner_id, 'c1.4xlarge', exceptions.InstanceQuotaExceeded, 5)
        self._helper_validate_number_of_instances_does_not_exceed_limit(owner_id, 'c1.xlarge', exceptions.InstanceQuotaExceeded, 5)

    def test_delete_validate_snapshot_not_present_exception(self):
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        self._helper_delete_validate_snapshot_not_present_exception(1, snapshot_name, exceptions.DBSnapshotNotFound)

    def test_delete_validate_snapshot_not_active_exception(self):
        instance_name = 'instance-' + str(uuid.uuid4())
        snapshot_name = 'snapshot-' + str(uuid.uuid4())
        instance = self._helper_create_test_instance_in_db(instance_name)
        snapshot = self._helper_create_test_snapshot_in_db(instance, snapshot_name)
        self._helper_delete_validate_snapshot_not_active_exception(
            1,
            snapshot_name,
            exceptions.InvalidDBSnapshotState)

###################################################################
############### Test DescribeDBInstances Validation ###############
###################################################################
    def test_describe_db_instances_validation_happy_case(self):
        self.query_parameters['DBInstanceIdentifier'] = " Shank-Instance-12345"
        params = apis.validate_describe_db_instances(self.query_parameters)

        self._helper_validate_common_params(params)
        self.assertEqual(
            params.db_instance_identifier.value,
            self.query_parameters['DBInstanceIdentifier'].strip().lower(),
            'Invalid DbInstanceIdentifier')


###################################################################
############### Test RestoreDBInstanceFromSnapshot Validation ###############
###################################################################
    def test_restore_db_instance_from_snapshot_validation_mandatory_params(self):
        self.query_parameters['DBInstanceIdentifier'] = "Varun-Instance-12345"
        self.query_parameters['DBSnapshotIdentifier'] = "Test-varun-snapshot"
        params = apis.validate_restore_db_instance_from_db_snapshot(self.query_parameters)

        self._helper_validate_common_params(params)
        self.assertEqual(
            params.db_instance_identifier.value,
            self.query_parameters['DBInstanceIdentifier'].strip().lower(),
            'Invalid DbInstanceIdentifier')
        self.assertEqual(
            params.db_snapshot_identifier.value,
            self.query_parameters['DBSnapshotIdentifier'].strip().lower(),
            'Invalid DBSnapshotIdentifier')

    def test_restore_db_instance_from_snapshot_validation_missing_mandatory_params(self):
        self.query_parameters['DBInstanceIdentifier'] = "Varun-Instance-12345"

        with self.assertRaises(exceptions.MissingParameter) as context_manager:
            apis.validate_restore_db_instance_from_db_snapshot(self.query_parameters)

    def test_restore_db_instance_from_snapshot_validation_all_params(self):
        self.query_parameters['DBInstanceIdentifier'] = " Shank-Instance-12345"
        self.query_parameters['DBSnapshotIdentifier'] = "Test-varun-snapshot"
        self.query_parameters['DBInstanceClass'] = 'c1.4xlarge'
        self.query_parameters['AllocatedStorage'] = '100'
        self.query_parameters['MasterUsername'] = '  shank  '
        self.query_parameters['MasterUserPassword'] = 'abcde12345  '
        self.query_parameters['Port'] = self.port
        self.query_parameters['BackupRetentionPeriod'] = 9
        self.query_parameters['PreferredMaintenanceWindow'] = 'Wed:23:19-Thu:00:11'
        self.query_parameters['PreferredBackupWindow'] = '06:13-07:02'

        params = apis.validate_restore_db_instance_from_db_snapshot(self.query_parameters)
        self._helper_validate_common_params(params)

        self.assertEqual(
            params.db_instance_identifier.value,
            self.query_parameters['DBInstanceIdentifier'].strip().lower(),
            'Invalid DbInstanceIdentifier')
        self.assertEqual(
            params.db_snapshot_identifier.value,
            self.query_parameters['DBSnapshotIdentifier'].strip().lower(),
            'Invalid DBSnapshotIdentifier')
        self.assertEqual(
            params.db_instance_class.value,
            self.query_parameters['DBInstanceClass'].strip().lower(),
            'Invalid DbInstanceClass')
        self.assertEqual(
            params.allocated_storage.value,
            int(self.query_parameters['AllocatedStorage']),
            'Invalid AllocatedStorage')
        self.assertEqual(
            params.master_username.value,
            self.query_parameters['MasterUsername'].strip(),
            'Invalid MasterUsername')
        self.assertEqual(
            params.master_user_password.value,
            self.query_parameters['MasterUserPassword'].strip(),
            'Invalid MasterUserPassword')
        self.assertEqual(
            params.port.value,
            int(self.query_parameters['Port']),
            'Invalid Port')
        self.assertEqual(
            params.backup_retention_period.value,
            int(self.query_parameters['BackupRetentionPeriod']),
            'Invalid BackupRetentionPeriod')
        self.assertEqual(
            params.preferred_maintenance_window.value,
            self.query_parameters['PreferredMaintenanceWindow'],
            'Invalid PreferredMaintenanceWindow')
        self.assertEqual(
            params.preferred_backup_window.value,
            self.query_parameters['PreferredBackupWindow'],
            'Invalid PreferredBackupWindow')



###################################################################
################ Test CreateDBInstance Validation #################
###################################################################
    @patch('namo.db.mysqlalchemy.api.supported_engine_versions_get_all_values')
    @patch('namo.db.mysqlalchemy.api.supported_engines_get_all_names')
    @patch('namo.db.mysqlalchemy.api.supported_nova_flavors_get_all_names')
    def test_create_db_instance_validation_happy_case_no_defaults(
        self,
        mock_flavor_call,
        mock_engine_call,
        mock_engine_ver_call):

        mock_flavor_call.return_value = ['m1.small', 'm1.medium']
        mock_engine_call.return_value = ['oracle', 'mysql','postgresql']
        mock_engine_ver_call.return_value = self.supported_engine_versions_list

        self.query_parameters['DBInstanceIdentifier'] = " Shank-Instance-12345"
        self.query_parameters['DBInstanceClass'] = ' m1.small   '
        self.query_parameters['Engine'] = ' '+self.database_engine+'   '
        self.query_parameters['AllocatedStorage'] = '100'
        self.query_parameters['MasterUsername'] = '  shank  '
        self.query_parameters['MasterUserPassword'] = 'abcde12345  '
        self.query_parameters['EngineVersion'] = self.database_engine_major_version
        self.query_parameters['Port'] = self.port
        self.query_parameters['BackupRetentionPeriod'] = 9
        self.query_parameters['PreferredMaintenanceWindow'] = 'Wed:23:19-Thu:00:11'
        self.query_parameters['PreferredBackupWindow'] = '06:13-07:02'

        params = apis.validate_create_db_instance(self.query_parameters)

        self._helper_validate_common_params(params)
        self.assertEqual(
            params.db_instance_identifier.value,
            self.query_parameters['DBInstanceIdentifier'].strip().lower(),
            'Invalid DbInstanceIdentifier')
        self.assertEqual(
            params.db_instance_class.value,
            self.query_parameters['DBInstanceClass'].strip().lower(),
            'Invalid DbInstanceClass')
        self.assertEqual(
            params.engine.value,
            self.query_parameters['Engine'].strip().lower(),
            'Invalid Engine')
        self.assertEqual(
            params.allocated_storage.value,
            int(self.query_parameters['AllocatedStorage']),
            'Invalid AllocatedStorage')
        self.assertEqual(
            params.master_username.value,
            self.query_parameters['MasterUsername'].strip(),
            'Invalid MasterUsername')
        self.assertEqual(
            params.master_user_password.value,
            self.query_parameters['MasterUserPassword'].strip(),
            'Invalid MasterUserPassword')
        self.assertEqual(
            params.engine_version.value,
            self.query_parameters['EngineVersion'].strip(),
            'Invalid EngineVersion')
        self.assertEqual(
            params.port.value,
            int(self.query_parameters['Port']),
            'Invalid Port')
        self.assertEqual(
            params.backup_retention_period.value,
            int(self.query_parameters['BackupRetentionPeriod']),
            'Invalid BackupRetentionPeriod')
        self.assertEqual(
            params.preferred_maintenance_window.value,
            self.query_parameters['PreferredMaintenanceWindow'],
            'Invalid PreferredMaintenanceWindow')
        self.assertEqual(
            params.preferred_backup_window.value,
            self.query_parameters['PreferredBackupWindow'],
            'Invalid PreferredBackupWindow')

        mock_flavor_call.assert_called_once_with()
        mock_engine_call.assert_called_once_with()
        mock_engine_ver_call.assert_called_once_with(self.database_engine)

    @patch('namo.db.mysqlalchemy.api.get_preferred_engine_version_for_engine')
    @patch('namo.db.mysqlalchemy.api.supported_engines_get_all_names')
    @patch('namo.db.mysqlalchemy.api.supported_nova_flavors_get_all_names')
    def test_create_db_instance_validation_happy_case_all_defaults(
        self,
        mock_flavor_call,
        mock_engine_call,
        mock_engine_ver_call):

        mock_flavor_call.return_value = ['m1.small', 'm1.medium']
        mock_engine_call.return_value = ['oracle', 'mysql','postgresql']
        mock_engine_ver_call.return_value = self.database_engine_major_version

        self.query_parameters['DBInstanceIdentifier'] = " Shank-Instance-12345"
        self.query_parameters['DBInstanceClass'] = ' m1.small   '
        self.query_parameters['Engine'] = ' '+self.database_engine+'   '
        self.query_parameters['AllocatedStorage'] = '100'
        self.query_parameters['MasterUsername'] = '  shank  '
        self.query_parameters['MasterUserPassword'] = 'abcde12345  '
        # Missing Engine Version on purpose to verify default value
        # Missing Port on purpose to verify default value
        # Missing BackupRetentionPeriod on purpose to verify default value
        # Missing PreferredMaintenanceWindow on purpose to verify default value
        # Missing PreferredBackupWindow on purpose to verify default value

        params = apis.validate_create_db_instance(self.query_parameters)

        self._helper_validate_common_params(params)
        self.assertEqual(
            params.db_instance_identifier.value,
            self.query_parameters['DBInstanceIdentifier'].strip().lower(),
            'Invalid DbInstanceIdentifier')
        self.assertEqual(
            params.db_instance_class.value,
            self.query_parameters['DBInstanceClass'].strip().lower(),
            'Invalid DbInstanceClass')
        self.assertEqual(
            params.engine.value,
            self.query_parameters['Engine'].strip().lower(),
            'Invalid Engine')
        self.assertEqual(
            params.allocated_storage.value,
            int(self.query_parameters['AllocatedStorage']),
            'Invalid AllocatedStorage')
        self.assertEqual(
            params.master_username.value,
            self.query_parameters['MasterUsername'].strip(),
            'Invalid MasterUsername')
        self.assertEqual(
            params.master_user_password.value,
            self.query_parameters['MasterUserPassword'].strip(),
            'Invalid MasterUserPassword')
        self.assertEqual(
            params.engine_version.value,
            self.database_engine_major_version,
            'Invalid EngineVersion')
        self.assertEqual(
            params.port.value,
            conf.user_parameter_defaults['port_'+self.database_engine],
            'Invalid Port')
        self.assertEqual(
            params.backup_retention_period.value,
            conf.user_parameter_defaults['backup_retention_period'],
            'Invalid DbInstanceIdentifier')

        # Verify PreferredMaintenanceWindow
        mw_value = params.preferred_maintenance_window.value.split('-')
        datetime.strptime(mw_value[0], '%a:%H:%M')
        datetime.strptime(mw_value[1], '%a:%H:%M')

        # Verify PreferredBackupWindow
        bw_value = params.preferred_backup_window.value.split('-')
        datetime.strptime(bw_value[0], '%H:%M')
        datetime.strptime(bw_value[1], '%H:%M')

        mock_flavor_call.assert_called_once_with()
        mock_engine_call.assert_called_once_with()
        mock_engine_ver_call.assert_called_once_with(self.database_engine)

###################################################################
################ Test DeleteDBInstance Validation #################
###################################################################
    def test_delete_db_instance_validation_happy_case_no_final_snapshot(self):
        self.query_parameters['DBInstanceIdentifier'] = " Shank-Instance-12345"
        self.query_parameters['SkipFinalSnapshot'] = 'tRuE'

        params = apis.validate_delete_db_instance(self.query_parameters)

        self._helper_validate_common_params(params)
        self.assertEqual(
            params.db_instance_identifier.value,
            self.query_parameters['DBInstanceIdentifier'].strip().lower(),
            'Invalid DbInstanceIdentifier')
        self.assertEqual(
            params.skip_final_snapshot.value,
            True,
            'Invalid SkipFinalSnapshot')
        self.assertIsNone(
            params.final_db_snapshot_identifier.value,
            'Expected FinalSnapshotIdentifier to be None')

    def test_delete_db_instance_validation_happy_case_with_final_snapshot(self):
        self.query_parameters['DBInstanceIdentifier'] = " Shank-Instance-12345"
        self.query_parameters['SkipFinalSnapshot'] = 'faLSE'
        self.query_parameters['FinalDBSnapshotIdentifier'] = "  Shank-SNapshot-123456   "

        params = apis.validate_delete_db_instance(self.query_parameters)

        self._helper_validate_common_params(params)
        self.assertEqual(
            params.db_instance_identifier.value,
            self.query_parameters['DBInstanceIdentifier'].strip().lower(),
            'Invalid DbInstanceIdentifier')
        self.assertEqual(
            params.skip_final_snapshot.value,
            False,
            'Invalid SkipFinalSnapshot')
        self.assertEqual(
            params.final_db_snapshot_identifier.value,
            self.query_parameters['FinalDBSnapshotIdentifier'].strip().lower(),
            'Invalid FinalDBSnapshotIdentifier')

    def test_delete_db_instance_validation_final_snapshot_no_name_provided(self):
        self.query_parameters['DBInstanceIdentifier'] = " Shank-Instance-12345"
        self.query_parameters['SkipFinalSnapshot'] = 'faLSE'

        with self.assertRaises(exceptions.MissingParameter) as context_manager:
            apis.validate_delete_db_instance(self.query_parameters)
        self.assertIn('FinalDBSnapshotIdentifier', context_manager.exception.msg)

    def test_delete_db_instance_validation_skip_final_snapshot_name_provided(self):
        self.query_parameters['DBInstanceIdentifier'] = " Shank-Instance-12345"
        self.query_parameters['SkipFinalSnapshot'] = 'tRUE'
        self.query_parameters['FinalDBSnapshotIdentifier'] = "  Shank-SNapshot-123456   "

        with self.assertRaises(exceptions.InvalidParameterCombination) as context_manager:
            apis.validate_delete_db_instance(self.query_parameters)

###################################################################
############### Test DescribeDBTypes Validation ###################
###################################################################
    def test_describe_db_types_validation_happy_case(self):
        self.query_parameters['DBTypeId'] = self.database_engine
        params = apis.validate_describe_db_types(self.query_parameters)

        self._helper_validate_common_params(params)
        self.assertEqual(
            params.db_type_identifier.value,
            self.query_parameters['DBTypeId'].strip().lower(),
            'Invalid DbTypeId')

###################################################################
######################### Helper methods ##########################
###################################################################

    def _helper_test_missing_common_param(self, param):
        del self.query_parameters[param]
        self._helper_validate_common_param_expect_exception(param, exceptions.MissingParameter)

    def _helper_test_unsupported_common_param_value(self, param):
        # Create a UUID as the param value so that it doesn't match one of its supported values
        param_value = str(uuid.uuid4())
        self.query_parameters[param] = param_value
        self._helper_validate_common_param_expect_exception(param, exceptions.InvalidParameterValue)

    def _helper_test_blank_common_param(self, param):
        self.query_parameters[param] = ''
        self._helper_validate_common_param_expect_exception(param, exceptions.InvalidParameterValue)

    def _helper_validate_common_param_expect_exception(self, param, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_and_set_common_parameters(self.base_params, self.query_parameters)

        self.assertIn(param, context_manager.exception.msg)

    def _helper_validate_db_instance_identifier_expect_exception(self, identifier, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_db_instance_identifier(self.create_db_instance_params.db_instance_identifier, identifier)
        self.assertIn('DBInstanceIdentifier', context_manager.exception.msg)

    def _helper_validate_snapshot_identifier_expect_exception(self, identifier, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_snapshot_identifier(self.delete_db_instance_params.final_db_snapshot_identifier, identifier)
        self.assertIn('SnapshotIdentifier', context_manager.exception.msg)

    def _helper_validate_allocated_storage_expect_exception(self, storage, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_allocated_storage(
                self.create_db_instance_params.allocated_storage,
                storage)
        self.assertIn('AllocatedStorage', context_manager.exception.msg)

    def _helper_validate_master_username_expect_exception(self, username, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_master_username(
                self.create_db_instance_params.master_username,
                username,
                self.database_engine)
        self.assertIn('MasterUsername', context_manager.exception.msg)

    def _helper_validate_master_user_password_expect_exception(self, password, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_master_user_password(
                self.create_db_instance_params.master_user_password,
                password,
                self.database_engine)
        self.assertIn('MasterUserPassword', context_manager.exception.msg)

    def _helper_validate_port_expect_exception(self, port, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_port(
                self.create_db_instance_params.port,
                port)
        self.assertIn('Port', context_manager.exception.msg)

    def _helper_validate_backup_retention_expect_exception(self, br_period, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_backup_retention_period(
                self.create_db_instance_params.backup_retention_period,
                br_period)
        self.assertIn('BackupRetention', context_manager.exception.msg)

    def _helper_validate_maintenance_window_expect_exception(self, mw, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_preferred_maintenance_window(
                self.create_db_instance_params.preferred_maintenance_window,
                mw)
        self.assertIn('PreferredMaintenanceWindow', context_manager.exception.msg)

    def _helper_validate_backup_window_expect_exception(self, bw, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_preferred_backup_window(
                self.create_db_instance_params.preferred_backup_window,
                bw)
        self.assertIn('PreferredBackupWindow', context_manager.exception.msg)

    def _helper_validate_common_params(self, params):
        self.assertEqual(
            params.access_key_id.value,
            self.query_parameters[self.base_params.access_key_id.name],
            'Invalid access key id')
        self.assertEqual(
            params.signature.value,
            self.query_parameters[self.base_params.signature.name],
            'Invalid signature')
        self.assertEqual(
            params.signature_method.value,
            self.query_parameters[self.base_params.signature_method.name],
            'Invalid signature method')
        self.assertEqual(
            params.signature_version.value,
            self.query_parameters[self.base_params.signature_version.name],
            'Invalid signature version')
        self.assertEqual(
            params.version.value,
            self.query_parameters[self.base_params.version.name],
            'Invalid version')
        self.assertEqual(
            params.timestamp.value,
            self.query_parameters[self.base_params.timestamp.name],
            'Invalid timestamp')

    def _helper_validate_atleast_one_optional_parameter_present_exception(self, query, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_modify_atleast_one_optional_parameter_present(
                query)
        self.assertIn('The input fails to satisfy the constraints specified by a JCS service.', context_manager.exception.msg)

    def _helper_validate_instance_in_active_none_exception(self, lifecycle, changestate, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_instance_lc_cs(
                lifecycle,
                changestate,
                'ACTIVE',
                'NONE')
        self.assertIn('The specified DB instance is not in the available state.', context_manager.exception.msg)

    def _helper_validate_snapshot_already_present_exception(self, owner_id, snapshot_name, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_snapshot_not_present(owner_id,snapshot_name)
        message = snapshot_name + ' is already used by an existing snapshot.'
        self.assertIn(message, context_manager.exception.msg)

    def _helper_modify_instance_already_present_exception(self, owner_id, instance_name, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_db_instance_not_present(owner_id,instance_name)
        message = "User already has a DB instance with the given identifier " + instance_name
        self.assertIn(message, context_manager.exception.msg)

    def _helper_create_test_instance_in_db(self, instance_name, owner_id=1, database_flavor=None):
        if not database_flavor:
            database_flavor = self.database_flavor

        instance = db.instance_create_entry_from_user_supplied_values(
            context='',
            owner_id=owner_id,
            database_engine=self.database_engine,
            database_engine_version=self.database_engine_major_version,
            database_name=instance_name,
            database_flavor=database_flavor,
            database_storage_size=100,
            master_username='randomUsername',
            master_user_password='randomPassword',
            preferred_backup_window_start='23:45',
            preferred_backup_window_end='00:15',
            preferred_maintenance_window_start_day='SUN',
            preferred_maintenance_window_start_time='22:45',
            preferred_maintenance_window_end_day='SUN',
            preferred_maintenance_window_end_time="23:40",
            port=self.port,
            log_retention_period=1,
            backup_retention_period_days=1)

        return instance

    def _helper_create_test_snapshot_in_db(self, instance, snapshot_name):
        snapshot = db.backup_create_from_instance(
            context='',
            backup_type='MANUAL',
            instance=instance,
            backup_name=snapshot_name,
            session=None)
        return snapshot

    def _helper_validate_instance_present_exception(self, owner_id, instance_name, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_instance_present(owner_id,instance_name)
        message = instance_name + ' does not refer to an existing DB instance.'
        self.assertIn(message, context_manager.exception.msg)

    def _helper_validate_snapshot_type_exception(self, snapshot_type_name, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_snapshot_type(self.describe_db_snapshots_params.snapshot_type,snapshot_type_name)
        message = "An invalid or out-of-range value was supplied for the input parameter: SnapshotType"
        self.assertIn(message, context_manager.exception.msg)

    def _helper_validate_snapshot_present_exception(self, owner_id, snapshot_name, exception_type):
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_snapshot_present(owner_id, snapshot_name)
        message = snapshot_name + " does not refer to an existing DB snapshot."
        self.assertIn(message, context_manager.exception.msg)

    def _helper_validate_both_parameter_not_present_exception(self, instance_name, snapshot_name, exception_type):
        query_parameters = {}
        query_parameters['DBInstanceIdentifier'] = instance_name
        query_parameters['DBSnapshotIdentifier'] = snapshot_name
        with self.assertRaises(exception_type) as context_manager:
            helpers.validate_both_parameter_not_present(self.describe_db_snapshots_params.db_instance_identifier, self.describe_db_snapshots_params.db_snapshot_identifier, query_parameters)
        message = "Parameters that must not be used together were used together: DBInstanceIdentifier and DBSnapshotIdentifier"
        self.assertIn(message, context_manager.exception.msg)

    def _helper_validate_number_of_instances_does_not_exceed_limit(self, owner_id, instance_class, exception_type, quota):
        with self.assertRaises(exception_type) as context_manager:
            quota_validators.validate_instances_quotas(owner_id, instance_class)
        message = "Request would result in user exceeding the allowed quota {0} of total DB instances. " + \
        "Please contact JCS customer service to increase your total DB instances quota."
        message = message.format(quota)
        self.assertIn(message, context_manager.exception.msg)

    def _helper_validate_number_of_instances_of_class_does_not_exceed_limit(self, owner_id, instance_class, exception_type, quota):
        with self.assertRaises(exception_type) as context_manager:
            quota_validators.validate_instances_quotas(owner_id, instance_class)
        message = "Request would result in user exceeding the allowed quota {0} of DB instances for the instance " + \
        "class - {1}. Please contact JCS customer service to increase your total DB instances quota for this " + \
        "instance class."
        message = message.format(quota, instance_class)
        self.assertIn(message, context_manager.exception.msg)

    def _helper_validate_number_of_snapshots_does_not_exceed_limit(self, owner_id, exception_type, quota):
        with self.assertRaises(exception_type) as context_manager:
            quota_validators.validate_number_of_snapshots_does_not_exceed_limit(owner_id)
        message = "Request would result in user exceeding the allowed quota {0} of total DB snapshots across all " + \
        "DB instances. Please delete your old snapshots or contact JCS customer service to increase your total DB " + \
        "snapshots quota."
        message = message.format(quota)
        self.assertIn(message, context_manager.exception.msg)

    def _helper_validate_storage_size_of_instances_does_not_exceed_limit(self, owner_id, current_storage_requirement, exception_type, quota):
        with self.assertRaises(exception_type) as context_manager:
            quota_validators.validate_storage_size_of_instances_does_not_exceed_limit(owner_id, current_storage_requirement)
        message = "Request would result in user exceeding the allowed quota {0} GB of storage available across " + \
        "all DB instances. Please contact JCS customer service to increase your total data storage quota."
        message = message.format(quota)
        self.assertIn(message, context_manager.exception.msg)

    def _helper_delete_validate_snapshot_not_present_exception(self, owner_id, snapshot_name, exception_type):
        self.delete_db_snapshot_params.db_snapshot_identifier.value = snapshot_name
        delete_db_snapshot = DeleteDBSnapshotHandler()
        with self.assertRaises(exception_type) as context_manager:
            delete_db_snapshot.handle_request(self.delete_db_snapshot_params, owner_id)
        message = snapshot_name + " does not refer to an existing DB snapshot."
        self.assertIn(message, context_manager.exception.msg)

    def _helper_delete_validate_snapshot_not_active_exception(self, owner_id, snapshot_name, exception_type):
        self.delete_db_snapshot_params.db_snapshot_identifier.value = snapshot_name
        delete_db_snapshot = DeleteDBSnapshotHandler()
        with self.assertRaises(exception_type) as context_manager:
            delete_db_snapshot.handle_request(self.delete_db_snapshot_params, owner_id)
        message = "The specified DB snapshot is not in the available state."
        self.assertIn(message, context_manager.exception.msg)

if __name__ == '__main__':
    unittest.main()

#################################################################################################
##################### FOR RUNNING TEST CASES FOR ANY DATABASE ENGINE#############################
#################################################################################################

class MysqlTestValidators(BaseTestValidators,unittest.TestCase):
    database_engine='mysql'
    database_engine_major_version='5.6'
    database_engine_minor_version='2'
    port=3307
    supported_engine_versions_list=['5.5','5.6']
    unsupported_database_engine_major_version='5.1'

class PostgresqlTestValidators(BaseTestValidators,unittest.TestCase):
    database_engine='postgresql'
    database_engine_major_version = '9.5'
    database_engine_minor_version = '3'
    port = 5432
    supported_engine_versions_list=['9.3','9.5']
    unsupported_database_engine_major_version = '9.1'
