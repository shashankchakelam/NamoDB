import unittest
import uuid

from namo.common import exception
from namo.db.mysqlalchemy import api as db
from apiwebservice import exceptions
from apiwebservice.apihandlers.restore_db_instance_from_db_snapshot_handler import RestoreDBInstanceFromDBSnapshotHandler
from apiwebservice.constants import parameters
from apiwebservice.tests.unit_tests import test_handlers_base

class TestRestoreDBInstanceFromSnapshotHandler(test_handlers_base.TestHandlersBase):
    """
    Unit Tests for all RestoreDBInstanceFromSnapshot handler method
    """

    def setUp(self):
        self.instance_name = 'instance-' + str(uuid.uuid4())
        self.snapshot_name = 'snapshot-' + str(uuid.uuid4())
        self.restore_db_instance_from_snapshot_params = parameters.RestoreDBInstanceFromDBSnapshotParams()
        self.restore_db_instance_from_snapshot_params.db_instance_identifier.value = self.instance_name
        self.restore_db_instance_from_snapshot_params.db_snapshot_identifier.value = self.snapshot_name

    ###################################################################
    ####### Test RestoreDBInstanceFromSnapshot Handler ################
    ###################################################################
    def test_restore_db_instance_from_snapshot_handler_instance_happy_case(self):
        # Create test instance
        old_instance = self._helper_create_test_instance_in_available_state(
            "old-" + self.instance_name
        )
        # Create db snapshot
        snapshot = self._helper_create_test_snapshot_in_available_state(db_instance=old_instance,
                                                                        snapshot_name=self.snapshot_name)

        restore_db_from_db_snapshot_handler = RestoreDBInstanceFromDBSnapshotHandler()
        # Call the RestoreDBInstanceFromSnapshot API handler
        restore_db_from_db_snapshot_handler.handle_request(
            self.restore_db_instance_from_snapshot_params,
            self.owner_id
        )

        # Verify the instance is in Creating/PENDING state
        instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        old_instance = db.instance_get(None, self.owner_id, "old-" + self.instance_name, None)
        snapshot = db.backup_get_by_owner_id_and_name(None, self.owner_id, self.snapshot_name)
        self.addCleanup(db._delete_db_obj, '', instance)
        self.addCleanup(db._delete_db_obj, '', old_instance)

        self.addCleanup(db._delete_db_obj, '', snapshot)

        self.assertEqual(instance.lifecycle, 'RESTORING', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'PENDING', "Unexpected Changestate: " + instance.changestate)
        self.assertEqual(snapshot.lifecycle, 'RESTORING', "Unexpected Lifecycle: " + snapshot.lifecycle)
        self.assertEqual(snapshot.changestate, 'PENDING', "Unexpected Changestate: " + snapshot.changestate)

    def test_restore_db_instance_from_snapshot_handler_snapshot_not_active_none(self):
        # Create test instance
        old_instance = self._helper_create_test_instance_in_available_state(
            "old" + self.instance_name
        )
        # Create db snapshot
        snapshot = self._helper_create_test_snapshot_in_db(db_instance=old_instance,
                                                           snapshot_name=self.snapshot_name)
        old_instance = db.instance_get(None, self.owner_id, "old" + self.instance_name, None)
        self.addCleanup(db._delete_db_obj, '', old_instance)
        self.addCleanup(db._delete_db_obj, '', snapshot)

        restore_db_from_db_snapshot_handler = RestoreDBInstanceFromDBSnapshotHandler()
        with self.assertRaises(exceptions.InvalidDBSnapshotState):
            restore_db_from_db_snapshot_handler.handle_request(
                self.restore_db_instance_from_snapshot_params,
                self.owner_id
            )

    def test_restore_db_instance_from_snapshot_handler_instance_already_exists(self):
        # Create test instance
        old_instance = self._helper_create_test_instance_in_available_state(
            self.instance_name
        )
        # Create db snapshot
        snapshot = self._helper_create_test_snapshot_in_available_state(db_instance=old_instance,
                                                                        snapshot_name=self.snapshot_name)
        old_instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        self.addCleanup(db._delete_db_obj, '', old_instance)
        self.addCleanup(db._delete_db_obj, '', snapshot)

        restore_db_from_db_snapshot_handler = RestoreDBInstanceFromDBSnapshotHandler()
        with self.assertRaises(exceptions.DBInstanceAlreadyExists):
            restore_db_from_db_snapshot_handler.handle_request(
                self.restore_db_instance_from_snapshot_params,
                self.owner_id
            )

    def test_restore_db_instance_from_snapshot_handler_snapshot_not_found(self):
        # Don't create test instance

        restore_db_from_db_snapshot_handler = RestoreDBInstanceFromDBSnapshotHandler()
        # Call the DeleteDBInstance API handler
        with self.assertRaises(exceptions.DBSnapshotNotFound):
           restore_db_from_db_snapshot_handler.handle_request(
                self.restore_db_instance_from_snapshot_params,
                self.owner_id
            )

    ###################################################################
    ######################### Helper methods ##########################
    ###################################################################


if __name__ == '__main__':
    unittest.main()
