#Instance should be in active none state
#Instance should not allow already running paramters
#test rollback

import unittest
import uuid

from mock import patch
from sqlalchemy.orm import exc as orm_exc

from apiwebservice import exceptions
from apiwebservice.apihandlers.create_db_instance_handler import CreateDBInstanceHandler
from apiwebservice.apihandlers.modify_db_instance_handler import ModifyDBInstanceHandler
from apiwebservice.constants import parameters
from apiwebservice.validators import helpers
from apiwebservice.tests.unit_tests import test_handlers_base
from namo.db.mysqlalchemy import api as db


class TestModifyBInstance(test_handlers_base.TestHandlersBase):
    """
    Unit Tests for all CreateDBInstance handler method
    """

    def setUp(self):
        self.instance_name = 'instance-' + str(uuid.uuid4())
        self.create_db_instance_params = parameters.CreateDBInstanceParams()
        self.create_db_instance_params.db_instance_identifier.value = self.instance_name
        self.create_db_instance_params.engine.value = self.database_engine
        self.create_db_instance_params.engine_version.value = self.database_major_engine_version
        self.create_db_instance_params.db_instance_class.value = self.database_flavor
        self.create_db_instance_params.allocated_storage.value = self.database_storage_size
        self.create_db_instance_params.master_username.value = self.master_username
        self.create_db_instance_params.master_user_password.value = self.master_user_password
        self.create_db_instance_params.preferred_backup_window.value = \
            self.preferred_backup_window_start + "-" + self.preferred_backup_window_end
        self.create_db_instance_params.preferred_maintenance_window.value = \
            self.preferred_maintenance_window_start_day + ":" + self.preferred_maintenance_window_start_time + "-" + \
            self.preferred_maintenance_window_end_day + ":" + self.preferred_maintenance_window_end_time
        self.create_db_instance_params.backup_retention_period.value = self.backup_retention_period_days
        self.create_db_instance_params.port.value = self.port

        self.modify_db_instance_params = parameters.ModifyDbInstanceParams()
        self.modify_db_instance_params.db_instance_identifier.value = self.instance_name
        self.modify_db_instance_params.db_instance_class.value = self.database_flavor_down_scale

    def test_modify_db_instance_handler_instance_not_present(self):

        modify_db_instance_params = parameters.ModifyDbInstanceParams()
        modify_db_instance_params.db_instance_identifier.value = 'instance-' + str(uuid.uuid4())
        modify_db_instance_params.db_instance_class.value = self.database_flavor_down_scale


        with self.assertRaises(exceptions.DBInstanceNotFound):
            modify_db_instance_handler = ModifyDBInstanceHandler()
            modify_db_instance_handler.handle_request(
                modify_db_instance_params,
                self.owner_id
            )





    def test_modify_db_instance_handler_instance_in_active_none_happy_case(self):
        # Call the CreateDBInstance API handler
        create_db_instance_handler = CreateDBInstanceHandler()
        create_db_instance_handler.handle_request(
            self.create_db_instance_params,
            self.owner_id
        )

        # Verify the instance is in Creating/PENDING state

        instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        self.addCleanup(db._delete_db_obj, '', instance)
        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['lifecycle'] = 'ACTIVE'
        values['changestate'] = 'NONE'
        db.instance_update(None,instance.id,values,None)

        modify_db_instance_handler = ModifyDBInstanceHandler()
        modify_db_instance_handler.handle_request(
            self.modify_db_instance_params,
            self.owner_id
        )

        instance_ids = []
        instance_ids.append(instance.id)
        dp_tasks_id_handle = db.get_pending_dp_task_ids_query(None,instance_ids).all()
        dp_tasks = db.dp_task_get_by_id(None,dp_tasks_id_handle[0][0],None)
        self.addCleanup(db._delete_db_obj, '', dp_tasks)

        instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        self.assertEqual(instance.lifecycle, 'MODIFYING', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'PENDING', "Unexpected Changestate: " + instance.changestate)

    def test_modify_db_instance_handler_instance_not_in_active_none_case(self):
        # Call the CreateDBInstance API handler
        create_db_instance_handler = CreateDBInstanceHandler()
        create_db_instance_handler.handle_request(
            self.create_db_instance_params,
            self.owner_id
        )

        # Verify the instance is in Creating/PENDING state
        instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        self.addCleanup(db._delete_db_obj, '', instance)

        self.assertEqual(instance.lifecycle, 'CREATING', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'PENDING', "Unexpected Changestate: " + instance.changestate)

        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['changestate'] = 'APPLYING'
        db.instance_update(None,instance.id,values,None)

        # Call the ModifyDBInstance API handler to check exception
        with self.assertRaises(exceptions.InvalidDBInstanceState):
            modify_db_instance_handler = ModifyDBInstanceHandler()
            modify_db_instance_handler.handle_request(
                self.modify_db_instance_params,
                self.owner_id
            )

        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['lifecycle'] = 'DELETING'
        values['changestate'] = 'PENDING'
        db.instance_update(None,instance.id,values,None)

        # Call the ModifyDBInstance API handler to check exception
        with self.assertRaises(exceptions.InvalidDBInstanceState):
            modify_db_instance_handler = ModifyDBInstanceHandler()
            modify_db_instance_handler.handle_request(
                self.modify_db_instance_params,
                self.owner_id
            )


        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['lifecycle'] = 'DELETING'
        values['changestate'] = 'APPLYING'
        db.instance_update(None,instance.id,values,None)

        # Call the ModifyDBInstance API handler to check exception
        with self.assertRaises(exceptions.InvalidDBInstanceState):
            modify_db_instance_handler = ModifyDBInstanceHandler()
            modify_db_instance_handler.handle_request(
                self.modify_db_instance_params,
                self.owner_id
            )

        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['lifecycle'] = 'RESTORING'
        values['changestate'] = 'PENDING'
        db.instance_update(None,instance.id,values,None)

        # Call the ModifyDBInstance API handler to check exception
        with self.assertRaises(exceptions.InvalidDBInstanceState):
            modify_db_instance_handler = ModifyDBInstanceHandler()
            modify_db_instance_handler.handle_request(
                self.modify_db_instance_params,
                self.owner_id
            )

        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['lifecycle'] = 'RESTORING'
        values['changestate'] = 'APPLYING'
        db.instance_update(None,instance.id,values,None)

        # Call the ModifyDBInstance API handler to check exception
        with self.assertRaises(exceptions.InvalidDBInstanceState):
            modify_db_instance_handler = ModifyDBInstanceHandler()
            modify_db_instance_handler.handle_request(
                self.modify_db_instance_params,
                self.owner_id
            )

        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['lifecycle'] = 'BACKUP'
        values['changestate'] = 'PENDING'
        db.instance_update(None,instance.id,values,None)

        # Call the ModifyDBInstance API handler to check exception
        with self.assertRaises(exceptions.InvalidDBInstanceState):
            modify_db_instance_handler = ModifyDBInstanceHandler()
            modify_db_instance_handler.handle_request(
                self.modify_db_instance_params,
                self.owner_id
            )

        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['lifecycle'] = 'BACKUP'
        values['changestate'] = 'APPLYING'
        db.instance_update(None,instance.id,values,None)

        # Call the ModifyDBInstance API handler to check exception
        with self.assertRaises(exceptions.InvalidDBInstanceState):
            modify_db_instance_handler = ModifyDBInstanceHandler()
            modify_db_instance_handler.handle_request(
                self.modify_db_instance_params,
                self.owner_id
            )


        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['lifecycle'] = 'MODIFYING'
        values['changestate'] = 'PENDING'
        db.instance_update(None,instance.id,values,None)

        # Call the ModifyDBInstance API handler to check exception
        with self.assertRaises(exceptions.InvalidDBInstanceState):
            modify_db_instance_handler = ModifyDBInstanceHandler()
            modify_db_instance_handler.handle_request(
                self.modify_db_instance_params,
                self.owner_id
            )

        values = {}
        values['owner_id'] = instance.owner_id
        values['name'] = instance.name
        values['lifecycle'] = 'MODIFYING'
        values['changestate'] = 'APPLYING'
        db.instance_update(None,instance.id,values,None)

        # Call the ModifyDBInstance API handler to check exception
        with self.assertRaises(exceptions.InvalidDBInstanceState):
            modify_db_instance_handler = ModifyDBInstanceHandler()
            modify_db_instance_handler.handle_request(
                self.modify_db_instance_params,
                self.owner_id
            )
    


if __name__ == '__main__':
    unittest.main()
