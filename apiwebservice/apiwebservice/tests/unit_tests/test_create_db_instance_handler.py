import unittest
import uuid

from mock import patch
from sqlalchemy.orm import exc as orm_exc

from apiwebservice import exceptions
from apiwebservice.apihandlers.create_db_instance_handler import CreateDBInstanceHandler
from apiwebservice.constants import parameters
from apiwebservice.validators import helpers
from apiwebservice.tests.unit_tests import test_handlers_base
from namo.db.mysqlalchemy import api as db


class TestCreateBInstance(test_handlers_base.TestHandlersBase):
    """
    Unit Tests for all CreateDBInstance handler method
    """

    def setUp(self):
        self.instance_name = 'instance-' + str(uuid.uuid4())
        self.create_db_instance_params = parameters.CreateDBInstanceParams()
        self.create_db_instance_params.db_instance_identifier.value = self.instance_name
        self.create_db_instance_params.engine.value = self.database_engine
        self.create_db_instance_params.engine_version.value = self.database_major_engine_version
        self.create_db_instance_params.db_instance_class.value = self.database_flavor
        self.create_db_instance_params.allocated_storage.value = self.database_storage_size
        self.create_db_instance_params.master_username.value = self.master_username
        self.create_db_instance_params.master_user_password.value = self.master_user_password
        self.create_db_instance_params.preferred_backup_window.value = \
            self.preferred_backup_window_start + "-" + self.preferred_backup_window_end
        self.create_db_instance_params.preferred_maintenance_window.value = \
            self.preferred_maintenance_window_start_day + ":" + self.preferred_maintenance_window_start_time + "-" + \
            self.preferred_maintenance_window_end_day + ":" + self.preferred_maintenance_window_end_time
        self.create_db_instance_params.backup_retention_period.value = self.backup_retention_period_days
        self.create_db_instance_params.port.value = self.port

    def test_create_db_instance_handler_instance_happy_case(self):
        # Call the CreateDBInstance API handler
        create_db_instance_handler = CreateDBInstanceHandler()
        create_db_instance_handler.handle_request(
            self.create_db_instance_params,
            self.owner_id
        )

        # Verify the instance is in Creating/PENDING state

        instance = db.instance_get(None, self.owner_id, self.instance_name, None)

        self.addCleanup(db._delete_db_obj, '', instance)

        self.assertEqual(instance.lifecycle, 'CREATING', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'PENDING', "Unexpected Changestate: " + instance.changestate)

    def test_create_db_instance_handler_instance_duplicate_entry(self):
        # Call the CreateDBInstance API handler
        create_db_instance_handler = CreateDBInstanceHandler()
        create_db_instance_handler.handle_request(
            self.create_db_instance_params,
            self.owner_id
        )

        # Verify the instance is in Creating/PENDING state
        instance = db.instance_get(None, self.owner_id, self.instance_name, None)

        self.addCleanup(db._delete_db_obj, '', instance)

        self.assertEqual(instance.lifecycle, 'CREATING', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'PENDING', "Unexpected Changestate: " + instance.changestate)

        # Call the CreateDBInstance API handler again to create a duplicate
        with self.assertRaises(exceptions.DBInstanceAlreadyExists):
            create_db_instance_handler.handle_request(
                self.create_db_instance_params,
                self.owner_id
            )

    @patch('namo.db.mysqlalchemy.api.get_user_visible_instance_properties')
    def test_rollback_create_instance_handler_get_instance_exception(self, mock_db_call):
        # throwing exceptions when calling get_user_visible_instance_properties function
        mock_db_call.side_effect = orm_exc.NoResultFound()
        # Call the CreateDBInstance API handler
        create_db_instance_handler = CreateDBInstanceHandler()

        # Verify the instance does not exist.
        # Call the CreateDBInstance API handler and expects NOResultFound exception.
        with self.assertRaises(orm_exc.NoResultFound):
            create_db_instance_handler.handle_request(
                self.create_db_instance_params,
                self.owner_id
            )

        with self.assertRaises(exceptions.DBInstanceNotFound):
            # Check that instance should not exist after rollback
            helpers.validate_instance_present(self.owner_id,
                                              self.create_db_instance_params.db_instance_identifier.value)

if __name__ == '__main__':
    unittest.main()
