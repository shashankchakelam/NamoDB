import unittest
import uuid

from namo.db.mysqlalchemy import api as db
from apiwebservice import exceptions
from apiwebservice.apihandlers.describe_db_instances_handler import DescribeDBInstancesHandler
from apiwebservice.constants import parameters
from apiwebservice.tests.unit_tests import test_handlers_base

class TestDescribeDBInstancesHandler(test_handlers_base.TestHandlersBase):
    """
    Unit Tests for describe db instances.handlers methods
    """

    def setUp(self):
        self.instance_name = 'instance-' + str(uuid.uuid4())
        self.describe_db_instances_params = parameters.DescribeDBInstancesParams()

    ###################################################################
    ################## Test DescribeDBInstances Handler ##################
    ###################################################################
    def test_describe_db_instances_handler_instance_single_instance_happy_case(self):
        instance = self._helper_create_test_instance_in_available_state(
            self.instance_name
        )
        # self.addCleanup(db._delete_db_obj, '', instance)

        self.describe_db_instances_params.db_instance_identifier.value = instance.name
        describe_db_instance_handler = DescribeDBInstancesHandler()
        response = describe_db_instance_handler.handle_request(
            self.describe_db_instances_params,
            self.owner_id
        )

        self.assertIn(instance.name, response.body)

    def test_describe_db_instances_handler_instance_not_found(self):
        self.describe_db_instances_params.db_instance_identifier.value = str(uuid.uuid4())
        describe_db_instance_handler = DescribeDBInstancesHandler()
        with self.assertRaises(exceptions.DBInstanceNotFound):
            describe_db_instance_handler.handle_request(
                self.describe_db_instances_params,
                self.owner_id
            )

if __name__ == '__main__':
    unittest.main()
