import unittest

from apiwebservice import utils

class TestUtils(unittest.TestCase):
    def test_get_maintenance_window_components_happy_case(self):
        # Prepare maintenance window string
        mw_input_start_day = 'Mon'
        mw_input_start_time = '23:39'
        mw_input_end_day = 'Tue'
        mw_input_end_time = '00:17'
        mw_string = "{0}:{1}-{2}:{3}".format(
            mw_input_start_day,
            mw_input_start_time,
            mw_input_end_day,
            mw_input_end_time
        )

        # Get and validate output
        output = utils.get_maintenance_window_components(mw_string)
        self.assertEqual(output[0], mw_input_start_day.upper(), "Incorrect start day")
        self.assertEqual(output[1], mw_input_start_time, "Incorrect start time")
        self.assertEqual(output[2], mw_input_end_day.upper(), "Incorrect end day")
        self.assertEqual(output[3], mw_input_end_time, "Incorrect end time")