import os
import unittest

from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models
from pecan import set_config

class TestHandlersBase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        set_config(os.path.join(os.path.dirname(__file__), '../config.py'))
        cls.owner_id = 10

        # Not setting database_name so that it's randomly generated for uniqueness
        cls.database_engine = 'postgresql'
        cls.database_major_engine_version = '9.5'
        cls.database_minor_engine_version = '3'
        cls.database_flavor = 'c1.xlarge'
        cls.database_flavor_up_scale = 'c1.2xlarge'
        cls.database_flavor_down_scale = 'c1.large'
        cls.database_storage_size = 100
        cls.master_username = 'shank'
        cls.master_user_password = 'abcde12345'
        cls.preferred_backup_window_start = '23:45'
        cls.preferred_backup_window_end = "00:15"
        cls.preferred_maintenance_window_start_day = 'SUN'
        cls.preferred_maintenance_window_start_time = "22:45"
        cls.preferred_maintenance_window_end_day = 'SUN'
        cls.preferred_maintenance_window_end_time = "23:40"
        cls.backup_retention_period_days = 3
        cls.log_retention_period_days = 2
        cls.port = 5432

    ###################################################################
    ######################### Helper methods ##########################
    ###################################################################

    def _helper_create_test_instance_in_available_state(self, name):
        session = db.get_session()
        with session.begin(subtransactions=True):
            instance = self._helper_create_test_instance_in_db(name, session=session)

            # Make instance lccs ACTIVE/NONE
            instance = db.lccs_conditional_update(
                None,
                models.Instance,
                instance.id,
                'ACTIVE',
                'NONE',
                'CREATING',
                'PENDING',
                session)

            return instance

    def _helper_create_test_instance_in_db(self, name, session=None):
        instance = db.instance_create_entry_from_user_supplied_values(
            context=None,
            owner_id=self.owner_id,
            database_engine=self.database_engine,
            database_engine_version=self.database_major_engine_version,
            database_name=name,
            database_flavor=self.database_flavor,
            database_storage_size=self.database_storage_size,
            master_username=self.master_username,
            master_user_password=self.master_user_password,
            preferred_backup_window_start=self.preferred_backup_window_start,
            preferred_backup_window_end=self.preferred_backup_window_end,
            preferred_maintenance_window_start_day=self.preferred_maintenance_window_start_day,
            preferred_maintenance_window_start_time=self.preferred_maintenance_window_start_time,
            preferred_maintenance_window_end_day=self.preferred_maintenance_window_end_day,
            preferred_maintenance_window_end_time=self.preferred_maintenance_window_end_time,
            backup_retention_period_days=self.backup_retention_period_days,
            log_retention_period=self.log_retention_period_days,
            port=self.port,
            session=session
        )

        return instance



    def _helper_create_test_snapshot_in_available_state(self, db_instance, snapshot_name):
        snapshot = self._helper_create_test_snapshot_in_db(db_instance=db_instance, snapshot_name=snapshot_name)

        # Make snapshot lccs ACTIVE/NONE
        db.lccs_conditional_update(
            None,
            models.Backup,
            snapshot['id'],
            'ACTIVE',
            'NONE',
            'CREATING',
            'PENDING',
            None)

        return snapshot

    def _helper_create_test_snapshot_in_db(self, db_instance, snapshot_name):
        snapshot = db.backup_create_from_instance('', 'MANUAL', db_instance, snapshot_name)
        return snapshot

if __name__ == '__main__':
    unittest.main()
