import unittest
import uuid

from mock import patch
from sqlalchemy.orm import exc as orm_exc

from apiwebservice import exceptions
from apiwebservice.apihandlers.create_db_snapshot_handler import CreateDBSnapshotHandler
from apiwebservice.constants import parameters
from apiwebservice.tests.unit_tests import test_handlers_base
from apiwebservice.validators import helpers
from namo.common import exception
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models


class TestCreateDBSnapshotHandler(test_handlers_base.TestHandlersBase):
    """
    Unit Tests for create db snapshot handlers methods
    """

    def setUp(self):
        self.instance_name = 'instance-' + str(uuid.uuid4())
        self.snapshot_name = 'snapshot-' + str(uuid.uuid4())
        self.create_db_snapshot_params = parameters.CreateDBSnapshotParams()
        self.create_db_snapshot_params.db_instance_identifier.value = \
            self.instance_name
        self.create_db_snapshot_params.db_snapshot_identifier.value = \
            self.snapshot_name
        # Create test instance
        self.instance = self._helper_create_test_instance_in_available_state(
            self.instance_name
        )

    def test_create_db_snapshot_handler_instance_happy_case(self):
        # Call the CreateDBSnapshot API handler
        create_db_snapshot_handler = CreateDBSnapshotHandler()
        create_db_snapshot_handler.handle_request(
            self.create_db_snapshot_params,
            self.owner_id
        )
        instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        # Verify that a backup has been created
        snapshot = db.backup_get_by_owner_id_and_name(None, self.owner_id, self.snapshot_name, None)
        self.addCleanup(db._delete_db_obj, '', snapshot)
        self.addCleanup(db._delete_db_obj, '', instance)

        self.assertEqual(instance.lifecycle, 'BACKUP', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'PENDING', "Unexpected Changestate: " + instance.changestate)

        self.assertEqual(snapshot.lifecycle, 'CREATING', "Unexpected Lifecycle: " + snapshot.lifecycle)
        self.assertEqual(snapshot.changestate, 'PENDING', "Unexpected Changestate: " + snapshot.changestate)

    @patch('namo.db.mysqlalchemy.api.lccs_conditional_update')
    def test_rollback_when_lccs_conditional_update_fails(self, mock_db_call):
        # throwing exception by method to rollback  database changes.
        mock_db_call.side_effect = exception.DbConditionalUpdateFailed()

        # Call CreateDBInstance API handler
        create_db_snapshot_handler = CreateDBSnapshotHandler()

        with self.assertRaises(exceptions.InvalidDBInstanceState):
            create_db_snapshot_handler.handle_request(
                self.create_db_snapshot_params,
                self.owner_id
            )

        instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        self.addCleanup(db._delete_db_obj, '', instance)

        # validate that instance should  be in ACTIVE ,NONE state.
        self.assertEqual(instance.lifecycle, 'ACTIVE', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'NONE', "Unexpected Changestate: " + instance.changestate)

        # Verify the snapshot should not exist
        helpers.validate_snapshot_not_present(self.owner_id,
                                              self.create_db_snapshot_params.db_snapshot_identifier.value)

    @patch('namo.db.mysqlalchemy.api.backup_create_from_instance')
    def test_rollback_when_backup_create_from_instance_fails(self, mock_db_call):
        # throwing exceptions to get instance properties.
        mock_db_call.side_effect = exceptions.DBSnapshotAlreadyExists()

        # Call the CreateDBSnapshot API handler
        create_db_snapshot_handler = CreateDBSnapshotHandler()

        with self.assertRaises(exceptions.DBSnapshotAlreadyExists):
            create_db_snapshot_handler.handle_request(
                self.create_db_snapshot_params,
                self.owner_id
            )

        instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        self.addCleanup(db._delete_db_obj, '', instance)
        # validate that instance should  be in ACTIVE ,NONE state.
        self.assertEqual(instance.lifecycle, 'ACTIVE', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'NONE', "Unexpected Changestate: " + instance.changestate)

        # Verify the snapshot should not exist
        helpers.validate_snapshot_not_present(self.owner_id,
                                              self.create_db_snapshot_params.db_snapshot_identifier.value)

    @patch('namo.db.mysqlalchemy.api.backup_get_by_owner_id_and_name')
    def test_rollback_when_backup_get_by_owner_id_and_name_fails(self, mock_db_call):
        # throwing exceptions to get instance properties.
        mock_db_call.side_effect = orm_exc.NoResultFound()

        # Call the CreateDBSnapshot API handler
        create_db_snapshot_handler = CreateDBSnapshotHandler()

        # Call CreateDBInstance API handler
        with self.assertRaises(orm_exc.NoResultFound):
            create_db_snapshot_handler.handle_request(
                self.create_db_snapshot_params,
                self.owner_id
            )

        # validate that instance should  be in ACTIVE ,NONE state.
        instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        self.addCleanup(db._delete_db_obj, '', instance)

        self.assertEqual(instance.lifecycle, 'ACTIVE', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'NONE', "Unexpected Changestate: " + instance.changestate)

        with self.assertRaises(exception.BackupNotFoundByOwnerIdAndName):
            db.get_only_entry('',
                              models.Backup,
                              exception.BackupNotFoundByOwnerIdAndName,
                              session=None,
                              name=self.snapshot_name,
                              owner_id=self.owner_id,
                              deleted=0)

if __name__ == '__main__':
    unittest.main()
