import unittest
import uuid

from namo.common import exception
from namo.db.mysqlalchemy import api as db
from apiwebservice import exceptions
from apiwebservice.apihandlers.delete_db_instance_handler import DeleteDBInstanceHandler
from apiwebservice.constants import parameters
from apiwebservice.tests.unit_tests import test_handlers_base

class TestDeleteDBInstanceHandler(test_handlers_base.TestHandlersBase):
    """
    Unit Tests for delete db instance.handlers methods
    """

    def setUp(self):
        self.instance_name = 'instance-' + str(uuid.uuid4())
        self.snapshot_name = 'snapshot-' + str(uuid.uuid4())
        self.delete_db_instance_params = parameters.DeleteDBInstanceParams()
        self.delete_db_instance_params.db_instance_identifier.value = \
            self.instance_name
        self.delete_db_instance_params.final_db_snapshot_identifier.value = \
            self.snapshot_name
        self.delete_db_instance_params.skip_final_snapshot.value = False

    ###################################################################
    ################## Test DeleteDBInstance Handler ##################
    ###################################################################
    def test_delete_db_instance_handler_instance_happy_case_final_snapshot(self):
        # Create test instance
        instance = self._helper_create_test_instance_in_available_state(
            self.instance_name
        )

        # Call the DeleteDBInstance API handler
        delete_db_instance_handler = DeleteDBInstanceHandler()
        delete_db_instance_handler.handle_request(
            self.delete_db_instance_params,
            self.owner_id
        )

        # Verify the instance is in DELETING/PENDING
        instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        self.addCleanup(db._delete_db_obj, '', instance)

        self.assertEqual(instance.lifecycle, 'DELETING', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'PENDING', "Unexpected Changestate: " + instance.changestate)

        # Verify that a backup has been created
        snapshot = db.backup_get_by_owner_id_and_name(None, self.owner_id, self.snapshot_name, None)
        self.addCleanup(db._delete_db_obj, '', snapshot)
        self.assertEqual(snapshot.lifecycle, 'CREATING', "Unexpected Lifecycle: " + snapshot.lifecycle)
        self.assertEqual(snapshot.changestate, 'PENDING', "Unexpected Changestate: " + snapshot.changestate)

    def test_delete_db_instance_handler_instance_happy_case_no_final_snapshot(self):
        # Create test instance
        instance = self._helper_create_test_instance_in_available_state(
            self.instance_name
        )

        self.delete_db_instance_params.skip_final_snapshot.value = True
        self.delete_db_instance_params.final_db_snapshot_identifier.value = None

        # Call the DeleteDBInstance API handler
        delete_db_instance_handler = DeleteDBInstanceHandler()
        delete_db_instance_handler.handle_request(
            self.delete_db_instance_params,
            self.owner_id
        )

        # Verify the instance is in DELETING/PENDING
        instance = db.instance_get(None, self.owner_id, self.instance_name, None)
        self.addCleanup(db._delete_db_obj, '', instance)

        self.assertEqual(instance.lifecycle, 'DELETING', "Unexpected Lifecycle: " + instance.lifecycle)
        self.assertEqual(instance.changestate, 'PENDING', "Unexpected Changestate: " + instance.changestate)

        # Verify that no backup has been created
        with self.assertRaises(exception.BackupNotFoundByOwnerIdAndName):
            db.backup_get_by_owner_id_and_name(None, self.owner_id, self.snapshot_name, None)

    def test_delete_db_instance_handler_instance_not_active_none(self):
        # Create instance in CREATING/PENDING state
        instance = self._helper_create_test_instance_in_db(self.instance_name)
        self.addCleanup(db._delete_db_obj, '', instance)

        delete_db_instance_handler = DeleteDBInstanceHandler()

        with self.assertRaises(exceptions.InvalidDBInstanceState):
            delete_db_instance_handler.handle_request(
                self.delete_db_instance_params,
                self.owner_id
            )

    def test_delete_db_instance_handler_instance_snapshot_already_exists(self):
        # Create test instance
        instance = self._helper_create_test_instance_in_available_state(
            self.instance_name
        )
        # self.addCleanup(db._delete_db_obj, '', instance)

        # Create a snapshot with the same name
        snapshot = db.backup_create_from_instance(None, 'MANUAL', instance, self.snapshot_name)
        self.addCleanup(db._delete_db_obj, '', snapshot)

        delete_db_instance_handler = DeleteDBInstanceHandler()
        with self.assertRaises(exceptions.DBSnapshotAlreadyExists):
            delete_db_instance_handler.handle_request(
                self.delete_db_instance_params,
                self.owner_id
            )

    def test_delete_db_instance_handler_instance_not_found(self):
        # Don't create test instance

        delete_db_instance_handler = DeleteDBInstanceHandler()

        # Call the DeleteDBInstance API handler
        with self.assertRaises(exceptions.DBInstanceNotFound):
            delete_db_instance_handler.handle_request(
                self.delete_db_instance_params,
                self.owner_id
            )

if __name__ == '__main__':
    unittest.main()
