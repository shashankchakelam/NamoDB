import json
from collections import namedtuple
import logging
import requests
from pecan import conf

LOG = logging.getLogger(__name__)

'''
The guest agent helper.

It is responsible for interactions with guest agent.

Note that all the methods will return HTTP 200 OK in case of success, and
will throw HTTP 400 internal server error for any and every thing else
(which might not be ideal, but it works! :) ).
'''

# region Private functions


def __get_port(raga_type):
    if raga_type.lower() == 'primary':
        return str(conf.ga['primary_port'])
    elif raga_type.lower() == 'secondary':
        return str(conf.ga['secondary_port'])
    else:
        raise Exception


def __get_full_url(ip, url, raga_type=None):
    """
    Generate full URL from provided params.

    E.g. if ip is '192.168.1.1', type is 'primary', and url is
    '/v1/health', the full URL will be 'http://192.168.1.1:8080/v1/health'
    """
    if raga_type is None:
        raga_type = 'primary'
    else:
        raga_type = raga_type.lower()
    return 'http://%s:%s%s' % (ip, __get_port(raga_type), url)


def __do_post_request(ip, type, url, body=None):
    if body:
        LOG.info('body is' + str(body))
        LOG.info(__get_full_url(ip, url, type))
    resp = requests.post(__get_full_url(ip, url, type), data=body)
    LOG.info('status code of post resp is:' + str(resp.status_code))
    LOG.info('response message:' + resp.content)
    if resp.status_code >= 400:
        raise Exception(
            'Post request failed. Status code: ' + str(resp.status_code) + ' ,response content: ' + resp.content)
    return resp

# endregion

# region Public functions

def upload_log_files_instantly(body):
    try:
        response = __do_post_request(body['ip'],
                                          body['type'],
                                          '/v1/upload_db_logs_instantly'
                                    )
        return json.loads(response.content, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
    except:
        LOG.exception("Error getting response from ga")
        raise

# endregion