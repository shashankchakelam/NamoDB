import logging

from apiwebservice import exceptions
from apiwebservice import responses
from base_action_handler import BaseActionHandler
from namo.common import exception
from namo.db.mysqlalchemy import api as db

LOG = logging.getLogger(__name__)

"""
Handler to handle requests related to describe sb instance
"""


class DescribeDBInstancesHandler(BaseActionHandler):
    """
    Overriding the IActionHandler class method handleRequest.
    """

    def handle_request(self, parameters, owner_id):
        """
        Handle processing of DescribeDBInstances API request. Returns back all user
        visible properties of all instances owned by the customer OR of the single
        instance that was specified

        Parameters
        ----------
        parameters: Parameters to filter instances by
        owner_id: Customer Account ID that owns this instance

        Returns
        -------
        HTTP Response
        """
        if parameters.db_instance_identifier.value:
            try:
                LOG.info(
                    "DescribeDBInstances for %s:%s being processed",
                    owner_id,
                    parameters.db_instance_identifier.value)

                try:
                    instance = db.get_user_visible_instance_properties(
                                    owner_id,
                                    parameters.db_instance_identifier.value)
                except exception.InstanceNotFoundByOwnerIdAndName:
                    LOG.exception(
                        "Invalid instance: {0} specified by customer",
                        parameters.db_instance_identifier.value)
                    raise exceptions.DBInstanceNotFound(name=parameters.db_instance_identifier.value)

                xml_body = responses.generate_describe_db_instances_body([instance])
                return responses.generate_response(xml_body)
            except exception.InstanceNotFoundById:
                LOG.exception("Invalid instance specified by customer")
                raise exceptions.DBInstanceNotFound(name=parameters.db_instance_identifier.value)
        instances = db.instance_get_all('', owner_id)
        xml_body = responses.generate_describe_db_instances_body(instances)
        return responses.generate_response(xml_body)
