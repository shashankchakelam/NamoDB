import logging

from apiwebservice import exceptions
from apiwebservice import responses
from apiwebservice.validators import helpers
from base_action_handler import BaseActionHandler
from namo.common import exception
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models

LOG = logging.getLogger(__name__)

"""
Handler to handle requests related to delete db snapshot
"""


class DeleteDBSnapshotHandler(BaseActionHandler):
    """
    Overriding the IActionHandler class method handleRequest.
    """

    def handle_request(self, parameters, owner_id):
        """
        Handle processing of DeleteDBSnapshot API request. We make an entry in our
        metadata backend so that the Delete Workflow can be kicked off. We return
        back the properties of the snapshot being deleted.

        Parameters
        ----------
        parameters: Parameters related to deleting the snapshot
        owner_id: Customer Account ID that owns this snapshot

        Returns
        -------
        HTTP Response
        """
        LOG.info(
            "DeleteDBSnapshot for {0}:{1} being handled",
            owner_id,
            parameters.db_snapshot_identifier.value)

        session = db.get_session()
        with session.begin(subtransactions=True):
            # Verify that snapshot exists
            snapshot = helpers.validate_snapshot_present(owner_id, parameters.db_snapshot_identifier.value, session)

            # Verify the snapshot is in ACTIVE/NONE and shift to DELETING/PENDING.
            try:
                db.lccs_conditional_update(
                        None,
                        models.Backup,
                        snapshot.id,
                        'DELETING',
                        'PENDING',
                        'ACTIVE',
                        'NONE',
                        session)
            except exception.DbConditionalUpdateFailed:
                raise exceptions.InvalidDBSnapshotState(state='available')

            snapshot = db.backup_get_by_owner_id_and_name('',
                                                          owner_id=owner_id,
                                                          backup_name=snapshot.name,
                                                          session=session)

        xml_body = responses.generate_delete_db_snapshot_body(snapshot)
        return responses.generate_response(xml_body)
