import logging

from apiwebservice import exceptions
from apiwebservice import responses
from apiwebservice import utils
from apiwebservice.validators import helpers, quota_validators
from base_action_handler import BaseActionHandler
from namo.common import exception
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models

LOG = logging.getLogger(__name__)

"""
Handler to handle requests related to restore db instance
"""


class RestoreDBInstanceFromDBSnapshotHandler(BaseActionHandler):
    """
    Overriding the IActionHandler class method handleRequest.
    """

    def handle_request(self, parameters, owner_id):
        """
        Handle processing of RestoreDBInstanceFromDBSnapshot API request. We restore db
        instance by from the snapshot as requested by user.A meta data row will be
        created in instance table and then workflow will be kicked off to start db restore process.

        Parameters
        ----------
        parameters: Parameters to filter instances by
        owner_id: Customer Account ID that owns this instance

        Returns
        -------
        HTTP Response
        """
        LOG.info(
            "RestoreDBInstanceFromDBSnapshot for {0}:{1} being handled",
            owner_id,
            parameters.db_instance_identifier.value)

        instance_name = parameters.db_instance_identifier.value
        snapshot_name = parameters.db_snapshot_identifier.value

        session = db.get_session()
        with session.begin(subtransactions=True):
            # Verify that db snapshot exist or not.
            # If db snapshot does not exist then throw exception that db snapshot does not exist.
            snapshot = helpers.validate_snapshot_present(owner_id=owner_id,
                                                         snapshot_name=snapshot_name,
                                                         session=session)

            if parameters.db_instance_class.value is not None:
                instance_class = parameters.db_instance_class.value
            else:
                nova_flavor = db.nova_flavor_get_by_id(snapshot.nova_flavor_id)
                instance_class = nova_flavor.flavor_name

            quota_validators.validate_instances_quotas(
                owner_id,
                instance_class,
                session=session)

        session = db.get_session()
        with session.begin(subtransactions=True):
            try:
                db.lccs_conditional_update(
                    None,
                    models.Backup,
                    snapshot.id,
                    'RESTORING',
                    'PENDING',
                    'ACTIVE',
                    'NONE',
                    session=session)
            except exception.DbConditionalUpdateFailed:
                raise exceptions.InvalidDBSnapshotState(state='available')
            # Verify that db instance exist or not
            # if exist - then throw exception that db instance already exist.
            # if not - then we would like to create a db instance with that name.
            helpers.validate_db_instance_not_present(owner_id, instance_name, session=session)

            # Restore database from snapshot and using parameters values.

            # Getting parameters values from snapshot.
            db_storage_size = snapshot.storage_size
            db_port = snapshot.port
            db_master_user_name = snapshot.master_user_name
            db_master_user_password = snapshot.master_user_password
            db_preferred_backup_window_start = snapshot.preferred_backup_window_start
            db_preferred_backup_window_end = snapshot.preferred_backup_window_end
            db_preferred_maintenance_window_start_day = snapshot.preferred_maintenance_window_start_day
            db_preferred_maintenance_window_start_time = snapshot.preferred_maintenance_window_start_time
            db_preferred_maintenance_window_end_day = snapshot.preferred_maintenance_window_end_day
            db_preferred_maintenance_window_end_time = snapshot.preferred_maintenance_window_end_time
            db_backup_retention_period_days = snapshot.backup_retention_period_days
            db_log_retention_period_days = snapshot.log_retention_period
            db_flavor_id = snapshot.nova_flavor_id
            db_artefact_id = snapshot.artefact_id
            db_backup_id = snapshot.id

            # Overriding snapshot values from customer parameters
            db_instance_identifier = parameters.db_instance_identifier.value

            db_flavor = parameters.db_instance_class.value

            # Parse preferred maintenance window start and end datetimes
            if parameters.preferred_maintenance_window.value is not None:
                db_preferred_maintenance_window_start_day, db_preferred_maintenance_window_start_time, \
                db_preferred_maintenance_window_end_day, \
                db_preferred_maintenance_window_end_time = utils.get_maintenance_window_components(
                    parameters.preferred_maintenance_window.value)

            # Parse preferred backup window start and end datetimes
            if parameters.preferred_backup_window.value is not None:
                bw_components = parameters.preferred_backup_window.value.split('-')
                db_preferred_backup_window_start = bw_components[0]
                db_preferred_backup_window_end = bw_components[1]

            if parameters.allocated_storage.value is not None:
                db_storage_size = parameters.allocated_storage.value

            if parameters.master_username.value is not None:
                db_master_user_name = parameters.master_username.value

            if parameters.master_user_password.value is not None:
                db_master_user_password = parameters.master_user_password.value

            if parameters.backup_retention_period.value is not None:
                db_backup_retention_period_days = parameters.backup_retention_period.value

            if parameters.log_retention_period.value is not None:
                db_log_retention_period_days = parameters.log_retention_period.value

            if parameters.port.value is not None:
                db_port = parameters.port.value

            # Create entry in instances table. Instance will be RESTORING/PENDING
            # Currently Engine and engine version is coming from snapshot,
            # in some future releases user would be given option to provide these values as well

            instance_ref = db.restore_db_instance_from_snapshot(
                context=None,
                owner_id=owner_id,
                database_name=db_instance_identifier,
                database_flavor=db_flavor,
                database_storage_size=db_storage_size,
                master_username=db_master_user_name,
                master_user_password=db_master_user_password,
                preferred_backup_window_start=db_preferred_backup_window_start,
                preferred_backup_window_end=db_preferred_backup_window_end,
                preferred_maintenance_window_start_day=db_preferred_maintenance_window_start_day,
                preferred_maintenance_window_start_time=db_preferred_maintenance_window_start_time,
                preferred_maintenance_window_end_day=db_preferred_maintenance_window_end_day,
                preferred_maintenance_window_end_time=db_preferred_maintenance_window_end_time,
                backup_retention_period_days=db_backup_retention_period_days,
                log_retention_period=db_log_retention_period_days,
                port=db_port,
                backup_id=db_backup_id,
                nova_flavor_id=db_flavor_id,
                artefact_id=db_artefact_id,
                session=session
            )

            # Get instance properties from metadata backend and send response back
            instance = db.get_user_visible_instance_properties(
                owner_id,
                parameters.db_instance_identifier.value, session=session)
        xml_body = responses.generate_restore_db_instance_from_db_snapshot_body(instance)
        return responses.generate_response(xml_body)
