import logging

from apiwebservice import exceptions
from apiwebservice import responses
from base_action_handler import BaseActionHandler
from namo.common import exception
from namo.db.mysqlalchemy import api as db

LOG = logging.getLogger(__name__)

"""
Handler to handle requests related to describe db-types
"""


class DescribeDBTypesHandler(BaseActionHandler):
    """
    Overriding the IActionHandler class method handleRequest.
    """

    def handle_request(self, parameters, owner_id):
        """
        Handle processing of DescribeDBTypes API request. Returns back all user
        visible properties of all database engines supported by us

        Parameters
        ----------
        parameters: Parameters to filter db-types by
        owner_id: Customer Account ID that wants to know about the db-types available

        Returns
        -------
        HTTP Response
        """
        if parameters.db_type_identifier.value:
            if parameters.db_type_identifier.value not in db.supported_engines_get_all_names():
                raise exceptions.DBTypeNotFoundByName(name=parameters.db_type_identifier.value)
            try:
                LOG.info(
                    "DescribeDBTypes for %s being processed",
                    owner_id)
                db_type = db.get_user_visible_db_type_properties(parameters.db_type_identifier.value)

                xml_body = responses.generate_describe_db_types_body([db_type])
                return responses.generate_response(xml_body)
            except exception.DBTypeNotFoundByName:
                LOG.exception("Invalid database engine type specified by customer")
                raise exceptions.DBTypeNotFoundByName(name=parameters.db_type_identifier.value)
        db_types = db.supported_db_types_get_all()
        xml_body = responses.generate_describe_db_types_body(db_types)
        return responses.generate_response(xml_body)
