import logging

from apiwebservice import responses
from apiwebservice.validators import helpers
from base_action_handler import BaseActionHandler
from namo.db.mysqlalchemy import api as db

LOG = logging.getLogger(__name__)

"""
Handler to handle requests related to describe db snapshot
"""


class DescribeDBSnapshotsHandler(BaseActionHandler):
    """
    Overriding the IActionHandler class method handleRequest.
    """

    def handle_request(self, parameters, owner_id):
        """
        Handle processing of DescribeDBSnapshots API request. Returns back all user
        visible properties of all shapshots owned by the customer OR of the single
        instance/snapshot that was specified

        Parameters
        ----------
        parameters: Parameters to filter snapshots by
        owner_id: Customer Account ID that owns this instance

        Returns
        -------
        HTTP Response
        """
        if parameters.db_snapshot_identifier.value:
            helpers.validate_snapshot_present(owner_id, parameters.db_snapshot_identifier.value)
            LOG.info(
                "DescribeDBSnapshots for %s:%s being processed",
                owner_id,
                parameters.db_snapshot_identifier.value)
            snapshot = db.backup_get_by_owner_id_and_name('', owner_id, parameters.db_snapshot_identifier.value)
            xml_body = responses.generate_describe_db_snapshots_body([snapshot])
            return responses.generate_response(xml_body)

        if parameters.db_instance_identifier.value:
            instance = helpers.validate_instance_present(owner_id, parameters.db_instance_identifier.value)
            snapshots = db.backup_get_all_for_instance('', instance["id"], parameters.snapshot_type.value)
        else:
            # if both DBInstanceIdentifier and DBSnapshotIdentifier are absent then
            # we return all snapshots of that owner
            snapshots = db.backup_get_all_for_owner('', owner_id, parameters.snapshot_type.value)
        xml_body = responses.generate_describe_db_snapshots_body(snapshots)
        return responses.generate_response(xml_body)
