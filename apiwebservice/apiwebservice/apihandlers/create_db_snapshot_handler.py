import logging

from apiwebservice import exceptions
from apiwebservice import responses
from apiwebservice.validators import helpers
from apiwebservice.validators import quota_validators
from base_action_handler import BaseActionHandler
from namo.common import exception
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models


LOG = logging.getLogger(__name__)

"""
Handler to handle requests related to create db snapshot
"""


class CreateDBSnapshotHandler(BaseActionHandler):
    """
    Overriding the IActionHandler class method handleRequest.
    """

    def handle_request(self, parameters, owner_id):
        """
        Handle processing of CreateDBSnapshot API request. Returns back all user
        visible properties.

        Parameters
        ----------
        parameters: Parameters to filter instances by
        owner_id: Customer Account ID that owns this instance

        Returns
        -------
        HTTP Response
        """
        LOG.info(
            "CreateDBSnapshot for %s:%s snapshot is being processed",
            owner_id,
            parameters.db_snapshot_identifier.value)
        instance_name = parameters.db_instance_identifier.value
        snapshot_name = parameters.db_snapshot_identifier.value

        session = db.get_session()
        with session.begin(subtransactions=True):
            # the code to check if the addition of the number of snapshots created by users does not exceed the limit
            quota_validators.validate_number_of_snapshots_does_not_exceed_limit(owner_id, session=session)

            instance = helpers.validate_instance_present(owner_id, instance_name, session)

            # Whether it is in actually in active or deletion in progress state.
            # Verify the instance is in ACTIVE/NONE
            helpers.validate_instance_lc_cs(instance["lifecycle"], instance["changestate"], 'ACTIVE', 'NONE')

            helpers.validate_snapshot_not_present(owner_id, snapshot_name, session)
            db.backup_create_from_instance('', 'MANUAL', instance, snapshot_name, session)

            try:
                db.lccs_conditional_update(
                    None,
                    models.Instance,
                    instance['id'],
                    'BACKUP',
                    'PENDING',
                    'ACTIVE',
                    'NONE',
                    session=session)
            except exception.DbConditionalUpdateFailed:
                raise exceptions.InvalidDBInstanceState(state='available')

            # Get instance properties from metadata backend and send response back
            snapshot = db.backup_get_by_owner_id_and_name('',
                                                          owner_id=owner_id,
                                                          backup_name=snapshot_name,
                                                          session=session)

        xml_body = responses.generate_create_db_snapshot_body(snapshot)
        return responses.generate_response(xml_body)
