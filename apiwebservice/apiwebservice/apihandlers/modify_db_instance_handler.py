import logging

from apiwebservice import exceptions
from apiwebservice import responses
from apiwebservice.validators import helpers
from base_action_handler import BaseActionHandler
from apiwebservice.validators import quota_validators
from namo.common import exception
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models
LOG = logging.getLogger(__name__)

"""
Handler to handle requests related to modify db instance
"""


class ModifyDBInstanceHandler(BaseActionHandler):
    """
    Overriding the IActionHandler class method handleRequest.
    """
    def handle_request(self, parameters, owner_id):
        """
        Handle processing of ModifyDBInstance API request. We change the entries in the
        backend on the instance on which modify is applied. We return
        back the properties of the instance being modified. We accept the request
        only if no other operation is running on the instance

        Parameters
        ----------
        parameters: Parameters related to modifying the instance
        owner_id: Customer Account ID that owns this instance

        Returns
        -------
        HTTP Response
        """
        LOG.info(
            "ModifyDBInstance for %s:%s being handled",
            owner_id,
            parameters.db_instance_identifier.value)
        session = db.get_session()
        with session.begin(subtransactions=True):
            # Verify that the instance exists

            try:
                instance = db.instance_get(
                    None,
                    owner_id,
                    parameters.db_instance_identifier.value,
                    session)
            except exception.InstanceNotFoundByOwnerIdAndName:
                LOG.exception(
                    "Invalid instance: {0} specified by customer",
                    parameters.db_instance_identifier.value)
                raise exceptions.DBInstanceNotFound(name=parameters.db_instance_identifier.value)

             # Verify whether the instance is in ACTIVE/NONE
            helpers.validate_instance_lc_cs(instance["lifecycle"], instance["changestate"], 'ACTIVE', 'NONE')
            new_values_for_instant_update = {}
            new_values_for_workflow_list = []

            check_new_values_for_instant_update = {}

            new_values_for_instant_update['name'] = parameters.db_instance_identifier.value
            new_values_for_instant_update['owner_id'] = owner_id

            if parameters.new_db_instance_identifier.value is not None:
                helpers.validate_db_instance_not_present(owner_id,
                                                         parameters.new_db_instance_identifier.value,
                                                         session=session)
                new_values_for_instant_update['new_name'] = parameters.new_db_instance_identifier.value

            if parameters.backup_retention_period.value is not None:
                check_new_values_for_instant_update['backup_retention_period_days'] = parameters.backup_retention_period.value
                if helpers.validate_modifying_parameter(parameters.backup_retention_period.name,check_new_values_for_instant_update,instance):
                    new_values_for_instant_update.update(check_new_values_for_instant_update)

            #Update Dp_tasks as well to handle the case of Scheduled Modify cases
            if parameters.preferred_maintenance_window.value is not None:
                preferred_maintenance_window = parameters.preferred_maintenance_window.value
                check_new_values_for_instant_update["preferred_maintenance_window_start_day"] = preferred_maintenance_window[:3].upper()
                preferred_maintenance_window_start_time = preferred_maintenance_window[4:9]
                preferred_maintenance_window_start_time += ':00'
                check_new_values_for_instant_update["preferred_maintenance_window_start_time"] = preferred_maintenance_window_start_time
                check_new_values_for_instant_update["preferred_maintenance_window_end_day"] = preferred_maintenance_window[10:13].upper()
                preferred_maintenance_window_end_time = preferred_maintenance_window[14:]
                preferred_maintenance_window_end_time += ':00'
                check_new_values_for_instant_update["preferred_maintenance_window_end_time"] = preferred_maintenance_window_end_time
                if helpers.validate_modifying_parameter(parameters.preferred_maintenance_window.name, check_new_values_for_instant_update,instance):
                    new_values_for_instant_update.update(check_new_values_for_instant_update)


            if parameters.preferred_backup_window.value is not None:
                preferred_backup_window = parameters.preferred_backup_window.value
                preferred_backup_window_start = preferred_backup_window[:5]
                preferred_backup_window_start += ':00'
                check_new_values_for_instant_update["preferred_backup_window_start"] = preferred_backup_window_start
                preferred_backup_window_end = preferred_backup_window[6:]
                preferred_backup_window_end += ':00'
                check_new_values_for_instant_update["preferred_backup_window_end"] = preferred_backup_window_end
                if helpers.validate_modifying_parameter(parameters.preferred_backup_window.name, check_new_values_for_instant_update,
                                                 instance):
                    new_values_for_instant_update.update(check_new_values_for_instant_update)

            #Checking for all parameters that will update after workflow should be done after this

            if parameters.db_instance_class.value is not None:
                db_instance_class = parameters.db_instance_class.value

                quota_validators.validate_instances_quotas(
                    owner_id,
                    parameters.db_instance_class.value,
                    session=session)

                if db_instance_class != instance.nova_flavor.nova_flavor_id:
                    new_values_for_workflow = {}
                    new_values_for_workflow["parameter"] = "nova_flavor_id"
                    new_values_for_workflow["targetValue"] = db_instance_class
                    new_values_for_workflow["currentValue"] = instance.nova_flavor.nova_flavor_id
                    new_values_for_workflow_list.append(new_values_for_workflow)

            if len(new_values_for_instant_update) > 2:
                db.instance_update('', 'instance_id?', new_values_for_instant_update, session)

            if len(new_values_for_workflow_list) > 0:

                try:
                    db.lccs_conditional_update(
                        None,
                        models.Instance,
                        instance['id'],
                        'MODIFYING',
                        'PENDING',
                        'ACTIVE',
                        'NONE',
                        session=session)
                except exception.DbConditionalUpdateFailed:
                    raise exceptions.InvalidDBInstanceState(state='available')

                db.insert_into_dp_tasks(instance,new_values_for_workflow_list,session)
        # TODO(Varun): Need to do changes in generating xml so that we can include this also inside session.
        # Get instance properties from metadata backend and send response back
        if parameters.new_db_instance_identifier.value is not None:
            instance = db.get_user_visible_instance_properties(
                    owner_id,
                    parameters.new_db_instance_identifier.value)
        else:
            instance = db.get_user_visible_instance_properties(
                            owner_id,
                            parameters.db_instance_identifier.value)

        xml_body = responses.generate_modify_db_instance_body(instance)
        return responses.generate_response(xml_body)
