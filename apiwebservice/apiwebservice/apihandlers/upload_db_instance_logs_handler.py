import logging

from apiwebservice import exceptions
from apiwebservice import responses
from apiwebservice.helpers import ga

from base_action_handler import BaseActionHandler
from namo.common import exception
from namo.db.mysqlalchemy import api as db


LOG = logging.getLogger(__name__)

"""
Handler to handle requests related to upload db logs instantly
"""


class UploadDBInstanceLogsHandler(BaseActionHandler):
    """
    Overriding the IActionHandler class method handleRequest.
    """

    def handle_request(self, parameters, owner_id):
        """
        Handle processing of UploadDBInstanceLogs API request. We directly calls the ga agent 
        which calls the instance service to upload logs directly to dss.

        Parameters
        ----------
        parameters: Parameters related to uploading logs of db from instance to dss.
        owner_id: Customer Account ID that owns this instance

        Returns
        -------
        HTTP Response
        """
        LOG.info(
            "UploadDBInstanceLogs for {0}:{1} being handled",
            owner_id,
            parameters.db_instance_identifier.value)
        if parameters.db_instance_identifier.value:
            try:
                instance = db.get_user_visible_instance_properties(
                                owner_id,
                                parameters.db_instance_identifier.value)
                body = {'ip': instance['endpoint'], 'type': 'primary'}
                ga.upload_log_files_instantly(body)
                xml_body = responses.generate_upload_db_instance_logs_body()
                return responses.generate_response(xml_body)
            except exception.InstanceNotFoundByOwnerIdAndName:
                LOG.exception(
                    "Invalid instance: {0} specified by customer",
                    parameters.db_instance_identifier.value)
                raise exceptions.DBInstanceNotFound(name=parameters.db_instance_identifier.value)
            except Exception:
                error_msg = "Error while uploading Instance Logs"
                LOG.exception(error_msg)
                raise exceptions.InternalFailure(message=error_msg)
