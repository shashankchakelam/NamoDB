import logging

from apiwebservice.exceptions import InvalidAction
from base_action_handler import BaseActionHandler

LOG = logging.getLogger(__name__)

"""
Handler to handle those action request which don't exist.
"""


class NoSuchActionHandler(BaseActionHandler):
    """
    Handle processing of CreateDBInstance API request. We create a new instance
    row and place it in CREATING/PENDING before returning back a response to
    the user

    Parameters
    ----------
    parameters: Parameters to create the instance row
    owner_id: Customer Account ID that owns this instance

    Returns
    -------
    HTTP Response
    """

    def handle_request(self, parameters, owner_id):
        raise InvalidAction
