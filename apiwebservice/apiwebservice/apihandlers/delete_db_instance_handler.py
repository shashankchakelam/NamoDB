import logging

from apiwebservice import exceptions
from apiwebservice import responses
from base_action_handler import BaseActionHandler
from namo.common import exception
from namo.db.mysqlalchemy import api as db
from namo.db.mysqlalchemy import models


LOG = logging.getLogger(__name__)

"""
Handler to handle requests related to delete db instance
"""


class DeleteDBInstanceHandler(BaseActionHandler):
    """
    Overriding the IActionHandler class method handleRequest.
    """

    def handle_request(self, parameters, owner_id):
        """
        Handle processing of DeleteDBInstance API request. We make an entry in our
        metadata backend so that the Delete Workflow can be kicked off. We return
        back the properties of the instance being deleted. We accept the request
        only if no other operation is running on the instance

        Parameters
        ----------
        parameters: Parameters related to deleting the instance
        owner_id: Customer Account ID that owns this instance

        Returns
        -------
        HTTP Response
        """
        LOG.info(
            "DeleteDBInstance for {0}:{1} being handled",
            owner_id,
            parameters.db_instance_identifier.value)

        session = db.get_session()
        with session.begin(subtransactions=True):
            # Verify that the instance exists
            try:
                instance = db.instance_get(
                    None,
                    owner_id,
                    parameters.db_instance_identifier.value,
                    session)
            except exception.InstanceNotFoundByOwnerIdAndName:
                LOG.exception(
                    "Invalid instance: {0} specified by customer",
                    parameters.db_instance_identifier.value)
                raise exceptions.DBInstanceNotFound(name=parameters.db_instance_identifier.value)

            # If SkipFinalSnapshot is false, verify the snapshot identifier specified
            # isn't already assigned to a snapshot
            if not parameters.skip_final_snapshot.value:
                try:
                    db.backup_get_by_filters(
                        None,
                        session=session,
                        name=parameters.final_db_snapshot_identifier.value)

                    # We raise this exception if we don't get a DbEntryNotFound
                    # exception i.e. a snapshot with the same name already exists
                    LOG.debug(
                        "Snapshot {0} already exists",
                        parameters.final_db_snapshot_identifier.value)
                    raise exceptions.DBSnapshotAlreadyExists(
                        name=parameters.final_db_snapshot_identifier.value)
                except exception.DbEntryNotFound:
                    pass

            # Verify the instance is in ACTIVE/NONE and shift to DELETING/PENDING.
            # Create backup entry in CREATING/PENDING lccs.
            # Do all this atomically.
            instance = self._set_instance_deleting_pending(
                None,
                owner_id,
                parameters.db_instance_identifier.value,
                parameters.final_db_snapshot_identifier.value, session=session)

        xml_body = responses.generate_delete_db_instance_body(instance)
        return responses.generate_response(xml_body)

    def _set_instance_deleting_pending(
            self,
            context,
            owner_id,
            name,
            final_snapshot_name=None,
            session=None):
        """
        Set instance in DELETING/PENDING and possibly also create a record for the
        final snapshot of the instance

        Parameters
        ----------
        context: Context of the request
        owner_id: Customer Account ID that owns the instance
        name: DBInstanceIdentifier of the instance
        final_snapshot_name: Name of the final snapshot specified by the customer
        session: Session under which the database queries are being executed

        Returns
        -------
        Instance properties
        """
        session = session or db.get_session()

        # Using subtransactions here for atomicity and for allowing nested sessions
        # to all be part of the single transaction that is initiated here. If any
        # of the nested sessions does not have 'subtransactions=True', we get an
        # error back that "InvalidRequestError: A transaction is already begun.  Use
        # subtransactions=True to allow subtransactions."
        with session.begin(subtransactions=True):
            # Set instance lccs to DELETING/PENDING
            instance_ref = db.instance_get(context, owner_id, name, session=session)

            try:
                db.lccs_conditional_update(
                    context,
                    models.Instance,
                    instance_ref.id,
                    'DELETING',
                    'PENDING',
                    'ACTIVE',
                    'NONE',
                    session)
            except exception.DbConditionalUpdateFailed:
                raise exceptions.InvalidDBInstanceState(state='available')

            # If a snapshot name is passed, create a corresponding record in the backups table.
            # This essentially represents a request for a final snapshot by the customer.
            # Performing this operation in the same session for atomicity.
            if final_snapshot_name:
                db.backup_create_from_instance(
                None,
                'MANUAL',
                instance_ref,
                final_snapshot_name,
                session)

            # TODO(shank): Tried catching sqlalchemy.exc.IntegrityError so that if we
            # were inserting a duplicate entry then we could catch it and gracefully
            # handle it but unable to do this. INstead I get an InvalidRequestError.
            # Need to look into this more. Of course, we do a check earlier that the
            # snapshot name doesn't exist but there's always a chance for a race :)

            # Get and return instance properties
            return db.get_user_visible_instance_properties(
                owner_id,
                name,
                session
                )
