import logging

from apiwebservice import exceptions
from apiwebservice import responses
from apiwebservice import utils
from apiwebservice.validators import quota_validators
from base_action_handler import BaseActionHandler
from namo.db.mysqlalchemy import api as db
from oslo_db import exception as mysql_exc

LOG = logging.getLogger(__name__)

"""
Handler to handle requests related to create db instance
"""


class CreateDBInstanceHandler(BaseActionHandler):
    """
    Overriding the IActionHandler class method handleRequest.
    """

    def handle_request(self, parameters, owner_id):
        """
        Handle processing of CreateDBInstance API request. We create a new instance
        row and place it in CREATING/PENDING before returning back a response to
        the user

        Parameters
        ----------
        parameters: Parameters to create the instance row
        owner_id: Customer Account ID that owns this instance

        Returns
        -------
        HTTP Response
        """
        LOG.info(
            "CreateDBInstance for %s:%s being processed",
            owner_id,
            parameters.db_instance_identifier.value)

        # Parse preferred maintenance window start and end datetimes
        mw_start_day, mw_start_time, mw_end_day, mw_end_time = \
            utils.get_maintenance_window_components(
                parameters.preferred_maintenance_window.value)

        # Parse preferred backup window start and end datetimes
        bw_components = parameters.preferred_backup_window.value.split('-')
        bw_start = bw_components[0]
        bw_end = bw_components[1]

        session = db.get_session()
        with session.begin(subtransactions=True):
            # this code to check if the addition of the number of instances and
            # allocated storage created by users does not exceed the limit
            quota_validators.validate_instances_quotas(
                owner_id,
                parameters.db_instance_class.value,
                session=session)
            quota_validators.validate_storage_size_of_instances_does_not_exceed_limit(
                owner_id,
                parameters.allocated_storage.value,
                session=session)

            # Create entry in instances table. Instance will be CREATING/PENDING
            try:
                db.instance_create_entry_from_user_supplied_values(
                    context=None,
                    owner_id=owner_id,
                    database_engine=parameters.engine.value,
                    database_engine_version=parameters.engine_version.value,
                    database_name=parameters.db_instance_identifier.value,
                    database_flavor=parameters.db_instance_class.value,
                    database_storage_size=parameters.allocated_storage.value,
                    master_username=parameters.master_username.value,
                    master_user_password=parameters.master_user_password.value,
                    preferred_backup_window_start=bw_start,
                    preferred_backup_window_end=bw_end,
                    preferred_maintenance_window_start_day=mw_start_day,
                    preferred_maintenance_window_start_time=mw_start_time,
                    preferred_maintenance_window_end_day=mw_end_day,
                    preferred_maintenance_window_end_time=mw_end_time,
                    backup_retention_period_days=parameters.backup_retention_period.value,
                    log_retention_period=parameters.log_retention_period.value,
                    port=parameters.port.value,
                    session=session
                )
            except mysql_exc.DBDuplicateEntry:
                LOG.exception("Instance already exists: %s" % parameters.db_instance_identifier.value)
                raise exceptions.DBInstanceAlreadyExists(name=parameters.db_instance_identifier.value)

            # Get instance properties from metadata backend and send response back
            instance = db.get_user_visible_instance_properties(
                owner_id,
                parameters.db_instance_identifier.value, session=session)
        xml_body = responses.generate_create_db_instance_body(instance)
        return responses.generate_response(xml_body)
