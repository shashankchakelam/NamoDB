"""
Handlers take care of processing
the API request once parsing and validation is complete. By the time we reach a
method here, validation of the user input parameters has taken place and we pass
in a parameter object that contains all the user specified parameters and also
default values for any parameters not explicitly specified
"""


class BaseActionHandler(object):
    """
    Base Class for creating action handlers.
    There is an abstract method in action handler to handle generic request.
    """

    def handle_request(self, parameters, owner_id):
        raise NotImplementedError("Should have implemented this")
