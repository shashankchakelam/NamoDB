from apiwebservice.apihandlers.create_db_instance_handler import CreateDBInstanceHandler
from apiwebservice.apihandlers.create_db_snapshot_handler import CreateDBSnapshotHandler
from apiwebservice.apihandlers.delete_db_instance_handler import DeleteDBInstanceHandler
from apiwebservice.apihandlers.delete_db_snapshot_handler import DeleteDBSnapshotHandler
from apiwebservice.apihandlers.describe_db_instances_handler import DescribeDBInstancesHandler
from apiwebservice.apihandlers.describe_db_snapshots_handler import DescribeDBSnapshotsHandler
from apiwebservice.apihandlers.modify_db_instance_handler import ModifyDBInstanceHandler
from apiwebservice.apihandlers.noaction_required_handler import NoSuchActionHandler
from apiwebservice.apihandlers.restore_db_instance_from_db_snapshot_handler import RestoreDBInstanceFromDBSnapshotHandler
from apiwebservice.apihandlers.describe_db_types_handler import DescribeDBTypesHandler
from apiwebservice.apihandlers.upload_db_instance_logs_handler import UploadDBInstanceLogsHandler

action_handler_mapping = {
        "create_db_instance": CreateDBInstanceHandler,
        "delete_db_instance": DeleteDBInstanceHandler,
        "modify_db_instance": ModifyDBInstanceHandler,
        "create_db_snapshot": CreateDBSnapshotHandler,
        "delete_db_snapshot": DeleteDBSnapshotHandler,
        "restore_db_instance_from_db_snapshot": RestoreDBInstanceFromDBSnapshotHandler,
        "describe_db_instances": DescribeDBInstancesHandler,
        "describe_db_snapshots": DescribeDBSnapshotsHandler,
        "describe_db_types": DescribeDBTypesHandler,
        "upload_db_instance_logs":UploadDBInstanceLogsHandler
    }

# TODO:(Varun)- Need to decide whether this approach is better or dynamic dispatching of methods is better.


def get_action_handler(action_name):

    """

    Parameters
    ----------
    action_name: Name of the action handler in lower case underscore separated

    Returns
    -------
    Action handler of action.
    """
    return action_handler_mapping.get(action_name, NoSuchActionHandler)()
