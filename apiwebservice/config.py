# Server Specific Configurations
server = {
    'port': '3000',
    'host': '0.0.0.0'
}

# Pecan Application Configurations
app = {
    'root': 'apiwebservice.controllers.root.RootController',
    'modules': ['apiwebservice'],
    'static_root': '%(confdir)s/public',
    'template_path': '%(confdir)s/apiwebservice/templates',
    'hooks': lambda: [__import__('apiwebservice.common.RequestIdPecanHook', fromlist=["RequestIdHook"]).RequestIdHook()], # The hook needs to be iterable and must implement/subclass from PecanHook
    'debug': True,
    'errors': {
        '__force_dict__': True
    }
}

logging = {
    'root': {'level': 'INFO', 'handlers': ['console']},
    'loggers': {
        'apiwebservice': {'level': 'DEBUG', 'handlers': ['applogfile']},
        'wsgi': {'level': 'INFO', 'handlers': ['accesslogfile'], 'qualname': 'wsgi'},
        'pecan': {'level': 'DEBUG', 'handlers': ['console']},
        'py.warnings': {'handlers': ['console']},
        '__force_dict__': True
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'color'
        },
        'applogfile': {
            'level': 'DEBUG',
            'class': 'apiwebservice.common.FilteredRotatingFileHandler.FilteredRotatingHandlerClass', # For log rotation with request_id filter
            'filename': '/opt/rds/apiwebservice/logs/app.log',  # Ensure we create this folder
            'mode': 'a',                    # Appends to current file
            'maxBytes': '10485760',         # 10 MB is the size of each log file
            'backupCount': '20',            # 20 backup files will be maintained
            'formatter': 'simple'
        },
        'accesslogfile': {
            'level': 'INFO',
            'class': 'apiwebservice.common.FilteredRotatingFileHandler.FilteredRotatingHandlerClass', # For log rotation with request_id filter
            'filename': '/opt/rds/apiwebservice/logs/access.log',  # Ensure we create this folder
            'mode': 'a',                    # Appends to current file
            'maxBytes': '10485760',         # 10 MB is the size of each log file
            'backupCount': '20',            # 20 backup files will be maintained
            'formatter': 'simple'
        }
    },
    'formatters': {
        'simple': {
            'format': ('%(asctime)s %(req_id)-15s %(levelname)-5.5s [%(name)s]'
                       '[%(threadName)s] %(message)s')
        },
        'color': {
            '()': 'pecan.log.ColorFormatter',
            'format': ('%(asctime)s [%(padded_color_levelname)s] [%(name)s]'
                       '[%(threadName)s] %(message)s'),
        '__force_dict__': True
        }
    }
}

ga = {
    'primary_port': 8080,
    'secondary_port': 8081
}

database = {
    'connection': 'mysql://root:tempdbpass@10.140.214.37/namo?charset=utf8',
}

app_dirs = {
    'home': '/opt/rds/apiwebservice/',
    'logs': '/opt/rds/apiwebservice/logs/'
}

xml_namespace = {
    'current': 'https://rds.ind-west-1.jiocloudservices.com/doc/2016-03-01/'
}

user_parameter_defaults = {
    'port_mysql': 3306,
    'port_postgresql': 5432,
    'backup_retention_period': 1,           # Value in days
    'log_retention_period': 7,              # Value in days
    'maintenance_window_size_mins': 30,
    'backup_window_size_mins': 30,
    'night_block_start_hour': 22,
    'night_block_size_mins': 480,
}

user_parameter_constraints = {
    'access_key_id_min_length': 16,
    'access_key_id_max_length': 32,
    'min_storage_size_supported': 5,        # Value in GB
    'max_storage_size_supported': 1024,     # Value in GB

    'mysql_master_username_min_length': 2,
    'mysql_master_username_max_length': 16,
    'mysql_master_user_pwd_min_length': 8,
    'mysql_master_user_pwd_max_length': 41,
    'mysql_master_user_pwd_unsupported_chars': ['/', '"', '@'],

    'postgresql_master_username_min_length': 2,
    'postgresql_master_username_max_length': 16,
    'postgresql_master_user_pwd_min_length': 8,
    'postgresql_master_user_pwd_max_length': 128,
    'postgresql_master_user_pwd_unsupported_chars': ['/', '"', '@'],

    'port_range_min': 1150,
    'port_range_max': 65535,
    'min_backup_retention_period': 0,       # Value in days
    'max_backup_retention_period': 35,      # Value in days
    'min_log_retention_period': 1,          # Value in days
    'max_log_retention_period': 7,         # Value in days
    'min_maintenance_window_size': 30,      # Value in mins
    'max_maintenance_window_size': 60,      # Value in mins
    'min_backup_window_size': 30,           # Value in mins
    'max_backup_window_size': 60,           # Value in mins
}

keystone = {
    'url': 'https://iam.ind-west-1.staging.jiocloudservices.com',
    'token_url': 'https://iam.ind-west-1.staging.jiocloudservices.com/token-auth',
    'ec2_auth_url': 'https://iam.ind-west-1.staging.jiocloudservices.com/ec2-auth'
}

auth_params = {
    'mapping_file': '%(confdir)s/apiwebservice/constants/resource_action_mapping.json',  #need to find a better way to specify absolute path
}

iam_authentication = {
    'authorize' : 'true',
    'dummy_customer_id' : 1  #this customer id is returned when above authorize field is false.
}

