#!/bin/bash
set -ex

# Specifying --local will ensure that it takes of setting up your local environment:
# 1. Dropping namo db and re-creating it so that we can register the latest models
# 2. Populate seed data into namo db
# 3. Don't delete the virtualenv so that you don't need to keep downloading packages
if [ "${1}" == "--local" ]; then
    cd ../namo
    ./initialize_test_namo_db.sh
    cd ../apiwebservice
fi

# Create virtualenv. Interesting point to note: you don't need to check if you're
# already running in a virtualenv as it's a safe command to simply create and activate
# a new one (even of the same name)
virtualenv .venv
source .venv/bin/activate

# Build and install namo module as it's a dependency
cd ../namo
python setup.py install
cd ../apiwebservice

# Build, install and run tests of the apiwebservice module
pip install -r requirements.txt
pip install -r test-requirements.txt
python setup.py install
python setup.py test

# Even if you don't explicitly deactivate here, once the script exits the virtualenv
# will be deactivated. This is because the source command loads the virtualenv in the
# context of this script but once it exits, will deactivate the virtualenv
deactivate
if [ "${1}" != "--local" ]; then
    rm -rf .venv/
fi
