import os
import subprocess
import tempfile
import uuid
from ga.common import utils
from unittest import TestCase
import fake
from ga.controllers import v1
import mock

class TestUnits(TestCase):
    """
    Unit TestCases are independent of
    the database_engine used.
    They use functions of the base class
    common to the database-engines.
    """
    @mock.patch('ga.utils.get_image_info_dict_from_json',fake.get_postgresql_image_info)
    def setUp(self):
        self.ga = v1.ControllerFactory.getController()
        _, self.tmpfname = tempfile.mkstemp()

    def tearDown(self):
        os.remove(self.tmpfname)


    def test_get_json_dict(self):
        req = {'{"key": "value"}': ''}
        expected_resp = {'key': 'value'}
        actual_resp = self.ga._get_json_dict(req)
        self.assertDictEqual(actual_resp, expected_resp)

    def test_dict_from_file(self):
        # Initialize a file with multiple key values
        subprocess.call('echo "key1=val1" > %s; echo "key2=val2" >> %s' % (self.tmpfname, self.tmpfname), shell=True)

        expected_resp = {'key1': 'val1', 'key2': 'val2'}
        actual_resp = self.ga._dict_from_file(self.tmpfname)
        self.assertDictEqual(actual_resp, expected_resp)

    def test_create_directory(self):
        dir_name = str(uuid.uuid4())
        dir_path = './' + dir_name
        utils.create_directory(dir_path)
        ls_command = 'ls ' + dir_path
        result = subprocess.call(ls_command, shell=True)
        assert result == 0
        delete_command = 'rm -rf ' + dir_path
        result = subprocess.call(delete_command, shell=True)
        assert result == 0

    def test_create_file_and_get_path(self):
        dir_name = str(uuid.uuid4())
        file_name = str(uuid.uuid4())
        dir_path = './' + dir_name
        file_path = utils.create_file_and_get_path(dir_path, file_name)
        ls_command = 'ls ' + file_path
        result = subprocess.call(ls_command, shell=True)
        assert result == 0
        delete_command = 'rm -rf ' + dir_path
        result = subprocess.call(delete_command, shell=True)
        assert result == 0

    def test_tail(self):
        dir_name = str(uuid.uuid4())
        file_name = str(uuid.uuid4())
        dir_path = './' + dir_name
        file_path = utils.create_file_and_get_path(dir_path, file_name)
        data = str(uuid.uuid4())
        echo_command = 'echo line 1' +'> ' + file_path
        result = subprocess.call(echo_command, shell=True)
        assert result == 0
        echo_command = 'echo ' + data + '>> ' + file_path
        result = subprocess.call(echo_command, shell=True)
        assert result == 0
        response = utils.tail(file_path)
        assert data == response
        delete_command = 'rm -rf ' + dir_path
        result = subprocess.call(delete_command, shell=True)
        assert result == 0

    def test_get_auth_headers(self):
        auth_token = str(uuid.uuid4())
        result_auth = utils.get_auth_headers(auth_token)
        assert auth_token == result_auth['X-Auth-Token']
        assert 'application/json' == result_auth['Content-Type']




