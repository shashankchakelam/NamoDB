import os
import fake
import mock
import json
import pecan
from ga.tests.test_functional import TestRootController

"""
Setting the database_engine parameter
for running the test cases for
postgresql. It is used to mock the
ga.utils.get_image_info_dict_from_json
function for corresponding database_engine
"""
fake.database_engine = 'postgresql'

class PostgresqlTestRootController(TestRootController):

    '''
    TODO run restart_db_services also without mocking it,
    '''
    @mock.patch('ga.utils.restart_db_services')
    def test_port_change_postgresql(self,mock_restart_db_service):
        body = {}
        body['port'] = 12345
        self.helper_create_conf_file_copy('./ga/tests/test-postgres.conf')

        self.app.put('/v1/set_port', json.dumps(body))

        port = self.helper_get_parameter_from_conf_file('port')
        self.assertEquals(port, '12345')

    @mock.patch('ga.utils.restart_db_services')
    def test_port_remains_same_postgresql(self,mock_db_services):
        body={}
        body['port'] = 5432
        self.helper_create_conf_file_copy('./ga/tests/test-postgres.conf')

        self.app.put('/v1/set_port', json.dumps(body))

        port_info = self.helper_get_parameter_from_conf_file('port')
        self.assertIn('5432', port_info)

    @mock.patch('subprocess.check_output')
    def test_service_get_postgresql_running(self, mock_checkoutput):
        mock_checkoutput.return_value = '9.3/main (port 5432): online'
        response = self.app.get('/v1/service')
        self._helper_validate_req_id(response)
        assert response.status_int == 200
        result = json.loads(response.body)
        assert result["status"] == 'start/running'

    @mock.patch('subprocess.check_output')
    def test_service_get_postgresql_notrunning(self, mock_checkoutput):
        mock_checkoutput.return_value = '9.3/main (port 5432): down'
        response = self.app.get('/v1/service')
        self._helper_validate_req_id(response)
        assert response.status_int == 200
        result = json.loads(response.body)
        assert result["status"] == 'stop/waiting'

    def test_set_parameter_happy_cases(self):
        self.helper_create_conf_file_copy('./ga/tests/test-postgres.conf')

        body = {}
        body['parameters'] = [
            {'name': 'shared_buffers', 'value': 25600.0, 'type': 'static'},        # Uncommented in test-postgres.conf
            {'name': 'checkpoint_timeout', 'value': '1h', 'type': 'dynamic'},      # Commented in test-mysql.cnf
        ]

        self.app.post('/v1/parameters', json.dumps(body))

        # Validate the contents of the conf file
        shared_buffers = self.helper_get_parameter_from_conf_file('shared_buffers')
        self.assertEquals(shared_buffers, '25600.0')

        checkpoint_timeout = self.helper_get_parameter_from_conf_file('checkpoint_timeout')
        self.assertEquals(checkpoint_timeout, '1h')

    def test_set_parameter_empty_request_body(self):
        self.helper_create_conf_file_copy('./ga/tests/test-postgres.cnf')

        body = {}
        response = self.app.post('/v1/parameters', json.dumps(body), expect_errors=True)

        self.assertEquals(response.status_int, 400)
        self.assertIn('Empty or incorrect request body', response.body)

    def test_set_parameter_incorrect_request_body(self):
        self.helper_create_conf_file_copy('./ga/tests/test-postgres.cnf')

        body = {}
        body['parameter'] = [{'name': 'shared_buffers', 'value': '256MB', 'type': 'static'}]
        response = self.app.post('/v1/parameters', json.dumps(body), expect_errors=True)

        self.assertEquals(response.status_int, 400)
        self.assertIn('Empty or incorrect request body', response.body)

    def test_set_parameter_throw_exception_if_parameter_not_found(self):
        self.helper_create_conf_file_copy('./ga/tests/test-postgres.conf')

        body = {}
        body['parameters'] = [
            {'name': 'not_parameter', 'value': 'random', 'type': 'static'},  # Doesn't exist in test-postgres.conf
        ]

        response = self.app.post('/v1/parameters', json.dumps(body), expect_errors=True)

        self.assertEquals(
            500,
            response.status_int
        )
        self.assertIn(
            "Expected to find parameter {0} in conf file (commented or not)".format(body['parameters'][0]['name']),
            response.body)
