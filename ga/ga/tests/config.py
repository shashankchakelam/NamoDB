# Server Specific Configurations
server = {
    'port': '8080',
    'host': '0.0.0.0'
}

# Pecan Application Configurations
app = {
    'root': 'ga.controllers.root.RootController',
    'modules': ['ga'],
    'static_root': '%(confdir)s/../../public',
    'template_path': '%(confdir)s/../templates',
    'hooks': lambda: [__import__('ga.common.RequestIdPecanHook', fromlist=["RequestIdHook"]).RequestIdHook()], # The hook needs to be iterable and must implement/subclass from PecanHook
    'debug': True,
    'errors': {
        '404': '/error/404',
        '__force_dict__': True
    }
}

file_paths = {
    'db_install_state': '/home/ubuntu/ga-state/install',
    'image_info' : 'image-info.json',
    'mysql_image_info' : 'mysql-image-info.json',
    'postgresql_image_info': 'postgresql-image-info.json'
}

backup = {
    'backup_directory': './Backup',
    'freeze_directory': '.',
    'verify_ssl': False,
    'unfreeze_timeout': 1
}

my_conf_location = {
    'location': 'test.cnf',
    'temp_location': 'test_temp.cnf',
}
