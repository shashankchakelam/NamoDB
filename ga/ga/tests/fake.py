import json
import uuid

"""
Parameter to decide which
database-engine is being tested
and setting the image-info-dict-from-json
accordingly
"""
database_engine = 'mysql'

def create_snapshot(volume_id):
    create_snapshot_response = {}
    create_snapshot_response['status'] = 'pending'
    create_snapshot_response['snapshotId'] = str(uuid.uuid4())
    return_value = {}
    return_value['CreateSnapshotResponse'] = create_snapshot_response
    return  return_value


def describe_volumes(**options):
    describe_volumes_response = {}
    describe_volumes_response['status'] = 'in-use'
    return_value = {}
    return_value['DescribeVolumesResponse'] = {}
    return_value['DescribeVolumesResponse']['volumeSet'] = [describe_volumes_response]
    return return_value

def get_mysql_image_info():
    dict = {}
    dict["database_engine"]= "mysql"
    dict["database_engine_version"]= "5.6"
    dict["database_engine_conf_file_path"]= "test.cnf"
    return dict

def get_postgresql_image_info():
    dict = {}
    dict["database_engine"]= "postgresql"
    dict["database_engine_version"]= "9.5"
    dict["database_engine_conf_file_path"]= "test.cnf"
    return dict

def fake_get_image_info():
    if database_engine == 'mysql':
        return get_mysql_image_info()
    elif database_engine == 'postgresql':
        return get_postgresql_image_info()
    else :
        return {}

def get_with_status_code_200_success(cinder_show_url,
                                     verify,
                                     headers):
    response = Response()
    response.status_code = 200
    result = {}
    result['volume'] = {}
    result['volume']['id'] = 'test'
    result['volume']['status'] = 'in-use'
    response.content = json.dumps(result)
    return response

def get_with_status_code_200_failure(cinder_show_url,
                                     verify,
                                     headers):
    response = Response()
    response.status_code = 200
    result = {}
    result['volume'] = {}
    result['volume']['id'] = 'test'
    result['volume']['status'] = 'backup'
    response.content = json.dumps(result)
    return response


class Response:
    def __init__(self):
        self.status_code = 202
        self.content = ''
