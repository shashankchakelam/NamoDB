import os
import fake
import mock
import json
import pecan
import shutil
import uuid
from ga import utils
from ga.tests import FunctionalTest
from pecan import set_config
from pecan.testing import load_test_app

class TestRootController(FunctionalTest):

    """
    Based on the database_engine parameter
    in fake.py the app is loaded and the
    corresponding image-info-dict-from-json
    is returned setting the controller
    for corresponding database_engine correctly
    """
    @mock.patch('ga.utils.get_image_info_dict_from_json',fake.fake_get_image_info)
    def setUp(self):
        self.app = load_test_app(os.path.join(
            os.path.dirname(__file__),
            'config.py'
        ))

    def tearDown(self):
        set_config({}, overwrite=True)

    # Check invalid URLs, malformed JSON, etc.

    def test_absent_urls(self):
        response = self.app.get('/v1/', expect_errors=True)
        assert response.status_int == 400 # TODO: raise 404? How?

        response = self.app.get('/', expect_errors=True)
        assert response.status_int == 404

        response = self.app.get('/v1', expect_errors=True)
        assert response.status_int == 302 # Found redirection

        response = self.app.get('/a/bogus/url', expect_errors=True)
        assert response.status_int == 404

        response = self.app.get('/v1/bogus/url', expect_errors=True)
        assert response.status_int == 404

    def test_service_malformed_post(self):
        # Improperly specified arguments
        response = self.app.post('/v1/service', params={'blah': 'blah'}, expect_errors=True)
        assert response.status_int == 400 # Malformed request

        response = self.app.post('/v1/service', params={'{"blah": "blah"}': ''}, expect_errors=True)
        assert response.status_int == 422 # Malformed request

        # No param specified
        response = self.app.post('/v1/service', params={}, expect_errors=True)
        assert response.status_int == 400 # Malformed request

        # Invalid value for 'action'
        response = self.app.post('/v1/service', params={'{"action": "blah"}': ''}, expect_errors=True)
        assert response.status_int == 422 # Unprocessable entity

    # NOTE: this test is not basic sanity test. We should move it to
    # the directory/file where we are actually doing functional tests
    def test_service_get(self):
        response = self.app.get('/v1/service')
        self._helper_validate_req_id(response)
        assert response.status_int == 200



    @mock.patch('subprocess.check_output')
    def test_service_get_errorindb(self,mock_checkoutput):
        mock_checkoutput.return_value = 'blah blah'
        response=self.app.get('/v1/service')
        self._helper_validate_req_id(response)
        assert response.status_int == 200
        result = json.loads(response.body)
        assert result["status"] == 'unknown'


    @mock.patch('ga.backup._freeze')
    @mock.patch('ga.backup._unfreeze')
    @mock.patch('traceback.print_exc')
    def test_backup_POST_cinder_create_failure_connection_error(self,
                                                                mock_freeze,
                                                                mock_unfreeze,
                                                                mock_traceback):
        body = {}
        body['access_key'] = 'dummy'
        body['secret_key'] = 'dummy'
        body['backup_id'] = str(uuid.uuid4())
        body['compute_url'] = 'http://localhost:3000'
        body['vpc_url'] = 'http://localhost:3000'
        body['volume_id'] = str(uuid.uuid4())
        body['block_time'] = '1'

        response = self.app.post('/v1/backup', json.dumps(body))
        assert response.status_int == 202
        result = {}
        while True:
            response = self.app.get('/v1/backup/' + body['backup_id'])
            self._helper_validate_req_id(response)
            if response.body != '""':
                result = json.loads(response.body)
                if result['status'] == 'BACKUP_FAILED':
                        break
        assert result['connection-error'] == 'yes'
        shutil.rmtree(pecan.conf.backup['backup_directory'])


    @mock.patch('ga.backup._freeze')
    @mock.patch('ga.backup._unfreeze')
    @mock.patch('requests.get', side_effect=Exception('other error'))
    @mock.patch('traceback.print_exc')
    def test_backup_POST_cinder_create_failure_other_error(self,
                                                           mock_freeze,
                                                           mock_unfreeze,
                                                           mock_traceback,
                                                           mock_request_post):
        body = {}
        body['access_key'] = ''
        body['secret_key'] = 'http://localhost:3000'
        body['backup_id'] = str(uuid.uuid4())
        body['compute_url'] = 'http://localhost:3000'
        body['vpc_url'] = 'http://localhost:3000'
        body['volume_id'] = str(uuid.uuid4())
        body['block_time'] = '1'
        response = self.app.post('/v1/backup', json.dumps(body))
        self._helper_validate_req_id(response)
        assert response.status_int == 202
        result = {}
        while True:
            response = self.app.get('/v1/backup/' + body['backup_id'])
            if response.body != '""':
                result = json.loads(response.body)
                if result['status'] == 'BACKUP_FAILED':
                        break
        assert result['other-error'] == 'yes'
        shutil.rmtree(pecan.conf.backup['backup_directory'])

    """ TODO Need to Figure out how to fix this
    @mock.patch('ga.backup._freeze')
    @mock.patch('ga.backup._unfreeze')
    @mock.patch('requests.post', fake.backup_post_with_success)
    @mock.patch('traceback.print_exc')
    def test_backup_POST_cinder_status_connection_error(self,
                                                        mock_freeze,
                                                        mock_unfreeze,
                                                        mock_traceback):
        body = {}
        body['authtoken'] = ''
        body['cinder_show_url'] = 'http://localhost:3000'
        body['backup_id'] = str(uuid.uuid4())
        body['backup_create_url'] = 'http://localhost:3001/'
        body['block_time'] = '1'
        backup = {}
        backup['volume_id'] = ''
        body['backup_create_params'] = {"backup": backup}
        response = self.app.post('/v1/backup', json.dumps(body))
        assert response.status_int == 202
        result = {}
        while True:
            response = self.app.get('/v1/backup/' + body['backup_id'])
            print 'rushi'
            if response.body != '""':
                result = json.loads(response.body)
                if result['status'] == 'BACKUP_FAILED':
                        break
        assert result['connection-error'] == 'yes'
        shutil.rmtree(pecan.conf.backup['backup_directory'])
    """


    @mock.patch('ga.backup._freeze')
    @mock.patch('ga.backup._unfreeze')
    @mock.patch('client.cloud.create_snapshot', fake.create_snapshot)
    @mock.patch('client.cloud.describe_volumes', fake.describe_volumes)
    @mock.patch('requests.get', fake.get_with_status_code_200_success)
    @mock.patch('traceback.print_exc')
    def test_backup_POST_success(self,
                                 mock_freeze,
                                 mock_unfreeze,
                                 mock_traceback):
        body = {}
        body['access_key'] = 'dummy'
        body['secret_key'] = 'dummy'
        body['backup_id'] = str(uuid.uuid4())
        body['compute_url'] = 'http://localhost:3000'
        body['vpc_url'] = 'http://localhost:3000'
        body['volume_id'] = str(uuid.uuid4())
        body['block_time'] = '1'
        response = self.app.post('/v1/backup', json.dumps(body))
        assert response.status_int == 202
        result = {}
        while True:
            response = self.app.get('/v1/backup/' + body['backup_id'])
            self._helper_validate_req_id(response)
            if response.body != '""':
                result = json.loads(response.body)
                if result['status'] == 'BACKUP_SUCCESS':
                        break
        shutil.rmtree(pecan.conf.backup['backup_directory'])


    @mock.patch('subprocess.Popen')
    def test_crash_recovery_POST(self, mock_Popen):
        response = self.app.post('/v1/crash_recovery')
        self._helper_validate_req_id(response)
        self.assertEqual(response.status_code, 202)

    @mock.patch('ga.controllers.base_controller.BaseController._dict_from_file')
    def test_crash_recovery(self, mock_dict):
        mock_dict.return_value = {'status': 'pending'}
        response = self.app.get('/v1/crash_recovery')
        self._helper_validate_req_id(response)
        self.assertEqual(json.loads(response.body), mock_dict.return_value)

    def test_unique_request_id(self):
       req_dict = {}
       for x in range(0, 100):
            response = self.app.get('/v1/service')
            req_id = self._helper_validate_req_id(response)
            if req_dict.get(req_id) is None:
                req_dict[req_id] = x
            else:
                self.fail("In iteration %s : A request id [%s] already exists from iteration # %s"%(x, req_id, req_dict[req_id]))

    def helper_create_conf_file_copy(self, conf_file_path):
        copy_command = 'sudo cp {0} {1}'.format(conf_file_path, pecan.conf.my_conf_location['location'])
        os.system(copy_command)
        self.addCleanup(utils.safe_delete_file, pecan.conf.my_conf_location['location'])
        grant_access_to_file_command = 'sudo chmod -R 777 %s' % pecan.conf.my_conf_location['location']
        os.system(grant_access_to_file_command)

    def helper_get_parameter_from_conf_file(self, param_name):
        param_value = None

        with open(pecan.conf.my_conf_location['location'], 'r') as f:
            filedata = f.readlines()
            for line in filedata:
                if (line.startswith(param_name)):
                    start = line.find('=') + 1
                    param_value = line[start:].strip()
                    break

        return param_value

    def _helper_validate_req_id(
            self,
            response):
        req_id = response.headers.get('x-jcs-rds-RequestId')
        self.assertTrue(len(req_id) >= 16, "Length of request_id %s is less than 16"%(req_id))
        return req_id
