import os
import fake
import mock
import json
import pecan
from ga.tests.test_functional import TestRootController
from ga import utils


"""
Setting the database_engine parameter
for running the test cases for
mysql. It is used to mock the
ga.utils.get_image_info_dict_from_json
function for corresponding database_engine
"""
fake.database_engine = 'mysql'

class MysqlTestRootController(TestRootController):

    def test_port_change_mysql(self):
        body = {}
        body['port'] = 12345
        self.helper_create_conf_file_copy('./ga/tests/test-mysql.cnf')
        self.app.put('/v1/set_port', json.dumps(body))
        port = self.helper_get_parameter_from_conf_file('port')
        self.assertEquals(port , '12345')

    def test_port_remains_same_mysql(self):
        body={}
        body['port'] = 3306
        self.helper_create_conf_file_copy('./ga/tests/test-mysql.cnf')
        self.app.put('/v1/set_port', json.dumps(body))
        port = self.helper_get_parameter_from_conf_file('port')
        self.assertEquals (port , '3306')

    @mock.patch('subprocess.check_output')
    def test_service_get_mysql_running(self,mock_checkoutput):
        mock_checkoutput.return_value = 'mysql start/running, process 1694'
        response = self.app.get('/v1/service')
        self._helper_validate_req_id(response)
        assert response.status_int == 200
        result = json.loads(response.body)
        assert result["status"] == 'start/running'

    @mock.patch('subprocess.check_output')
    def test_service_get_mysql_notrunning(self, mock_checkoutput):
        mock_checkoutput.return_value = 'mysql stop/waiting'
        response = self.app.get('/v1/service')
        self._helper_validate_req_id(response)
        assert response.status_int == 200
        result = json.loads(response.body)
        assert result["status"] == 'stop/waiting'

    def test_set_parameter_happy_cases(self):
        self.helper_create_conf_file_copy('./ga/tests/test-mysql.cnf')

        body = {}
        body['parameters'] = [
            {'name': 'innodb_buffer_pool_size', 'value': 50000.0, 'type': 'static'}, # Doesn't exist in test-mysql.cnf
            {'name': 'thread_stack', 'value': '256K', 'type': 'dynamic'},            # Exists in test-mysql.cnf
            {'name': 'thread_concurrency', 'value': 12, 'type': 'static'},           # Commented out in test-mysql.cnf
            {'name': 'prompt', 'value': "'\u@\h [\d]'", 'type': 'dynamic'},          # Parameter contains single quotes
            {'name': 'wsrep_node_name', 'value': "\"Node A\"", 'type': 'static'},    # Parameter contains double quotes
        ]

        self.app.post('/v1/parameters', json.dumps(body))

        # Validate the contents of the conf file
        innodb_buffer_pool_size = self.helper_get_parameter_from_conf_file('innodb_buffer_pool_size')
        self.assertEquals(innodb_buffer_pool_size, '50000.0')

        thread_stack = self.helper_get_parameter_from_conf_file('thread_stack')
        self.assertEquals(thread_stack, '256K')

        thread_concurrency = self.helper_get_parameter_from_conf_file('thread_concurrency')
        self.assertEquals(thread_concurrency, '12')

        prompt = self.helper_get_parameter_from_conf_file('prompt')
        self.assertEquals(prompt, "'\u@\h [\d]'")

        wsrep_node_name = self.helper_get_parameter_from_conf_file('wsrep_node_name')
        self.assertEquals(wsrep_node_name, "\"Node A\"")

    def test_set_parameter_empty_request_body(self):
        self.helper_create_conf_file_copy('./ga/tests/test-mysql.cnf')

        body = {}
        response = self.app.post('/v1/parameters', json.dumps(body), expect_errors=True)

        self.assertEquals(response.status_int, 400)
        self.assertIn('Empty or incorrect request body', response.body)

    def test_set_parameter_incorrect_request_body(self):
        self.helper_create_conf_file_copy('./ga/tests/test-mysql.cnf')

        body = {}
        body['parameter'] = [{'name': 'innodb_buffer_pool_size', 'value': '50000', 'type': 'static'}]
        response = self.app.post('/v1/parameters', json.dumps(body), expect_errors=True)

        self.assertEquals(response.status_int, 400)
        self.assertIn('Empty or incorrect request body', response.body)
