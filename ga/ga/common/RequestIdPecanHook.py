import pecan
from pecan.hooks import PecanHook
from ga.common import CustomThreadLocal as tl

class RequestIdHook(PecanHook):

    # Pecan Hook needs a priority based on which it sorts the list of hooks
    priority = 100

    def before(self, state):
        # Add anything that needs to be done before the api call here
        tl.CustomThreadLocalClass().clear_req_id()
        if 'x-jcs-rds-RequestId' in pecan.request.headers and \
            pecan.request.headers['x-jcs-rds-RequestId'] is not None :
                tl.CustomThreadLocalClass().set_req_id(req_id)
        else:
            tl.CustomThreadLocalClass().create_req_id()
    
    def after(self, state):
        # Clear the reqid from ThreadLocal so that next request (by the same thread) does not get the same request id
        pecan.response.headers['x-jcs-rds-RequestId'] = tl.CustomThreadLocalClass().get_req_id()

    def on_error(self, state, exc):
        pecan.response.headers['x-jcs-rds-RequestId'] = tl.CustomThreadLocalClass().get_req_id()

