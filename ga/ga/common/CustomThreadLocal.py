import logging
import threading
import uuid
from threading import current_thread

class CustomThreadLocalClass:
    threadLocal = threading.local()

    def set_req_id(self, new_req_id):
        self.threadLocal.req_id = newReqId

    def create_req_id(self):
        req_id = getattr(self.threadLocal, 'req_id', None)
        if req_id is None:
            self.threadLocal.req_id = str (uuid.uuid1())
        return self.threadLocal.req_id

    # Returns null if req_id is not set
    def get_req_id(self):
        return getattr(self.threadLocal, 'req_id', None)

    def clear_req_id(self):
        self.threadLocal.req_id = None
