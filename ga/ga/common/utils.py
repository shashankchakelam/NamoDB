import errno
import os


# create directory if it doest not exists
def create_directory(dir_path):
    try:
       os.makedirs(dir_path)
    except OSError as exc:
       if exc.errno == errno.EEXIST and os.path.isdir(dir_path):
           pass
       else:
           raise


# check if directory exists if not create it
# and then create a file if it does not exists
def create_file_and_get_path(dir_path,
                             filename):
    backup_status_file = dir_path + '/' + filename
    create_directory(dir_path)
    open(backup_status_file, 'a').close()
    return backup_status_file


# Given a file path and no of lines
# this function will fetch those number of lines from end
def tail(filepath, lines=1):
    f = open(filepath, 'r')
    total_lines_wanted = lines
    block_size = 1024
    f.seek(0, 2)
    block_end_byte = f.tell()
    lines_to_go = total_lines_wanted
    block_number = -1
    blocks = []  # blocks of size block_size, in reverse order starting from the end of the file
    while lines_to_go > 0 and block_end_byte > 0:
        if block_end_byte - block_size > 0:
            # read the last block we haven't yet read
            f.seek(block_number*block_size, 2)
            blocks.append(f.read(block_size))
        else:
            # file too small, start from begining
            f.seek(0,0)
            # only read what was not read
            blocks.append(f.read(block_end_byte))
        lines_found = blocks[-1].count('\n')
        lines_to_go -= lines_found
        block_end_byte -= block_size
        block_number -= 1
    all_read_text = ''.join(reversed(blocks))
    f.close()
    return '\n'.join(all_read_text.splitlines()[-total_lines_wanted:])


def get_auth_headers(authtoken):
    auth = {}
    auth['X-Auth-Token'] = authtoken
    auth['Content-Type'] = 'application/json'
    return auth
