from ga import utils
from ga.controllers.guest_agent_mysql_controller import GuestAgentMysqlController
from ga.controllers.guest_agent_postgresql_controller import GuestAgentPostgresqlController


class ControllerFactory(object):
    @classmethod
    def getController(cls):
        image_info_dict_from_json = utils.get_image_info_dict_from_json()
        database_engine = str(image_info_dict_from_json["database_engine"])
        if database_engine == "mysql":
            return GuestAgentMysqlController()
        elif database_engine == "postgresql":
            return GuestAgentPostgresqlController()
        else:
            assert 0, "Bad Database Engine specified: %s" % cls.database_engine

