import json
import logging
import pecan
import subprocess
from ga import backup
from ga import utils
from ga.services import db_log_service
from ga.common import utils as common_utils


LOG = logging.getLogger(__name__)
"""
Base Controller Class:
Contains common functions handling
urls for all database-engines
"""
class BaseController(object):

    def __init__(self):
        super(BaseController,self).__init__()
        image_info_dict_from_json = utils.get_image_info_dict_from_json()

        self.database_engine = str(image_info_dict_from_json["database_engine"])
        self.database_engine_version = str(image_info_dict_from_json["database_engine_version"])
        self.database_engine_conf_file_path = str(image_info_dict_from_json["database_engine_conf_file_path"])

    @pecan.expose(generic=True, template='json')
    def index(self):
        self._log_request()
        pecan.abort(400)

    # region db logs methods

    @pecan.expose(generic=True, template='json')
    def check_instance_service_health(self):
        self._log_request()
        try:
            db_log_service.check_instance_service_health()
            return {'status': 'UP'}
        except Exception as e:
            LOG.info("Instance service is not up")
            return {'status': 'DOWN'}

    @pecan.expose(generic=True, method='POST', template='json')
    def start_db_logs_tasks(self):
        self._log_request()
        try:
            params = self._get_json_dict(pecan.request.POST)
            bucket_name = params['bucket_name']
            log_retention_time = params['log_retention_period']
            bucket_prefix = params['bucket_prefix']
            db_log_service.start_db_logs_tasks(bucket_name, log_retention_time, bucket_prefix)
            pecan.response.status_code = 202
            return {'status': 'ok'}
        except Exception as e:
            LOG.exception("Error while starting db log tasks" + e.message)
            return pecan.abort(500)

    @pecan.expose(generic=True, method='POST', template='json')
    def update_db_log_retention_time(self):
        self._log_request()
        try:
            params = self._get_json_dict(pecan.request.POST)
            retention_time = params['log_retention_period']
            db_log_service.update_db_log_retention_time(retention_time)
            pecan.response.status_code = 202
            return {'status': 'ok'}
        except Exception as e:
            LOG.exception("Error while updating db log retention time" + e.message)
            return pecan.abort(500)

    @pecan.expose(generic=True, method='POST', template='json')
    def pause_db_logs_tasks(self):
        self._log_request()
        try:
            db_log_service.pause_db_logs_tasks()
            pecan.response.status_code = 202
            return {'status': 'ok'}
        except Exception as e:
            LOG.exception("Error while pausing db logs tasks" + e.message)
            return pecan.abort(500)

    @pecan.expose(generic=True, method='POST', template='json')
    def resume_db_logs_tasks(self):
        self._log_request()
        try:
            db_log_service.resume_db_logs_tasks()
            pecan.response.status_code = 202
            return {'status': 'ok'}
        except Exception as e:
            LOG.exception("Error while resuming db logs tasks" + e.message)
            return pecan.abort(500)

    @pecan.expose(generic=True, method='POST', template='json')
    def update_db_log_bucket(self):
        self._log_request()
        try:
            params = self._get_json_dict(pecan.request.POST)
            bucket_name = params['bucket_name']
            db_log_service.update_db_log_bucket(bucket_name)
            pecan.response.status_code = 202
            return {'status': 'ok'}
        except Exception as e:
            LOG.exception("Error while updating db log bucket" + e.message)
            return pecan.abort(500)

    @pecan.expose(generic=True, method='POST', template='json')
    def upload_db_logs_instantly(self):
        self._log_request()
        try:
            db_log_service.upload_db_logs_instantly()
            pecan.response.status_code = 202
            return {'status': 'ok'}
        except Exception as e:
            LOG.exception("Error while uploading db logs instantly" + e.message)
            return pecan.abort(500)
    # endregion

    def _get_json_dict(self, multidict):
        json_str = ''
        for k in multidict.keys():
            json_str = k
            break
        try:
            return json.loads(json_str)
        except ValueError:
            pecan.abort(400)

    def _service_POST(self):
        self._log_request()

        req_dict = self._get_json_dict(pecan.request.POST)
        # NOTE(rushiagr): we're not checking if start/stop/restart failed
        if req_dict.get('action') in ['start', 'stop', 'restart']:
            subprocess.Popen(['sudo', 'service', self.database_engine, req_dict['action']])
        else:
            # Raise unprocessable entity error
            pecan.abort(422)

        pecan.response.status_code = 202
        return {'status': 'ok'}


    @pecan.expose(generic=True, template='json')
    def format(self):
        self._log_request()
        # install-state for database-type are in file given by mysql_install_state
        return self._dict_from_file(pecan.conf.file_paths['db_install_state'])


    @format.when(method='POST', template='json')
    def format_POST(self):
        self._log_request()
        # TODO(rushiagr): Instead of calling a shell script, call a python script.
        # That will allow us better control when the script throws an error midway

        # Format script path for database-type specified in :_image_info
        subprocess.Popen('/usr/bin/format_' + self.database_engine + '.sh ' + self.database_engine_version, shell=True)

        pecan.response.status_code = 202
        return {'status': 'ok'}


    @pecan.expose(generic=True, template='json')
    def versioninfo(self):
        self._log_request()
        return dict(pecan.conf.version_info)


    @pecan.expose(generic=True, template='json')
    def upgrade(self):
        self._log_request()
        pecan.abort(400)


    @upgrade.when(method='POST', template='json')
    def upgrade_POST(self):
        self._log_request()
        req_dict = self._get_json_dict(pecan.request.POST)
        pkg_url = req_dict.get('pkg_url')
        # TODO(rushiagr): Instead of calling a shell script, call a python script.
        # That will allow us better control when the script throws an error midway
        subprocess.Popen('/usr/bin/upgrade.sh %s' % pkg_url, shell=True)

        pecan.response.status_code = 202
        return {'status': 'ok'}


    @pecan.expose(generic=True, template='json')
    def backup(self, backup_id):
        self._log_request()
        filename = backup_id  # we are using backup_id as a file name
        backup_status_file = common_utils.create_file_and_get_path(pecan.conf.backup['backup_directory'],
                                                                   filename)
        line = common_utils.tail(backup_status_file, 1)
        try:
            if line == '':
                return line
            return json.loads(line)
        except ValueError:
            pecan.abort(500)


    @backup.when(method='POST', template='json')
    def backup_POST(self):
        self._log_request()
        try:
            body = self._get_json_dict(pecan.request.POST)
            file_name = str(body['backup_id'])
            access_key = body['access_key']
            secret_key = body['secret_key']
            compute_url = body['compute_url']
            vpc_url = body['vpc_url']
            volume_id = body['volume_id']
            block_time = body['block_time']
            backup_status_file = common_utils.create_file_and_get_path(pecan.conf.backup['backup_directory'],
                                                                       file_name)

            backup.start_backup_process(backup_status_file,
                                        access_key,
                                        secret_key,
                                        compute_url,
                                        vpc_url,
                                        volume_id,
                                        int(block_time))

            pecan.response.status_code = 202
            return {'status': 'ok'}
        except KeyError:
            return pecan.abort(500)
        except Exception:
            LOG.exception("Backup_POST failed")
            return pecan.abort(500)

    @pecan.expose(generic=True, template='json')
    def crash_recovery(self):
        self._log_request()
        return self._dict_from_file(pecan.conf.file_paths['db_install_state'])


    @crash_recovery.when(method='POST', template='json')
    def crash_recovery_POST(self):
        self._log_request()
        # Restore script path for both database-type specified in :_image_info
        subprocess.Popen('/usr/bin/restore_' + self.database_engine + '.sh '+ self.database_engine_version, shell=True)
        pecan.response.status_code = 202
        return {'status': 'ok'}

    @pecan.expose(method='PUT',template='json')
    def set_port(self):
        self._log_request()
        body=self._get_json_dict(pecan.request.POST)
        utils.set_port(body['port'],self.database_engine_conf_file_path)
        utils.restart_db_services(self.database_engine)



    #########################################
    ############Helper Methods###############
    #########################################
    def _log_request(self):
        LOG.debug(
            'METHOD: %s, CONTENT_TYPE: %s, URL: %s, HEADERS: %s, BODY: %s',
            pecan.request.method,
            pecan.request.content_type,
            pecan.request.url,
            pecan.request.headers.items(),
            pecan.request.body
        )


    def _dict_from_file(self, filepath):
        d = {}
        try:
            with open(filepath, 'r') as f:
                lines = f.readlines()
                for line in lines:
                    key, val = line.strip().split('=')
                    d[key] = val
        except IOError:
            # 'format' operation not yet started
            d['status'] = 'pending'

        return d

    def generate_response(self, body=None, status_code=200, content_type='application/json'):
        """
        Generate API-WS HTTP Response

        Parameters
        ----------
        body: HTTP Response Body
        status_code: HTTP Response Status Code
        content_type: HTTP Response Content Type

        Returns
        -------
        Formatted HTTP Response
        """

        return pecan.Response(
            body,
            status_code,
            None,
            None,
            content_type
        )
