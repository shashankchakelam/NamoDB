import pecan
import subprocess
import logging
from ga.controllers.base_controller import BaseController
from ga import utils

LOG = logging.getLogger(__name__)
###############################################################
################   CONTROLLER CLASS FOR MYSQL   ###############
###############################################################
"""
Derived from GuestAgentRootController
Contains additionally the functions
handling urls specific to the mysql
"""
class GuestAgentMysqlController(BaseController):

    @pecan.expose(generic=True, template='json')
    def service(self):
        self._log_request()
        try:
            ret_str = subprocess.check_output(['sudo', 'service', self.database_engine, 'status'])
        except subprocess.CalledProcessError as _error:
            return {'status': 'unknown'}
        # ret_str = str(ret_str, 'utf-8') # Bytes to string conversion (Py3)

        # Status based on database-status-query result format
        # MySQL     : mysql start/running, process 1578
        ret_str = ret_str.strip()
        if ret_str.startswith(self.database_engine + ' stop/waiting'):
            return {'status': 'stop/waiting'}
        elif ret_str.startswith(self.database_engine + ' start/running'):
            return {'status': 'start/running'}
        else:
            return {'status': 'unknown'}

    @service.when(method='POST', template='json')
    def service_POST(self):
        self._service_POST()

    @pecan.expose(generic=True, method='POST', template='json')
    def master_user(self):
        self._log_request()
        try:
            body = self._get_json_dict(pecan.request.POST)

            #Adding user from the body of post request
            add_user_sql_command = ("CREATE USER '%s'@'%%%%' IDENTIFIED BY '%s';") % (body['MasterUsername'], body['MasterUserPassword'])
            utils.execute_command(add_user_sql_command, pecan.conf.database['mysql_connection'])

            #Giving privileges to above created user
            privileges = pecan.conf.mysql_grant_privileges
            grant_privileges = ', '.join(privileges)
            give_privileges_sql_command = ("GRANT %s ON *.* TO '%s'@'%%%%';") % (grant_privileges,body['MasterUsername'])
            utils.execute_command(give_privileges_sql_command, pecan.conf.database['mysql_connection'])

        except Exception as e:
            LOG.exception("Create Master User failed")
            raise

    @pecan.expose(generic=True, template='json')
    def parameters(self):
        '''
        Get the current value of specific parameters
        '''
        raise NotImplementedError()

    @parameters.when(method='POST', template='json')
    def parameters_POST(self):
        '''
        API to set value of parameters in the database engine's conf file. Request body contains a lift of parameter
        dictionaries that are to be set.
        '''
        self._log_request()
        body = self._get_json_dict(pecan.request.POST)
        if not body or 'parameters' not in body:
            pecan.abort(400, detail='Empty or incorrect request body')

        # Read contents of my.cnf file
        # Open file in read mode since we need to read the contents of the file
        LOG.debug('Reading contents of conf file: %s', self.database_engine_conf_file_path)
        with open(self.database_engine_conf_file_path, 'r') as conf_file:
            contents = conf_file.readlines()

        # Parse sections to get limits of the mysqld section
        section_indices = [i for i, line in enumerate(contents) if line.startswith('[')]

        # Find start and end index of the [mysqld] section. Note that a few parameters may need to be written in other
        # sections. However, all currently known ones are exclusively in the [mysqld] section
        mysqld_start_index = -1
        mysqld_end_index = -1

        for i, index in enumerate(section_indices):
            if '[mysqld]' in contents[index]:
                mysqld_start_index = index

                # Checking to see if [mysqld] is the last section or not. If it is the last section, then set
                # mysqld_end_index to be the end of the file, else make it one line before the next section starts
                if i != len(section_indices) - 1:
                    mysqld_end_index = section_indices[i+1] - 1
                else:
                    mysqld_end_index = len(contents) - 1

        LOG.debug('Section [mysqld] start and end line numbers: %d %d', mysqld_start_index + 1, mysqld_end_index + 1)

        # For each parameter, check if it already exists. If so, update the value. Else, insert a new line
        for parameter in body['parameters']:
            param_exists = False
            param_line_to_insert = parameter['name'] + '\t= ' + str(parameter['value']) + '\n'
            for i in range(mysqld_start_index, mysqld_end_index):
                # TODO(shank): Make more resilient for scenarios where one parameter is a prefix of another
                if contents[i].startswith(parameter['name']):
                    LOG.debug('Updating value of existing parameter %s to %s', parameter['name'], parameter['value'])
                    param_exists = True
                    contents[i] = param_line_to_insert

            if param_exists is False:
                LOG.debug('Inserting parameter %s with value %s', parameter['name'], parameter['value'])
                contents.insert(mysqld_end_index, param_line_to_insert)

        # Write contents back to the file and close it
        LOG.debug('Overriding conf file with updated parameter values')
        with open(self.database_engine_conf_file_path, 'w') as conf_file:
            conf_file.writelines(contents)

        # Return success
        pecan.response.status_code = 200
        return
