import pecan
import subprocess
import logging
from ga.controllers.base_controller import BaseController
from ga import utils


LOG = logging.getLogger(__name__)
###############################################################
################CONTROLLER CLASS FOR POSTGRESQL################
###############################################################
"""
Derived from GuestAgentRootController
Contains additionally the functions
handling urls specific to the postgresql
"""
class GuestAgentPostgresqlController(BaseController):

    @pecan.expose(generic=True, template='json')
    def service(self):
        self._log_request()
        ''' postgresql returns error-exit status 3 with output,
            if an error occurs the response eventually is  "{'status': 'unknown'}"
        '''
        try:
            ret_str = subprocess.check_output(['sudo', 'service', self.database_engine, 'status'])
        except subprocess.CalledProcessError as _error:
            ret_str=_error.output
        # ret_str = str(ret_str, 'utf-8') # Bytes to string conversion (Py3)

        # Status based on database-status-query result format
        # PostgreSQL: 9.5/main (port 5432): "status-here"
        ret_str = ret_str.strip()
        if ret_str.endswith('down'):
             return {'status': 'stop/waiting'}
        elif ret_str.endswith('online'):
             return {'status': 'start/running'}
        else:
            return {'status': 'unknown'}

    @service.when(method='POST', template='json')
    def service_POST(self):
        self._service_POST()

    @pecan.expose(generic=True, method='POST', template='json')
    def master_user(self):
        self._log_request()
        try:
            body = self._get_json_dict(pecan.request.POST)

            #Adding role from the body of post request
            create_rds_superuser_command='CREATE ROLE rds_superuser;'
            utils.execute_command(create_rds_superuser_command, pecan.conf.database['postgres_connection'])
            create_master_user_command=('CREATE ROLE %s WITH PASSWORD \'%s\' VALID UNTIL \'infinity\';')%( body['MasterUsername'],body['MasterUserPassword'])
            utils.execute_command(create_master_user_command, pecan.conf.database['postgres_connection'])

            # granting privileges of rds_superuser to master-user for customer
            grant_rds_to_user_command=('BEGIN;GRANT rds_superuser TO %s ;COMMIT;')%body['MasterUsername']
            utils.execute_command(grant_rds_to_user_command, pecan.conf.database['postgres_connection'])
            privileges = pecan.conf.postgresql_grant_privileges
            grant_privileges = ' '.join(privileges)
            grant_privileges_to_user = ('ALTER ROLE %s WITH %s;')%(body['MasterUsername'],grant_privileges)
            utils.execute_command(grant_privileges_to_user, pecan.conf.database['postgres_connection'])

            # set owner of template1, postgres to the user
            alter_template1_owner_command = 'ALTER DATABASE template1 owner to %s;' % body['MasterUsername']
            utils.execute_command(alter_template1_owner_command, pecan.conf.database['postgres_connection'])
            alter_postgres_owner_command = 'ALTER DATABASE postgres owner to %s;' % body['MasterUsername']
            utils.execute_command(alter_postgres_owner_command, pecan.conf.database['postgres_connection'])

        except Exception as e:
            LOG.exception("Create Master User failed")
            raise

    @pecan.expose(generic=True, template='json')
    def parameters(self):
        '''
        Get the current value of specific parameters
        '''
        raise NotImplementedError()

    @parameters.when(method='POST', template='json')
    def parameters_POST(self):
        '''
        API to set value of parameters in the database engine's conf file. Request body contains a lift of parameter
        dictionaries that are to be set.
        '''
        self._log_request()
        body = self._get_json_dict(pecan.request.POST)
        if not body or 'parameters' not in body:
            pecan.abort(400, detail='Empty or incorrect request body')

        # Read contents of postgres.conf file
        # Open file in read mode since we need to read the contents of the file
        LOG.debug('Reading contents of conf file: %s', self.database_engine_conf_file_path)
        with open(self.database_engine_conf_file_path, 'r') as conf_file:
            contents = conf_file.readlines()

        # For each parameter, check if it is commented or uncommented. Either case, replace with the new line
        # contents. The main reason we're replacing the line instead of appending is to maintain the informal
        # format structure in the postgres.conf where parameters are grouped by type (not strictly). This is
        # just to improve readability
        for parameter in body['parameters']:
            param_exists = False
            param_line_to_insert = parameter['name'] + ' = ' + str(parameter['value']) + '\n'
            for index, line in enumerate(contents):
                # TODO(shank): Make more resilient for scenarios where one parameter is a prefix of anothers
                # Also, currently there's no space between '#' and param name but check for that too
                if line.startswith('#' + parameter['name']) or line.startswith(parameter['name']):
                    LOG.debug('Setting value of parameter %s to %s', parameter['name'], parameter['value'])
                    param_exists = True
                    contents[index] = param_line_to_insert

            if param_exists is False:
                raise ValueError('Expected to find parameter {0} in conf file (commented or not)'.format(parameter['name']))

        # Write contents back to the file and close it
        LOG.debug('Overriding conf file with updated parameter values')
        with open(self.database_engine_conf_file_path, 'w') as conf_file:
            conf_file.writelines(contents)

        # Return success
        pecan.response.status_code = 200
        return
