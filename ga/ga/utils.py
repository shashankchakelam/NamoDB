import errno
import os
import pecan
import subprocess
import json
from sqlalchemy import create_engine

####################################################################################
################# UTILITY HELPER FUNCTIONS FOR GUEST AGENT #########################
####################################################################################


def execute_command(command, connection_string):
    eng = create_engine(connection_string)
    with eng.connect() as cxn:
        cxn.execute(command)

def restart_db_services(database_engine):
    '''
    this function restarts db services and checks the status for
    start/running or online, raises an error if db failed to restart
    '''
    subprocess.check_output(['sudo', 'service', database_engine, 'restart'])
    database_engine_status = subprocess.check_output(['sudo', 'service', database_engine, 'status'])
    restart_status_fail = database_engine_status.find('start/running')==-1 and database_engine_status.find('online')==-1
    if (restart_status_fail):
        raise Exception (database_engine+' failed to restart')

def remove_port_from_string(line):
    '''
    This function takes input a string that starts with "port"
    and returns a string of the form "port =" , i.e., the original string without the actual port address                                       '''
    start = line.find('=') + 1
    pre_port = line[start:].strip()
    start_port = line.find(pre_port)
    return line[:start_port]

def get_port(line):
    '''
    This function returns the value of the port in the conf file in the form of a string
    '''
    start = line.find('=')+1
    end = line.find('#')               # to remove the comment-part if it exists
    port = line[start:end].strip()
    return port

def set_port(new_port,database_engine_conf_file_path):
    '''
    The following function reads the conf file, checks if the conf file's
    port is the different from the one specified by the user. If it is the same
    then the function simply returns without doing anything otherwise it changes
    the conf file and then restarts the mysql server.
    '''
    new_port = str(new_port)
    with open (database_engine_conf_file_path,'r') as f:
        filedata = f.readlines()
        port_found = 0
        for line in filedata:
        #checks if the previous port is the same as the new one, if it is then we don't do anything
            if (line.startswith('port')):
                port_found = 1
                if (new_port == get_port(line)):
                    return
                else:
                    break
    if(port_found == 0):
        raise Exception ('Port not found in config file')
    with open (database_engine_conf_file_path,'w') as f:
        for line in filedata:
            if (line.startswith('port')):
                line = remove_port_from_string(line)
                line += new_port
                line += '\n'
            f.write(line)

def safe_mkdirs(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

def safe_delete_file(filename):
    try:
        os.remove(filename)
    except OSError as e:
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise

def get_image_info_dict_from_json():
    try:
        with open(pecan.conf.file_paths['image_info'], 'r') as myfile:
            data = myfile.read().replace('\n', '')
        image_info_dict_from_json = json.loads(data)
    except:
        raise Exception ('image-info.json file is not present')
    return image_info_dict_from_json


def get_is_service_end_point():
    port = pecan.conf.is_service['port']
    host = pecan.conf.is_service['host']
    return 'http://%s:%s' % (host, port)
