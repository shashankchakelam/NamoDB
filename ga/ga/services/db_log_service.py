import json
import logging

import requests
from ga import utils

LOG = logging.getLogger(__name__)

"""
This is the service to do anything related to database log files.

"""


def start_db_logs_tasks(bucket_name, log_retention_time, bucket_prefix):
    """
    Start the db log tasks by calling the InstanceService.
    :param bucket_name: bucket name to which we need to publish the logs.
    :param log_retention_time: time till when we want our logs to be retained in the dss buckets.
    :return: response from the instance service
    """

    try:
        body = {'bucketName': bucket_name, 'logRetentionTime': log_retention_time, 'bucket_prefix': bucket_prefix }
        __do_post_request('/startLogTasks', body)
    except:
        LOG.exception("Error getting response from instance service")
        raise


def check_instance_service_health():
    try:
        return __do_get_request('/health')
    except:
        LOG.exception("Service is not up")
        raise


def update_db_log_retention_time(log_retention_time):
    """
    Update db logs retention time
    :param log_retention_time: time till when we want to retain our logs in dss bucket.
    :return: response from the instance service
    """
    try:
        body = {'logRetentionTime': log_retention_time}
        __do_post_request('/updateLogRetentionTime', body)
    except:
        LOG.exception("Error getting response from instance service")
        raise


def pause_db_logs_tasks():
    """
    Pause the db logs tasks:
    1.Uploading logs to dss
    2.Expire logs from dss.
    :return: response from the instance service
    """
    try:
        __do_post_request('/pauseLogTasks')
    except:
        LOG.exception("Error getting response from instance service")
        raise


def resume_db_logs_tasks():
    """
    Resume the db logs tasks:
    1.Uploading logs to dss
    2.Expire logs from dss.
    :return: response from the instance service
    """
    try:
        __do_post_request('/resumeLogTasks')
    except:
        LOG.exception("Error getting response from instance service")
        raise


def update_db_log_bucket(bucket_name):
    """
    Update the db log bucket name - where we want to send db logs in future.
    :param bucket_name: new bucket name where we want to send db logs in future.
    :return: response from the instance service
    """
    try:
        body = {'bucketName': bucket_name}
        __do_post_request('/updateLogBucket', body)
    except:
        LOG.exception("Error getting response from instance service")
        raise


def upload_db_logs_instantly():
    """
    Rotate and upload the logs instantly to DSS bucket.
    :return: response from the instance service
    """
    try:
        __do_post_request('/uploadLogsImmediately')
    except:
        LOG.exception("Error getting response from instance service")
        raise


# region Private functions

def __do_get_request(path):
    full_url = utils.get_is_service_end_point() + path
    resp = requests.get(full_url)
    LOG.info('status code of get resp is:' + str(resp.status_code))
    LOG.info('response message:' + resp.content)
    if resp.status_code >= 400:
        raise Exception(
            'Get request failed. Status code: ' + str(resp.status_code) + ' ,response content: ' + resp.content)
    return resp.content


def __do_post_request(path, body=None):
    """
    Helper method to send post request.
    :param path: endpoint path of instance service which we want to call.
    :param body: payload of post request.
    :return: response from instance service and in case of some error throw exception.
    """
    if body:
        LOG.info('body is' + str(body))
    full_url = utils.get_is_service_end_point() + path
    resp = requests.post(full_url, data=body)
    LOG.info('status code of post resp is:' + str(resp.status_code))
    LOG.info('response message:' + resp.content)
    if resp.status_code >= 400:
        raise Exception(
            'Post request failed. Status code: ' + str(resp.status_code) + ' ,response content: ' + resp.content)
    return resp

# endregion
