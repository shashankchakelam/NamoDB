import json
import logging
import pecan
import requests
import subprocess
import uuid
from client import cloud as jcs_client
from client import common
from datetime import datetime
from multiprocessing import TimeoutError
from multiprocessing.pool import ThreadPool

LOG = logging.getLogger(__name__)

# clears connection and other variables
def _clear_errors_variables(dictionary):
    dictionary['connection-error'] = 'no'
    dictionary['other-error'] = 'no'

# This function will make a https call to cinder ( TODO nova in future)
# If it gets desired result it will retur true else it will return false
# which mean back up is failed
def _cinder_create_backup(access_key,
                          secret_key,
                          compute_url,
                          vpc_url,
                          volume_id,
                          dictionary,
                          backup_status_file):

    try:
        _print_dictionary('BACKUP_CREATE_CALL_STARTING',
                          dictionary,
                          backup_status_file)
        LOG.info("Setting up JCS Client with: AccessKey: {0}, ComputeURL: {1}, VPCURL: {2}".
                  format(access_key, compute_url, vpc_url))
        common.setup_client(access_key,
                            secret_key,
                            compute_url,
                            vpc_url)
        LOG.info("Calling SBS CreateSnapshot for VolumeId: " + volume_id)
        resp_dict = jcs_client.create_snapshot(volume_id)
        LOG.info(str(resp_dict))
        dictionary['status_code'] = resp_dict['CreateSnapshotResponse']['status']
    except requests.exceptions.ConnectionError:
        LOG.exception("Connection error when calling CreateSnapshot")
        dictionary['connection-error'] = 'yes'
        _print_dictionary('BACKUP_CREATE_CALL_DONE_FAILURE',
                          dictionary,
                          backup_status_file)
        return False
    except Exception:
        LOG.exception("Unknown exception when calling CreateSnapshot")
        dictionary['other-error'] = 'yes'
        _print_dictionary('BACKUP_CREATE_CALL_DONE_FAILURE',
                          dictionary,
                          backup_status_file)
        return False

    if resp_dict['CreateSnapshotResponse']['status'] == 'pending':
        dictionary['backup_id'] = resp_dict['CreateSnapshotResponse']['snapshotId']
        _print_dictionary('BACKUP_CREATE_CALL_DONE_SUCCESS',
                          dictionary,
                          backup_status_file)
        success = True
    else:
        _print_dictionary('BACKUP_CREATE_CALL_DONE_FAILURE',
                          dictionary, backup_status_file)
        success = False
    return success


# In this function we are trying to check the state of the attached volume
# if the volume state changes to in-use from backup state then we consider backup is done
# else we keep checking the status till the state changes to in use or time allocated for backup is over
# based on the success or failure true or false will be send
def _check_cinder_volume_status(access_key,
                                secret_key,
                                compute_url,
                                vpc_url,
                                volume_id,
                                dictionary,
                                block_time,
                                freeze_start_date_time,
                                backup_status_file):
    while True:
        present_date_time = datetime.now()
        if (present_date_time - freeze_start_date_time).seconds > block_time:
            LOG.debug("Block Time Execeeded by " + str((present_date_time - freeze_start_date_time).seconds))
            _print_dictionary('BLOCK_TIME_EXCEEDED',
                          dictionary, backup_status_file)
            return False
        try:
            _print_dictionary('CHECK_VOLUME_STATUS_CALL_STARTING',
                              dictionary, backup_status_file)
            _clear_errors_variables(dictionary)
            LOG.info("Setting up JCS Client with: AccessKey: {0}, ComputeURL: {1}, VPCURL: {2}".
                  format(access_key, compute_url, vpc_url))
            common.setup_client(access_key,secret_key,
                                compute_url,
                                vpc_url)
            LOG.info("Calling SBS DescribeVolumes for VolumeId: " + volume_id)
            resp_dict = jcs_client.describe_volumes(VolumeId=volume_id)
            LOG.info(str(resp_dict))
            dictionary['status_code'] = resp_dict['DescribeVolumesResponse']['volumeSet'][0]['status']
            _clear_errors_variables(dictionary)
        except requests.exceptions.ConnectionError:
            LOG.exception("ConnectionError when calling DescribeVolumes")
            dictionary['connection-error'] = 'yes'
            _print_dictionary('CHECK_VOLUME_STATUS_CALL_FAILED',
                              dictionary, backup_status_file)
            continue

        except Exception:
            LOG.exception("Unknown exception when calling DescribeVolumes")
            dictionary['other-error'] = 'yes'
            _print_dictionary('CHECK_VOLUME_STATUS_CALL_FAILED',
                              dictionary, backup_status_file)
            continue

        if dictionary['status_code'] == 'in-use':
            _print_dictionary('CHECK_VOLUME_STATUS_CALL_DONE_SUCCESS',
                              dictionary, backup_status_file)
            return True
        else:
            _print_dictionary('CHECK_VOLUME_STATUS_CALL_DONE_FAILED',
                              dictionary, backup_status_file)
    return False


# prints the dictionary into file as json
def _print_dictionary(status,
                      dictionary,
                      backup_status_file):
    dictionary['status'] = status
    dictionary['time'] = str(datetime.now())
    fp = open(backup_status_file, 'a')
    fp.write(json.dumps(dictionary) + '\n')
    fp.close()


# Freezes the block volume with fsfreeze command
def _freeze():
    freeze_command = 'sudo fsfreeze -f ' + pecan.conf.backup['freeze_directory']
    LOG.info("Calling freeze command: " + freeze_command)
    subprocess.call([freeze_command], shell=True)


# Un freezes the block volume with fsfreeze command
def _unfreeze():
    unfreeze_command = 'sudo fsfreeze -u ' + pecan.conf.backup['freeze_directory']
    LOG.info("Calling unfreeze command: " + unfreeze_command)
    subprocess.call([unfreeze_command], shell=True)


# checks if un freeze done by create a random file and deleting it
def _check_unfreeze_done(result):
    file_name = pecan.conf.backup['freeze_directory'] + str(uuid.uuid1())
    create_command = 'touch ' + file_name
    delete_command = 'rm ' + file_name
    command = create_command + ' ; ' + delete_command
    LOG.info("Checking unfreeze done: " + command)
    res = subprocess.call([command], shell=True)
    if res == 0:
        result['success'] = True
        LOG.info("Unfreeze successful")
    LOG.info("Unfreeze not successful")


# unfreeze and then checks if unfreeze is done
def _unfreeze_and_check_unfreeze_done():
    while True:
        _unfreeze()
        mpool = ThreadPool(processes=1)
        try:
            result = {}
            result['success'] = False
            res = mpool.apply_async(_check_unfreeze_done,
                                    [result]).get(timeout=pecan.conf.backup['unfreeze_timeout'])
            if result['success']:
                break
            else:
                continue
        except TimeoutError:
            LOG.info("Unfreeze check timed out")
            continue
        break


# updates the final backup status in file based on the result
def _update_backup_status(success,
                          dictionary, backup_status_file):
    if not success:
        _print_dictionary('BACKUP_FAILED',
                          dictionary, backup_status_file)
    else:
        _print_dictionary('BACKUP_SUCCESS',
                          dictionary, backup_status_file)


# this function is runs under separate thread
# Firstly Freeze the io
# Makes a cinder call to take backup
# Check if a loop to see if backup is done
# Un Freezes the iof
def _take_backup(backup_status_file,
                 access_key,
                 secret_key,
                 compute_url,
                 vpc_url,
                 volume_id,
                 block_time):
    dictionary = {}
    # setting backup_id to none so that if backup is not created we will send null for backup_id field in json
    dictionary['backup_id'] = None
    try:
        _print_dictionary('FREEZING_TO_START',
                      dictionary, backup_status_file)
        _freeze()
        _print_dictionary('FREEZED',
                          dictionary, backup_status_file)

        freeze_start_date_time = datetime.now()
        success = _cinder_create_backup(access_key,
                                        secret_key,
                                        compute_url,
                                        vpc_url,
                                        volume_id,
                                        dictionary,
                                        backup_status_file)
        if success:
            success = _check_cinder_volume_status(access_key,
                                                  secret_key,
                                                  compute_url,
                                                  vpc_url,
                                                  volume_id,
                                                  dictionary,
                                                  block_time,
                                                  freeze_start_date_time,
                                                  backup_status_file)
        _print_dictionary('UNFREEZE_TO_START',
                          dictionary,
                          backup_status_file)
    except Exception:
        LOG.exception("Unhandled exception while taking backup")
    finally:
        _unfreeze_and_check_unfreeze_done()
        _print_dictionary('UNFREEZE_DONE',
                          dictionary,
                          backup_status_file)
        _update_backup_status(success,
                              dictionary,
                              backup_status_file)


# Create a thread and assigns task of taking backup to it
def start_backup_process(backup_status_file,
                         access_key,
                         secret_key,
                         compute_url,
                         vpc_url,
                         volume_id,
                         block_time):
    pool = ThreadPool(processes=1)
    result = pool.apply_async(_take_backup,
                              [backup_status_file,
                               access_key,
                               secret_key,
                               compute_url,
                               vpc_url,
                               volume_id,
                               block_time])
