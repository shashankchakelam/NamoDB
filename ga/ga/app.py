from ga import utils
import pecan
from pecan import make_app
from paste.translogger import TransLogger

def setup_app(config):

    app_conf = dict(config.app)

    app = make_app(
        app_conf.pop('root'),
        logging=getattr(config, 'logging', {}),
        **app_conf
    )

    app = TransLogger(app, setup_console_handler=False)
    return app

def _make_app_dirs():
     # Ensure app folder locations exist, else create
    app_folder = pecan.conf.app_dirs['home']
    app_log_folder = pecan.conf.app_dirs['logs']

    utils.safe_mkdirs(app_folder)
    utils.safe_mkdirs(app_log_folder)