#!/bin/bash
set -ex

# If you want to preserve the virtual environment and not delete it, pass
# '--preserve-venv' param to the script

# Create virtualenv. Interesting point to note: you don't need to check if you're
# already running in a virtualenv as it's a safe command to simply create and activate
# a new one (even of the same name)
virtualenv .venv
source .venv/bin/activate

# Build, install and run tests of the ga module
pip install -r requirements.txt
pip install -r test-requirements.txt

python setup.py install
python -m pytest ga/tests/test_units.py
python -m pytest ga/tests/test_functional_mysql.py
python -m pytest ga/tests/test_functional_postgresql.py

# Even if you don't explicitly deactivate here, once the script exits the virtualenv
# will be deactivated. This is because the source command loads the virtualenv in the
# context of this script but once it exits, will deactivate the virtualenv
deactivate
if [ "${1}" != "--preserve-venv" ]; then
    rm -rf .venv/
fi
