#! /bin/bash -eux

# Assumption: volume is attached to instance at /dev/vdb. This needs to be made
# configurable in future
version="$1"
# Create directory 'ga-state' where we're storing status of format operation
# in a text file
mkdir -p /home/ubuntu/ga-state/
echo "status=started" > /home/ubuntu/ga-state/install
echo "started=$(date)" >> /home/ubuntu/ga-state/install

# Create EXT4 filesystem on top of /dev/vdb block device
sudo mkfs.ext4 /dev/vdb
# Create directory which will be the data directory of postgresql. This is the
# directory where we will mount the volume, so that all the actual data within
# postgresql databases will reside on the volume
sudo mkdir /home/ubuntu/datadir/
# Now do the actual mount
sudo mount /dev/vdb /home/ubuntu/datadir
#creating a directory for the database in the mounted volume
#sudo mkdir /home/ubuntu/datadir/postgresql
# Copy over contents from original data directory to the new data directory.
# This is required, otherwise postgresql won't work properly is the guess.
sudo cp -rp /var/lib/postgresql/"$version"/main /home/ubuntu/datadir/postgresql

#changing it's owner to postgres user
sudo chown -R postgres:postgres /home/ubuntu/datadir/postgresql
#initializes folder as a database folder
# Need to stop postgresql server before changing postgresql's data directory
sudo service postgresql stop


## Copy over contents from original data directory to the new data directory.
## This is required, otherwise postgresql won't work properly is the guess.
#sudo cp -rp /var/lib/postgresql /home/ubuntu/datadir/postgresql


# In postgresql's config file, point to the new location as the data directory
sudo sed -i s,/var/lib/postgresql/"$version"/main,/home/ubuntu/datadir/postgresql,g /etc/postgresql/"$version"/main/postgresql.conf

#to give access to user globally
# first uncomment listenaddress and change it to ‘*’ as give in comments of postgresql.conf
sudo sed -i -e 's/#listen_addresses/listen_addresses/g' /etc/postgresql/"$version"/main/postgresql.conf
sudo sed -i -e '0,/localhost/s/localhost/*/' /etc/postgresql/"$version"/main/postgresql.conf
#next edit the pg_hba file ro grant access over all Ip's  for user to loginchange password option to md5 to use hash as password
sudo sed -i -e 's/127.0.0.1\/32/0.0.0.0\/0/g' /etc/postgresql/"$version"/main/pg_hba.conf
sudo sed -i -e 's/md5/password/g' /etc/postgresql/"$version"/main/pg_hba.conf
sudo sed -i -e 's/peer/password/g' /etc/postgresql/"$version"/main/pg_hba.conf
sudo sed -i -e 's/postgres/rdsadmin/g' /etc/postgresql/"$version"/main/pg_hba.conf
sudo sed -i -e 's/::1\/128/::\/0/g' /etc/postgresql/"$version"/main/pg_hba.conf

# Now we can safely start postgresql service
sudo service postgresql start

# Update the text file to notify format operation is completed
echo "status=completed" >> /home/ubuntu/ga-state/install
echo "completed=$(date)" >> /home/ubuntu/ga-state/install


