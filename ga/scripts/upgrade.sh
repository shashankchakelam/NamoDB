#! /bin/bash -eux

# Params:
# $1 is the URL of the file to download

sudo mkdir -p /home/ubuntu/ga/debs
cd /home/ubuntu/ga/debs
sudo wget $1

FILENAME=$(ls /home/ubuntu/ga/debs -sort -t | grep deb$ | tail -1 | awk '{print $9}')

sudo dpkg -i --force-confnew --force-overwrite $FILENAME
sudo service supervisor restart
