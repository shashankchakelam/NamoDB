#! /bin/bash

# Script which installs guest agent in the machine, as a Python package. By
# default, it just installs primary. If you want to install secondary, run this
# script as:
#   ./dev-install.sh --secondary

echo "Make sure you export proxy if accessing internet requires it"
sleep 2

#making the shell non-interactive
export DEBIAN_FRONTEND=noninteractive


#setting the proxy
http_prxy=`grep Proxy /etc/apt/apt.conf | cut -d '"' -f2`
https_prxy=`grep Proxy /etc/apt/apt.conf | cut -d '"' -f2`

export http_proxy=$http_prxy
export https_proxy=$https_prxy

sudo apt-get install -y python-dev python-pip build-essential libssl-dev libffi-dev libmysqlclient-dev python-setuptools build-essential libssl-dev libffi-dev postgresql-server-dev-9.3

 sudo -E apt-get install -y supervisor python-pip python-mysqldb

cd ../

sudo -E pip install psycopg2
sudo -E pip install -r requirements.txt
sudo -E python setup.py install


echo -e "\n\n\n\nCopying Raga primary file"
sleep 5

if [ "${1}" == "--secondary" ]; then
    sudo cp conf/raga-secondary-supervisor-dev.conf /etc/supervisor/conf.d/
else
    sudo cp conf/raga-primary-supervisor-dev.conf /etc/supervisor/conf.d/
    echo "Copy completed"
    ls -ltr conf
    cat conf/raga-primary-supervisor-dev.conf
    cat /etc/supervisor/conf.d/raga-primary-supervisor-dev.conf
fi

sleep 5

if [ -f /etc/mysql/my.cnf ]
then
	echo "Making changes in my.cnf"
	sudo sed -i 's/bind-address/#bind-address/g' /etc/mysql/my.cnf
fi

sudo cp scripts/*.sh /usr/bin

sudo mkdir -p /opt/rds/ga/logs
sudo chmod -R 777 /opt/rds

sudo service supervisor restart

# Unset proxy now
unset http_proxy https_proxy

echo "Sleeping for 5 seconds"
sleep 5

curl localhost:8080/v1/versioninfo
echo ""
echo 'If the above line prints {"git_hash": "GITHASH"}, then it means this script ran successfully'
