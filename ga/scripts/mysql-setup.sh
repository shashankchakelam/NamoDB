#! /bin/bash

# With our DBaaS, we should be using pre-built virtual machine images, which
# have database packages already installed. However, if you're starting
# from scratch, you can use this script to install the database server
# (mysql 5.6 currently) and other related things.

# Make the script verbose, and bail out immediately in case of an error
set -eux

#making the shell non-interactive
export DEBIAN_FRONTEND=noninteractive

# Update pointers to repositories
sudo apt-get -y update

# Also upgrade all packages. This is so that we apply any security patches
# which are available in the APT repository
sudo apt-get -y dist-upgrade

# Install MySQL 5.6 in a non-interactive way (i.e. without any prompt)
# NOTE: a dummy root password is provided as of now
# NOTE: This MySQL installation is possibly not the most secure
#UUID=$(python -c "import uuid;print uuid.uuid4().hex")
#echo "DB Password is $UUID"
#sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $UUID"
#sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $UUID"
sudo -E apt-get -q -y install mysql-server-5.6
mysqladmin -u root password kaka123

cp /home/ubuntu/NamoDB/ga/ga/mysql.image-info.json /home/ubuntu/image-info.json

# Install supervisor and sleep
sudo apt-get -y install supervisor

sleep 10

# TODO(rushiagr): write steps to remove /home/ubuntu directory from image
# which needs to be done before uploading the image to glance
