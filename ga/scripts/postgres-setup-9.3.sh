#! /bin/bash

# With our DBaaS, we should be using pre-built virtual machine images, which
# have database packages already installed. However, if you're starting
# from scratch, you can use this script to install the database server

# Make the script verbose, and bail out immediately in case of an error
set -eux

#making the shell non-interactive
export DEBIAN_FRONTEND=noninteractive


#setting the proxy
http_prxy=`grep Proxy /etc/apt/apt.conf | cut -d '"' -f2`
https_prxy=`grep Proxy /etc/apt/apt.conf | cut -d '"' -f2`

export http_proxy=$http_prxy
export https_proxy=$https_prxy


# Update pointers to repositories

sudo -E sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'

wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo -E apt-key add -

sudo apt-get -y update

# Also upgrade all packages. This is so that we apply any security patches
# which are available in the APT repository
sudo apt-get -y dist-upgrade

# Install Postgres

sudo apt-get install postgresql-9.3 postgresql-contrib-9.3 postgresql-plpython-9.3 postgresql-plpython3-9.3 postgresql-pltcl-9.3 postgresql-9.3-plv8 postgresql-9.3-postgis-2.1 postgresql-9.3-postgis-2.1-scripts postgresql-plperl-9.3 postgresql-9.3-ip4r -y

#UUID=$(python -c "import uuid;print uuid.uuid4().hex")
#echo "DB Password is $UUID"
sudo -u postgres psql -d postgres -c "UPDATE pg_authid SET rolname = 'rdsadmin' WHERE rolname = 'postgres';ALTER ROLE rdsadmin WITH PASSWORD 'kaka123';"
#copying pgrds.so library file
#This extension implements extension whitelisting, and will actively prevent users from installing extensions not in the provided list.
# Also, this extension implements a form of sudo facility in that the whitelisted extensions will get installed as ifsuperuser.
# Privileges are dropped before handing the control back to the user.
sudo mkdir -p /usr/lib/postgresql/9.3/lib/plugins
sudo cp /home/ubuntu/NamoDB/ga/scripts/pgrds.so /usr/lib/postgresql/9.3/lib/pgrds.so
sudo ln -s /usr/lib/postgresql/9.3/lib/pgrds.so /usr/lib/postgresql/9.3/lib/plugins/pgrds.so

#adding the extensions into postgresql.conf
echo "rds.extensions='address_standardizer,address_standardizer_data_us,btree_gin,btree_gist,chkpass,citext,cube,dblink,dict_int,dict_xsyn,earthdistance,fuzzystrmatch,hstore,intagg,intarray,ip4r,isn,ltree,pgcrypto,pgrowlocks,pgstattuple,pg_buffercache,pg_stat_statements,pg_trgm,plperl,plpgsql,pltcl,plv8,postgis,postgis_tiger_geocoder,postgis_topology,postgres_fdw,sslinfo,tablefunc,test_parser,tsearch2,unaccent,uuid-ossp'" | sudo tee -a /etc/postgresql/9.3/main/postgresql.conf
sudo sed -i -e 's/#local_preload_libraries/local_preload_libraries/g' /etc/postgresql/9.3/main/postgresql.conf
sudo sed -i "s/\(local_preload_libraries *= *\).*/\1'pgrds'/" /etc/postgresql/9.3/main/postgresql.conf

cp /home/ubuntu/NamoDB/ga/ga/postgresql_9.3.image-info.json /home/ubuntu/image-info.json

# Install supervisor
sudo apt-get -y install supervisor

sleep 10

# TODO(rushiagr): write steps to remove /home/ubuntu directory from image
# which needs to be done before uploading the image to glance
