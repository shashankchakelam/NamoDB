#! /bin/bash -eux

# Assumption: volume is attached to instance at /dev/vdb. This needs to be made
# configurable in future

# Create directory 'ga-state' where we're storing status of format operation
# in a text file
mkdir -p /home/ubuntu/ga-state/
echo "status=started" > /home/ubuntu/ga-state/install
echo "started=$(date)" >> /home/ubuntu/ga-state/install

# Create directory which will be the data directory of mysql. This is the
# directory where we will mount the volume, so that all the actual data within
# mysql databases will reside on the volume
sudo mkdir /home/ubuntu/datadir
# Now do the actual mount
sudo mount /dev/vdb /home/ubuntu/datadir

# Need to stop mysql server before changing mysql's data directory
sudo service mysql stop
# In mysql's config file, point to the new location as the data directory
sudo sed -i s,/var/lib/mysql,/home/ubuntu/datadir/mysql,g /etc/mysql/my.cnf
# This is related to security. Not sure what it does, but this is required,
# otherwise things won't work. Basic idea: we need to add an entry to apparmor
# (which is some security mechanism of ubuntu) and restart the apparmor service
# TODO(rushiagr): find out more about this step, so that we understand security
# implications of this step.
echo "alias /var/lib/mysql/ -> /home/ubuntu/datadir/mysql," | sudo tee -a /etc/apparmor.d/tunables/alias
# Reload configuration for apparmor service. The guess is this should be
# enough, but since we don't know much about it, we're restarting the service
# in the next step just to be sure
sudo service apparmor reload
sudo service apparmor restart
# Now we can safely start mysql service
sudo service mysql start

# Update the text file to notify format operation is completed
echo "status=completed" >> /home/ubuntu/ga-state/install
echo "completed=$(date)" >> /home/ubuntu/ga-state/install
