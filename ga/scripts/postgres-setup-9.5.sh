#! /bin/bash

# With our DBaaS, we should be using pre-built virtual machine images, which
# have database packages already installed. However, if you're starting
# from scratch, you can use this script to install the database server

# Make the script verbose, and bail out immediately in case of an error
set -eux


#setting the proxy
http_prxy=`grep Proxy /etc/apt/apt.conf | cut -d '"' -f2`
https_prxy=`grep Proxy /etc/apt/apt.conf | cut -d '"' -f2`

export http_proxy=$http_prxy
export https_proxy=$https_prxy


# Update pointers to repositories

sudo -E sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'

wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo -E apt-key add -

sudo apt-get -y update

# Also upgrade all packages. This is so that we apply any security patches
# which are available in the APT repository
sudo apt-get -y dist-upgrade

# Install Postgres

sudo -E apt-get install postgresql-9.5 postgresql-contrib-9.5 postgresql-plperl-9.5 postgresql-plpython-9.5 postgresql-plpython3-9.5 postgresql-pltcl-9.5 postgresql-9.5-plv8 postgresql-9.5-postgis-2.2 postgresql-9.5-postgis-scripts -y

UUID=$(python -c "import uuid;print uuid.uuid4().hex")
echo "DB Password is $UUID"
sudo -u postgres psql -d postgres -c "ALTER ROLE postgres WITH PASSWORD \'$UUID\';"
sudo -u postgres psql -d postgres -c "UPDATE pg_authid SET rolname = 'rdsadmin' WHERE rolname = 'postgres';"

cp /home/ubuntu/NamoDB/ga/ga/postgresql_9.5.image-info.json /home/ubuntu/image-info.json

# Install supervisor
sudo apt-get -y install supervisor

sleep 10

# TODO(rushiagr): write steps to remove /home/ubuntu directory from image
# which needs to be done before uploading the image to glance
