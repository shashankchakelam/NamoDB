#! /bin/bash -eux

# Script to create debian package for raga-primary and raga-secondary
# To install those packages, run:
#   sudo dpkg -i --force-overwrite --force-confnew <pkgname>.deb

if [[ $(ls --directory */ | grep -c '^debian/$') == 0 ]]; then
    echo "No 'debian' directory present in the current directory from where script is run"
    exit 1
fi

if [[ $(dpkg -l | awk '{print $2}' | grep -c '^debhelper$\|^dh-virtualenv$\|^python-dev$') != 4 ]]; then
    echo "One or more of required APT packages is not installed. Installing now..."
    sudo apt-get -y install debhelper dh-virtualenv python-dev
fi

# Do cleanup. Some files/directories might be left over in case of failed
# package building
rm -rf debian/raga-primary
rm -rf debian/raga-secondary
sed -i s/primary/TYPE/g debian/*
sed -i s/secondary/TYPE/g debian/*
OLD_GITHASH=$(cat conf/raga-primary-pecan-config.py | grep git_hash | cut -d "'" -f 4)
sed -i s/$OLD_GITHASH/GITHASH/g conf/raga-primary-pecan-config.py
OLD_GITHASH=$(cat conf/raga-secondary-pecan-config.py | grep git_hash | cut -d "'" -f 4)
sed -i s/$OLD_GITHASH/GITHASH/g conf/raga-secondary-pecan-config.py

GITHASH=$(git log | head -1 | cut -d ' ' -f 2 | head --bytes 8)
sed -i s/GITHASH/$GITHASH/g debian/*
sed -i s/GITHASH/$GITHASH/g conf/*.conf
sed -i s/GITHASH/$GITHASH/g conf/*.py

# Build first package
sed -i s/TYPE/primary/g debian/*
dpkg-buildpackage -us -uc
rm -rf debian/raga-primary

# Build second package
sed -i s/primary/secondary/g debian/*
dpkg-buildpackage -us -uc
rm -rf debian/raga-secondary

# Restore all string substitutions
sed -i s/$GITHASH/GITHASH/g debian/*
sed -i s/secondary/TYPE/g debian/*
OLD_GITHASH=$(cat conf/raga-primary-pecan-config.py | grep git_hash | cut -d "'" -f 4)
sed -i s/$OLD_GITHASH/GITHASH/g conf/raga-primary-pecan-config.py
OLD_GITHASH=$(cat conf/raga-secondary-pecan-config.py | grep git_hash | cut -d "'" -f 4)
sed -i s/$OLD_GITHASH/GITHASH/g conf/raga-secondary-pecan-config.py
