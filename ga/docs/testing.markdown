Testing
=======

Unit tests
----------
* Use python modules unittest for writing tests.
* For mocking, use 'mock' framework.
* In future, use tox maybe when we test multiple things e.g. pep8 tests, py2, py3, etc.
