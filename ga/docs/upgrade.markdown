'Upgrade' (workflow service)
----------------------------
Except for third step, all other tasks are to be done by workflow service (namo)
    1. [verify no other workflow is running]if instance is 'active/none', change state to 'patching/applying'.
    2. [verify the vm is in a healthy state]verify health of guest agent (see of both services are running and healthy). Also, check health of nova vm
    3. go ahead with installation. Details in guest agent design doc.
    4. [check again if vm is in healthy state]
    5. change state from 'patchin/applying' to 'active/none'

TODO: discuss rollbacks for each step

workflow controller method:
    'atomic change lccs'
    check_raga_health(primary or secondary)
    check vm health (via nova) -- just call already existing get_vm_active
    #check_vm_upgrade_state -- state can be 'active', upgrade_pending, upgrading, active # TODO: think of scenario when upgrade gets stuck. Maybe handled by mistral already
    POST upgrade vm from v1 to v2
        db method: register_intent_for_upgrade(vm, v1, v2) : will write: from:v1 to:v2 to database
        call raga to upgrade it's peer
    GET upgrade vm from v1 to v2 -- should return 'ongoing', 'done', 'error/bad versions'
        should first check from db to get what's the initial and final version
        should then contact raga to find it's version
        if final = raga's version, clean up database and return 'done'. Else ongoing. If version mismatch, error out

Mistral task:
    'atomic change lccs'
    check raga health of primary
    check raga health of secondary
    check vm health via nova
    POST upgrade raga-primary of vm 'vm' from version 'v1' to version 'v2'
    (in a loop) GET upgrade raga-primary vm v1->v2 successful
    check raga health of primary
    POST upgrade raga-secondary of vm 'vm' from version 'v1' to version 'v2'
    (in a loop) GET upgrade raga-secondary vm v1->v2 successful
    check raga health of secondary
    check raga health of primary
    check vm health via nova
    'atomic change lccs'

TODO: what if mistral crashes between upgrades? We will need to clean up entry in database when mistral comes back up
