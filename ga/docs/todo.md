High level tasks
----------------
* Set up dev env with contrail
* Guest Agent Installer
* Supplying database name, username and password for that db, and setting this
  actually for the database
* Discuss if authentication is required for guestagent <-> workflow service communication
* Testing: Unit, functional and integration tests
* Discuss about continuous integration server
* Discuss about if we will require changing configuration params based on vm flavor (workers). If yes,
  is this the job of guest agent installer?
* Discuss if it is our job to also care about user permissions etc. TODO: new database role for user.
* Security: Do we need to make pre-installed databases (e.g. performance_schema) inaccessible to end user?
* Logs: add more verbosity to logs. Also do not store logs at /tmp dir. TODO: find out more about log streaming
* Do not use shell scripts, but use python code with proper error handling
* Attaching multiple block volumes and RAIDing them
* Create image for mysql 5.6 also. TODO: do it first
* Get rid of supervisor completely. Also involves understanding packaging more
* Upload a default dbaas image to s3 and use that everywhere

Small fixes
-----------
* In post-install thing of guest agent, restart supervisor
* raga primary and secondary both uses same format.sh

Others/optional/enhancements
----------------------------
* Debian packaging: find out how to add apt dependencies, find out how to put deb package
  into an APT repository, find out if we can roll back a debian package to previous version.
* Make the code more generic, so that it will work with non-mysql databases too
* Autodetect what all volumes are available for vm, and format and raid all of them together
* Change response of GET /v1/service from 'start/running', 'stop/waiting' to more
  generic (and less mysql-specific) 'running', 'stopped'.
* Make number of workers and threads configurable
* COnfiguration options of guest agent to be change-able by guest agent installer?
* Format date time in API responses, instead of just returning what is being
  returned by shell 'date' command
* Allow overriding config file /etc/supervisor/conf.d/ga.conf. See
  https://wiki.debian.org/ConfigPackages and
  https://raphaelhertzog.com/2010/09/21/debian-conffile-configuration-file-managed-by-dpkg/
  for more info.
* Shelling out for GET /v1/service is slow
* Find a suitable place to keep format.sh instead of /usr/bin
