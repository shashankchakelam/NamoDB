Guest Agent API Document
========================

Database service related operations
-----------------------------------

###Get service info

    Request: *GET /v1/service*
    Response body: {"status": "start/running"}
    Description: The status can be "start/running", "stop/waiting", or "unknown".

###Update service

    Request: *POST /v1/service {"action": "start"}*
    Response header: HTTP 202 Accepted
    Response body: None
    Description: Action can be "start", "stop", or "restart"

###Get Format info

    Request: *GET /v1/format*
    Response body: {"status": "pending"}
    Description: The status can be "pending", "started", or "completed".

###Do format operation

    Request: *POST /v1/format* (No POST body)
    Response header: HTTP 202 Accepted
    Response body: None

### Get version info

    Request: *GET /v1/versioninfo*
    Response header: HTTP 200 OK
    Response body: {"git_hash": "abcdef12"}

### Upgrade Guest Agent

    Request: *POST /v1/upgrade {"pkg_url": "<package URL>"}*
    Response header: HTTP 202 Accepted
    Response body: None
