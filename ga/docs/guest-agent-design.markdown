Guest Agent Design
==================

There will be a guest agent (henceforth 'GA') service running inside the customer VM, but how will
we update the service in case of new release or a bugfix?

Different approaches:

1. Have a '*guest agent installer*' service, whose sole job is to install and upgrade
  guest agent. Install and upgrade of GA will be done by GAI based on an external
  API call, that is, 'push mechanism'. *Advantages* clean interface, separation of concerns.
  *Disadvantages* No clear way to upgrade GAI itself in case an upgrade is needed (e.g. security
  fix).

2. SSH connection: workflow service (namo) will SSH into the VM, and will
carry out upgrade of GA service/package. *Advantage* no need to maintain another
service. *Disadvantage* when we have a large number of servers, and during a maintenance window
we have to do stuff (installation/upgrade) on all of them in a small amount of time, we'll have
a large number of connections open (since installation/upgrade is not an instantaneous operation), which might potentially be a memory hog. Also,
this is the only part where we'll be writing code which will interact via ssh connection, and not as a rest api.

3. One code repository, two services running same code. One instance will act as GA, and the second instance will
act as GAI. In case any one of the service is not responding, we will have another service to help bringing the
first one back up, or in worst case act as the other 'down' service.

We have tentatively decided we'll go ahead with method 3. More details below.

Two services from the same code
-------------------------------

We will create two packages using the same code. Let's call them ga1 and ga2. When a customer db vm boots up, it will have two services running: say ga1 on port 8080 and ga2 on port 8081. Both the packages will be exactly identical with regards to code. Only configuration files will vary (for example: config file of ga1 will say port = 8080 and that of ga2 will say port = 8081). ga1 service will act as the primary guest agent under normal operation. That is, all the calls related to guest agent -- format block device, manipulate db service, create db users -- will go to this ga1 service. During normal operation, ga2 will act as guest agent installer -- will just upgrade/rollback guest agent installer, and do nothing else.

Note that this is not a peer-to-peer setup. Both the services have designated and different job to do.

Such kind of setup will empower us with two things:

1. Some sort of high-availability. If one service goes down, we can use another for rescue operation. Only when both services are down that we are in trouble.
2. This will help us also easily upgrade guest agent installer service. There was no mechanism to do the same in other
alternatives.

### Upgrades/rollbacks
PUSH model

ga will have to expose an api, saying which package version it is running
right now. For now, consider this version as the SHA1 hash of the latest
git commit in the package. Both the services should be able to read the current
package version from a configuration file at startup. Only then with gai be able to ping ga periodically
and find out if the version number has changed from the previous version
number.

1. an api call to gai will be made to take two deb urls
3. gai will install/upgrade/rollback ga
4. this will change package version of ga
5. gai will keep polling ga for package version
6. once gai detects ga has been upgraded, it will ask ga to upgrade gai by making an api call

ALTERNATIVE to step 5: instead of gai asking ga to upgrade gai, ga can check version
of gai at startup and if it finds out that gai is running an older version than ga, it will
proceed to upgrade gai. This is 'poll' or 'pull' model (possibly), original step 5 is push.

NOTE: note that at every stage, we'll keep syncing state to disk. E.g. gai_status=installing_ga.
    This might help in case of crash recovery, to find out what stages of upgrade got completed
    and what not.  We might also require an api call to get the stored state from gai/ga, maybe as a json
NOTE: all disk operation to be synchronous.  We're not expecting them to take more than a second.
TODO: decide upon a version number


### Not-normal circumstances, and possible solutions
1. What if ga gets stuck (e.g. became unresponsive or rapidly restarting) during upgrade?
   gai can poll ga every 5 seconds during the upgrade phase and if after say 30 seconds
   ga doesn't respond, it can roll back the upgrade by installing the previous version of the package.
2. What if gai gets stuck during upgrade? There is no way we can roll back gai AND ga both
   to previously known working condition because to roll back ga, gai should be up. Oh wait, if we (ga)
   can roll back gai to the previous working state successfully, and then if ga tells gai to rollback
   ga, then we might get out of this situation also. In such a case, we can let our external monitoring
   detect such a scenario, and then make a call to ga for upgrade. Since we now made a call to ga for upgrade,
   it will upgrade gai first to a new version (this new version will hopefully contain the patch which will
   fix the existing issue of service getting stuck), and then ga will get upgraded.
3. What if ga becomes unresponsive when no upgrade is taking place? Monitoring service will
   detect this and once we have a patched debian package ready, we can make an api call to gai to upgrade
   ga and gai itself to newer version
4. What if gai becomes unresponsive when no upgrade is taking place? Same as above.
5. What if both becomes unresponsive when no upgrade is taking place? SSH :)
6. To think: although we are first upgrading one service and only when it completes do we try to upgrade
   the next one. BUT, what if both services become unresponsive for some reason? I think there's no way but ssh
   and manual fix the mess. Note that such case is going to be extremely rare (if we have implemented and
   tested our solution well enough), and hopefully will happen only randomly, affecting only a fraction
   of VMs and not all of them :)

TODO: how will these two services check health of each other? Do we need to do that, or leave this job to the monitoring service?
TODO: what if the upgrade after new code makes BOTH the services go down/stuck/unresponsive? Is our mechanism of upgrade (which is to be discussed, but something like "upgrade A first, check if upgrade happened properly, and then do upgrade B") going to handle this?
TODO: rolling upgrades across the fleet?
