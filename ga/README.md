DBaaS Guest Agent
=================

Summary
-------
Guest agent is a service which is going to reside inside the database VM. It
will be be accessible from outside by exposing HTTP APIs.

The service should have a low memory footprint, and should be light on CPU.

When a user will request a database instance from DBaaS service, a VM will be created
with one or more block volumes attached to the VM. The Glance image from which the VM
will be created will already contain the required database files (MySQL for now).
Guest agent will format these
block volumes (possibly RAIDing them together), and will configure MySQL to start
using the block volume as data directory.


Primary tasks of guest agent
----------------------------
Guest agent should
 1. partition/format the block volume
 2. CRUD database, user, username, password
 3. Report back status of any ongoing operations
 4. Should have a ReST HTTP server communicating with DBaaS service

Current status
--------------
There are two APIs exposed as of now: /v1/format and /v1/service. More details
about the API can be found in `docs/api-doc.markdown` file.

Packaging
---------
The plan is to create debian package for guest agent. We're using `dh-virtualenv`
utility to create self-contained virtual environment-type package for guest agent. Refer
to documentation of dh-virtualenv for more details.

Upgrade
-------
There will be another service 'Guest Agent Installer', whose job will be to
just install and upgrade guest agent. Details TBD.


Testing
-------
 * Unit and functional tests should be written. Functional coverage should be
 the priority.

Debugging
---------
We should maintain logs for easy debugging of guest agent. Currently, logs
are going to reside on DB vm only, and that too at /tmp/ga.log.

Security
--------
TODO: DoS? HTTPS? How to not expose the port on which guest agent is running to
outside world (except our internal dbaas services)?

Need to discuss if we need authentication for contacting guest agent or not.

Installation
------------

    cd ga/
    sudo dpkg-buildpackage -us -uc

Running the server
------------------

Run the gunicorn server with 2 workers and 5 threads, in dev mode:

    gunicorn_pecan conf/dev-config.py --workers 2 --threads 5

By default, it runs on port 8080. Note that `--threads 5` part only works with
Python 3, not 2. We're using Python2 for now for debian packages.
