# Server Specific Configurations
server = {
    'port': '8080',
    'host': '0.0.0.0'
}

is_service = {
    'port': '9001',
    'host': 'localhost'
}

# Pecan Application Configurations
app = {
    'root': 'ga.controllers.root.RootController',
    'modules': ['ga'],
    'static_root': '%(confdir)s/public',
    'template_path': '%(confdir)s/ga/templates',
    'hooks': lambda: [__import__('ga.common.RequestIdPecanHook', fromlist=["RequestIdHook"]).RequestIdHook()], # The hook needs to be iterable and must implement/subclass from PecanHook
    'debug': True,
    'errors': {
        404: '/error/404',
        '__force_dict__': True
    }
}

# NOTE(rushiagr): DO NOT CHANGE THE FORMAT OF THE FOLLOWING DICTIONARY
# The following dictionary is parsed by a shell script. If you want to change
# this dictionary, make sure you don't change the way it's indented. You can
# add another key-value pair only on the next line. Don't forget to put a
# comma at the end of the newly added key-value pair, even if it's redundant.
# Also, do not use double quotes, but just use single quotes for this specific
# dictionary
version_info = {
    'git_hash': 'abcd1234',
    }

file_paths = {
    'db_install_state': '/home/ubuntu/ga-state/install',
    'image_info' : '/home/ubuntu/image-info.json'
}

backup = {
    'backup_directory': './Backup',
    'restore_directory': './Restore',
    'freeze_directory': '.',
    'verify_ssl': False,
    'unfreeze_timeout': 1
}
# Custom Configurations must be in Python dictionary format::
#
# foo = {'bar':'baz'}
#
# All configurations are accessible at::
# pecan.conf

myconf = {'key': 'val'}

"""
For MySQL : mysql://root:kaka123@127.0.0.1/mysql?charset=utf8
For PostgreSQL : postgresql://rdsadmin:kaka123@127.0.0.1/postgres
"""
database = {
    'connection' : 'postgresql://rdsadmin:kaka123@127.0.0.1/postgres',
}

my_conf_location = {
    'location': '/etc/mysql/my.cnf' ,
    'temp_location': '/etc/mysql/my_temp.cnf'
}

mysql_grant_privileges = [
    'create',
    'drop',
    'create temporary tables',
    'select',
    'references',
    'event',
    'alter',
    'delete',
    'index',
    'update',
    'insert',
    'lock tables',
    'trigger',
    'create view',
    'show view',
    'alter routine',
    'execute',
    'create user',
    'process',
    'show databases',
    'grant option',
]

postgresql_grant_privileges = [
    'CREATEROLE',
    'CREATEDB',
    'LOGIN'
]

logging = {
    'root': {'level': 'INFO', 'handlers': ['console']},
    'loggers': {
        'ga': {'level': 'DEBUG', 'handlers': ['applogfile']},
        'wsgi': {'level': 'INFO', 'handlers': ['accesslogfile'], 'qualname': 'wsgi'},
        'pecan': {'level': 'DEBUG', 'handlers': ['console']},
        'py.warnings': {'handlers': ['console']},
        '__force_dict__': True
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'color'
        },
        'applogfile': {
            'level': 'DEBUG',
            'class': 'ga.common.FilteredRotatingFileHandler.FilteredRotatingHandlerClass', # For log rotation with request_id filter
            'filename': '/opt/rds/ga/logs/app.log',  # Ensure we create this folder
            'mode': 'a',                    # Appends to current file
            'maxBytes': '10485760',         # 10 MB is the size of each log file
            'backupCount': '20',            # 20 backup files will be maintained
            'formatter': 'simple'
        },
        'accesslogfile': {
            'level': 'INFO',
            'class': 'ga.common.FilteredRotatingFileHandler.FilteredRotatingHandlerClass', # For log rotation with request_id filter
            'filename': '/opt/rds/ga/logs/access.log',  # Ensure we create this folder
            'mode': 'a',                    # Appends to current file
            'maxBytes': '10485760',         # 10 MB is the size of each log file
            'backupCount': '20',            # 20 backup files will be maintained
            'formatter': 'simple'
        }
    },
    'formatters': {
        'simple': {
            'format': ('%(asctime)s %(req_id)-15s %(levelname)-5.5s [%(name)s]'
                       '[%(threadName)s] %(message)s')
        },
        'color': {
            '()': 'pecan.log.ColorFormatter',
            'format': ('%(asctime)s [%(padded_color_levelname)s] [%(name)s]'
                       '[%(threadName)s] %(message)s'),
        '__force_dict__': True
        }
    }
}

app_dirs = {
    'home': '/opt/rds/ga/',
    'logs': '/opt/rds/ga/logs/'
}
