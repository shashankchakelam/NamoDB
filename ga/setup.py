# -*- coding: utf-8 -*-
try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

setup(
    name='ga',
    version='0.1',
    description='',
    author='',
    author_email='',
    install_requires=[
        "pecan", "gunicorn", 'futures', 'mock', 'Paste==2.0.2', 'pecan', 'requests>=2.9.1', 'SQLAlchemy==1.0.11'
    ],
    test_suite='ga',
    zip_safe=False,
    include_package_data=True,
    packages=find_packages(exclude=['ez_setup'])
)
