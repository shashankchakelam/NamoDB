#This script will delete images from STaging RDS account
#Note: imageid is the UUID which we have to take from compute database or the glance imageid which we can see while listing images. 


#Setting the environment
echo -n  "Enter the environment [stag/prod]: "
read envrn

if [ $envrn == "stag" ]
then
        source openrc.stag
elif [ $envrn == "prod" ]
then
        source openrc.prod
else
        echo "Wrong entry.. Try again...!!!!"
        exit 1
fi

#Taking input from user
echo -n  "Enter image ID to delete: "
read imageid
echo -n  "Enter the account name: "
read accnt_name
echo -n  "Enter encrypted password: "
read encryp_pwd



#checking whether user has provided values or not
if [ -z $accnt_name ]
then
	echo "No account name provided.. Please try again"
	exit 1
fi

if [ -z $encryp_pwd ]
then
        echo "No encrypted password provided.. Please try again"
        exit 2
fi

#Checking whether user has entered any value or not
if [ -z $imageid ]
then
	echo "Image ID is required to delete the image"
	exit 1
else
	# Generating the token
	token=`curl -i -X POST -H "Content-Type: application/json" -d '{"auth": {"identity": {"password": {"user": {"access": "'$ACCESS_KEY'", "password":"'$encryp_pwd'", "name": "'$accnt_name'"}}, "methods": ["password"]}}}' "$IAM_URL/auth/tokens" -k | grep "X-Subject-Token:" | cut -d " " -f2`
	
	#Deleting the image
	curl -g -i -X DELETE "$UPLOAD_URL:8774/v2/00000000000000000000999448331327/images/$imageid" -H "User-Agent: python-novaclient" -H "Accept: application/json" -H "X-Auth-Token: $token" -k

fi
