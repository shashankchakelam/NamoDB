#This script will create a VM to make the required changes in Ubuntu hardened image

#Taking inputs from user
echo -n  "Enter absolute image path: "
read filename
echo -n "Enter Gitlab username: "
read user
echo -n "Enter Gitlab password: "
read -s passwd
echo -en "\nEnter Branch: "
read branch
echo -n "Enter DB name [Mysql/Postgres-9.3]: "
read dbname

#Checking if user has entered all the values or not
if [ -z $filename ]
then
        echo "Image path not entered"
        echo "Exiting!!!!!!"
        exit 1

elif  [ -z $user ]
then
        echo "User name not entered"
        echo "Exiting!!!!!!"
        exit 2

elif  [ -z $passwd ]
then
        echo "Password not entered"
        echo "Exiting!!!!!!"
        exit 3

elif  [ -z $branch ]
then
        echo "Branch name not entered"
        echo "Exiting!!!!!!"
        exit 4

elif  [ -z $dbname ]
then
        echo "DB name not entered"
        echo "Exiting!!!!!!"
        exit 5

else
	#Staring the VM using image
	sudo qemu-system-x86_64 -enable-kvm -m 1024 -vnc :7 -k tr -hda $filename -net user,hostfwd=tcp::2222-:22 -net nic -net tap,ifname=tap0,script=no &
	
	echo "Starting the server..."
	
	while :
	do
		sleep 10
		nc -v -z -w 5 127.0.0.1 2222 &> /dev/null
		if [ $? -eq 0 ];
		then
			ssh -o "StrictHostKeyChecking no" -i $HOME/rds-prod-tools.pem -p 2222 ubuntu@127.0.0.1 "exit"  > /dev/null 2>&1
			if [ $? -eq 0 ];
			then
				echo "Service is Alive"
				echo "Ready to login"
				break
			else
				echo "Retrying... Please wait..!!!"
			fi
		else
			echo "Server is booting up"
		fi
	done

	#Removing the old key from local file, copying the server_installation.sh script to VM and then executing the script by passing some 
	#variables as arguments
	ssh-keygen -f "/home/ubuntu/.ssh/known_hosts" -R [127.0.0.1]:2222
	scp -P 2222 -i $HOME/rds-prod-tools.pem -o StrictHostKeyChecking=no $HOME/server_installation.sh ubuntu@127.0.0.1:~/
	ssh -o "StrictHostKeyChecking no" -i $HOME/rds-prod-tools.pem -p 2222 ubuntu@127.0.0.1 "bash ./server_installation.sh $user $passwd $branch $dbname"

fi
