#This script will convert a raw volume to qcow2 image which we can upload to rds account

#Taking input from user
echo -n  "Enter absolute volume path: "
read vol_path
echo -n  "Enter new image name (Without .img extension): "
read img_name

#Checking if user has provided both values or not. If yes converting the raw volume to image and storing the image to images folder if it
# exists else it will be stored in the home directory.


if [ -z $vol_path ]
then
	echo "Volume path is mandatory... TRY AGAIN...!!!!"
else
	if [ -z $img_name ]
	then
		echo "Image name is mandatory... TRY AGAIN...!!!!"
	else
		qemu-img convert -c -f raw -O qcow2 $vol_path $HOME/$img_name.img
		
		if [ -d $HOME/images/ ]
		then
			mv $HOME/$img_name.img $HOME/images/
		fi
	fi
fi
