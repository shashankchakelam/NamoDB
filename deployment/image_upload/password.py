#Python code to generate the encrypted password using password and secret key

from Crypto.Cipher import AES
import base64

# ... Console side encryption 
password = 'Password123' #dummy password is set by default
pass_length = len(password)
password = password.rjust(((pass_length/16)+1)*16)
secret_key = '99999999999999qqqqqqqqqwwwwwwww' # dummy secret key of console is set.
cipher = AES.new(secret_key,AES.MODE_ECB) 
encoded = base64.b64encode(cipher.encrypt(password))
print encoded

