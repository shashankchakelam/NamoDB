#This script will upload the RDS image to staging account.

#Killing the qemu process to stop the VM
for pid in $(ps -ef | awk '/qemu/ {print $2}'); do sudo kill -9 $pid; done;


#Setting the environment
echo -n  "Enter the environment [stag/prod]: "
read envrn

if [ $envrn == "stag" ]
then
	source openrc.stag
elif [ $envrn == "prod" ]
then
	source openrc.prod
else
	echo "Wrong environment name.. Try again...!!!!"
	exit 1
fi

#Taking inputs from user
echo -n  "Enter absolute image path: "
read filename
echo -n  "Enter new image name [without .img extension]: "
read img_name
echo -n  "Enter Account Name : "
read accnt_name
echo -n  "Enter encrypted password: "
read encryp_pwd


#validating values
if [ -z $filename ]
then
        echo "Image path not entered"
        echo "Exiting!!!!!!"
        exit 1

elif  [ -z $img_name ]
then
        echo "Image name not entered"
        echo "Exiting!!!!!!"
        exit 2

elif  [ -z $accnt_name ]
then
        echo "Account name not entered"
        echo "Exiting!!!!!!"
        exit 3

elif [ -z $encryp_pwd ]
then
        echo "Encrypted password not entered"
        echo "Exiting!!!!!!"
        exit 4

elif  [ -z $ACCESS_KEY ]  || [ -z $SECRET_KEY ] || [ -z $IAM_URL ] || [ -z $UPLOAD_URL ]
then
        echo "Openrc file not loaded.. Please check and try again..!!!"
        echo "Exiting!!!!!!"
        exit 2
fi

#Make sure you have set Access key and secret key as environemnt variables
echo "Make sure you have set Access key and secret key of STAGING account as environemnt variables"
sleep 2

#Unsetting the proxy variables
unset http_proxy https_proxy

#Generating the auth token
token=`curl -i -X POST -H "Content-Type: application/json" -d '{"auth": {"identity": {"password": {"user": {"access": "'$ACCESS_KEY'", "password":"'$encryp_pwd'", "name": "'$accnt_name'"}}, "methods": ["password"]}}}' "$IAM_URL/auth/tokens" -k | grep "X-Subject-Token:" | cut -d " " -f2`

#setting up the variable required to upload image
snapid=`shuf -i 2000-65000 -n 1`
minsize=10
img_size=`stat -c %s $filename`


#Uploading the image to staging
curl -k -g -i -X POST -H 'Accept-Encoding: gzip, deflate, compress' -H 'x-image-meta-container_format: bare' -H 'Accept: ?<200b>*/*<200b>?' -H "X-Auth-Token: $token"  -H 'x-image-meta-is_public: None' -H 'User-Agent: python-glanceclient' -H 'Content-Type: application/octet-stream' -H 'x-image-meta-disk_format: qcow2' -H "x-image-meta-min_disk: $minsize" -H 'x-image-meta-property-architecture: x86_64' -H "x-image-meta-property-snapshot_id: snap-000000$snapid" -H 'x-image-meta-property-root_device: /dev/vda' -H "x-image-meta-size: $img_size"  -H "x-image-meta-name: $img_name" $UPLOAD_URL:9292/v1/images --data-binary @$filename


# Listing the images
jcs compute describe-images --insecure | head -34

