#!/bin/bash

#This script will install postgres or mysql in the image

#setting the proxy in OS environment and git
http_prxy=`grep Proxy /etc/apt/apt.conf | cut -d '"' -f2`
https_prxy=`grep Proxy /etc/apt/apt.conf | cut -d '"' -f2`
export http_proxy=$http_prxy
export https_proxy=$https_prxy
git config --global http.proxy $http_prxy
git config --global https.proxy $https_prxy


#Storing the variables value passed as an arguments
user=$1
passwd=$2
branch=$3
dbname=$4

#Cloning the NamoDB repo
git clone https://$user:$passwd@gitlab.com/jiocloudservices/NamoDB.git

#Checkingout to required branch and changing to multiple directories
cd $HOME/NamoDB
git checkout $branch
cd $HOME/NamoDB/ga/scripts/

sudo apt-get install -y debconf-utils dialog libterm-readline-gnu-perl

#installing mysql or pastgres as per requirement
if [ "$dbname" == "Mysql" ]
then
	./mysql-setup.sh
	./dev-install.sh
elif [ "$dbname" == "Postgres-9.3" ]
then
        ./postgres-setup-9.3.sh
        ./dev-install.sh

else
	echo "DB doesnt match"
fi

#removing the proxy file and ubuntu user credentials from the image
sudo rm /etc/apt/apt.conf
rm $HOME/.ssh/authorized_keys
touch $HOME/.ssh/authorized_keys
