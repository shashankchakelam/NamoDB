Please go through the steps given in the sites ( Please go through the comment written in file in this folder,
please read them before copying these files to specific places as mentioned below )
http://haproxy.debian.net/#?distribution=Ubuntu&release=trusty&versionlow

I was running Ubuntu Trusty 14.04 LTS and I wanted to install HA proxy version 1.5 Stable 
So I had to run the following command.  As we are running the HA proxy in cluster mode 
run the following commands in two different machine lb1 / lb2

We also need to get a virtual IP from infrastructure team before going forward with the below steps.

sudo echo deb http://archive.ubuntu.com/ubuntu trusty-backports main universe | sudo tee /etc/apt/sources.list.d/backports.list 
sudo apt-get update 
sudo apt-get install haproxy -t trusty-backports

Step 1 On both lb1 and lb2 do the following
sudo cp /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg_orig 
sudo su cat /dev/null > /etc/haproxy/haproxy.cfg 
#copy the haproxy.cfg  file in this directory to   /etc/haproxy/haproxy.cfg 
Open up this file /etc/default/haproxy and add line ENABLED=1 
apt-get install heartbeat
open uth this file /etc/sysctl.conf and the line net.ipv4.ip_nonlocal_bind=1
run sysctl -p
copy file authkeys in the current directory to /etc/ha.d/authkeys
chmod 600 /etc/ha.d/authkeys
copy file haresources in current directory to /etc/ha.d/haresources
This file will be same for both the machine. First column would be primary load balancer host name and other will be Virtual IP to which we want to bind these two load balancers.

Step 2
copy fiile ha.cf_lb1 to /etc/ha.d/ha.cf in lb1 machine
copy file ha.cf_lb2 to /etc/ha.d/ha.cf in lb2 machine
In the above files :-
The udpport, bcast, mcast, and ucast options specify how the two heartbeat nodes communicate with each other 
to find out if the other node is still alive. You can leave the udpport, bcast, and mcast lines as shown above, 
but in the ucast line it's important that you specify the IP address of the other heartbeat node; 
in this lb1 case ip address if lb2 and vice versa.

Step 4 :- In both lb1/ lb2 machines open /etc/hosts add the dns map of other load balancer.

Step 5:
Finally we start heartbeat on both load balancers:

/etc/init.d/heartbeat start ( by running this command in both the machines)

On lb1 when you run ip addr sh eth0 ( or em3 which ever is ur network port).
you should find that lb1 is now listening on the shared IP address 

You can also check this my running command ifconfig.

On lb2 when you run


Step 6: Do this on both lb1/lb2
/etc/init.d/haproxy start

sudo service restart haproxy is also a command to restart haproxy.

You might have noticed that we have used the options stats enable and stats auth someuser:somepassword in the HAProxy configuration in chapter 4. This allow us to access (password-protected) HAProxy statistics under the URL http://<virtual IP>/haproxy?stats
In our case we have no password.


Please go through the https://www.howtoforge.com/high-availability-load-balancer-haproxy-heartbeat-debian-etch-p2 
for any more clarifications. 

Enable SSL in haproxy.  
To enable SSL we need to do this in one of the machine and create a pem file then take the pem file 
and copy it into other machine in sudo vi /etc/haproxy/haproxy.cfg ha proxy config file 
we need to extend the bind ip line to the following (This will enable ssl at load balancer level)
bind 10.140.214.45:8777 ssl crt /etc/ssl/rds.io/rds.io.pem

This is how create a self signed certificate and pem file. 

sudo mkdir /etc/ssl/rds.io
sudo openssl genrsa -out /etc/ssl/rds.io/rds.io.key 1024
sudo openssl req -new -key /etc/ssl/rds.io/rds.io.key -out /etc/ssl/rds.io/rds.io.csr

You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:IN
State or Province Name (full name) [Some-State]:AP
Locality Name (eg, city) []:Vizag
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Reliance
Organizational Unit Name (eg, section) []:Jio
Common Name (e.g. server FQDN or YOUR name) []:Cloud
Email Address []:

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:

sudo openssl x509 -req -days 365 -in /etc/ssl/rds.io/rds.io.csr -signkey /etc/ssl/rds.io/rds.io.key -out /etc/ssl/rds.io/rds.io.crt

sudo cat /etc/ssl/rds.io/rds.io.crt /etc/ssl/rds.io/rds.io.key | sudo tee /etc/ssl/rds.io/rds.io.pem

To enable logging please follow the steps given in the link https://www.percona.com/blog/2014/10/03/haproxy-give-me-some-logs-on-centos-6-5/

Common Mistakes

Make sure log file has write permission

Make sure in ha.cf you disable the following lines
bcast  wlan0
mcast wlan0 225.0.0.1 694 1 0
and also make sure u run heart beat in some other port than udpport 694. If you use same port number as other service in same network
it will bring down you system

HA Proxy has bug when we restart stop does not the service.
