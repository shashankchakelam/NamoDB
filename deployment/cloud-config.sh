# One-time configurations

# TODO(rushiagr): above might not be a secure way. Waiting for VPC thing to be
# clear before this can be updated
nova secgroup-add-rule default tcp 22 22 0.0.0.0/0
nova secgroup-add-rule default tcp 8080 8080 0.0.0.0/0
nova secgroup-add-rule default tcp 8081 8081 0.0.0.0/0
