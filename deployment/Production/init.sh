#!/bin/bash

# NOTE: this script is more like notes. Do not run this script blindly
# without understanding each and every line

# step 1: install required pkgs
sudo apt-get install vim git htop tmux curl python-pip python-dev libffi-dev libssl-dev libmysqlclient-dev

#Set Proxy 

export https_proxy=http://10.140.212.11:3128
export http_proxy=http://10.140.212.11:3128

# Once we get all the libraries we need to do unset again

#make sure nameserver 10.140.218.59 is added in /etc/resolve.conf
