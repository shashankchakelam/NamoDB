# todo: mistral still in single-process mode
# todo: mistral raises some exceptions in logs when it starts
# todo: 'mistral-db-manage populate' raises error even though it finishes successfully
# todo: mistral database config parameters, and many more params need tuning

#DO: Get kilo mistral package wget mistral-kilo_2015.1.0-1-u14.04+mos2_all.deb

wget https://s3-ap-southeast-1.amazonaws.com/jio-dbaas/mistral-kilo_2015.1.0-1-u14.04%2Bmos2_all.deb 
sudo dpkg -i mistral-kilo_2015.1.0-1-u14.04+mos2_all.deb
sudo apt-get install -f
sudo dpkg -i mistral-kilo_2015.1.0-1-u14.04+mos2_all.deb

# change mistral .conf 

sudo apt-get install mysql-client-5.6
mistral-db-manage upgrade head
mistral-db-manage populate # Ignore 'no such file or directory' error for this command

#DO: update config file to one included here. Make sure you provide correct
# db credentials, and db machine IP address

# Run mistral server
mistral-server

# u might need to do unset http_proxy https_proxy
