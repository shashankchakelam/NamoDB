export http_proxy=http://10.140.212.10:3128
export https_proxy=https://10.140.212.10:3128

mkdir /opt/rds
mkdir /opt/rds/apiwebservice
mkdir /opt/rds/apiwebservice/logs

sudo chmod 777 /opt/rds/
sudo chmod 777 /opt/rds/apiwebservice/
sudo chmod 777 /opt/rds/apiwebservice/logs/

git clone https://gitlab.com/jiocloudservices/NamoDB.git
cd /home/rds_team/NamoDB
git checkout integrate_auth_api
sudo -E pip install virtualenv
virtualenv .venv
##########################################################################
tmux new-session -d -s apiwebservice-tmux -n one
tmux new-window -t apiwebservice-tmux:3 -n mistral
tmux attach-session -t apiwebservice-tmux
##########################################################################
cd /home/rds_team/NamoDB
source .venv/bin/activate
cd namo
pip install -r requirements.txt
python setup.py install
cd ../apiwebservice
pip install -r requirements.txt
python setup.py install
cp -r apiwebservice/templates /home/rds_team/NamoDB/.venv/local/lib/python2.7/site-packages/apiwebservice-0.1-py2.7.egg/apiwebservice/
export http_proxy=
export https_proxy=
pecan serve config.py
##########################################################################
tmux kill-session -t apiwebservice-tmux