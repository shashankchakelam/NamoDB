#Create a new tmux Session tmux new -s Production

sudo -E pip install virtualenv
virtualenv .venv # Creates a Virtual Env in .venv folder and this folder is a hidden folder
. .venv/bin/activate # Now we are virtual env

# Dont know if this is required sudo apt-get install mysql-client-5.6

pip install -r requirements.txt
pip install -e git+https://github.com/JioCloudCompute/jcsclient.git@new_jcs_client#egg=jcsclient
python setup.py install

#copy cofig files

sudo mkdir /etc/namo-workflowapi

sudo cp conf/*yaml /etc/namo-workflowapi/

sudo cp  conf/namo-workflowapi-paste.ini /etc/namo-workflowapi/

#Copy namo-workflowapi.conf file

#First create database tables:
    python sync_db.py --config-dir /etc/namo-workflowapi

#Then add this database entry:
    insert into masterworkflow values (0, NOW(), NOW(), 1, NULL);

unset http_proxy
unset https_proxy
#Start service as:
    python namo/cmd/workflowapi.py --config-dir /etc/namo-workflowapi


# Deploying first time in production
# we need to create VPC Subnet and then we need to create Add security groups rules

#update the following values in /etc/namo-workflowapi/namo-workflowapi.conf

access_key='prduction_access_key'
secret_key='production_secret_key'
compute_url='https://compute.ind-west-1.internal.jiocloudservices.com/'
vpc_url='https://vpc.ind-west-1.internal.jiocloudservices.com/'
is_secure=false
SubnetId=subnet-eda4d5fb
SecurityGroupId=sg-42382bd7

# Need open up port on this security groups so that we can make guest agent changes

# The follow commands will do it
jcs vpc AuthorizeSecurityGroupIngress --GroupId sg-42382bd7 --IpPermissions.1.IpProtocol icmp --IpPermissions.1.FromPort -1 --IpPermissions.1.ToPort -1 --IpPermissions.1.IpRanges.1.CidrIp 0.0.0.0/0 

jcs vpc AuthorizeSecurityGroupIngress --GroupId sg-42382bd7 --IpPermissions.1.IpProtocol tcp --IpPermissions.1.FromPort 22 --IpPermissions.1.ToPort 22 --IpPermissions.1.IpRanges.1.CidrIp 0.0.0.0/0
    
jcs vpc AuthorizeSecurityGroupIngress --GroupId sg-42382bd7 --IpPermissions.1.IpProtocol tcp --IpPermissions.1.FromPort 8080 --IpPermissions.1.ToPort 8081 --IpPermissions.1.IpRanges.1.CidrIp 0.0.0.0/0

jcs vpc AuthorizeSecurityGroupIngress --GroupId sg-42382bd7 --IpPermissions.1.IpProtocol tcp --IpPermissions.1.FromPort 3306 --IpPermissions.1.ToPort 3306 --IpPermissions.1.IpRanges.1.CidrIp 0.0.0.0/0


[workflow]
workflow_url = http://10.140.209.82:3001 #LB url 
default_private_network_name=private

[mistral]
mistral_url = http://10.140.209.76:8989/v2

[database]
connection = mysql://root:<production_password>@10.140.209.70/namo?charset=utf8

