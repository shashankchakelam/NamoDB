#! /bin/bash

# todo: use a better password than tempdbpass
# todo: use mysql_secure_installation
# todo: don't do grant all priveleges, and do something very restrictive
# todo: don't use root user, but another user to access database from outside
# todo: make tempdbpass (db password) a config param

sudo apt-get -y update
sudo apt-get install -y vim git tmux htop

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password tempdbpass'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password tempdbpass'
sudo apt-get -y install mysql-server-5.6

sudo mysql -uroot -ptempdbpass -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'tempdbpass';"
sudo mysql -uroot -ptempdbpass -e "CREATE DATABASE mistral"
sudo mysql -uroot -ptempdbpass -e "CREATE DATABASE namo"

#DO: change bind address in /etc/mysql/mysql.conf from 127.0.0.1 to 10.140.214.35, and then restart the service
