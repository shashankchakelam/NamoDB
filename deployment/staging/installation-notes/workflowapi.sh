To kill processe: for pid in $(ps aux | grep namo-api | awk '{print $2}'); do kill -9 $pid; done

sudo apt-get install python-dev build-essential libffi-dev libxslt1-dev libssl-dev

Set up venv, and install requirements.txt

Move all conf files to /etc/namo-workflowapi
Move all yaml files to /etc/namo-workflowapi

pip install -r requirements.txt

First create database tables:
    python sync_db.py --config-dir /etc/namo-workflowapi

Then add this database entry:
    insert into masterworkflow values (0, NOW(), NOW(), 1, NULL);

Start service as:
    python namo/cmd/workflowapi.py --config-dir /etc/namo-workflowapi

