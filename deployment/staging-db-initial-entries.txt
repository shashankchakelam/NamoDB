insert into db_engine values (0, NOW(), NOW(), 1, 'mysql', '5.6', '26', 'http://mysql.org', 'PREFERRED_GLOBAL');
insert into os values (0, NOW(), NOW(), 1, 'ubuntu', '14.04', '4', 'http://ubuntu.com', 'PREFERRED');
insert into raga values (0, NOW(), NOW(), 1, 'stgimg2', 'http://gitlab.com/jiocloudservices/NamoDB.git', 'PREFERRED');
insert into artefact values (0, NOW(), NOW(), 1, 1, 1, 1);
insert into images values (0, NOW(), NOW(), 1, 'ami-963cc1ff', 1);

# TODO(rushiagr): we shouldn't have nova_flavor_id column at all
insert nova_flavors values (0, NOW(), NOW(), 1, 'c1.small', 'c1.small');
insert nova_flavors values (0, NOW(), NOW(), 2, 'c1.medium', 'c1.medium');
insert nova_flavors values (0, NOW(), NOW(), 3, 'c1.large', 'c1.large');
insert nova_flavors values (0, NOW(), NOW(), 4, 'c1.xlarge', 'c1.xlarge');

insert into masterworkflow values (0, NOW(), NOW(), 1, NULL);

insert into user_quotas values (0, NOW(), NOW(), 1, -1, 'total_instances', 5);
insert into user_quotas values (0, NOW(), NOW(), 2, -1, 'total_snapshots', 25);
insert into user_quotas values (0, NOW(), NOW(), 3, -1, 'total_storage', 5120);


insert into instances ( deleted, created_at, updated_at, name, owner_id, storage_size, nova_flavor_id, lifecycle, changestate, artefact_id, backup_retention_period_days, preferred_backup_window_start, preferred_backup_window_end, preferred_maintenance_window_start_time , preferred_maintenance_window_end_time, preferred_maintenance_window_start_day , preferred_maintenance_window_end_day, master_username, master_user_password, port ) values ( 0, now(), now(), 'Test_1', '123456789012', 1, 1, 'CREATING','PENDING', 1, 1, now(), now(), now(), now(), 'SUN', 'MON', 'adarsh', 'chumma', 3306);
