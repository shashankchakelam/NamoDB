User openrc


export OS_NO_CACHE='true'
export OS_TENANT_NAME='service_tenant_CHANGE_THIS'
export OS_USERNAME='user_CHANGE_THIS'
export OS_PASSWORD='password_CHANGE_THIS'
export OS_AUTH_URL='https://iam.staging.jiocloudservices.com:5000/v3/'
export OS_AUTH_STRATEGY='keystone'
export OS_REGION_NAME='RegionOne'
export CINDER_ENDPOINT_TYPE='publicURL'
export GLANCE_ENDPOINT_TYPE='publicURL'
export KEYSTONE_ENDPOINT_TYPE='publicURL'
export NOVA_ENDPOINT_TYPE='publicURL'
export NEUTRON_ENDPOINT_TYPE='publicURL'
export OS_PROJECT_DOMAIN_NAME='default'
export OS_USER_DOMAIN_NAME='default'
export OS_IDENTITY_API_VERSION=3



=========
Admin openrc

export OS_NO_CACHE='true'
export OS_TENANT_NAME='openstack'
export OS_USERNAME='admin_user_CHANGE_THIS'
export OS_PASSWORD='password_CHANGE_THIS'
export OS_AUTH_URL='https://iam.staging.jiocloudservices.com:35357/v3/'
export OS_AUTH_STRATEGY='keystone'
export OS_REGION_NAME='RegionOne'
export CINDER_ENDPOINT_TYPE='publicURL'
export GLANCE_ENDPOINT_TYPE='publicURL'
export KEYSTONE_ENDPOINT_TYPE='publicURL'
export NOVA_ENDPOINT_TYPE='publicURL'
export NEUTRON_ENDPOINT_TYPE='publicURL'
export OS_PROJECT_DOMAIN_NAME='default'
export OS_USER_DOMAIN_NAME='default'
export OS_IDENTITY_API_VERSION=3

