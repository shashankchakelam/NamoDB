#!/bin/bash
set -ex

# Script which first installs debian package, and then adds certificates
# with requests package
# Usage:
#   ./install-with-cert-fix.sh <package.deb> <cert_dir> [--force-confnew] [--force-overwrite]

# TODO(rushiagr): error out if two params are not provided

sudo dpkg -i $3 $4 $1

PKG_NAME=$(echo $1 | rev | cut -d'/' -f1 | rev | cut -d '_' -f1)
for CERT in $(ls $2); do
    cat $2/$CERT | sudo tee -a /usr/share/python/$PKG_NAME/lib/python2.7/site-packages/requests/cacert.pem
done
