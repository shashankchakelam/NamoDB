# TODO(rushiagr): get token by calling keystone first
TOKEN=7c25f790d0de41afbc35afa190d0d5b1

# List all instances
curl http://10.140.214.37:8779/v1/rushi/instances --header "X-Ath-Token: $TOKEN"

# Create an instance
curl http://10.140.214.37:8779/v1/rushi/instances --header "X-Ath-Token: $TOKEN" -d '{"name": "blah", "storage_size": 1, "vm_class": "tiny", "owner_id": "rushi"}'

# Delete the instance
curl http://10.140.214.37:8779/v1/rushi/instances/blah --header "X-Ath-Token: $TOKEN"

