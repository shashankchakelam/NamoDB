#!/bin/bash

/usr/share/python/namo-api/bin/python /usr/share/python/namo-api/lib/python2.7/site-packages/namo/cmd/sync_db.py --config-dir /etc/namo-api/
/usr/share/python/namo-api/bin/python /usr/share/python/namo-api/lib/python2.7/site-packages/namo/cmd/api.py --config-dir /etc/namo-api/
