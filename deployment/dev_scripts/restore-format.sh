#! /bin/bash

# Restores everything back to default mysql installation. Undoes the effect of running format.sh

sudo sed -i s,/home/ubuntu/datadir/mysql,/var/lib/mysql,g /etc/mysql/my.cnf
sudo service mysql restart
sudo rm -rf /home/ubuntu/datadir/*
sudo umount /home/ubuntu/datadir
sudo rm -rf /home/ubuntu/datadir
sudo rm -rf /home/ubuntu/ga-state
sudo sed -i 's|alias /var/lib/mysql/ -> /home/ubuntu/datadir/mysql,||g' /etc/apparmor.d/tunables/alias
sudo service apparmor restart
