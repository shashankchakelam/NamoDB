#! /bin/bash -ex

if [ "$TMUX" != "" ]; then
    echo "This script can't be run from inside a tmux session. Please run it outside tmux session"
    exit 1
fi

if [ $(env | grep -c ^OS) -lt 4 ]; then
    echo "Please source openrc before running this script"
    exit 1
fi

# Ensure mistral installed
if [ $(ls /usr/bin | grep -c mistral-server) == 0 ]; then
    echo "Mistral not installed. Run install-mistral.sh to install mistral"
    exit 1
fi

# Install mysql-5.6 as DateTime(3) is not supported with mysql-5.5
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password kaka123"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password kaka123"
sudo apt-get -y install mysql-server-5.6

# See if workflowapi processes are already running, and if yes, kill them
# TODO(rushiagr): in future, also check if mistral service is running, and kill
# it
PIDS=$(ps aux | grep api.py | grep api.conf$ | awk '{print $2}')
for PID in $PIDS; do
    sudo kill -9 $PID
done

# Kill 'namo' tmux session if already running
if [[ $(tmux list-sessions | grep -c namo) == "1" ]]; then
    tmux kill-session -t namo
fi

# Run mistral service inside 'namo' tmux session
tmux new-session -d -s namo -n one
tmux new-window -t namo:3 -n mistral
tmux send-keys -t namo:3 "/usr/bin/mistral-server --config-file /etc/mistral/mistral.conf" C-m
tmux detach-client -s namo

# NOTE(rushiagr): wait for a few seconds to ensure mistral service is running
# before we go forward
sleep 5

# Stop all mistral executions, delete workbooks, delete any running nova VMs
# TODO(rushiagr): following openstack cli commands will fail if no resource
#   (workbook, execution, nova vm, etc) are present. Write script in such a
#   way that it first checks if executions are present and then delete them
CREATE_EXEC_ID=$(mistral execution-list | grep createdb | awk '{print $2}')
MASTER_EXEC_ID=$(mistral execution-list | grep master | awk '{print $2}')
if [ ! -z $CREATE_EXEC_ID ]; then
    /usr/bin/mistral execution-delete $CREATE_EXEC_ID
fi
if [ ! -z $MASTER_EXEC_ID ]; then
    /usr/bin/mistral execution-delete $MASTER_EXEC_ID
fi

# TODO(rushiagr): re-enable the commented out lines, after adding checks
# to ensure the workbooks/vms are present
#mistral workbook-delete deletedb
#mistral workbook-delete createdb
#mistral workbook-delete master
#nova delete $(nova list | head -n -1 | tail -n +4 | awk '{print $2}')

sudo apt-get install -y libmysqlclient-dev python-mysqldb

if [[ $(mysql -uroot -pkaka123 -e 'show databases;' | grep -c namo) == 1 ]]; then
    mysql -uroot -pkaka123 -e "drop database namo;"
fi

mysql -uroot -pkaka123 -e "create database namo;"

cd ~/NamoDB


# TODO(rushiagr): need to add step to remove keystone auth from mistral.conf

/usr/bin/mistral workbook-create namo/conf/CreateDBInstance.yaml
/usr/bin/mistral workbook-create namo/conf/DeleteDBInstance.yaml
/usr/bin/mistral workbook-create namo/conf/master.yaml
/usr/bin/mistral workbook-create namo/conf/automated-backup.yaml

# Install namoclient
# TODO(rushiagr): not using namoclient any more. SHould remove the commented
# text in future
#cd namoclient
#sudo python setup.py install
#sudo pip install namoclient
#pwd
#sudo cp ../namo/files/namo /usr/local/bin/
#cd ..

# Install namo service
# TODO(rushiagr): put everything in a venv, and not pollute global things
cd namo
sudo python setup.py install
sudo pip install namo

sudo mkdir -p /etc/namo
# TODO(rushiagr): remove old unused conf files at files/namo*.conf, as new
# files are presetnt in files/devstack/namo*.conf
sudo cp files/devstack/namo-* /etc/namo

# Create database tables
python namo/cmd/db-create.py

# To run tests:
# python namo/tests/unit/workflowapi/test_workflowapi.py




mysql -uroot -pkaka123 -D namo -e "insert into masterworkflow (deleted, created_at, updated_at, execution_id ) values(0, now(), now(), NULL);"

#exit

cd namo/cmd


# Run services: 'workflowapi' and 'api' in a tmux session
# TODO(rushiagr): add api web service once shashank is done with it
tmux new-window -t namo:4 -n workflowapi
#tmux new-window -t namo:4 -n api
tmux send-keys -t namo:4 "python workflowapi.py --config-file /etc/namo/namo-workflowapi.conf" C-m
#tmux send-keys -t namo:4 "python api.py --config-file /etc/namo/namo-api.conf" C-m
tmux detach-client -s namo

# Sleep for 3 seconds to give time for workflow and api services to be up
# Not sure if this is necessary, but anyways adding it here
sleep 3

# This will populate database with dummy values.
python /usr/local/lib/python2.7/dist-packages/namo/db/mysqlalchemy/api.py

GLANCE_UUID=$(glance image-list | grep mysqlTest2 | awk '{print $2}')

mysql -uroot -pkaka123 -e "update namo.images set glance_uuid=\"$GLANCE_UUID\""

# This is a hack
#mysql -uroot -pkaka123 -e "insert into namo.db_engine (deleted, created_at, updated_at, id, name, major_version, minor_version, url, state) values (0, now(), now(), 1, 'name', 'majv', 'minv', 'url', 'PREFERRED_GLOBAL');"
#mysql -uroot -pkaka123 -e "insert into namo.os (deleted, created_at, updated_at, id, name, major_version, minor_version, url, state) values (0, now(), now(), 1, 'name', 'majv', 'minv', 'url', 'PREFERRED');"
#mysql -uroot -pkaka123 -e "insert into namo.raga (deleted, created_at, updated_at, id, version, url, state) values (0, now(), now(), 1, 'abcd1234', 'url', 'PREFERRED');"
#mysql -uroot -pkaka123 -e "insert into namo.artefact (deleted, created_at, updated_at, id, raga_id, os_id, db_engine_id) values (0, now(), now(), 1, 1, 1, 1);"
#mysql -uroot -pkaka123 -e "insert into namo.images (deleted, created_at, updated_at, id, glance_uuid, artefact_id) values (0, now(), now(), 1, 'glanceuuid', 1);"
#mysql -uroot -pkaka123 -e "insert into namo.nova_flavors (deleted, created_at, updated_at, id, nova_flavor_id, flavor_name) values (0, now(), now(), 1, 'flavoruuid', 'myflavor');"
#



#mysql -uroot -pkaka123 -e "insert into namo.instances (deleted, created_at, updated_at, name, owner_id, nova_flavor_id, storage_size, lifecycle, changestate, artefact_id, backup_retention_period_days, preferred_backup_window_start, preferred_backup_window_end, preferred_maintenance_window_start, preferred_maintenance_window_end, port) values (0, now(), now(), 'instance1', 12345, 1, 1, 'CREATING', 'PENDING', 16, 1, now(), now(), now(), now(), 3306);"
