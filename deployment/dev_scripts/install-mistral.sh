#! /bin/bash -x

# This script installs mistral. You still need to run mistral server by running
# 'mistral-server --config-file /etc/mistral/mistral.conf'

# TODO(rushiagr): see if mistral is installed. If yes, then don't install it
# TODO(shubham): standardize the PATH
cd ~

# see if mistral package is downloaded (in home folder), if yes don't download
if [[ $(ls | grep 'mistral-kilo_2015.1.0-1-u14.04+mos2_all.deb' | wc -l) == 0 ]]; then  
    echo "Downloading mistral kilo from s3"
    wget https://s3-ap-southeast-1.amazonaws.com/jio-dbaas/mistral-kilo_2015.1.0-1-u14.04%2Bmos2_all.deb
fi

sudo dpkg -i mistral-kilo_2015.1.0-1-u14.04+mos2_all.deb
sudo apt-get install -y -f
sudo dpkg -i mistral-kilo_2015.1.0-1-u14.04+mos2_all.deb


if [[ $(mysql -uroot -pkaka123 -e 'show databases;' | grep -c mistral) == 1 ]]; then
    mysql -uroot -pkaka123 -e "drop database mistral;"
fi

mysql -uroot -pkaka123 -e "create database mistral;"

sudo cp ~/NamoDB/namo/files/devstack/mistral.conf /etc/mistral/

/usr/bin/mistral-db-manage upgrade head

# This command throws an error, but does its job before failing.
/usr/bin/mistral-db-manage populate

echo ""
echo "Mistral installation successful. Ignore above error :-)"
