#! /bin/bash
set -ex

# Script to make plain devstack ready for DBaaS work. The script first downloads
# and uploads Ubuntu image, and then configures network to allow logging into
# the VM. Note that it is assumed that devstack is set up with Neutron, and NOT
# nova-network.

cd ~
source devstack/openrc admin admin

## Get Ubuntu image & upload it to Glance
#if [ $(ls | grep -c "dbaas2s.qcow2") -eq 1 ]; then
if [ $(ls | grep -c "ubuntu-14.04-server-cloudimg-amd64-disk1.img") -eq 1 ]; then
    echo 'A DBaaS cloud image file is already present, skipping download...'
else
    #wget https://s3-ap-southeast-1.amazonaws.com/jio-dbaas/mysqltest3.qcow2
    wget http://uec-images.ubuntu.com/releases/14.04/release/ubuntu-14.04-server-cloudimg-amd64-disk1.img
fi

#glance image-create --visibility public --disk-format qcow2 --container-format bare --name mysqlTest2 < mysqltest3.qcow2
glance image-create --visibility public --disk-format qcow2 --container-format bare --name trusty < ubuntu-14.04-server-cloudimg-amd64-disk1.img

#ubuntu_image=$(nova image-list | grep mysqlTest2 | awk '{print $2}')
ubuntu_image=$(nova image-list | grep trusty | awk '{print $2}')

# TODO(rushiagr): do not create keypair if already present, along with its PEM file
#  Create a keypair with Nova
nova keypair-add dbaas > ~/dbaas.pem
chmod 600 ~/dbaas.pem

net_id=$(neutron net-list | grep private | awk '{print $2}')

# Ubuntu's image wasn't working with any of the existing flavors. So create one!
# 24: flavor id, 512: ram, 4: disk, 2: vcpu

# TODO(rushiagr): do not create flavor if already present
nova flavor-create m1.little 24 512 5 2

nova boot --image $ubuntu_image --flavor m1.little --key-name dbaas --nic net-id=$net_id dbaas

# Looks like we should make sure nova VM becomes active. Putting a dumb sleep
# for now
# TODO(rushiagr): we should write a loop and wait till vm becomes active
sleep 10

server_id=$(nova list | grep dbaas | awk '{print $2}')

neutron floatingip-create public

floating_ip=$(neutron floatingip-list | grep 172 | awk '{print $5}')

nova floating-ip-associate $server_id $floating_ip

# TODO(rushiagr): check if these secgroup rules are added, and only if no add them
nova secgroup-add-rule default icmp -1 -1 0.0.0.0/0
nova secgroup-add-rule default tcp 22 22 0.0.0.0/0
nova secgroup-add-rule default tcp 8080 8080 0.0.0.0/0
nova secgroup-add-rule default tcp 8081 8081 0.0.0.0/0

#cinder create --name dbaas 1
#volume_id=$(cinder list | grep dbaas | awk '{print $2}')
#
#nova volume-attach $server_id $volume_id
