#! /bin/bash

# Script which removes mistral and namo setup on this system. Kills both the
# processes, uninstalls the package if present, and also
# cleans up their dbs if present

mysql -uroot -pkaka123 -e 'drop database mistral;';
mysql -uroot -pkaka123 -e 'drop database namo;';

if [[ $(tmux list-sessions | grep -c namo) == "1" ]]; then
    tmux kill-session -t namo
fi

sudo dpkg -r mistral-kilo
