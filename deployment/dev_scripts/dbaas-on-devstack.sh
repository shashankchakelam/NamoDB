#! /bin/bash
set -ex

# Script to make plain devstack ready for DBaaS work. The script first downloads
# and uploads Ubuntu image, and then configures network to allow logging into
# the VM. Note that it is assumed that devstack is set up with Neutron, and NOT
# nova-network.

cd ~
source devstack/openrc admin admin

## Get Ubuntu image & upload it to Glance
if [ $(ls | grep -c "trusty_mysql56_74a45260.qcow2") -eq 1 ]; then
    echo 'A DBaaS cloud image file is already present, skipping download...'
else
    wget https://s3-ap-southeast-1.amazonaws.com/jio-dbaas/trusty_mysql56_74a45260.qcow2
    #wget https://s3-ap-southeast-1.amazonaws.com/jio-dbaas/mysqltest3.qcow2
    #wget http://uec-images.ubuntu.com/releases/14.04/release/ubuntu-14.04-server-cloudimg-amd64-disk1.img
    #wget http://uec-images.ubuntu.com/releases/14.04/14.04.3/ubuntu-14.04-server-cloudimg-amd64-disk1.img
fi

# TODO(rushiagr): don't add image if already present
glance image-create --visibility public --disk-format qcow2 --container-format bare --name mysqlTest2 < trusty_mysql56_74a45260.qcow2

#ubuntu_image=$(nova image-list | grep trusty | awk '{print $2}')

# Create a keypair with Nova
#nova keypair-add dbaas > ~/dbaas.pem
#chmod 600 ~/dbaas.pem

#net_id=$(neutron net-list | grep private | awk '{print $2}')

# Ubuntu's image wasn't working with any of the existing flavors. So create one!
# 24: flavor id, 512: ram, 3: disk, 2: vcpu
nova flavor-create m1.little 25 2048 5 2
#
#nova boot --image $ubuntu_image --flavor m1.little --key-name dbaas --nic net-id=$net_id dbaas
#
#server_id=$(nova list | grep dbaas | awk '{print $2}')
#
#neutron floatingip-create public
#
#floating_ip=$(neutron floatingip-list | grep 172 | head -1 | awk '{print $5}')
#
#nova floating-ip-associate $server_id $floating_ip

nova secgroup-add-rule default icmp -1 -1 0.0.0.0/0
nova secgroup-add-rule default tcp 22 22 0.0.0.0/0
nova secgroup-add-rule default tcp 8080 8080 0.0.0.0/0
nova secgroup-add-rule default tcp 8081 8081 0.0.0.0/0
nova secgroup-add-rule default tcp 3306 3306 0.0.0.0/0

#cinder create --name dbaas 1
#volume_id=$(cinder list | grep dbaas | awk '{print $2}')
#
#nova volume-attach $server_id $volume_id
