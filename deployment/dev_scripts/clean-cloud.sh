#! /bin/bash -x
source ~/devstack/openrc admin admin

# TODO(rushiagr): if no instance, volume or kp present, skip instead of calling
# the following commands
nova delete $(nova list | head -n -1 | tail -n +4 | awk '{print $2}')

sleep 10

cinder delete $(cinder list | head -n -1 | tail -n +4 | awk '{print $2}')

for kp in $(nova keypair-list | head -n -1 | tail -n +4 | awk '{print $2}'); do
    nova keypair-delete $kp
done

for fip in $(nova floating-ip-list | head -n -1 | tail -n +4 | awk '{print $4}'); do
    nova floating-ip-delete $fip
done
