#! /bin/bash

# Script to install devstack with neutron, essential to work with DBaaS.
# After running this script, you just need to run './stack.sh'

cd ~
if [ $(ls | grep -c ^devstack$) -eq 1 ]; then
    cd devstack
    git pull origin master
    cd ..
else
    git clone https://github.com/openstack-dev/devstack
fi
cd devstack

cat > local.conf << EOF
[[local|localrc]]
disable_service n-net
enable_service q-svc
enable_service q-agt
enable_service q-dhcp
enable_service q-l3
enable_service q-meta

ADMIN_PASSWORD=nova
DATABASE_PASSWORD=kaka123
RABBIT_PASSWORD=nova
SERVICE_PASSWORD=nova
SERVICE_TOKEN=s0m3-r4nd0m-53rv1c3-t0k3n

HOST_IP=127.0.0.1

VOLUME_BACKING_FILE_SIZE=30000M
EOF
