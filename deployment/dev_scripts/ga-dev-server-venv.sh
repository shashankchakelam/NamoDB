#! /bin/bash

# TODO(rushiagr): 1. check if directory is correct. 2. Check if venv not
# activated. 3. check if pip installed. 4. check if virtualenv installed. 5.
# also write script which will run in global python and not venv. 6. put bin
# files at proper locations

echo "Make sure you run this file from inside 'scripts' directory, as:"
echo "  ./ga-dev-server.sh"
cd ../../ga/ # Move to ga package to get corresponding requirements file

if [ $(ls -a | grep -c '.venv') == 0 ]; then
    virtualenv .venv
fi

. .venv/bin/activate

pip install -r requirements.txt

python setup.py develop

gunicorn_pecan conf/dev-config.py --workers 2
