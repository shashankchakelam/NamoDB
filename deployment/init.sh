#!/bin/bash

# NOTE: this script is more like notes. Do not run this script blindly
# without understanding each and every line

# step 1: install required pkgs
sudo apt-get install vim git htop tmux curl python-pip python-dev libffi-dev libssl-dev libmysqlclient-dev
