from client import exceptions
from datetime import datetime
import logging
from random import randint
from rdstest import constants
from rdstest import helpers
from rdstest.db import query
import time
import uuid

helpers.initialize_logging()

def run_infinite_smoke_test():
    """
    Runs an infinite loop of basic smoke tests. Making it resilient so that it
    will log failures but will still go on so that we have data points on when
    and why failures occurred while tests still proceed

    Take a look at the following for the setup steps for this to work:
    https://docs.google.com/document/d/13f7O0Fwkrq-6xDo1QlL_FUQ3Nd2sSGIS7pp3oWyyAr8/edit

    Returns
    -------
    Nothing
    """
    test_run_id = str(uuid.uuid4())

    try:
        logging.info("BEGIN!")

        # Create test_run in test database
        query.create_test_run(test_run_id)
        logging.info("Starting test_run id: {0}".format(test_run_id))

        # Test variables
        test_round_number = 1
        test_round_success = False
        test_iteration_dict = {}
        test_user_name = 'rds-test-01'
        #test_user_name = 'rds_user_0045'

        # Parameters for creating the instance
        instance_prefix = 'test-instance-' + str(randint(0,100000)) + "-"

        # Setup client
        helpers.setup_jcsclient(test_user_name)

        while True:
            try:
                # Create test_iteration in database
                query.create_test_iteration(test_round_number, test_run_id)
                logging.info("#1. Starting test_round_number: %d", test_round_number)
                test_iteration_dict = {}
                test_round_success = False
                test_error_message = ''

################################################################################
                # Create test instance
                instance_name = instance_prefix + str(test_round_number)
                logging.info("#2. Creating DB Instance: %s", instance_name)
                helpers.create_test_instance(instance_name)

                # Update test_iteration with instance create start time
                test_iteration_dict['instance_create_start_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)

                logging.info("#3. Waiting for Create DB Instance to complete")
                helpers.wait_for_instance_status(
                    instance_name,
                    'available',
                    constants.wait_for_instance_creation_in_secs
                )

                test_iteration_dict['instance_create_end_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)
                logging.info("#4. Instance successfully created!!!")
################################################################################
                logging.info("#5. Verify connectivity to instance")
                helpers.verify_connectivity_test_instance(instance_name)
################################################################################
                snapshot_name = 'test-snap-' + instance_name
                logging.info("#6. Creating DB Snapshot: %s", snapshot_name)
                helpers.create_test_snapshot(instance_name, snapshot_name)

                test_iteration_dict['backup_create_start_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)

                logging.info("#7. Waiting for Create DB Snapshot to complete")
                helpers.wait_for_instance_status(
                    instance_name,
                    'available',
                    constants.wait_for_instance_creation_in_secs
                )

                test_iteration_dict['backup_create_end_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)
                logging.info("#8. Snapshot successfully created!!!")
################################################################################
                logging.info("#9. Deleting DB Instance")
                helpers.delete_instance(instance_name)
                test_iteration_dict['instance_delete_start_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)

                logging.info("#10. Waiting for Delete DB Instance to complete")
                helpers.wait_for_instance_deletion(
                    instance_name,
                    constants.wait_for_instance_deletion_in_secs
                )
                test_iteration_dict['instance_delete_end_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)
                logging.info("#11. Instance successfully deleted!!!")
################################################################################
                rest_instance_name = 'test-rest-' + instance_name
                logging.info("#12. Restoring DB Instance: %s", rest_instance_name)
                helpers.restore_instance(rest_instance_name, snapshot_name)

                test_iteration_dict['instance_restore_start_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)

                logging.info("#13. Waiting for Restore DB Instance to complete")
                helpers.wait_for_instance_status(
                    rest_instance_name,
                    'available',
                    constants.wait_for_instance_creation_in_secs
                )

                test_iteration_dict['instance_restore_end_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)
                logging.info("#14. Instance successfully restored!!!")
################################################################################
                logging.info("#15. Verify connectivity to restored instance")
                helpers.verify_connectivity_test_instance(rest_instance_name)
################################################################################
                logging.info("#16. Deleting Restored DB Instance")
                helpers.delete_instance(rest_instance_name)
                test_iteration_dict['restored_instance_delete_start_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)

                logging.info("#17. Waiting for Delete DB Instance to complete")
                helpers.wait_for_instance_deletion(
                    rest_instance_name,
                    constants.wait_for_instance_deletion_in_secs
                )
                test_iteration_dict['restored_instance_delete_end_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)
                logging.info("#18. Restored Instance successfully deleted!!!")
################################################################################
                logging.info("#19. Deleting DB Snapshot")
                helpers.delete_snapshot(snapshot_name)

                test_iteration_dict['backup_delete_start_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)

                logging.info("#20. Waiting for Delete DB Snapshot to complete")
                helpers.wait_for_snapshot_deletion(
                    snapshot_name,
                    constants.wait_for_instance_deletion_in_secs
                )
                test_iteration_dict['backup_delete_end_time'] = datetime.now()
                query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)
                logging.info("#21. Snapshot successfully deleted!!!")
################################################################################
                test_round_success = True
            except exceptions.HTTP400 as e:
                logging.exception("Got a 400 error:")
                test_error_message = e.message
                # This is because of the quota error. Waiting so that we don't
                # unproductively spam APIWebService and see thousands of failed
                # tests. Need to improve this by checking for the specific msg
                time.sleep(600)
            except Exception as e:
                logging.exception(e.message)
                test_error_message = e.message
            finally:
                if test_round_success:
                    logging.info("SUCCESSFULLY completed test_round_number: %d", test_round_number)
                    test_iteration_dict['state'] = 'success'
                    test_iteration_dict['end_time'] = datetime.now()
                    query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)
                else:
                    logging.info("UNSUCCESSFULLY completed test_round_number: %d", test_round_number)
                    test_iteration_dict['state'] = 'failure'
                    test_iteration_dict['end_time'] = datetime.now()
                    test_iteration_dict['message'] = test_error_message
                    query.update_test_iteration(test_round_number, test_run_id, test_iteration_dict)
                logging.info("====================================================================")
                test_round_number += 1
    except Exception:
        logging.exception("Unexpected exception while initializing test_run: {0}".format(test_run_id))
    finally:
        update_values = {}
        # TODO(shank): Update this correctly
        update_values['state'] = 'success'
        update_values['end_time'] = datetime.now()
        query.update_test_run(test_run_id, update_values)

if __name__ == '__main__':
    run_infinite_smoke_test()
