from datetime import datetime
from rdstest import constants
from rdstest.db import models
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import and_

engine = create_engine(constants.rds_test_db_cxn)

def get_session():
    """
    Creates a new session

    Returns
    -------
    Newly created session
    """
    Session = sessionmaker(bind=engine, autocommit=True)
    session = Session()
    return session

def model_query(*args, **kwargs):
    session = kwargs.get('session') or get_session()

    with session.begin(subtransactions=True):
        query = session.query(*args)
        return query

def get_credential(user_name):
    return model_query(models.Credential).filter_by(name=user_name).one()

def get_test_run(test_run_id):
    return model_query(models.TestRun).filter_by(id=test_run_id).one()

def get_test_runs_in_progress():
    return model_query(models.TestRun).filter_by(state='in-progress')

def get_test_iteration(test_iteration_id, test_run_id):
    return model_query(models.TestIteration).filter_by(id=test_iteration_id, test_run_id=test_run_id).one()

def get_test_iterations_for_test_run_datetime_range(test_run_id, start_datetime, end_timedate):
    return model_query(models.TestIteration).\
        filter(and_(models.TestIteration.test_run_id == test_run_id,
                    models.TestIteration.end_time > start_datetime,
                    models.TestIteration.end_time < end_timedate))

def create_test_run(test_run_id):
    test_run_ref = models.TestRun()
    test_run_ref.id = test_run_id
    test_run_ref.start_time = datetime.now()
    test_run_ref.state = 'in-progress'

    session = get_session()
    with session.begin(subtransactions=True):
        session.add(test_run_ref)
        session.flush()
        return test_run_ref

def create_test_iteration(test_iteration_id, test_run_id):
    test_iteration_ref = models.TestIteration()
    test_iteration_ref.id = test_iteration_id
    test_iteration_ref.test_run_id = test_run_id
    test_iteration_ref.start_time = datetime.now()
    test_iteration_ref.state = 'in-progress'

    session = get_session()
    with session.begin(subtransactions=True):
        session.add(test_iteration_ref)
        session.flush()
        return test_iteration_ref.id

def update_test_run(test_run_id, values):
    session = get_session()
    with session.begin(subtransactions=True):
        test_run_ref = get_test_run(test_run_id)
        for k, v in values.iteritems():
            setattr(test_run_ref, k, v)
        session.add(test_run_ref)
        session.flush()
        return test_run_ref

def update_test_iteration(test_iteration_id, test_run_id, values):
    session = get_session()
    with session.begin(subtransactions=True):
        test_iteration_ref = get_test_iteration(test_iteration_id, test_run_id)
        for k, v in values.iteritems():
            setattr(test_iteration_ref, k, v)
        session.add(test_iteration_ref)
        session.flush()
        return test_iteration_ref
