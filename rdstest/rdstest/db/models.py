from sqlalchemy import Column, Integer, String, Enum, ForeignKey, PrimaryKeyConstraint
from sqlalchemy import create_engine
from sqlalchemy.dialects import mysql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from rdstest import constants

Base = declarative_base()

class TestRun(Base):
    __tablename__ = 'test_runs'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    id = Column(String(36), primary_key=True)
    start_time = Column(mysql.DATETIME, nullable=False)
    end_time = Column(mysql.DATETIME, nullable=True)
    state = Column(Enum('in-progress', 'success', 'failure'), nullable=False)

class TestIteration(Base):
    __tablename__ = 'test_iterations'
    __table_args__ = (
        PrimaryKeyConstraint('id', 'test_run_id'),
        {'mysql_engine': 'InnoDB'}
    )

    id = Column(Integer, nullable=False)
    test_run_id = Column(String(36), ForeignKey('test_runs.id'), nullable=False)
    start_time = Column(mysql.DATETIME, nullable=False)
    end_time = Column(mysql.DATETIME, nullable=True)
    state = Column(Enum('in-progress', 'success', 'failure'), nullable=False)

    # Granular start and end times for monitoring
    instance_create_start_time = Column(mysql.DATETIME, nullable=True)
    instance_create_end_time = Column(mysql.DATETIME, nullable=True)
    instance_delete_start_time = Column(mysql.DATETIME, nullable=True)
    instance_delete_end_time = Column(mysql.DATETIME, nullable=True)
    backup_create_start_time = Column(mysql.DATETIME, nullable=True)
    backup_create_end_time = Column(mysql.DATETIME, nullable=True)
    backup_delete_start_time = Column(mysql.DATETIME, nullable=True)
    backup_delete_end_time = Column(mysql.DATETIME, nullable=True)
    instance_restore_start_time = Column(mysql.DATETIME, nullable=True)
    instance_restore_end_time = Column(mysql.DATETIME, nullable=True)
    restored_instance_delete_start_time = Column(mysql.DATETIME, nullable=True)
    restored_instance_delete_end_time = Column(mysql.DATETIME, nullable=True)
    message = Column(String(2000), nullable=True)

    test_run = relationship("TestRun", backref="test_iterations",
            foreign_keys=test_run_id)

class Credential(Base):
    __tablename__ = 'credentials'
    __table_args__ = {'mysql_engine': 'InnoDB'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    access_key = Column(String(50), nullable=False)
    secret_key = Column(String(50), nullable=False)

def register_models():
    models = [TestRun, TestIteration, Credential]

    connection_str = constants.rds_test_db_cxn
    engine = create_engine(connection_str, echo=True)
    for model in models:
        model.metadata.create_all(engine)

if __name__ == '__main__':
    register_models()
