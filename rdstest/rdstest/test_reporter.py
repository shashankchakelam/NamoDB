from datetime import datetime, timedelta
from rdstest import helpers, mail
from rdstest.db import query
import logging
import subprocess
import time
import requests
import json

helpers.initialize_logging('test_reporter.log')

def test_reporter():
    while True:
        time.sleep(3600)
        # Query for all test_iterations completed in the last 1 hour
        current_time = datetime.now()
        start_time = current_time - timedelta(hours=1, minutes=1) # A little more than 1 hour

        # Get number of success and failure. Set as subject
        test_count = 0
        test_success = 0
        test_failure = 0
        sum_create_time = timedelta()
        sum_delete_time = timedelta()
        sum_restore_time = timedelta()
        sum_snapshot_time = timedelta()
        sum_del_snapshot_time = timedelta()

        message_body = """
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>RDSInfiniteTest Results</title>
  <style type="text/css" media="screen">
    table{{
        empty-cells:hide;
	width:100%;
	font-size:100%;
    }}
    h1{{
	font-size: 16pt;
	color: gray;
    }}
    td.cell{{
        background-color: white;
	border: solid 2px gray;
	text-align: center;
    }}
    th.cell{{
        background-color: light-blue;
	border: solid 2px gray;
	text-align: center;
    }}
  </style>
</head>
<body>
  <h1>Each test covers: CreateDBInstance, DescribeDBInstances, DeleteDBInstance, CreateDBSnapshot, DescribeDBSnapshots, DeleteDBSnapshot, RestoreDBInstanceFromDBSnapshot</h1>
  <table style="border: black 2px solid; border-collapse: collapse;">
    <tr><th class="cell">TestRunId</th><th class="cell">TestIterationId</th><th class="cell">Result</th><th class="cell">Message</th></tr>
    {0}
  </table>
  <h1>Statistics</h1>
  {1}
</body>
"""
        test_result = """<tr><td class="cell">{0}</td><td class="cell">{1}</td><td class="cell">{2}</td><td class="cell">{3}</td></tr>"""
        test_results = ''
        statistics = ''

        test_runs = query.get_test_runs_in_progress()

        for test_run in test_runs:
            print test_run.id
            test_iterations = query.get_test_iterations_for_test_run_datetime_range(
                test_run.id,
                start_time,
                current_time
            )

            for test_iteration in test_iterations:
                test_count += 1
                if test_iteration.state == 'success':
                    test_results += test_result.format(test_run.id, test_iteration.id, "<span style=\"background-color: green\">success</span>", None)
                    test_success += 1
                    sum_create_time += (test_iteration.instance_create_end_time - test_iteration.instance_create_start_time)
                    sum_delete_time += (test_iteration.instance_delete_end_time - test_iteration.instance_delete_start_time)
                    sum_delete_time += (test_iteration.restored_instance_delete_end_time - test_iteration.restored_instance_delete_start_time)
                    sum_restore_time += (test_iteration.instance_restore_end_time - test_iteration.instance_restore_start_time)
                    sum_snapshot_time += (test_iteration.backup_create_end_time - test_iteration.backup_create_start_time)
                    sum_del_snapshot_time += (test_iteration.backup_delete_end_time - test_iteration.backup_delete_start_time)
                if test_iteration.state == 'failure':
                    test_results += test_result.format(test_run.id, test_iteration.id, "<span style=\"background-color: red\">failure</span>", test_iteration.message)
                    test_failure += 1

        if test_success != 0:
            avg_create_time = sum_create_time / test_success
            avg_delete_time = sum_delete_time / (2*test_success)
            avg_restore_time = sum_restore_time / test_success
            avg_snapshot_time = sum_snapshot_time / test_success
            avg_del_snapshot_time = sum_del_snapshot_time / test_success

            statistics += "<p>Average Successful Create Time (seconds): " + str(avg_create_time.total_seconds()) + "</p>"
            statistics += "<p>Average Successful Delete Time (seconds): " + str(avg_delete_time.total_seconds()) + "</p>"
            statistics += "<p>Average Successful Restore Time (seconds): " + str(avg_restore_time.total_seconds()) + "</p>"
            statistics += "<p>Average Successful Snapshot Create Time (seconds): " + str(avg_snapshot_time.total_seconds()) + "</p>"
            statistics += "<p>Average Successful Snapshot Delete Time (seconds): " + str(avg_del_snapshot_time.total_seconds()) + "</p>"

        subject = "RDSInfiniteTest Hourly: Tests: {0}, Success: {1}, Failure: {2}".format(test_count, test_success, test_failure)

        message_body = message_body.format(test_results, statistics)

        print subject
        print message_body
        
        # Send alert on slack also when there are failure.
        if test_failure != 0:
            slack_text= subject
            slack_payload = {"username":"RDSInfiniteTest", 
                             "attachments": [
                                              {
                                                "color": "#ff0000",
                                                "title": "Check Email for more details",
                                                "title_link": "https://mail.ril.com/"
                                              }
                                            ],
                             "text": slack_text
                            }
            r = requests.post("https://hooks.slack.com/services/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", data=json.dumps(slack_payload))
            print(r.status_code, r.reason)
        mail.send_mail(subject, message_body, 'JioCloud.RDSTeam@ril.com', 'Shashank.Chakelam@ril.com')

if __name__ == '__main__':
    test_reporter()
