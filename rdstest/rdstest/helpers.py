from client import cloud as jcs_client
from client import common
from client import exceptions
from datetime import datetime
import logging
from rdstest import constants
from rdstest.db import query
from sqlalchemy import create_engine
import time

def initialize_logging(filename=None):
    """
    Initialize logging with config parameters

    Returns
    -------
    Nothing
    """
    if not filename:
        filename = constants.log_filename

    logging.basicConfig(
        format=constants.log_format,
        filename=filename,
        level=constants.log_level
)
# TODO(shank): Is there a better way to do this?
initialize_logging()

def setup_jcsclient(test_user_name):
    """
    Setup jcs client

    Parameters
    ----------
    test_user_name: Name of the test user whose credentials we'll use for setup

    Returns
    -------
    Nothing
    """
    credential = query.get_credential(test_user_name)
    access_key = credential.access_key
    secret_key = credential.secret_key
    logging.info("Fetched access_key: {0} for user: {1}".format(access_key, test_user_name))

    common.setup_client(
        access_key,
        secret_key,
        constants.compute_url,
        constants.vpc_url,
        rds_url=constants.rds_url
    )

    logging.info("JCSClient successfully setup")

def wait_for_instance_status(instance_name, expected_status, max_wait_time):
    """
    Wait for instance to reach a particular status

    Parameters
    ----------
    instance_name: Name of the instance to check
    expected_status: Expected instance status
    max_wait_time: Max time to wait for instance to reach status

    Returns
    -------
    Nothing
    """
    start_date_time = datetime.now()
    while True:
        # Check DescribeDBInstances API response
        describe_resp_dict = jcs_client.describe_db_instances(DBInstanceIdentifier=instance_name)
        status = describe_resp_dict['DescribeDBInstancesResponse']['DescribeDBInstancesResult']['DBInstances']['DBInstance']['DBInstanceStatus']

        if status == expected_status:
            return
        logging.info("Status is still not {0}, it is: {1}".format(expected_status, status))

        # Verify that we haven't waited for longer than 10 mins
        present_date_time = datetime.now()
        if (present_date_time - start_date_time).seconds > max_wait_time:
            raise Exception("Instance {0} not in {1} even after {2} seconds!".format(instance_name, expected_status, max_wait_time))

        # Sleep for 30 seconds
        time.sleep(constants.wait_between_describe_calls_in_secs)

def wait_for_instance_deletion(instance_name, max_wait_time):
    """
    Wait for specified instance to be deleted

    Parameters
    ----------
    instance_name: Name of the instance to check
    max_wait_time: Max time to wait for instance deletion

    Returns
    -------
    Nothing
    """
    start_date_time = datetime.now()
    while True:
        try:
            # Make DescribeDBInstances API call
            jcs_client.describe_db_instances(DBInstanceIdentifier=instance_name)
        except exceptions.HTTP404:
            logging.info("Instance successfully deleted")
            break
        except Exception:
            logging.exception("Caught unexpected exception")

        logging.info("Instance still not deleted yet")

        # Verify that we haven't waited for longer than 10 mins
        present_date_time = datetime.now()
        if (present_date_time - start_date_time).seconds > max_wait_time:
            raise Exception("Instance {0} not deleted even after {1} seconds!".format(instance_name, max_wait_time))

        # Sleep for 30 seconds
        time.sleep(constants.wait_between_describe_calls_in_secs)

def wait_for_snapshot_deletion(snapshot_name, max_wait_time):
    """
    Wait for specified snapshot to be deleted

    Parameters
    ----------
    snapshot_name: Name of the snapshot to check
    max_wait_time: Max time to wait for instance deletion

    Returns
    -------
    Nothing
    """
    start_date_time = datetime.now()
    while True:
        try:
            # Make DescribeDBSnapshots API call
            jcs_client.describe_db_snapshots(DBSnapshotIdentifier=snapshot_name)
        except exceptions.HTTP404:
            logging.info("Snapshot successfully deleted")
            break
        except Exception:
            logging.exception("Caught unexpected exception")

        logging.info("Snapshot still not deleted yet")

        # Verify that we haven't waited for longer than 10 mins
        present_date_time = datetime.now()
        if (present_date_time - start_date_time).seconds > max_wait_time:
            raise Exception("Snapshot {0} not deleted even after {1} seconds!".format(snapshot_name, max_wait_time))

        # Sleep for 30 seconds
        time.sleep(constants.wait_between_describe_calls_in_secs)

def create_test_snapshot(instance_name, snapshot_name):
    """
    Create a snapshot using test instance properties

    Parameters
    ----------
    instance_name: Name of the instance to take snapshot of
    snapshot_name: Name of the snapshot to be created

    Returns
    -------
    Dictionary of snapshot properties
    """
    create_resp_dict = jcs_client.create_db_snapshot(instance_name, snapshot_name)

    logging.info("Verifying CreateDBSnapshot API response")
    status = create_resp_dict['CreateDBSnapshotResponse']['CreateDBSnapshotResult']['DBSnapshot']['Status']

    if status == 'creating':
        return create_resp_dict

    return Exception("Unexpected status: {0} on creating snapshot: {1}".format(status, snapshot_name))

def create_test_instance(instance_name):
    """
    Create an instance using test instance properties

    Parameters
    ----------
    instance_name: Name of the instance to create

    Returns
    -------
    Dictionary of instance properties
    """
    create_resp_dict = jcs_client.create_db_instance(
        instance_name,
        constants.test_instance_class,
        constants.test_instance_engine,
        constants.test_instance_storage,
        constants.test_instance_master_username,
        constants.test_instance_master_user_password
    )

    logging.info("Verifying CreateDBInstance API response")
    status = create_resp_dict['CreateDBInstanceResponse']['CreateDBInstanceResult']['DBInstance']['DBInstanceStatus']

    if status == 'creating':
        return create_resp_dict

    return Exception("Unexpected status: {0} on creating instance: {1}".format(status, instance_name))

def restore_instance(instance_name, snapshot_name):
    """
    Restore specified instance from snapshot

    Parameters
    ----------
    instance_name: Name of the restored instance
    snapshot_name: Name of the snapshot to restore from

    Returns
    -------
    Dictionary of instance properties
    """

    restore_resp_dict = jcs_client.restore_db_instance_from_db_snapshot(instance_name, snapshot_name)

    logging.info("Verifying RestoreDBInstanceFromDBSnapshot API response")
    status = restore_resp_dict['RestoreDBInstanceFromDBSnapshotResponse']['RestoreDBInstanceFromDBSnapshotResult']['DBInstance']['DBInstanceStatus']

    if status == 'restoring':
        return restore_resp_dict

    return Exception("Unexpected status: {0} on restoring instance: {1}".format(status, instance_name))

def delete_instance(instance_name):
    """
    Delete specified instance

    Parameters
    ----------
    instance_name: Name of the instance to delete

    Returns
    -------
    Dictionary of instance properties
    """
    del_resp_dict = jcs_client.delete_db_instance(instance_name, SkipFinalSnapshot=True)

    logging.info("Verifying DeleteDBInstance API response")
    status = del_resp_dict['DeleteDBInstanceResponse']['DeleteDBInstanceResult']['DBInstance']['DBInstanceStatus']

    if status == 'deleting':
        return del_resp_dict

    return Exception("Unexpected status: {0} on deleting instance: {1}".format(status, instance_name))

def delete_snapshot(snapshot_name):
    """
    Delete specified Snapshot

    Parameters
    ----------
    snapshot_name: Name of the snapshot to delete

    Returns
    -------
    Dictionary of snapshot properties
    """
    del_resp_dict = jcs_client.delete_db_snapshot(snapshot_name)

    logging.info("Verifying DeleteDBSnapshot API response")
    status = del_resp_dict['DeleteDBSnapshotResponse']['DeleteDBSnapshotResult']['DBSnapshot']['Status']

    if status == 'deleting':
        return del_resp_dict

    return Exception("Unexpected status: {0} on deleting snapshot: {1}".format(status, snapshot_name))

def verify_connectivity_test_instance(instance_name):
    """
    Verify that you can connect to the mysql database on test instance

    Parameters
    ----------
    instance_name: Name of the instance

    Returns
    -------
    Nothing
    """
    endpoint = _get_instance_endpoint(instance_name)

    instance_cxn_string = constants.mysql_cxn_string.format(
        constants.test_instance_master_username,
        constants.test_instance_master_user_password,
        endpoint,
        'mysql'
    )

    eng = create_engine(instance_cxn_string)
    with eng.connect() as cxn:
        cxn.execute("SHOW DATABASES;")

def _get_instance_endpoint(instance_name):
    describe_resp_dict = jcs_client.describe_db_instances(DBInstanceIdentifier=instance_name)
    endpoint = describe_resp_dict['DescribeDBInstancesResponse']['DescribeDBInstancesResult']['DBInstances']['DBInstance']['Endpoint']['Address']
    return endpoint
