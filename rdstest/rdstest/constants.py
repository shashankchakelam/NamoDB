import logging
from pytz import timezone

# Generic mysql connection string
mysql_cxn_string = "mysql://{0}:{1}@{2}/{3}?charset=utf8"

# rds_test Database Endpoint
rds_test_user_name = 'root'
rds_test_password = 'kaka123'
rds_test_endpoint = '10.140.215.5'
rds_test_database = 'rds_test'
rds_test_db_cxn = mysql_cxn_string.format(rds_test_user_name, rds_test_password, rds_test_endpoint, rds_test_database)

# Wait times for user operations
wait_for_instance_creation_in_secs = 600
wait_for_instance_deletion_in_secs = 600
wait_between_describe_calls_in_secs = 30

#compute_url = 'https://compute.ind-west-1.staging.jiocloudservices.com'
#vpc_url = 'https://vpc.ind-west-1.staging.jiocloudservices.com'
#rds_url = 'https://rds.ind-west-1.staging.jiocloudservices.com'
compute_url = 'https://compute.ind-west-1.internal.jiocloudservices.com'
vpc_url = 'https://vpc.ind-west-1.internal.jiocloudservices.com'
rds_url = 'https://rds.ind-west-1.jiocloudservices.com'

# Constants for logging
log_format = '%(asctime)s %(levelname)-5.5s [%(name)s][%(threadName)s] %(message)s'
log_filename = 'smoke_test.log'
log_level = logging.INFO

# Constants for instance creation
test_instance_class = 'c1.small'
test_instance_engine = 'mysql'
test_instance_storage = 10
test_instance_master_username = 'shank'
test_instance_master_user_password = 'secret123'

# Timezone
indian_tz = timezone('Asia/Kolkata')

# RIL SMTP server
smtp_server = '10.137.2.23'