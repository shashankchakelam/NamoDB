# Welcome to NamoDB!

NamoDB is a platform for providing Relational Database as a Service. Any relational database (MySQL, Postgres, Oracle, SQL Server, etc) can be plugged in and make use of ~85% of the platform features.

Documents - https://drive.google.com/folderview?id=0ByLZWCQ68LMAOWhRQm1kU1hyR3M&usp=sharing

New Issue tracking - https://jira.ril.com/browse/JDB

Old Issue tracking - https://gitlab.com/shashankchakelam/NamoDB/issues

SchemaSpy - http://54.169.117.232/


## Code flow guidelines:

git checkout -b UpdateReadmeBranch

vi README.md

-- Make changes to file

git add README.md

git commit -m "Random changes made to README.md"

-- Before pushing the code changes remotely, run unit tests locally by executing \"./run_tests.sh --local\" for each component

git push origin UpdateReadmeBranch

-- Verify that the build passed in Gitlab CI. You can see this in the Gitlab UI or in the rds-gitlab slack channel

-- In Gitlab UI, create a Merge Request if you already haven't. If the changes are still in progress, make sure the title has prefix "WIP:"

-- Get review comments for Merge Request

-- Address comments and get at least one Approval for the MR. All SDEs in the team are approvers. You won't be able to merge without an approval

-- Get code merged via Gitlab UI and delete source branch
