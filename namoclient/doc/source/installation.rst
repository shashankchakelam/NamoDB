============
Installation
============

At the command line::

    $ pip install namoclient

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv namoclient
    $ pip install namoclient