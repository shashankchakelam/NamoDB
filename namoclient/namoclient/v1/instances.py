# Copyright 2013 OpenStack LLC.
# Copyright 2013 IBM Corp.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_utils import encodeutils
import six
import warlock

from namoclient.common import utils
from namoclient.v1 import schemas

DEFAULT_PAGE_SIZE = 20

SORT_DIR_VALUES = ('asc', 'desc')
SORT_KEY_VALUES = ('id','owner_id')


class Controller(object):
    def __init__(self, http_client, schema_client):
        self.http_client = http_client
        self.schema_client = schema_client

    @utils.memoized_property
    def model(self):
        schema = self.schema_client.get('instance')
        return warlock.model_factory(schema.raw(), schemas.SchemaBasedModel)

    def list(self, **kwargs):
        """Retrieve a listing of Task objects

        :param page_size: Number of instances to request in each paginated request
        :returns generator over list of Tasks
        """
        def paginate(url):
            resp, body = self.http_client.get(url)
            for instance in body['instances']:
                yield instance
            try:
                next_url = body['next']
            except KeyError:
                return
            else:
                for instance in paginate(next_url):
                    yield instance

        filters = kwargs.get('filters', {})

        if not kwargs.get('page_size'):
            filters['limit'] = DEFAULT_PAGE_SIZE
        else:
            filters['limit'] = kwargs['page_size']

        if 'marker' in kwargs:
            filters['marker'] = kwargs['marker']

        sort_key = kwargs.get('sort_key')
        if sort_key is not None:
            if sort_key in SORT_KEY_VALUES:
                filters['sort_key'] = sort_key
            else:
                raise ValueError('sort_key must be one of the following: %s.'
                                 % ', '.join(SORT_KEY_VALUES))

        sort_dir = kwargs.get('sort_dir')
        if sort_dir is not None:
            if sort_dir in SORT_DIR_VALUES:
                filters['sort_dir'] = sort_dir
            else:
                raise ValueError('sort_dir must be one of the following: %s.'
                                 % ', '.join(SORT_DIR_VALUES))

        for param, value in filters.items():
            if isinstance(value, six.string_types):
                filters[param] = encodeutils.safe_encode(value)

        url = '/v1/instances?%s' % six.moves.urllib.parse.urlencode(filters)
        url = '/v1/'+ kwargs.get('filters').get('owner_id') + '/instances'
        for instance in paginate(url):
            #NOTE(flwang): remove 'self' for now until we have an elegant
            # way to pass it into the model constructor without conflict
            instance.pop('self', None)
            yield self.model(**instance)

    def get(self, owner_id, name):
        """Get a instance based on given instance id."""
        url = '/v1/' + owner_id + '/instances/' + name
        resp, body = self.http_client.get(url)
        #NOTE(flwang): remove 'self' for now until we have an elegant
        # way to pass it into the model constructor without conflict
        body.pop('self', None)
        return self.model(**body)

    def create(self, **kwargs):
        """Create a new instance."""
        instance = self.model()

        for (key, value) in kwargs.items():
            try:
                setattr(instance, key, value)
            except warlock.InvalidOperation as e:
                raise TypeError(utils.exception_to_str(e))
        url = '/v1/' + instance.owner_id + '/instances'
        resp, body = self.http_client.post(url, data=instance)
        #NOTE(flwang): remove 'self' for now until we have an elegant
        # way to pass it into the model constructor without conflict
        body.pop('self', None)
        return self.model(**body)

    def delete(self, owner_id, name):
        """Delete an image."""
        url = '/v1/' + owner_id + '/instances/' + name
        self.http_client.delete(url)
