from namoclient.common import http
from namoclient.common import utils
from namoclient.v1 import instances
from namoclient.v1 import schemas


class Client(object):
    """Client for the OpenStack Images v1 API.

    :param string endpoint: A user-supplied endpoint URL for the namo
                            service.
    :param string token: Token for authentication.
    :param integer timeout: Allows customization of the timeout for client
                            http requests. (optional)
    """

    def __init__(self, endpoint, **kwargs):
        """Initialize a new client for the Images v1 API."""
        endpoint, version = utils.strip_version(endpoint)
        self.version = version or 1.0
        self.http_client = http.HTTPClient(endpoint, **kwargs)

        self.schemas = schemas.Controller(self.http_client)

        self.instances = instances.Controller(self.http_client, self.schemas)
