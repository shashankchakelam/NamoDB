# Copyright 2012 OpenStack Foundation
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import sys

from namoclient.common import progressbar
from namoclient.common import utils
from namoclient import exc
from namoclient.v1 import instances
import json
import os
from os.path import expanduser



@utils.arg('--sort-key', default='id',
           choices=instances.SORT_KEY_VALUES,
           help='Sort Instance list by specified field.')
@utils.arg('--sort-dir', default='desc',
           choices=instances.SORT_DIR_VALUES,
           help='Sort Instance list in specified direction.')
@utils.arg('--page-size', metavar='<SIZE>', default=None, type=int,
           help='Number of Instances to request in each paginated request.')
@utils.arg('--owner_id', metavar='<TYPE>',
           help='Filter by owner id')
def do_instance_list(gc, args):
    """List tasks you can access."""
    filter_keys = ['owner_id']
    filter_items = [(key, getattr(args, key)) for key in filter_keys]
    filters = dict([item for item in filter_items if item[1] is not None])

    kwargs = {'filters': filters}
    #kwargs = {}
    if args.page_size is not None:
        kwargs['page_size'] = args.page_size

    kwargs['sort_key'] = args.sort_key
    kwargs['sort_dir'] = args.sort_dir

    instances = gc.instances.list(**kwargs)

    columns = ['id', 'name', 'owner_id', 'state', 'storage_size', 'vm_class']
    utils.print_list(instances, columns)


@utils.arg('--owner_id', metavar='<STRING>', help='Owner Id')
@utils.arg('--name', metavar='<STRING>', help='Name of the instance')
def do_instance_show(gc, args):
    """Describe a specific task."""
    instance = gc.instances.get(args.owner_id, args.name)
    ignore = ['self', 'schema']
    instance = dict([item for item in instance.iteritems() if item[0] not in ignore])
    utils.print_dict(instance)


@utils.arg('--input', metavar='<STRING>', default='{}',
           help='Parameters of the instance to be launched')
def do_instance_create(gc, args):
    """Create a new task."""
    if not (args.input):
        utils.exit('Unable to create task. Specify input.')
    else:
        try:
            input = json.loads(args.input)
        except ValueError:
            utils.exit('Failed to parse the "input" parameter. Must be a '
                       'valid JSON object.')

        instance_values = input
        task = gc.instances.create(**instance_values)
        ignore = ['self', 'schema']
        task = dict([item for item in task.iteritems()
                     if item[0] not in ignore])
        utils.print_dict(task)


@utils.arg('--owner_id', metavar='<STRING>', help='Owner Id')
@utils.arg('--name', metavar='<STRING>', help='Name of the instance')
def do_instance_delete(gc, args):
    """Delete image member."""
    if not args.owner_id and not args.name:
        utils.exit('Unable to delete instance. Specify owner_id and name')
    else:
        gc.instances.delete(args.owner_id, args.name)


@utils.arg('--owner_id', metavar='<STRING>', help='Owner Id')
@utils.arg('--name', metavar='<STRING>', help='Name of the instance')
def do_instance_desc(gc, args):
    """Describe a specific task."""
    instance = gc.instances.get(args.owner_id, args.name)
    ignore = ['self', 'schema']
    instance = dict([item for item in instance.iteritems() if item[0] not in ignore])
    utils.print_dict(instance)